CartomENS-IA
===

[![pipeline status](https://gitlab.com/CartomENS-IA/labatarde/badges/master/build.svg)](https://gitlab.com/CartomENS-IA/labatarde/pipelines/)

Répertoire du client PC.

# Cadre
Projet Intégré du cours d'Informatique Fondamentale de l'ENS de Lyon, dans le cadre du Master 1 2017-2018. 

# Buts et objectifs
- Développer une intelligence artificielle jouant à la Bâtarde
- Implémenter une appli pour Android et PC pour y jouer contre l'IA
- Développer un mode multi joueurs en ligne
- Conquérir le monde

# Dependances
- `libsfml >= 1.5`
- `libsodium >= 1.0.16`
- `libpugixml >= 1.7`
- `make`
- `gcc/g++`

# Utilisation
- Compiler ```$ make```
- Lancer ```$ make run```
- Nettoyer ```$ make clean```
- Générer uniquement la doc ```$ make doc```

# Développeurs et contributeurs

- Benoit Tristan
- Bethune Louis
- Blondeau-Patissier Lison
- Champseix Nicolas
- Coiffier Guillaume
- Deschamps Quentin
- Devevey Julien
- Fernandez Simon
- La Xuan Hoang
- Lechine Ulysse
- Pitois François
- Pouget Stephane
- Rochette Denis
- Vacus Robin
- Valentin Herménégilde
- Valque Léo
- Vavrille Mathieu
- Florent Guepin
- Eddy Caron (Encadrant)
- Jules Marconnier (Créateur du jeu)
- Julien Montet (Graphismes)
