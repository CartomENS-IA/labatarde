
You have some instructions to create a tuto

#__________________ Remark __________________

BE CAREFUL WHITE SPACE : if you do "Message{ blabla }" the parser will not understand, it's "Message { blabla }"

#__________________ Message _________________

to draw text, use the instruction Message
#
Message Pos Position_x Position_y MessageKey
#

Pos Position_x Position_y are relative to the size of the window, you can too ommit them then the message is centered

MessageKey is the key of the message for each langage

#__________________ Comment _________________

you can comment your tuto :
#
/* hhheeeyyy, Mooorrtyyy gglrlrl */
#
the characters between "/*" and "*/" will be not executed

BE CAREFUL : DON'T PUT COMMENT IN THE MIDDLE OF AN INSTRUCTION

#__________________ Set Hand _________________

First, you need to create a hand:

#
SetHand {
Card valueCard1 suitCard1
Card valueCard2 suitCard2
.
.
.
}
#

valueCard is "Cat","Dog","Juggler","Musician","Entertainer","Knave","Knight","Queen" or "King"
suitCard is "club","diamond","heart" and "spade"

SetHand instruction erase the old hand

#_________________ Bet Phase ________________

Then, you can do the Bet Phase.

if you don't want Bet Phase, you can set the bet of players with SetBet

#
SetBet trumpBet {
valuePlayer1
valuePlayer2
.
.
}
#

trumpBet is "without_trump","club","diamond","heart","spade" and "all_trump", you can ommit to specify the trump then it will be "without_trump"
you can specify less than 4 value then other values will be 0
so "SetBet { }" is parsed


if you want have a Bet Phase, begin to do
#
BeginBetPhase
#

then to specify a bet do :
#
Bet playerID valueBet trumpBet
#
if the playerID is 0, the GUI wait that the player bets this value
if not the GUI will make this player bet

only for player 0, you can do :
#
Bet 0 _ heart
#
or
#
Bet 0 3 _
#
_ is any value which respect the rules

BE CAREFUL : DO A CORRECT BET SEQUENCE, PARSER DOESN'T VERIFY IF RULES ARE RESPECTED

if you want add Mechoune or Choune, add
#
Mechoune PlayerID
#
or
#
Choune PlayerID
#
if PlayerID=0, the GUI wait that the player mechounes (or chounes)

exemple of BetPhase:

#
BeginBetPhase
Bet 0 2 diamond
Bet 1 2 spade
Mechoune 3
Bet 2 1 spade
Bet 3 0 spade
Bet 0 3 spade
Choune 1
#

#_____________________ Play Phase ________________

Now, the Play Phase

like BetPhase begin to do :
#
BeginCardPhase
#

then to specify a play do :
#
Play playerID valueCard suitCard
#
like Bet if the playerID is 0, the GUI wait that the player plays this card
if not the GUI will make this player play

only for player 0, you can do :
#
MultiPlay {
valueCard1 suitCard1
valueCard2 suitCard2
.
.
} Wrong {
valueCard1 suitCard1 " message "
.
.
}
#
the player can play any of this card and if it tries to play a card of wrong, the message will draw

use TrickWin to trigger end of a trick
#
TrickWin IDwinner
#
give the winner of the trick

BE CAREFUL : DO A CORRECT PLAY SEQUENCE

exemple of PlayPhase:

#
BeginCardPhase
Play 0 Cat diamond
Play 1 Juggler spade
Play 2 Queen diamond
Play 3 Dog club
TrickWin 2
Play 2 Dog diamond
Play 3 Queen spade
Play 0 Dog spade
Play 1 Musician club
TrickWin 3
#

#____________________ Global ___________________

you can chain the round

#
SetHand ..
BeginBetPhase
.
.
BeginCardPhase
.
.
Scores ...
EndCardPhase

SetHand ..
BeginBetPhase
.
.
BeginCardPhase
.
.
Scores ...
EndCardPhase
#
