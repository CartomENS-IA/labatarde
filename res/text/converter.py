import os
import sys

# USAGE
# python3 converter.py <folder_name> <output_file_name>

def strip(file):
    l = [x.strip().split() for x in file.readlines()]
    print([x for x in l if len(x)<2])
    l = [ (x[0], " ".join(x[1:])) for x in l]
    return dict(l)

with open(sys.argv[2],'w') as output :
    output.write("<text>")
    fr = strip(open(sys.argv[1]+"/FR.txt", 'r'))
    en = strip(open(sys.argv[1]+"/EN.txt", 'r'))
    spa = strip(open(sys.argv[1]+"/SPA.txt", 'r'))
    ger = strip(open(sys.argv[1]+"/GER.txt", 'r'))
    por = strip(open(sys.argv[1]+"/POR.txt", 'r'))
    ita = strip(open(sys.argv[1]+"/ITA.txt", 'r'))
    pir = strip(open(sys.argv[1]+"/PIR.txt", 'r'))

    key_list = list(fr.keys())
    key_list.sort()
    for k in key_list:
        for d in (en,spa,ger,por,ita,pir):
            if k not in d:
                d[k]=" "
        output.write("<"+k+">")
        output.write("<FR>" + fr[k] + "</FR>")
        output.write("<EN>" + en[k] + "</EN>")
        output.write("<SPA>" + spa[k] + "</SPA>")
        output.write("<GER>" + ger[k] + "</GER>")
        output.write("<POR>" + por[k] + "</POR>")
        output.write("<ITA>" + ita[k] + "</ITA>")
        output.write("<PIR>" + pir[k] + "</PIR>")
        output.write("</"+k+">")
    output.write("</text>")
