/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef REMOTE_CORE_HPP
#define REMOTE_CORE_HPP

/** \file
 * This file must be included by any other file which uses the core API,
 * and should be the only one included.
 * The whole API is accessed by the namespace "RemoteCore"
 **/

#include "remoteCore/RemoteKernel.hpp"
#include "remoteCore/RemoteGameState.hpp"
#include "remoteCore/RemoteBetBox.hpp"
#include "remoteCore/RemotePlayer.hpp"

#endif
