/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "IA.hpp"
#include "core/fastKernel.hpp"

using namespace std;

int main(){
    //IA::Tournament tournoi();
    //IA::run();
    //run_card_phase();
    //return 0;

    int nbEssai = 1;

    std::array<double, 4> scoreTotal;
    scoreTotal.fill(0);
    for(int test=0; test<nbEssai; test++){
        /*
        IA::IAPlayer * ia_player0 = new IA::IAPlayer(200, std::array<double, 10> {1., 1., 1., 1., 1., 1., 1., 1., 1., 1.});
        IA::IAPlayer * ia_player1 = new IA::IAPlayer(400, std::array<double, 10> {1., 1., 1., 1., 1., 1., 1., 1., 1., 1.});
        IA::IAPlayer * ia_player2 = new IA::IAPlayer(1000, std::array<double, 10> {1., 1., 1., 1., 1., 1., 1., 1., 1., 1.});
        IA::IAPlayer * ia_player3 = new IA::IAPlayer(1000);*/
        IA::BotDeter * ia_player0 = new IA::BotDeter();
        IA::BotDeter * ia_player1 = new IA::BotDeter();
        IA::BotDeter * ia_player2 = new IA::BotDeter();
        IA::BotDeter * ia_player3 = new IA::BotDeter();
        auto kernel = make_shared<Core::Kernel>(ia_player0, ia_player1, ia_player2, ia_player3);
        auto scores = kernel->run();
        for(int i=0; i<4; i++){
            cout << scores[i] << " ";
            scoreTotal[i] += scores[i];
        }
        cout << endl;
    }
    cout << "\n\n\n";
    cout << "Total ";
    for(auto &score : scoreTotal)
        cout << score/nbEssai << " ";
    cout << "\n\n\n";

    return 0;
}
