/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <SFML/Network.hpp>
#include <iostream>
#include <stdexcept>

#include "GameMessage.hpp"
#include "RemoteBetBox.hpp"
#include "RemoteKernel.hpp"

using namespace Core;
using namespace Network;

namespace RemoteCore {

RemoteKernel::RemoteKernel(Player *p,
                           std::shared_ptr<Network::Messenger> const mess)
:
    m_messenger(mess),
    lobbyId(0),
    nbPlayers(0)
{
    m_player = unique_ptr<Player>(p);
    m_gameState = make_unique<Core::GameState>(0);
    m_player->set_game_state(*m_gameState);
    m_player->set_bet_box(new RemoteBetBox(m_messenger));
}

const GameState &RemoteKernel::get_game_state() const {
    return *(this->m_gameState);
}

std::array<int, 4> RemoteKernel::run() {
    for (auto i : Core::nbCardsPerTurn)
        while (listen_and_forward()) {
            (void)i;
        }
    return m_gameState->get_scores();
}

void RemoteKernel::stop() {
    std::cout << "[RemoteKernel] Called stop" << std::endl;
    m_messenger->disconnect();
}

bool RemoteKernel::listen_and_forward() {
    GameMessage* msg = static_cast<GameMessage*>(m_messenger->receive());

    switch (msg->header()) {
    case (GameMessageHeader::AskReady):
        reply_ready();
        break;
    case (GameMessageHeader::AskBet):
        reply_bet();
        break;
    case (GameMessageHeader::AskPlay):
        reply_play();
        break;
    case (GameMessageHeader::AskMechoune):
        reply_mechoune();
        break;
    case (GameMessageHeader::AskChoune):
        reply_choune();
        break;

    case (GameMessageHeader::NotifyBet):
        m_gameState->notify_bet(msg->read_bet());
        break;
    case (GameMessageHeader::NotifyPlay):
        m_gameState->notify_play(msg->read_card());
        break;
    case (GameMessageHeader::NotifyCards):
        cerr << "[RemoteKernel] Cards received by the player : ";
        for (auto c : msg->read_hand()) {
            cerr << "  " << c << endl;
        }
        cerr << endl;
        m_gameState->notify_cards(msg->read_hand());
        break;
    case (GameMessageHeader::NotifyMechoune):
        m_gameState->notify_mechoune(rotate(msg->read_player()));
        break;
    case (GameMessageHeader::NotifyChoune):
        m_gameState->notify_choune();
        break;
    case (GameMessageHeader::NotifyNextPlayer):
        m_gameState->notify_next_player(rotate(msg->read_player()));
        break;
    case (GameMessageHeader::NotifyTrickWin):
        m_gameState->notify_trick_win(rotate(msg->read_player()));
        break;
    case (GameMessageHeader::NotifyEndBetState):
        m_gameState->notify_end_bet_state();
        break;
    case (GameMessageHeader::NotifyEndCardState):
        m_gameState->notify_end_card_state();
        return false;

    case (GameMessageHeader::InitGameState):
        playerID = msg->read_player();
        break;

    case (GameMessageHeader::NotifyGameWin):
        m_gameState->notify_game_win(rotate(msg->read_player()));
        break;
    case (GameMessageHeader::NotifyReadyPlayer):
        m_gameState->notify_ready_player(rotate(msg->read_player()));
        break;
    case (GameMessageHeader::NotifyBeginBetState):
        m_gameState->notify_begin_bet_state();
        break;

    case (GameMessageHeader::NbPlayers):
        nbPlayers = msg->read_player();
        break;

    case (GameMessageHeader::Ready):
    case (GameMessageHeader::Bet):
    case (GameMessageHeader::Play):
    case (GameMessageHeader::Mechoune):
    case (GameMessageHeader::Choune):
        throw(std::runtime_error("client received action message"));
    default:
        throw(std::range_error("unknown header"));
    }
    return true;
}

int RemoteKernel::rotate(int player) { return (player + 4 - playerID) % 4; }

void RemoteKernel::reply_ready() {
    GameMessage msg;
    m_player->ready();
    msg.fill_with_ready();
    m_messenger->send(&msg);
}

void RemoteKernel::reply_bet() { m_player->ask_bet(); }

void RemoteKernel::reply_play() {
    GameMessage msg;
    Core::Card card = m_player->play();
    msg.fill_with_play(card);
    m_messenger->send(&msg);
}

void RemoteKernel::reply_mechoune() {
    GameMessage msg;
    m_player->ask_mechoune();
}

void RemoteKernel::reply_choune() {
    GameMessage msg;
    bool choune = m_player->choune();
    msg.fill_with_mechoune(choune); /* /!\ */
    m_messenger->send(&msg);
}
} // namespace RemoteCore
