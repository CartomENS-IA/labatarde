/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "RemoteGameState.hpp"
#include "GameMessage.hpp"
#include <SFML/Network.hpp>
#include <iostream> //for debug
#include <memory>

using namespace Core;
using namespace Network;

namespace RemoteCore {

RemoteGameState::RemoteGameState(int myId, std::shared_ptr<Network::Messenger> mess)
:
    GameState(myId),
    m_messenger(mess)
{}

void RemoteGameState::notify_bet(const Bet &bet) {
    GameState::notify_bet(bet);
    GameMessage msg;
    msg.fill_with_notifybet(bet);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_play(const Card &card) {
    GameState::notify_play(card);
    GameMessage msg;
    msg.fill_with_notifyplay(card);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_cards(const Hand &hand) {
    GameState::notify_cards(hand);
    GameMessage msg;
    msg.fill_with_notifycards(hand);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_mechoune(int player) {
    GameState::notify_mechoune(player);
    GameMessage msg;
    msg.fill_with_notifymechoune(player);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_choune() {
    GameState::notify_choune();
    GameMessage msg;
    msg.fill_with_notifychoune();
    m_messenger->send(&msg);
}

void RemoteGameState::notify_next_player(int player) {
    GameState::notify_next_player(player);
    GameMessage msg;
    msg.fill_with_notifynextplayer(player);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_trick_win(int player) {
    GameState::notify_trick_win(player);
    GameMessage msg;
    msg.fill_with_notifytrickwin(player);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_end_bet_state() {
    GameState::notify_end_bet_state();
    GameMessage msg;
    msg.fill_with_notifyendbetstate();
    m_messenger->send(&msg);
}

void RemoteGameState::notify_end_card_state() {
    GameState::notify_end_card_state();
    GameMessage msg;
    msg.fill_with_notifyendcardstate();
    m_messenger->send(&msg);
}

void RemoteGameState::notify_game_win(int player) {
    GameState::notify_game_win(player);
    GameMessage msg;
    msg.fill_with_notifygamewin(player);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_ready_player(int player) {
    GameState::notify_ready_player(player);
    GameMessage msg;
    msg.fill_with_notifyreadyplayer(player);
    m_messenger->send(&msg);
}

void RemoteGameState::notify_begin_bet_state() {
    GameState::notify_begin_bet_state();
    GameMessage msg;
    msg.fill_with_notifybeginbetstate();
    m_messenger->send(&msg);
}
} // namespace RemoteCore
