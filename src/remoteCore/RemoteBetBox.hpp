/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Network.hpp>
#include <memory>

#include "BetBox.hpp"
#include "CryptoBox.hpp"
#include "GameMessage.hpp"
#include "Messenger.hpp"

namespace RemoteCore {
class RemoteBetBox : public Core::BetBoxBase {
  public:
    RemoteBetBox(std::shared_ptr<Network::Messenger> const);
    virtual void bet(Core::Bet bet);
    virtual void mechoune();
    virtual void choune();

  private:
    std::shared_ptr<Network::Messenger> m_messenger;
};
} // namespace RemoteCore
