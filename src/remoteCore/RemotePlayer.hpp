/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <memory>

#include "Core.hpp"
#include "CryptoBox.hpp"
#include <SFML/Network.hpp>

namespace RemoteCore {

/** An network interface for a Core::Player, that just forwards the requests **/
class RemotePlayer : public Core::Player {
  public:
    /** \brief Initialises the RemotePlayer with a socket
     * \param socket A socket connected to the client on which a RemoteKernel is
     * listenning **/
    RemotePlayer(std::shared_ptr<Network::Messenger> const);
    virtual void ready();
    virtual void ask_bet();
    Core::Bet bet();
    bool mechoune();
    bool choune();
    Core::Card play();

    void set_game_state(const Core::GameState &gameState);

    /** \brief Create a RemotegameState as a GameState **/
    virtual std::unique_ptr<Core::GameState> create_game_state(int id);
private:
    std::shared_ptr<Network::Messenger> m_messenger;
};
} // namespace RemoteCore
