/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <SFML/Network.hpp>
#include <stdexcept>

#include "GameMessage.hpp"
#include "RemoteGameState.hpp"
#include "RemotePlayer.hpp"

using namespace Core;
using namespace Network;

namespace RemoteCore {

RemotePlayer::RemotePlayer(std::shared_ptr<Network::Messenger> const mess)
    : m_messenger(mess) {}

void RemotePlayer::ready() {
    GameMessage msg;
    msg.fill_with_askready();
    m_messenger->send(&msg);
    m_messenger->wait_for_receive();
    GameMessage *answer = static_cast<GameMessage*>(m_messenger->receive());
    if (answer->header() == GameMessageHeader::Ready)
        return;
    else
        throw(std::range_error("Wrong message header: expected a ready"));
}

void RemotePlayer::ask_bet() {
    GameMessage msg;
    msg.fill_with_askbet();
    m_messenger->send(&msg);
}

Bet RemotePlayer::bet() {
    GameMessage msg;
    msg.fill_with_askbet();
    m_messenger->send(&msg);
    m_messenger->wait_for_receive();
    GameMessage* answer = static_cast<GameMessage*>(m_messenger->receive());
    if (answer->header() == GameMessageHeader::Bet)
        return answer->read_bet();
    else
        throw(std::range_error("Wrong message header: expected a bet"));
}

bool RemotePlayer::mechoune() {
      GameMessage msg;
      msg.fill_with_askmechoune();
      m_messenger->send(&msg);
      m_messenger->wait_for_receive();
      GameMessage* reply = static_cast<GameMessage*>(m_messenger->receive());
      if(reply->header()==GameMessageHeader::Mechoune)
          return reply->read_bool();
      else
          throw(std::range_error("wrong message header"));
}

bool RemotePlayer::choune() {
    GameMessage msg;
    msg.fill_with_askchoune();
    m_messenger->send(&msg);
    m_messenger->wait_for_receive();
    GameMessage* answer = static_cast<GameMessage*>(m_messenger->receive());
    if (answer->header() == GameMessageHeader::Choune)
        return answer->read_bool();
    else
        throw(std::range_error("wrong message header: expected a choune"));
}

Card RemotePlayer::play() {
    GameMessage msg;
    msg.fill_with_askplay();
    m_messenger->send(&msg);
    m_messenger->wait_for_receive();
    GameMessage* answer = static_cast<GameMessage*>(m_messenger->receive());
    if (answer->header() == GameMessageHeader::Play)
        return answer->read_card();
    else
        throw(std::range_error("wrong message header: expected a play"));
}

void RemotePlayer::set_game_state(const GameState &gameState) {
    GameMessage msg;
    m_gameState = &gameState;
    msg.fill_with_initgamestate(gameState.my_id());
    m_messenger->send(&msg);
}

unique_ptr<GameState> RemotePlayer::create_game_state(int id) {
    return std::unique_ptr<GameState>(
        new RemoteGameState(id, m_messenger));
}
} // namespace RemoteCore
