/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Network.hpp>
#include <memory>
#include <string>

#include "KernelInterface.hpp"
#include "CryptoBox.hpp"
#include "Player.hpp"
#include "GameState.hpp"
#include "Messenger.hpp"

namespace RemoteCore {

/** \brief A class that allows to play on a remote server.
 * It uses the same Players and GameStates as Core::Kernel. This is a mere
 * network interface, just listenning and forwarding requests from the server to
 * the local Player, actions from the Player to the server and events from the
 * server to the local GameState **/
    class RemoteKernel : Core::KernelInterface {

  private:
    /** \brief The local Player **/
    std::unique_ptr<Core::Player> m_player;
    /** \brief The local GameState **/
    std::unique_ptr<Core::GameState> m_gameState;

    /** The socket connected to the server **/
    std::shared_ptr<Network::Messenger> m_messenger;

    /** Real Player id on the server **/
    int playerID;

    int rotate(int player);

    /** Main loop: listens on the socket and forwards incomming communications
     * **/
    bool listen_and_forward();

    /** Ask the Player for a ready and forwards it to the server **/
    void reply_ready();
    /** Ask the Player for a bet and forwards it to the server **/
    void reply_bet();
    /** Ask the Player for a mechoune and forwards it to the server **/
    void reply_mechoune();
    /** Ask the Player for a choune and forwards it to the server **/
    void reply_choune();
    /** Ask the Player for a play and forwards it to the server **/
    void reply_play();

  public:
    /** Constucts a RemoteKernel
     * \param player The player who plays locally
     * \param socket The socket to listen on, connected to the server
     **/
    RemoteKernel(Core::Player *p, std::shared_ptr<Network::Messenger> const);

    /** \brief Same as Core::Kernel::get_game_state **/
    const Core::GameState &get_game_state() const;
    /** \brief Run the game **/
    std::array<int, 4> run();

    void stop();

    char lobbyId;
    int nbPlayers;
};
} // namespace RemoteCore
