/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 * \brief Definition of class RemoteCore::RemoteBetQueue
 **/

#include <memory>
#include <vector>

#include "BetQueue.hpp"
#include "Messenger.hpp"

namespace RemoteCore {

/** \class
 * \brief TODO
 **/
class RemoteBetQueue : public Core::BetQueueBase {
  private:
    std::vector<std::shared_ptr<Network::Messenger>> m_messengers;
    sf::SocketSelector m_selector;
    void read_sockets();

  public:
    RemoteBetQueue(std::vector<std::shared_ptr<Network::Messenger>> &);
    virtual Core::Event *pop();
    virtual bool empty();
    virtual void clear();
};
} // namespace RemoteCore
