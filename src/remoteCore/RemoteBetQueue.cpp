/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <stdexcept>

#include "GameMessage.hpp"
#include "RemoteBetQueue.hpp"

using namespace Network;

namespace RemoteCore {

RemoteBetQueue::RemoteBetQueue(std::vector<std::shared_ptr<Messenger>> &vec)
    : m_messengers(vec) {
    for (auto s : vec)
        m_selector.add(*(s->socket()));
}

Core::Event *RemoteBetQueue::pop() {
    Core::Event *e;
    do {
        read_sockets();
        if (m_betEvents.empty())
            m_selector.wait();
    } while (m_betEvents.empty());
    e = m_betEvents.front();
    m_betEvents.pop_front();
    return e;
}

bool RemoteBetQueue::empty() {
    read_sockets();
    return m_betEvents.empty();
}

void RemoteBetQueue::read_sockets() {
    GameMessage* msg = new GameMessage();
    std::list<Core::Event *>::iterator beg = m_betEvents.begin();
    for (unsigned int i = 0; i < m_messengers.size(); i++) {
        m_messengers[i]->set_blocking(false);
        while (true) {
            msg = static_cast<GameMessage *>(m_messengers[i]->receive());
            if (msg->read_status() != sf::Socket::Status::Done) break;

            switch (msg->header()) {
            case GameMessageHeader::Bet:
                m_betEvents.insert(beg, new Core::BetEvent(msg->read_bet(), i));
                clog << "[RemoteBetQueue.cpp] Receiving BetEvent "
                     << msg->read_bet() << " by " << i << endl;
                break;
            case GameMessageHeader::Mechoune:
                m_betEvents.insert(beg, new Core::MechouneEvent(i));
                clog << "[RemoteBetQueue.cpp] Receiving MechouneEvent by " << i
                     << endl;
                break;
            case GameMessageHeader::Choune:
                m_betEvents.insert(beg, new Core::ChouneEvent());
                clog << "[RemoteBetQueue.cpp] Receiving ChouneEvent by " << i
                     << endl;
                break;
            default:
                throw std::range_error("[RemoteBetQueue.cpp] unknown header");
            }
        }
        m_messengers[i]->set_blocking(true);
    }
    delete msg;
}

void RemoteBetQueue::clear() {
    read_sockets();
    BetQueueBase::clear();
}
} // namespace RemoteCore
