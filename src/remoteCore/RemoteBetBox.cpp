/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "RemoteBetBox.hpp"

using namespace Core;
using namespace Network;

namespace RemoteCore {
RemoteBetBox::RemoteBetBox(std::shared_ptr<Network::Messenger> const mess)
    : m_messenger(mess) {}

void RemoteBetBox::bet(Bet bet) {
    GameMessage msg;
    msg.fill_with_bet(bet);
    m_messenger->send(&msg);
}

void RemoteBetBox::mechoune() {
    GameMessage msg;
    msg.fill_with_mechoune(true);
    m_messenger->send(&msg);
}

void RemoteBetBox::choune() {
    GameMessage msg;
    msg.fill_with_mechoune(true);
    m_messenger->send(&msg);
}

} // namespace RemoteCore
