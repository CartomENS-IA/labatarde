/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Core.hpp"
#include "CryptoBox.hpp"
#include <SFML/Network.hpp>
#include <memory>

namespace RemoteCore {
/** A class that mimics a GameState to update the local informations and
 * forwards events to the client **/
class RemoteGameState : public Core::GameState {
  public:
    /** \brief Constructs a RemoteGameState with a shared socket
     * \param socket A socket connected to the client, on which a RemoteKernel
     * is listenning **/
    RemoteGameState(int myid, std::shared_ptr<Network::Messenger> const);

    /** \brief The socket to communicate on **/
    std::shared_ptr<Network::Messenger> m_messenger;

    virtual void notify_bet(const Core::Bet &);
    virtual void notify_play(const Core::Card &);
    virtual void notify_cards(const Core::Hand &);
    virtual void notify_mechoune(int);
    virtual void notify_choune();
    virtual void notify_next_player(int);
    virtual void notify_trick_win(int);
    virtual void notify_game_win(int);
    virtual void notify_ready_player(int);
    virtual void notify_end_bet_state();
    virtual void notify_end_card_state();
    virtual void notify_begin_bet_state();
};
} // namespace RemoteCore
