/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef CORE_HPP
#define CORE_HPP

/** \file
 * This file must be included by any other file which uses the core API,
 * and should be the only one included.
 * The whole API is accessed by the namespace "Core"
 **/

#include "core/ConsolePlayer.hpp"
#include "core/Events.hpp"
#include "core/GameState.hpp"
#include "core/Kernel.hpp"
#include "core/ParallelPlayer.hpp"
//#include "core/BetQueue.hpp"
#include "core/fastKernel.hpp"

#endif
