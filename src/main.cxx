/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GUI.hpp"

using namespace std;

/**
 * \mainpage CartomENS-IA
 * \author CartomENS-IA-Team
 * \date 2017-2018
 *
 * CartomENS-IA is an application for the card game la Bâtarde.
 * Players can play both on computer and smartphone, and against other players
 * or against an artificial intelligence.
 *
 * Link to our website : http://graal.ens-lyon.fr/cartomensia
 *
 *
**/
int main() {
	GUI theGui;
	theGui.run();
}
