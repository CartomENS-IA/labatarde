/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Player.hpp"

#include <condition_variable>

/**
 * \file ParallelPlayer.hpp
 * \brief Definition of class Core::ParallelPlayer
 **/

namespace Core {
/** Class ParallelPlayer inherits from class Player
 * \brief A player taking action asynchronoulsy
 **/
class ParallelPlayer final : public Player {
  private:
    struct Sleeper {
      public:
        Sleeper(std::mutex &, std::condition_variable &, bool &);
        ~Sleeper();

      private:
        std::unique_lock<std::mutex> lock;
        std::condition_variable *const cv;
        bool *const waiting;
    };

    std::mutex m_mutex;
    std::condition_variable m_cv;

    Bet m_bet;
    Card m_card;
    bool m_mechoune;
    bool m_choune;

    bool m_waiting_bet;
    bool m_waiting_play;
    // bool m_waiting_mechoune;
    bool m_waiting_choune;
    bool m_waiting_ready;

  public:
    ParallelPlayer();

    /** \brief Return the bet of a player when the player has decided **/
    virtual Bet bet();
    virtual void ask_bet();
    /** \brief Return the card played by player when the player has decided **/
    virtual Card play();
    /** \brief Return the player is mechouning when the player has decided
     *   \todo Check that mechoune works this way
     **/
    // virtual bool mechoune();
    /** \brief Return the player is chouning when the player has decided
     *   \todo Check that choune works this way
     **/
    virtual bool choune();
    /** \brief Check if the player is ready **/
    virtual void ready();
    /** \brief The fact that a player is waiting to take it's decision
     * concerning a bet  **/
    bool waiting_bet() const;
    /** \brief The fact that a player is waiting to take it's decision
     * concerning a card to play  **/
    bool waiting_play() const;
    /** \brief The fact that a player is waiting to take it's decision
     * concerning mechouning  **/
    bool waiting_mechoune() const;
    /** \brief The fact that a player is waiting to take it's decision
     * concerning chouning  **/
    bool waiting_choune() const;
    /** \brief The fact that a player is waiting to be ready **/
    bool waiting_ready() const;
    /** \brief Set the bet, taking the decision and unlocking the kernel**/
    void set_bet(const Bet &);
    /** \brief Set the card to play, taking the decision and unlocking the
     * kernel **/
    void set_played_card(const Card &);
    /** \brief Set the decision of mechouning, unlocking the kernel **/
    void set_mechoune(bool);
    /** \brief Set the decision of chouning, unlocking the kernel **/
    void set_choune(bool);
    /** \brief Say that the player is ready, unlocking the kernel **/
    void set_ready();
};
} // namespace Core
