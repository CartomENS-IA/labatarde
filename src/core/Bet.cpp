/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Bet.hpp"

using namespace std;

namespace Core {
Bet::Bet(Trump _trump, int _value) : trump(_trump), value(_value) {}

bool operator==(const Bet &left, const Bet &right) {
    return left.value == right.value && left.trump == right.trump;
}

bool operator!=(const Bet &left, const Bet &right) { return !(left == right); }

bool operator<(const Bet &left, const Bet &right) {
    return (left.value < right.value) ||
           (left.value == right.value && left.trump < right.trump);
}

bool operator<=(const Bet &left, const Bet &right) {
    return left < right || left == right;
}

bool operator>(const Bet &left, const Bet &right) { return !(left <= right); }

bool operator>=(const Bet &left, const Bet &right) { return !(left < right); }

/** \brief Print a textual description bet in a stream **/
std::ostream &operator<<(ostream &out, const Bet &bet) {
    out << "Bet(" << bet.trump << "," << bet.value << ")";
    return out;
}
} // namespace Core
