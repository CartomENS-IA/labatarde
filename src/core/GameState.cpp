/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <algorithm>
#include <cstdlib>
#include <iostream>

#include "Card.hpp"
#include "GameState.hpp"
#include "Kernel.hpp"

using namespace std;

namespace Core {
CardStack::CardStack(Trump trump) : m_trump(trump) {}

Suit CardStack::suit() const { return m_suit; }

Card CardStack::strongest_card() const { return m_strongest; }

Card CardStack::get_card(int id) const {
    if (size() > (id - m_idOpener + 4) % 4)
        return m_stack[(id - m_idOpener + 4) % 4];
    return Card();
}

const vector<Card> &CardStack::stack() const { return m_stack; }

int CardStack::id_winner() const { return m_idStronger; }

int CardStack::id_first_player() const { return m_idOpener; }

bool CardStack::empty() const { return stack().empty(); }

int CardStack::size() const { return static_cast<int>(stack().size()); }

void CardStack::add_card(Card card, int idPlayer) {
    if (m_stack.empty() || stronger_than(card, strongest_card())) {
        set_strongest_card(card, idPlayer);
        if (m_stack.empty()) {
            m_suit = card.suit();
            m_idOpener = idPlayer;
        }
    }
    m_stack.push_back(card);
}

void CardStack::set_strongest_card(Card card, int idPlayer) {
    m_strongest = card;
    m_idStronger = idPlayer;
}

Trump CardStack::trump() const { return m_trump; }

bool CardStack::is_trump(const Card &card) const {
    return Core::is_trump(card, trump());
}

bool CardStack::stronger_than(const Card &left, const Card &right) const {
    return Core::stronger_than(left, right, trump());
}

Hand CardStack::playable_cards(const Hand &hand) const {
    if (empty()) // on ouvre avec n'importe quelle carte
        return hand;
    Hand playable;
    if (m_trump != m_suit &&
        m_trump != Trump::AllTrump) { // on joue la couleur demandée car ce
                                      // n'est pas un pli à atout
        copy_if(begin(hand), end(hand), back_inserter(playable),
                [this](const Card &card) { return card.suit() == suit(); });
        if (playable.empty()) { // sinon on joue un atout en montant à l'atout
            Hand my_trumps;
            copy_if(begin(hand), end(hand), back_inserter(my_trumps),
                    [this](const Card &card) {
                        return Core::is_trump(card, m_trump);
                    });
            if (my_trumps.empty()) // pas d'atout ? on défausse n'importe quoi
                return hand;
            copy_if(begin(my_trumps), end(my_trumps), back_inserter(playable),
                    [this](const Card &card) {
                        return stronger_than(card, strongest_card());
                    });
            if (playable.empty()) // pas possible de monter ? On jette n'importe
                                  // quel atout
                return my_trumps;
        }
    } else { // on joue un pli d'atouts
        Hand my_trumps;
        copy_if(begin(hand), end(hand), back_inserter(my_trumps),
                [this](const Card &card) {
                    return card.suit() == suit(); // il faut jouer le meme atout
                });
        if (my_trumps
                .empty()) // pas la bonne couleur ? on défausse n'importe quoi
            return hand;
        copy_if(begin(my_trumps), end(my_trumps), back_inserter(playable),
                [this](const Card &card) {
                    return stronger_than(card, strongest_card());
                });
        if (playable.empty()) // pas possible de monter ? On jette n'importe
                              // quel atout
            return my_trumps;
    }
    return playable;
}

GameState::GameState(int myId)
    : m_myId(myId), m_idCurrentPlayer(0), m_turnNumber(0), m_trickNumber(0),
      m_gamePart(GamePart::NotBeganYet), m_scores{0, 0, 0, 0}, m_bets(),
      m_bestBet(Bet(Trump::NoTrump, 0)), m_idBestBet(-1), m_mechoune(false),
      m_idMechouner(-1),
      m_choune(false), m_nbTricksObjective{0, 0, 0, 0}, m_nbTricksCurrent{0, 0,
                                                                          0, 0},
      m_cardStack(Trump::NoTrump), m_lastCardStack(Trump::NoTrump) {

    for (auto &vect : m_lastBetColor)
        vect.fill(-1);
}

const Hand GameState::my_hand() const {
    Hand copy(m_hand);
    return copy;
}

const Hand GameState::my_hand(Trump, bool inverted) const {
    // Because max 9 cards, I think it is faster to do a select sort, instead
    // of a merge sort or quick sort that take a lot of calculations but are
    // better in limits
    Hand sorted(m_hand);
    sort(begin(sorted), end(sorted),
         [this](const Card &left, const Card &right) {
             if (left.suit() != right.suit())
                 return left.suit() < right.suit();
             return !stronger_than(right, left);
         });
    if (inverted) {
        reverse(begin(sorted), end(sorted));
    }
    return sorted;
}

int GameState::my_id() const { return m_myId; }

int GameState::id_current_player() const { return m_idCurrentPlayer; }

int GameState::turn_number() const { return m_turnNumber; }

int GameState::first_player_round() const { return m_turnNumber % 4; }

int GameState::trick_number() const { return m_trickNumber; }

int GameState::nb_cards_round() const {
    int nb_card = m_turnNumber;
    if (nb_card >= 5)
        nb_card = 9 - nb_card;
    return nb_card + 5;
}

GamePart GameState::game_part() const { return m_gamePart; }

const array<int, 4> &GameState::get_scores() const { return m_scores; }

const AllBets &GameState::bets() const { return m_bets; }

const Bet &GameState::best_bet() const { return m_bestBet; }

int GameState::id_best_bet() const { return m_idBestBet; }

int GameState::nb_trick_bet() const {
    int buf = 0;
    for (auto &bet : m_bets) {
        buf += bet.value;
    }
    return buf;
}

Trump GameState::trump() const { return m_bestBet.trump; }

bool GameState::is_trump(const Card &card) const {
    return Core::is_trump(card, trump());
}

bool GameState::stronger_than(const Card &left, const Card &right) const {
    return Core::stronger_than(left, right, trump());
}

BetInfo GameState::bet_info(const Bet &bet) const {
    if (m_bets.empty())
        return BetInfo::ValidBet;
    if (is_mechoune() && bet.trump != trump())
        return BetInfo::BadBet;

    // If we are the last to bet, we must
    // check if nb of cards to take dont sum up to hand size
    if (int(m_bets.size()) + 1 == Core::nbPlayers) {
        if (bet.trump == trump() && bet.value == bet_forbidden())
            return BetInfo::BadNumber;
    }
    if (bet.trump == trump() || bet > m_bestBet)
        return BetInfo::ValidBet;
    return BetInfo::BadBet;
}

int GameState::bet_forbidden() const {
    if (int(m_bets.size()) + 1 == Core::nbPlayers) {
        int total = 0;
        for (auto b : m_bets)
            total += b.value;
        return (int)m_hand.size() - total;
    }
    return -1;
}

bool GameState::is_choune() const { return m_choune; }

bool GameState::is_mechoune() const { return m_mechoune; }

int GameState::id_mechouner() const { return m_idMechouner; }

int GameState::id_chouner() const {
    if (is_choune())
        return id_best_bet();
    return -1;
}

const std::array<std::array<int, 4>, 6> &GameState::last_bet_color() const {
    return m_lastBetColor;
}

const std::array<int, 4> &GameState::nb_tricks_objective() const {
    return m_nbTricksObjective;
}

const std::array<int, 4> &GameState::nb_tricks_current() const {
    return m_nbTricksCurrent;
}

const CardStack &GameState::card_stack() const { return m_cardStack; }

const CardStack &GameState::last_card_stack() const { return m_lastCardStack; }

Hand GameState::playable_cards() const {
    return m_cardStack.playable_cards(my_hand());
}

CardMovedInfo GameState::card_info(const Card &card) const {
    Hand playables = playable_cards();
    if (find(begin(playables), end(playables), card) == end(playables))
        return CardMovedInfo::BadMove;
    return CardMovedInfo::ValidMove;
}

bool GameState::state_changed() const { return !m_events.empty(); }

std::shared_ptr<Event> GameState::get_next_event() const {
    auto ptr = m_events.front();
    m_events.pop();
    return ptr;
}

void GameState::set_card_stack(const CardStack &cs) { m_cardStack = cs; }

void GameState::notify_bet(const Bet &bet) {
    m_nbTricksObjective[id_current_player()] = bet.value;
    if (m_bets.empty() || (bet > m_bestBet && bet.trump != trump())) {
        m_bestBet = bet;
        m_idBestBet = id_current_player();
        m_bets = AllBets();
        m_cardStack = CardStack(trump());
    }
    m_lastBetColor[static_cast<int>(bet.trump)][id_current_player()] =
        bet.value;
    m_bets.push_back(bet);
    m_events.emplace(new BetEvent(bet, id_current_player()));
}

void GameState::notify_play(const Card &card) {
    if (id_current_player() == my_id()) {
        remove(begin(m_hand), end(m_hand), card);
        m_hand.pop_back();
    }
    m_cardStack.add_card(card, id_current_player());
    m_events.emplace(new PlayEvent(card, id_current_player()));
}

void GameState::notify_cards(const Hand &hand) {
    m_hand = hand;
    m_events.emplace(new DealCardsEvent());
    m_gamePart = GamePart::BetPart;
}

void GameState::notify_mechoune(int mechouner) {
    m_mechoune = true;
    m_idMechouner = mechouner;
    m_events.emplace(new MechouneEvent(mechouner));
}

void GameState::notify_choune() {
    m_choune = true;
    m_events.emplace(new ChouneEvent());
}

void GameState::notify_next_player(int player) { m_idCurrentPlayer = player; }

void GameState::notify_trick_win(int winner) {
    m_nbTricksCurrent[winner] += 1;
    m_lastCardStack = m_cardStack;
    m_cardStack = CardStack(trump());
    m_trickNumber++;
    m_events.emplace(new TrickWonByEvent(winner));
}

void GameState::notify_end_bet_state() {
    m_gamePart = GamePart::CardPlayingPart;
    m_events.emplace(new EndBetPhaseEvent());
}

void GameState::notify_end_card_state() {
    int multiplier = 1;
    if (m_mechoune)
        multiplier = 2;
    if (m_choune)
        multiplier = 4;
    for (unsigned int player = 0; player < m_nbTricksObjective.size();
         ++player) {
        int delta = m_nbTricksObjective[player] - m_nbTricksCurrent[player];
        m_scores[player] += abs(delta) * multiplier;
    }
    for (auto &vect : m_lastBetColor)
        vect.fill(-1);
    m_bets = AllBets();
    m_bestBet = Bet(Trump::NoTrump, 0);
    m_idBestBet = -1;
    m_idMechouner = -1;
    m_mechoune = false;
    m_choune = false;
    m_turnNumber++;
    m_gamePart = GamePart::NotBeganYet;
    m_events.emplace(new EndCardPhaseEvent());
}

void GameState::notify_begin_bet_state() {
    m_events.emplace(new BeginBetPhaseEvent());
}

void GameState::notify_game_win(int winner) {
    m_events.emplace(new GameWonByEvent(winner));
}

void GameState::notify_ready_player(int player) {
    m_events.emplace(new ReadyEvent(player));
    m_nbTricksObjective.fill(0);
    m_nbTricksCurrent.fill(0);
}
} // namespace Core
