/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Events.hpp"

namespace Core {
BetEvent::BetEvent(Bet _bet, int _player) : bet(_bet), player(_player) {}
PlayEvent::PlayEvent(Card _card, int _player) : card(_card), player(_player) {}
MechouneEvent::MechouneEvent(int _player) : player(_player) {}
TrickWonByEvent::TrickWonByEvent(int _player) : player(_player) {}
ReadyEvent::ReadyEvent(int _player) : player(_player) {}
GameWonByEvent::GameWonByEvent(int _player) : player(_player) {}
} // namespace Core
