/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 *  \brief Definition of virtual class KernelInterface
 */

#include <array>
#include "GameState.hpp"

 namespace Core {

 class KernelInterface {
 public:
     virtual std::array<int, 4> run() = 0;
     virtual Core::GameState const &get_game_state() const = 0;
};

} // namespace Core
