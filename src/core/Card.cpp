/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Card.hpp"

#include <algorithm>
#include <random>

#define CASE_PRINT(T, V)                                                       \
    {                                                                          \
    case T::V:                                                                 \
        out << #V;                                                             \
        break;                                                                 \
    }

using namespace std;

namespace Core {
bool operator==(const Suit &suit, const Trump &trump) {
    return static_cast<Trump>(suit) == trump;
}
bool operator==(const Trump &trump, const Suit &suit) {
    return static_cast<Trump>(suit) == trump;
}
bool operator!=(const Suit &suit, const Trump &trump) {
    return static_cast<Trump>(suit) != trump;
}
bool operator!=(const Trump &trump, const Suit &suit) {
    return static_cast<Trump>(suit) != trump;
}

string trump_to_string(Trump const &trump) {
    switch (trump) {
    case Trump::NoTrump:
        return "without_trump";
    case Trump::Club:
        return "club";
    case Trump::Diamond:
        return "diamond";
    case Trump::Heart:
        return "heart";
    case Trump::Spade:
        return "spade";
    case Trump::AllTrump:
        return "all_trump";
    default:
        cerr << "in function trump_to_string : undefined trump object" << endl;
        return "";
    }
}

string suit_to_string(Suit const &suit) {
    switch (suit) {
    case Suit::Club:
        return "club";
    case Suit::Diamond:
        return "diamond";
    case Suit::Heart:
        return "heart";
    case Suit::Spade:
        return "spade";
    default:
        cerr << "in function trump_to_string : undefined suit object" << endl;
        return "";
    }
}

ostream &operator<<(ostream &out, const Trump &trump) {
    switch (trump) {
        CASE_PRINT(Trump, NoTrump);
        CASE_PRINT(Trump, Club);
        CASE_PRINT(Trump, Diamond);
        CASE_PRINT(Trump, Heart);
        CASE_PRINT(Trump, Spade);
        CASE_PRINT(Trump, AllTrump);
    default:
        throw runtime_error("[Card.cpp] Unable to print this trump");
    }
    return out;
}

ostream &operator<<(ostream &out, const Suit &suit) {
    out << static_cast<Trump>(suit);
    return out;
}

ostream &operator<<(ostream &out, const Value &value) {
    switch (value) {
        CASE_PRINT(Value, Cat);
        CASE_PRINT(Value, Dog);
        CASE_PRINT(Value, Juggler);
        CASE_PRINT(Value, Musician);
        CASE_PRINT(Value, Entertainer);
        CASE_PRINT(Value, Knave);
        CASE_PRINT(Value, Knight);
        CASE_PRINT(Value, Queen);
        CASE_PRINT(Value, King);
    default:
        throw runtime_error("[Card.cpp] Unable to print this value");
    }
    return out;
}

Card::Card(Suit suit, Value value) : m_suit(suit), m_value(value) {
    switch (m_value) {
    case Value::Cat:
    case Value::Dog:
        m_rank = Rank::Animal;
        break;
    case Value::Juggler:
    case Value::Musician:
    case Value::Entertainer:
        m_rank = Rank::Artist;
        break;
    case Value::Knave:
    case Value::Knight:
    case Value::Queen:
    case Value::King:
        m_rank = Rank::Face;
        break;
    }
}

Suit Card::suit() const { return m_suit; }

Value Card::value() const { return m_value; }

Rank Card::rank() const { return m_rank; }

bool CardComparatorForODS::operator()(const Card &left,
                                      const Card &right) const {
    if (left.value() != right.value())
        return left.value() < right.value();
    return left.suit() < right.suit();
}

bool operator==(const Card &left, const Card &right) {
    return left.suit() == right.suit() && left.value() == right.value();
}

bool operator!=(const Card &left, const Card &right) {
    return !(left == right);
}

ostream &operator<<(std::ostream &out, const Card &card) {
    out << "Card(" << card.suit() << "," << card.value() << ")";
    return out;
}

Hand generate_full_game() {
    Hand hand;
    for (auto value : {Value::Cat, Value::Dog, Value::Juggler, Value::Musician,
                       Value::Entertainer, Value::Knave, Value::Knight,
                       Value::Queen, Value::King}) {
        for (auto suit :
             {Suit::Club, Suit::Diamond, Suit::Heart, Suit::Spade}) {
            hand.emplace_back(suit, value);
        }
    }
    return hand;
}

static default_random_engine gen(7914);

bool is_trump(const Card &card, Trump trump) {
    return trump == Trump::AllTrump || card.suit() == trump;
}

bool stronger_than(const Card &left, const Card &right, Trump trump) {
    if (!is_trump(left, trump)) {
        if (left.suit() != right.suit())
            return false;
        return !is_trump(right, trump) &&
               static_cast<int>(left.value()) > static_cast<int>(right.value());
    }
    if (!is_trump(right, trump))
        return true;
    if (left.suit() != right.suit())
        return false;
    if (left.rank() != right.rank())
        return static_cast<int>(left.rank()) > static_cast<int>(right.rank());
    return static_cast<int>(left.value()) > static_cast<int>(right.value());
}
} // namespace Core
