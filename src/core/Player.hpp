/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "BetBox.hpp"
#include "GameState.hpp"

/** \file Player.hpp
 *   \brief Definition of class Core::Player
 **/

namespace Core {
/** \brief A player that can do action to change the state of the game **/
class Player {
  public:
    virtual ~Player();
    /** \brief The bet of a player **/
    virtual void ask_bet();
    /** \brief The fact that a player is 'mechouning'  **/
    void ask_mechoune();
    /** \brief The fact that a player is 'chouning'  **/
    virtual bool choune() = 0;
    /** \brief The fact that a player play **/
    virtual Card play() = 0;
    /** \brief The fact that a player is ready **/
    virtual void ready(){};

    virtual int my_id() const final;

    /** \brief  Set the state of the game
     *   \param gameState The state of a game for the player
     **/
    virtual void set_game_state(const GameState &gameState);

    /** \brief  Return the state of the game for the player
     *   \return An immutable state of a game
     **/
    virtual const GameState &get_game_state();

    virtual std::unique_ptr<GameState> create_game_state(int id);

    void set_bet_box(BetBoxBase *bB);

  protected:
    /** \brief The GameState of the player **/
    GameState const *m_gameState;
    BetBoxBase *m_betBox = nullptr;

  private:
    /** \brief The bet of a player **/
    virtual Bet bet() = 0;
    /** \brief The fact that a player is 'mechouning'  **/
    virtual bool mechoune();
};
} // namespace Core
