/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <algorithm>
#include <array>
#include <vector>

#include "Bet.hpp"
#include "Card.hpp"
#include "GameState.hpp"

/**
* \file
* \author ???
* \brief A Simplified Kernel build to run very quickly.
*
* This fast kernel is used by the MonteCarlo to run many
* descents in very little time
**/

namespace Core {
namespace FastKernel {
Core::Card play(const Hand &hand, const CardStack &cardStack);

std::array<int, 4> run_card_phase(AllHands hand, AllBets const &bet,
                                  int idBestBet,
                                  std::array<int, 4> const trick_done,
                                  CardStack cardstack, int nextPlayer);
} // namespace FastKernel
} // namespace Core
