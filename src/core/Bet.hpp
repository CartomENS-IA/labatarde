/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Card.hpp"
#include <ostream>

/** \file
 *   \brief Definition of Core::Bet struct
 **/

namespace Core {
/**
 * \brief A bet in a game
 *
 * It contains a Trump and the number of trick promised.
 **/
struct Bet {

    Bet() = default;
    /** \brief Construct the bet from the Trump and the number of trick **/
    Bet(Trump, int);

    /** \brief The trump **/
    Trump trump;

    /** \brief The number of trick promised **/
    int value;
};

/** \brief A list of  Bet **/
typedef std::vector<Bet> AllBets;

/** \brief The fact that the two bet are the same **/
bool operator==(const Bet &, const Bet &);
/** \brief The fact that the two bet are different **/
bool operator!=(const Bet &, const Bet &);

/** \brief The fact that the first bet is inferior to the ohter according to the
 *rule of the game
 *
 *   Either the first bet has a lesser number of trick or the first bet has an
 *lower Trump
 **/

bool operator<(const Bet &, const Bet &);

/** \brief The fact that the first bet is inferior or the same than to the other
 * according to the rule of the game **/
bool operator<=(const Bet &, const Bet &);
/** \brief The fact that the first bet is superior to the other according to the
 *rule of the game
 *
 *   Either the first bet has a highter number of trick or the first bet has an
 *highter Trump
 **/
bool operator>(const Bet &, const Bet &);
/** \brief The fact that the first bet is superior or the same than to the other
 * according to the rule of the game **/
bool operator>=(const Bet &, const Bet &);

/** \brief Print a textual description bet in a stream **/
std::ostream &operator<<(std::ostream &, const Bet &);
} // namespace Core
