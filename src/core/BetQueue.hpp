/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Events.hpp"
#include <SFML/Network.hpp>
#include <condition_variable>
#include <list>
#include <memory>
#include <mutex>

namespace Core {
class BetQueueBase {
  protected:
    std::list<Event *> m_betEvents;

  public:
    BetQueueBase();
    virtual ~BetQueueBase(){};
    virtual void add_bet(int id, Bet bet);
    virtual void add_mechoune(int id);
    virtual Event *pop() = 0;
    virtual bool empty();
    virtual void clear();
};

class BetQueue : public BetQueueBase {
  private:
    std::mutex m_mutex;
    std::condition_variable m_cv;

  public:
    virtual Event *pop();
    virtual void add_bet(int id, Bet bet);
    virtual void add_mechoune(int id);
};
} // namespace Core
