/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Bet.hpp"
#include "Player.hpp"

/** \file ConsolePlayer.hpp
 *   \brief Definition of class Core::ConsolePlayer
 **/

namespace Core {
class ConsolePlayer : public Player {
  public:
    virtual Bet bet();
    virtual bool mechoune();
    virtual bool choune();
    virtual Card play();
};
} // namespace Core
