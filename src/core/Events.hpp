/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <memory>

#include "Bet.hpp"

/** \file
 *   \brief Definition of classes Core::Event, Core::BetEvent, Core::PlayEvent,
 *Core::MechouneEvent and Core::ChouneEvent
 **/

namespace Core {
/** \brief A base class for designing events **/
class Event {
  public:
    Event() = default;
    virtual ~Event() = default;
};

template <typename Event_T>
std::shared_ptr<Event_T> handle(const std::shared_ptr<Event> &event) {
    return std::dynamic_pointer_cast<Event_T>(event);
}

class BetEvent : public Event {
  public:
    BetEvent(Bet, int);
    Bet bet;
    int player;
};

class PlayEvent : public Event {
  public:
    PlayEvent(Card, int);
    Card card;
    int player;
};

class MechouneEvent : public Event {
  public:
    MechouneEvent(int);
    int player;
};

class TrickWonByEvent : public Event {
  public:
    TrickWonByEvent(int);
    int player;
};

class GameWonByEvent : public Event {
  public:
    GameWonByEvent(int);
    int player;
};

class ReadyEvent : public Event {
  public:
    ReadyEvent(int);
    int player;
};

class ChouneEvent : public Event {};
class EndBetPhaseEvent : public Event {};
class EndCardPhaseEvent : public Event {};
class BeginBetPhaseEvent : public Event {};
class DealCardsEvent : public Event {};
} // namespace Core
