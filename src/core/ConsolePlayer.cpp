/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "ConsolePlayer.hpp"

#include <iostream>
#include <string>

#include "Bet.hpp"

namespace Core {
Bet ConsolePlayer::bet() {
    const Hand &hand = m_gameState->my_hand();
    int trump_number = -1;
    int trick;
    Trump trump;

    while (trump_number == -1) {
        std::cout << "Your hand:";
        for (auto &c : hand)
            std::cout << " " << c;
        std::cout << "\nNoTrump [0] | Club [1] | Diamond [2] | Heart [3] | "
                     "Spade [4] | AllTrump [5]\nChoose your Trump: ";
        std::cin >> trump_number;
        switch (trump_number) {
        case 0:
            trump = Trump::NoTrump;
            break;
        case 1:
            trump = Trump::Club;
            break;
        case 2:
            trump = Trump::Diamond;
            break;
        case 3:
            trump = Trump::Heart;
            break;
        case 4:
            trump = Trump::Spade;
            break;
        case 5:
            trump = Trump::AllTrump;
            break;
        default:
            trump_number = -1;
        }
    }
    std::cout << "Choose your trick number: ";
    std::cin >> trick;
    return Bet(trump, trick);
}

bool ConsolePlayer::mechoune() {
    std::string answer;

    while (answer != "y" && answer != "n") {
        std::cout << "Do you want to mechoune? [y/n] ";
        std::cin >> answer;
    }
    if (answer == "y")
        return true;
    else
        return false;
}

bool ConsolePlayer::choune() {
    std::string answer;

    while (answer != "y" && answer != "n") {
        std::cout << "Do you want to choune? [y/n] ";
        std::cin >> answer;
    }
    if (answer == "y")
        return true;
    else
        return false;
}

Card ConsolePlayer::play() {
    const Hand &hand = m_gameState->playable_cards();
    int card_number = -1;

    while (card_number == -1) {
        std::cout << "Your hand:";
        for (auto &c : hand)
            std::cout << " " << c;
        std::cout << "\nChoose a card [0.." << hand.size() - 1 << "]: ";
        std::cin >> card_number;
        if (card_number < 0 || card_number >= int(hand.size()))
            card_number = -1;
    }
    return hand[card_number];
}
} // namespace Core
