/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef CORE_CARD_HPP
#define CORE_CARD_HPP

/** \file
 *   \brief Define utility as Core::Card, Core::Values, Core::Suit and
 *Core::Trump types
 **/

#include <iostream>
#include <list>
#include <ostream>
#include <string>
#include <type_traits>
#include <vector>

namespace Core {
/** \brief An enumeration of all trump in order **/
enum class Trump : int { NoTrump, Club, Diamond, Heart, Spade, AllTrump };

/** \brief Conversion operator **/
std::string trump_to_string(Trump const &);

/** \brief An enumeration of the card's suit in Trump order **/
enum class Suit : int {
    Club = static_cast<std::underlying_type<Trump>::type>(Trump::Club),
    Diamond = static_cast<std::underlying_type<Trump>::type>(Trump::Diamond),
    Heart = static_cast<std::underlying_type<Trump>::type>(Trump::Heart),
    Spade = static_cast<std::underlying_type<Trump>::type>(Trump::Spade)
};

/** \brief Conversion operator **/
std::string suit_to_string(const Suit &);

std::ostream &operator<<(std::ostream &, const Trump &);
std::ostream &operator<<(std::ostream &, const Suit &);

/** \brief Test if a card's suit is a given Trump **/
bool operator==(const Suit &, const Trump &);
/** \brief Test if a Trump is a given card's Suit **/
bool operator==(const Trump &, const Suit &);
/** \brief Test if a card's suit is not a given Trump **/
bool operator!=(const Suit &, const Trump &);
/** \brief Test if a card'suit is a given Trump **/
bool operator==(const Suit &, const Trump &);
/** \brief Test if a Trump is not a given card's Suit **/
bool operator!=(const Trump &, const Suit &);

/** \brief An enumeration of all value of a card in classical order **/
enum class Value {
    Cat,
    Dog,
    Juggler,
    Musician,
    Entertainer,
    Knave,
    Knight,
    Queen,
    King
};

std::ostream &operator<<(std::ostream &, const Value &);

/** \brief An enumeration of the different rank of a card in the game **/
enum class Rank : int { Animal, Face, Artist };

/** \brief An immutable card in the game **/
class Card {
  public:
    Card() = default;
    /** \brief Constructor of a card **/
    Card(Suit, Value);
    /** \brief Accessor to the Suit of a card **/
    Suit suit() const;
    /** \brief Accessor to the Value of a card **/
    Value value() const;
    /** \brief Accessor to the Rank of a card **/
    Rank rank() const;

  private:
    Suit m_suit;
    Value m_value;
    Rank m_rank;
};

struct CardComparatorForODS {
    bool operator()(const Card &, const Card &) const;
};

/** \brief The fact that a card is the same as the other **/
bool operator==(const Card &, const Card &);

/** \brief The fact that a card is the same as the other **/
bool operator!=(const Card &, const Card &);

/** \brief Print a textual description of a card in a stream **/
std::ostream &operator<<(std::ostream &, const Card &);

/** \brief A list of Card **/
typedef std::vector<Card> Hand;

/** \brief A list of Hand **/
typedef std::vector<Hand> AllHands;

/** \brief Create a Hand containing all cards of the game **/
Hand generate_full_game();

/** \brief Test if the card is a trump **/
bool is_trump(const Card &card, Trump);

/** \brief Test if a card is strictly stronger than an other, given the trump
 * **/
bool stronger_than(const Card &, const Card &, Trump);
} // namespace Core

#endif
