/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "BetBox.hpp"

namespace Core {

void BetBox::bet(Bet bet) { m_betQ->add_bet(m_id, bet); }

void BetBox::mechoune() { m_betQ->add_mechoune(m_id); }
} // namespace Core
