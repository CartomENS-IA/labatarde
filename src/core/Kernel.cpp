/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <algorithm>
#include <chrono>
#include <exception>
#include <iostream>
#include <iterator>
#include <random>
#include <stdexcept>

#include "IA.hpp"
#include "Kernel.hpp"
#include "RemoteBetQueue.hpp"
#include "CoreConstants.hpp"

using namespace std;

namespace Core {
const char *Timeout::what() const throw() {
    return "A human timeouts : end of the game";
}

// Old values : 7816
static default_random_engine
    gen(chrono::system_clock::now().time_since_epoch().count());

Kernel::Kernel(shared_ptr<Player> a,
               shared_ptr<Player> b,
               shared_ptr<Player> c,
               shared_ptr<Player> d)
:
    _skip_bet(false),
    _interrupted(false)
{
    int id = 0;
    for (auto p : {a, b, c, d}) {
        m_gameStates.emplace_back(p->create_game_state(id++));
        m_players.emplace_back(p);
    }
    m_betQ = new BetQueue();
    for (int i = 0; i < nbPlayers; ++i) {
        m_players[i]->set_game_state(get_game_state(i));
        m_players[i]->set_bet_box(new BetBox(i, m_betQ));
    }
}

Kernel::Kernel(shared_ptr<Player> a, shared_ptr<Player> b,
               shared_ptr<Player> c, shared_ptr<Player> d, int nbh,
               vector<std::shared_ptr<Network::Messenger>> &mess)
:
    _skip_bet(false),
    nb_human(nbh),
    _interrupted(false)
{
    int id = 0;
    for (auto p : {a, b, c, d}) {
        m_gameStates.emplace_back(p->create_game_state(id++));
        m_players.emplace_back(p);
    }
    m_betQ = new RemoteCore::RemoteBetQueue(mess);
    for (int i = 0; i < nbPlayers; ++i) {
        m_players[i]->set_game_state(get_game_state(i));
        m_players[i]->set_bet_box(new BetBox(i, m_betQ));
    }
}

Kernel::Kernel(array<shared_ptr<Player>, Core::nbPlayers> const &player,
               AllHands const &hand, AllBets const &bet, int idBestBet,
               array<int, 4> const &trick_done, CardStack const &cardstack,
               int nextPlayer)
    : _skip_bet(true), _next_player(nextPlayer), _interrupted(false) {

    AllHands hand_tmp;
    int k = 0;

    for (int i = 0; i < nbPlayers; ++i) {
        m_gameStates.emplace_back(player[i]->create_game_state(i));
    }
    m_players.emplace_back(player[0]);
    m_players.emplace_back(player[1]);
    m_players.emplace_back(player[2]);
    m_players.emplace_back(player[3]);
    for (int i = 0; i < nbPlayers; ++i) {
        m_players[i]->set_game_state(get_game_state(i));
    }
    for (auto &e : hand_tmp)
        e = generate_full_game();
    this->notify_cards(hand_tmp);
    k = idBestBet;
    for (auto &e : bet) {
        this->notify_next_player(k);
        this->notify_bet(e);
        k++;
    }
    this->notify_end_bet_state();
    for (unsigned int i = 0; i < trick_done.size(); ++i) {
        for (int j = 0; j < trick_done[i]; ++j) {
            this->notify_trick_win(i);
        }
    }
    for (auto &state : m_gameStates) {
        state->set_card_stack(cardstack);
    }
    this->notify_cards(hand);
    notify_next_player(nextPlayer);
}

Kernel::~Kernel() {
    if (m_betQ != nullptr)
        delete m_betQ;
}

array<int, 4> Kernel::run() {
    int turn = 0;
    if (_interrupted)
        turn = m_gameStates[0]->turn_number();
    try {
        if (!(this->_skip_bet))
            for (; turn < (int)nbCardsPerTurn.size(); ++turn)
                run_turn(turn);
        else
            run_predefined();
        // clog << "\n### YOUHOU ! Player 0 wins (joie et félicité dans le coeur
        // des Hommes)" << endl;
    } catch (const Timeout &e) {
        int player = m_gameStates[0]->id_current_player();
        cout << "Player " << player << " timed out" << endl;
        cout << "Replacing him with IAPlayer" << endl;
        replace_player(player);
        if (nb_human <= 0) {
            cout << "All player disconnected, stopping game" << endl;
            return array<int, 4>{{0, 0, 0, 0}};
        }
        this->run();
        // clog << e.what() << endl;
    } catch (runtime_error e) {
        int player = m_gameStates[0]->id_current_player();
        cout << "Player " << player << " throwed the error: " << e.what()
             << endl;
        cout << "Replacing him with IAPlayer" << endl;
        replace_player(player);
        if (nb_human <= 0) {
            cout << "All player disconnected, stopping game" << endl;
            return array<int, 4>{{0, 0, 0, 0}};
        }
        this->run();
    }
    auto score = m_gameStates[0]->get_scores();
    auto min = std::min_element(score.cbegin(), score.cend());
    this->notify_game_win((int)std::distance(score.cbegin(), min));
    return score;
}

void Kernel::run_turn(int turn) {
    if (_interrupted) {
        turn = m_gameStates[0]->turn_number();
        switch (m_gameStates[0]->game_part()) {
        case (GamePart::NotBeganYet):
            notify_cards(deal(nbCardsPerTurn[turn]));
            /* Intentionnally falls through */
        case (GamePart::BetPart):
            run_bet_state(turn);
            /* Intentionnally falls through */
        case (GamePart::CardPlayingPart):
            run_card_playing_state(turn);
            break;
        }
    } else {
        notify_cards(deal(nbCardsPerTurn[turn]));
        run_bet_state(turn);
        run_card_playing_state(turn);
    }
}

void Kernel::run_predefined() {
    int winner;

    // clog << "\nPredefined Play state :" << endl;
    int i_player = (int)_next_player;
    int n = nbPlayers - m_gameStates[0]->card_stack().size();
    for (int j = 0; j < n; j++) {
        notify_next_player(i_player);
        Card card;
        do {
            card = m_players[i_player]->play();
        } while (m_gameStates[i_player]->card_info(card) !=
                 CardMovedInfo::ValidMove);
        notify_play(card);
        // clog << "Player " << i_player << "'s play: " << card << endl;
        i_player = (i_player + 1) % nbPlayers;
    }

    winner = m_gameStates[0]->card_stack().id_winner();
    notify_trick_win(winner);

    while (this->m_gameStates[0]->my_hand().size()) {
        for (i_player = 0; i_player < nbPlayers; ++i_player) {
            int player = (i_player + winner) % nbPlayers;
            notify_next_player(player);
            Card card;
            do {
                card = m_players[player]->play();
            } while (m_gameStates[player]->card_info(card) !=
                     CardMovedInfo::ValidMove);
            notify_play(card);
            // clog << "Player " << player << "'s play: " << card << endl;
        }
        winner = m_gameStates[0]->card_stack().id_winner();
        notify_trick_win(winner);
    }
    this->notify_end_card_state();
}

void Kernel::run_bet_state(int turn) {

    notify_begin_bet_state();

    // clog << "\nBet state :" << endl;
    for (int player = 0; player < nbPlayers; player++) {
        m_players[player]->ready();
        notify_ready_player(player);
    }

    int mechouner = -1;
    int player = turn % nbPlayers;
    bool mechouned = false, chouned = false;

    if (_interrupted) {
        mechouned = m_gameStates[0]->is_mechoune();
        chouned = m_gameStates[0]->is_choune();
        player = m_gameStates[0]->id_current_player();
        _interrupted = false;
    }

    while (player != m_gameStates[0]->id_best_bet()) {
        notify_next_player(player);
        Bet bet;
        Event *evt = new Event();
        {
        ask_bet:
            delete evt;
            if (m_betQ->empty())
                m_players[player]->ask_bet();
            evt = m_betQ->pop();
            if (BetEvent *betEvt = dynamic_cast<BetEvent *>(evt)) {
                cout << "[Kernel.cpp] Reading BetEvent " << betEvt->bet
                     << " by " << betEvt->player << endl;
                if (betEvt->player != player)
                    goto ask_bet;
                bet = betEvt->bet;
                if (m_gameStates[player]->bet_info(bet) != BetInfo::ValidBet)
                    goto ask_bet;
                delete evt;
            } else if (MechouneEvent *mechouneEvt =
                           dynamic_cast<MechouneEvent *>(evt)) {
                cout << "[Kernel.cpp] Reading MechouneEvent by "
                     << mechouneEvt->player << endl;
                if (mechouneEvt->player == m_gameStates[0]->id_best_bet() ||
                    m_gameStates[0]->id_best_bet() == -1)
                    goto ask_bet;
                mechouner = mechouneEvt->player;
                if (!mechouned) {
                    mechouned = true;
                    m_betQ->clear();
                    notify_mechoune(mechouner);
                }
                goto ask_bet;
            } else {
                clog << "[Kernel.cpp] Unknown event in the queue" << endl;
                goto ask_bet;
            }
        }

        m_betQ->clear();
        cout << "[Kernel.cpp] Notifying bet " << bet << endl;
        notify_bet(bet);
        gotMechouned();

        player++;
        player %= nbPlayers;
    }

    if (mechouned && !chouned) {
        if (m_players[m_gameStates[0]->id_best_bet()]->choune()) {
            chouned = true;
            // clog << "This round is also chouned" << endl;
            notify_choune();
        }
    }
    notify_end_bet_state();
}

void Kernel::run_card_playing_state(int turn) {
    // clog << "\nPlay state :" << endl;
    int winner = turn % nbPlayers;
    int trick = 0;
    int player = winner;
    if (_interrupted) {
        trick = m_gameStates[0]->trick_number();
        player = m_gameStates[0]->id_current_player();
        if (trick != 0)
            winner = m_gameStates[0]->last_card_stack().id_winner();
        _interrupted = false;
    }
    for (; trick < nbCardsPerTurn[turn]; ++trick) {
        player = winner;
        do {
            notify_next_player(player);
            Card card;
            do {
                card = m_players[player]->play();
            } while (m_gameStates[player]->card_info(card) !=
                     CardMovedInfo::ValidMove);
            notify_play(card);
            // clog << "Player " << player << "'s play: " << card << endl;
            player++;
            player %= nbPlayers;
        } while (player != winner);
        winner = m_gameStates[0]->card_stack().id_winner();
        notify_trick_win(winner);
    }
    notify_end_card_state();
}

void Kernel::gotMechouned() {
    for (const auto &player : m_players) {
        player->ask_mechoune();
    }
}

void Kernel::notify_bet(const Bet &bet) {
    for (const auto &state : m_gameStates) {
        state->notify_bet(bet);
    }
}

void Kernel::notify_play(const Card &card) {
    for (const auto &state : m_gameStates) {
        state->notify_play(card);
    }
}

void Kernel::notify_mechoune(int mechouner) /*id du mec qui mechoune*/ {
    cout << "[Kernel.cpp] Notiying mechoune by " << mechouner << endl;
    for (const auto &state : m_gameStates) {
        state->notify_mechoune(mechouner);
    }
}

void Kernel::notify_choune() {
    for (const auto &state : m_gameStates) {
        state->notify_choune();
    }
}

void Kernel::notify_next_player(int player) {
    for (const auto &state : m_gameStates) {
        state->notify_next_player(player);
    }
}

void Kernel::notify_cards(const AllHands &hands) {
    for (int idPlayer = 0; idPlayer < (int)hands.size(); ++idPlayer) {
        m_gameStates[idPlayer]->notify_cards(hands[idPlayer]);
    }
}

void Kernel::notify_trick_win(int winner) {
    for (const auto &state : m_gameStates) {
        state->notify_trick_win(winner);
    }
}

void Kernel::notify_end_bet_state() {
    for (const auto &state : m_gameStates) {
        state->notify_end_bet_state();
    }
}

void Kernel::notify_end_card_state() {
    for (const auto &state : m_gameStates) {
        state->notify_end_card_state();
    }
}

void Kernel::notify_begin_bet_state() {
    for (const auto &state : m_gameStates) {
        state->notify_begin_bet_state();
    }
}

void Kernel::notify_game_win(int winner) {
    for (const auto &state : m_gameStates) {
        state->notify_game_win(winner);
    }
}

void Kernel::notify_ready_player(int player) {
    for (const auto &state : m_gameStates) {
        state->notify_ready_player(player);
    }
}

const GameState &Kernel::get_game_state(int id) const {
    if (id >= (int)m_gameStates.size())
        abort(); // TODO : change this
    return *m_gameStates[id];
}

const GameState &Kernel::get_game_state() const {
    return get_game_state(0);
}

void Kernel::replace_player(int player) {
    m_players[player] = make_shared<IA::IAPlayer>(50);
    m_gameStates[player] = make_unique<GameState>(*m_gameStates[player]);
    m_players[player]->set_game_state(*(m_gameStates[player]));
    _interrupted = true;
    nb_human--;
}

AllHands deal(int nbCardsPerPlayer) {
    Hand hand = generate_full_game();
    shuffle(begin(hand), end(hand), gen);
    AllHands hands(Core::nbPlayers);
    for (int player = 0; player < Core::nbPlayers; ++player) {
        hands[player].resize(nbCardsPerPlayer);
        for (int card = 0; card < nbCardsPerPlayer; ++card) {
            hands[player][card] = hand[player * nbCardsPerPlayer + card];
        }
    }
    return hands;
}
} // namespace Core
