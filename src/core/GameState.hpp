/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef CORE_GAME_STATE_HPP
#define CORE_GAME_STATE_HPP

#include "Events.hpp"
#include "CoreConstants.hpp"
#include <array>
#include <mutex>
#include <queue>

/**
 * \file GameState.hpp
 * \brief Define Core::GameState that encapsulate fundamentals informations on
 *the game : hand, id, game part...
 **/

namespace Core {

/** \brief An enumeration of the phases of the game **/
enum class GamePart { NotBeganYet, BetPart, CardPlayingPart };

/** \brief A stack of Event **/
typedef std::queue<std::shared_ptr<Event>> EventStack;

/** \brief An enumeration of the possible state of correctness of a bet **/
enum class BetInfo { ValidBet, BadNumber, BadBet };

/** \brief An enumeration of the possible state of correctness of a the playing
 *of a card \todo find a sentence without four 'for' in it to describe this enum
 **/
enum class CardMovedInfo { ValidMove, BadMove, CardNotInHand };

/** \brief A trick in the game without knowledge about the rules
 **/
struct CardStack {
  public:
    CardStack(Trump);

    /** \brief Return the suit that it supposed to be play
     *   \return the Suit of the first card played
     **/
    Suit suit() const;

    /** \brief Return the strongest card in the trick **/
    Card strongest_card() const;

    /** \brief Return the card of this player
     * \warning Return a null card if this player has not yet played
     **/
    Card get_card(int id) const;

    /** \brief An accessor to the list of the cards in the trick **/
    const std::vector<Card> &stack() const;

    /** \brief Return an indentitier of who takes the trick **/
    int id_winner() const;

    /** \brief Return an indentitier of who has opened the trick **/
    int id_first_player() const;

    /** \brief Return the fact that the stack is empty **/
    bool empty() const;

    /** \brief Add a card to the trick
     *   \param card The card that the player is playing
     *   \param idPlayer An identifier of the player playing the card
     **/
    void add_card(Card, int);

    /** \brief Declare that a card played is the strongest in the trick
     *   \param card The card that a player has played
     *   \param idPlayer An identifier of the player that played the card
     **/
    void set_strongest_card(Card, int);

    /** \brief return the current size of the CardStack **/
    int size() const;

    /** \brief get trump of the current cardstack **/
    Trump trump() const;

    /** \brief Check that a card is of the Suit of the current Trump **/
    bool is_trump(const Card &) const;

    /** \brief Check that the left card is stronger than the right one according
     * to the current Trump **/
    bool stronger_than(const Card &, const Card &) const;

    /** \brief Return all playable cards of the input hand with this trump **/
    Hand playable_cards(const Hand &hand) const;

  private:
    Trump m_trump;
    Suit m_suit;
    Card m_strongest;
    std::vector<Card> m_stack;
    int m_idStronger;
    int m_idOpener;
};

/** \brief The state of a game from the point of view of some player
 *
 *  Contains the information about a player hand and score, and is capable of
 *self reasoning about the rule
 **/
class GameState {
  public:
    /** \brief Constructor of a GameState
     *   \param id The indentitier of the player owning the game state
     **/
    GameState(int id);

    GameState(const GameState &) = default;

    /** \brief Forbidden operation  **/
    GameState &operator=(const GameState &) = delete;

    /**
     \brief set card stack for predefined game
    **/
    void set_card_stack(const CardStack &);

    // Global infos
    /** \brief Accessor to the hand
     *   \param trump The trump to use to sort the hand
     * 	\param display Option to set the hand from right to left
     **/
    const Hand my_hand(Trump, bool) const;
    const Hand my_hand() const;
    /** \brief Accessor to the identifier of the player owning the game state
     * **/
    int my_id() const;
    /** \brief Accessor to the identifier of the player currently playing **/
    int id_current_player() const;
    /** \brief Accessor to the number of the turn in the game **/
    int turn_number() const;
    /** brief return the first player of the round **/
    int first_player_round() const;
    /** \brief Accessor to the number of the trick in the turn **/
    int trick_number() const;
    /** \brief Return the number of cards during actual round**/
    int nb_cards_round() const;
    /** \brief Accessor to which part of the game is on **/
    GamePart game_part() const;
    /** \brief Accessor to the score of all players **/
    const std::array<int, 4> &get_scores() const;

    // Bet part
    /** \brief Accessor to the bets of all players **/
    const AllBets &bets() const;
    /** \brief Accessor to the best actual bet **/
    const Bet &best_bet() const;
    /** \brief Accessor to the id of the player that hold the bets, if any, -1
     * else **/
    int id_best_bet() const;
    /** \brief Accessor to the number of trick bet **/
    int nb_trick_bet() const;
    /** \brief Accessor to the current Trump **/
    Trump trump() const;
    /** \brief Check that a card is of the Suit of the current Trump **/
    bool is_trump(const Card &) const;
    /** \brief Check that the left card is stronger than the right one according
     * to the current Trump **/
    bool stronger_than(const Card &, const Card &) const;
    /** \brief Return informations about the correctness of a Bet **/
    BetInfo bet_info(const Bet &) const;
    /** \brief Return the number forbidden when i am the last player who bets,
     * return -1 if you're not the last bet **/
    int bet_forbidden() const;
    /** \brief The fact that someone have chouned **/
    bool is_choune() const;
    /** \brief The fact that someone have mechouned **/
    bool is_mechoune() const;
    /** \brief Id of the mechouner, if any, -1 else **/
    int id_mechouner() const;

    const std::array<std::array<int, 4>, 6> &last_bet_color() const;

    /** \brief Id of the chouner, if any, -1 else **/
    int id_chouner() const;

    /** \brief Number of tricks announced by each player during bet part for the
     * current trump **/
    const std::array<int, 4> &nb_tricks_objective() const;
    /** \brief Number of tricks achieved by each player during card playing part
     * **/
    const std::array<int, 4> &nb_tricks_current() const;

    /** \brief Accessor to the trick
     *   \warning Doesn't make sense to call during bet part
     **/
    const CardStack &card_stack() const;
    /** \brief Accessor to the last trick
     *   \warning Doesn't make sense to call during bet part
     **/
    const CardStack &last_card_stack() const;
    /** \brief Compute and return the list o bool invertedle cards in the hand
    Trump trump, int displayof the player
    *   \warning Doesn't make sense to call during bet part

    **/
    Hand playable_cards() const;
    /** \brief  Return informations about the correctness of the play of a card
     *   \warning Doesn't make sense to call during bet part
     **/
    CardMovedInfo card_info(const Card &) const;

    // Events part
    /** \brief Return the fact that the state of the game changed since the last
     * time **/
    bool state_changed() const;
    /** \brief Return the event stack **/
    std::shared_ptr<Event> get_next_event() const;

    // For pattern observer : Kernel notifies each GameState
    // notification are assuming to being conform to the rules
    // (Kernel check everything)
    /** \brief  Notify that a bet (of the current player) has been accepted
     *   \warning Should be call by the Kernel or a reliable intermediare
     **/
    virtual void notify_bet(const Bet &);
    /** \brief  Notify that a card (of the current player) has been played
     *   \warning Should be call by the Kernel or a reliable intermediare
     **/
    virtual void notify_play(const Card &);
    /** \brief  Notify that the play part has started and th bool invertedrump
     *trump, int displayhas been received \warning Should be call by the Kernel
     *or a reliable intermediare \todo First player change, maybe we need to
     *tell the player what is the identifier of the first player to play
     **/
    virtual void notify_cards(const Hand &);
    /** \brief  Notify that a player mechouned, using the identifier of a player
     *   \warning Should be call by the Kernel or a reliable intermediare
     **/
    virtual void notify_mechoune(int);
    /** \brief  Notify that the current player had choune
     *   \warning Should be call by the Kernel or a reliable intermediare
     **/
    virtual void notify_choune();
    /** \brief  Notify that it's the turn of the next player
     *   \warning Should be call by the Kernel or a reliable intermediare
     **/
    virtual void notify_next_player(int);
    /** \brief  Notify that the play part has ended
     *   \warning Should be call by the Kernel or a reliable intermediare
     **/
    virtual void notify_trick_win(int);

    virtual void notify_game_win(int);
    virtual void notify_ready_player(int);

    virtual void notify_end_bet_state();
    virtual void notify_end_card_state();
    virtual void notify_begin_bet_state();

    // virtual void notify_all_bets(const AllBets &bets);

  private:
    // Global infos
    Hand m_hand;
    int m_myId;
    int m_handSize;
    int m_nbPlayers;
    int m_idCurrentPlayer;
    int m_turnNumber;
    int m_trickNumber;
    GamePart m_gamePart;
    std::array<int, 4> m_scores;

    // Bet infos
    AllBets m_bets; // THE ACTUAL VALID BETS, NOT THE HISTORY
    Bet m_bestBet;
    int m_idBestBet;
    bool m_mechoune;
    int m_idMechouner;
    bool m_choune;
    std::array<std::array<int, 4>, 6> m_lastBetColor;
    std::array<int, 4> m_nbTricksObjective;
    std::array<int, 4> m_nbTricksCurrent;

    // Playing Card infos
    CardStack m_cardStack;
    CardStack m_lastCardStack;

    // Events for GUI
    mutable EventStack m_events;
};
} // namespace Core

#endif
