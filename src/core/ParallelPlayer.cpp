/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "ParallelPlayer.hpp"

using namespace std;

namespace Core {
ParallelPlayer::Sleeper::Sleeper(mutex &m, condition_variable &_cv,
                                 bool &_waiting)
    : lock(m), cv(&_cv), waiting(&_waiting) {
    *waiting = true;
    cv->wait(lock);
}

ParallelPlayer::Sleeper::~Sleeper() { *waiting = false; }

ParallelPlayer::ParallelPlayer()
    : m_waiting_bet(false), m_waiting_play(false),
      // m_waiting_mechoune(false),
      m_waiting_choune(false), m_waiting_ready(false) {}

Bet ParallelPlayer::bet() {
    Sleeper sleep(m_mutex, m_cv, m_waiting_bet);
    return m_bet;
}

void ParallelPlayer::ask_bet() {
    unique_lock<mutex> lock(m_mutex);
    m_waiting_bet = true;
}

Card ParallelPlayer::play() {
    Sleeper sleep(m_mutex, m_cv, m_waiting_play);
    return m_card;
}

// bool ParallelPlayer::mechoune() {
//     Sleeper sleep(m_mutex, m_cv, m_waiting_mechoune);
//     return m_mechoune;
// }

bool ParallelPlayer::choune() {
    Sleeper sleep(m_mutex, m_cv, m_waiting_choune);
    return m_choune;
}

void ParallelPlayer::ready() { Sleeper sleep(m_mutex, m_cv, m_waiting_ready); }

bool ParallelPlayer::waiting_bet() const { return m_waiting_bet; }

bool ParallelPlayer::waiting_play() const { return m_waiting_play; }

bool ParallelPlayer::waiting_mechoune() const {
    // return m_waiting_mechoune;
    return m_gameState->game_part() == GamePart::BetPart &&
           m_gameState->id_best_bet() != -1 &&
           m_gameState->id_best_bet() != m_gameState->my_id() &&
           !m_gameState->is_mechoune();
}

bool ParallelPlayer::waiting_choune() const { return m_waiting_choune; }

bool ParallelPlayer::waiting_ready() const { return m_waiting_ready; }

void ParallelPlayer::set_bet(const Bet &bet) {
    m_betBox->bet(bet);
    m_waiting_bet = false;
}

void ParallelPlayer::set_played_card(const Card &card) {
    unique_lock<mutex> lock(m_mutex);
    m_card = card;
    m_cv.notify_one();
}

void ParallelPlayer::set_mechoune(bool mechoune) {
    // unique_lock<mutex> lock(m_mutex);
    // m_mechoune = mechoune;
    // m_cv.notify_one();
    if (mechoune) {
        cout << "[ParallelPlayer.cpp] set_mechoune call" << endl;
        m_betBox->mechoune();
    }
}

void ParallelPlayer::set_choune(bool choune) {
    unique_lock<mutex> lock(m_mutex);
    m_choune = choune;
    m_cv.notify_one();
}

void ParallelPlayer::set_ready() {
    unique_lock<mutex> lock(m_mutex);
    m_cv.notify_one();
}
} // namespace Core
