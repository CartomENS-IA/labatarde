/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Network.hpp>
#include <array>
#include <cstddef>
#include <string>
#include <vector>

#include "KernelInterface.hpp"
#include "Bet.hpp"
#include "BetQueue.hpp"
#include "Card.hpp"
#include "Messenger.hpp"
#include "Player.hpp"

/** \file
 *   \brief Definition of class Core::Kernel
 **/

namespace Core {

class Timeout : public std::exception {
  public:
    Timeout() = default;
    virtual const char *what() const throw();
};

/**
 * \brief A representation of a game
 *
 * It has access to players and the state of their game. It communicate using
 * the observer pattern and
 **/
class Kernel : public KernelInterface {
  private:
    std::vector<std::shared_ptr<Player>> m_players;
    std::vector<std::unique_ptr<GameState>> m_gameStates;
    bool _skip_bet;
    std::size_t _next_player; // only if _skip_bet is true;
    int nb_human = -1;
    bool _interrupted;

    BetQueueBase *m_betQ = nullptr;

    void run_turn(int turn);

    void run_predefined();
    void run_bet_state(int);
    void run_card_playing_state(int);

    void gotMechouned();

    // For pattern observer : Kernel notifies each GameState
    void notify_bet(const Bet &);
    void notify_play(const Card &);
    void notify_mechoune(int);
    void notify_choune();
    void notify_next_player(int);
    void notify_cards(const AllHands &);
    void notify_trick_win(int);
    void notify_end_bet_state();
    void notify_end_card_state();
    void notify_begin_bet_state();
    void notify_game_win(int);
    void notify_ready_player(int);

    void replace_player(int player);

  public:
    /**
     *   \brief Consruct a game for four players
     *   \param p1 The first player
     *   \param p2 The second player
     *   \param p3 The third player
     *   \param p4 The fourth player
     **/
    Kernel(std::shared_ptr<Player> p1,
           std::shared_ptr<Player> p2,
           std::shared_ptr<Player> p3,
           std::shared_ptr<Player> p4);

    Kernel(std::shared_ptr<Player> p1, std::shared_ptr<Player> p2,
           std::shared_ptr<Player> p3, std::shared_ptr<Player> p4, int nbh,
           std::vector<std::shared_ptr<Network::Messenger>> &mess);

    /**
     *   \brief Consruct a game for four players using a text file
     *   \param path The path of the file to read
     *   \note Proposal
     **/
    // Kernel(const std::string path);

    /**
     * \brief Construct a game given the hands and bet of all players
     * \param player An array of players
     * \param hand An array of hands corresponding to the previous
     * players (each player must have different cards, and the difference of
     * their hand size shouldn't exceed 1, due to the cardstack)
     * \param bet An array of bets corresponding to the previous players, betted
     * in the order of the array ; the trump will be the last trump is the array
     * \param trick_done An array of the number of trick done for each of the
     * previous players
     * \param cardstack The card stack
     **/
    Kernel(std::array<shared_ptr<Player>, 4> const &player,
           AllHands const &hand, AllBets const &bet, int idBestBet,
           std::array<int, 4> const &trick_done, CardStack const &cardstack,
           int nextPlayer);

    ~Kernel();

    Kernel(const Kernel &) = delete;
    class Kernel &operator=(class Kernel &) = delete;

    /**
     *   \brief Save the state of a game into a text file
     *   \param path The path to save the file
     *   \todo Define the format and implement
     **/
    // void save(const std::string& path) const;

    /**
     *   \brief Load a game using a text file
     *   \param path The path of the file to read
     *   \todo Define the format and implement
     **/
    // void load(const std::string& path);

    /**
     *   \brief Accessor to the game_state of a player
     *   \param id An integer from 1 to 3 identificating the player
     *   \return The game state of a player
     *   \throws Abort if id is not a proper identifier
     *   \todo Generate a proper exeception instead of the abord function
     **/
    const GameState &get_game_state(int id) const;

    /** \brief Accessor to the game_state of Player 0 **/
    const GameState &get_game_state() const;

    /**
     *   \brief Run the main loop a game
     **/
    std::array<int, 4> run();
};

AllHands deal(int nbCardsPerPlayer);

} // namespace Core
