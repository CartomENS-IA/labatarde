/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

/** \file
 * \brief Definition of the constants of the game
 **/

 #ifndef CORE_CONSTANTS_INCLUDED
 #define CORE_CONSTANTS_INCLUDED

#include <array>

namespace Core {

 /** \brief the number of players in a game **/
constexpr int nbPlayers = 4;

 /** \brief the number of cards in hand at each turn the game **/
const std::array<int, 10> nbCardsPerTurn = {{5, 6, 7, 8, 9, 9, 8, 7, 6, 5}};

} // namespace Core
#endif // CORE_CONSTANTS_INCLUDED
