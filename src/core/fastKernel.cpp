/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "fastKernel.hpp"

Core::Card Core::FastKernel::play(const Hand &hand,
                                  const CardStack &cardStack) {
    Hand playables = cardStack.playable_cards(hand);
    auto &card = playables[rand() % playables.size()];
    std::cout << card << std::endl;
    return card;
}

std::array<int, 4> Core::FastKernel::run_card_phase(
    AllHands hand, AllBets const &bet, int idBestBet,
    std::array<int, 4> const trick_done, CardStack cardStack, int nextPlayer) {

    const int nbPlayers = 4;
    auto scores = trick_done;

    int i_player = nextPlayer;
    int n = nbPlayers - cardStack.size();

    for (int j = 0; j < n; j++) {
        int player = (j + nextPlayer) % nbPlayers;
        std::cout << player << " ";
        Card card = play(hand[player], cardStack);

        std::remove(hand[player].begin(), hand[player].end(), card);
        hand[player].pop_back();
        cardStack.add_card(card, player);
    }

    int winner = cardStack.id_winner();
    scores[winner]++;
    cardStack = CardStack(bet[0].trump);

    while (hand[0].size()) {
        for (i_player = 0; i_player < nbPlayers; ++i_player) {
            int player = (i_player + winner) % nbPlayers;
            std::cout << player << " ";
            Card card = play(hand[player], cardStack);

            std::remove(hand[player].begin(), hand[player].end(), card);
            hand[player].pop_back();
            cardStack.add_card(card, player);
        }

        winner = cardStack.id_winner();
        scores[winner]++;
        cardStack = CardStack(bet[0].trump);
        std::cout << "TRICK WIN " << winner << std::endl;
    }

    for (i_player = 0; i_player < nbPlayers; ++i_player) {
        int player = (i_player + idBestBet) % nbPlayers;
        scores[player] = abs(scores[player] - bet[i_player].value);
    }
    return scores;
}
