/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Player.hpp"

namespace Core {
Player::~Player() {
    if (m_betBox != nullptr)
        delete m_betBox;
}

bool Player::mechoune() { return false; }

void Player::set_game_state(const GameState &gameState) {
    m_gameState = &gameState;
}

int Player::my_id() const { return m_gameState->my_id(); }

const GameState &Player::get_game_state() { return *m_gameState; }

std::unique_ptr<GameState> Player::create_game_state(int id) {
    return std::unique_ptr<GameState>(new GameState(id));
}

void Player::set_bet_box(BetBoxBase *bB) { m_betBox = bB; }

void Player::ask_bet() { m_betBox->bet(this->bet()); }

void Player::ask_mechoune() {
    if (this->mechoune())
        m_betBox->mechoune();
}
} // namespace Core
