/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "BetQueue.hpp"
#include <iostream>

namespace Core {
BetQueueBase::BetQueueBase() : m_betEvents(0) {}

void BetQueueBase::add_bet(int id, Bet bet) {
    m_betEvents.push_back(new BetEvent(bet, id));
}

void BetQueueBase::add_mechoune(int id) {
    m_betEvents.push_back(new MechouneEvent(id));
}

bool BetQueueBase::empty() { return m_betEvents.empty(); }

void BetQueueBase::clear() {
    for (Event *e : m_betEvents)
        delete e;
    m_betEvents.clear();
}

void BetQueue::add_bet(int id, Bet bet) {
    std::lock_guard<std::mutex> lg(m_mutex);
    BetEvent *e = new BetEvent(bet, id);
    m_betEvents.push_back(e);
    m_cv.notify_one();
}

void BetQueue::add_mechoune(int id) {
    std::lock_guard<std::mutex> lg(m_mutex);
    m_betEvents.push_back(new MechouneEvent(id));
    m_cv.notify_one();
}

Event *BetQueue::pop() {
    std::unique_lock<std::mutex> lock(m_mutex);
    Event *e;
    while (m_betEvents.empty()) {
        m_cv.wait(lock);
    }
    e = m_betEvents.front();
    m_betEvents.pop_front();
    lock.unlock();
    return e;
}

} // namespace Core
