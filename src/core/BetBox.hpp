/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Bet.hpp"
#include "BetQueue.hpp"

namespace Core {
class BetBoxBase {
  public:
    virtual ~BetBoxBase(){};
    virtual void bet(Bet bet) = 0;
    virtual void mechoune() = 0;
};

class BetBox : public BetBoxBase {
  private:
    BetQueueBase *const m_betQ;
    const int m_id;

  public:
    BetBox(int player, BetQueueBase *bQ) : m_betQ(bQ), m_id(player){};
    void bet(Bet bet);
    void mechoune();
};
} // namespace Core
