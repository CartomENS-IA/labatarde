/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef IA_HPP
#define IA_HPP

/** \file
 * \brief This file must be included by any other file which uses IA library,
 * and should be the only one included.
 * The whole API is accessed by the namespace "IA"
 *
 * Every IA that is defined here must inherit from the Core::Player interface.
 * This interface provides the methods the IA should implement in order to
 *function in the game (See the Core's documentation for further information)
 **/

#include "IA/BotDeter.hpp"
#include "IA/BotPlayer.hpp"
#include "IA/IAPlayer.hpp"
#include "IA/KnowledgeEngine.hpp"
#include "IA/RandomPlayer.hpp"
#include "IA/Tournament.hpp"

#endif
