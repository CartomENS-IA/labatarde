/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Network.hpp"
#include "Core.hpp"

int main(int argc, char** argv)
{
    char const* ipstring="localhost";
    if(argc>1)
        ipstring=argv[1];
    auto p=new(Core::ConsolePlayer);
    std::shared_ptr<sf::TcpSocket> s=std::make_shared<sf::TcpSocket>();
    sf::IpAddress ip(ipstring);
    s->connect(ip, 4242);
    char a = 7;
    size_t o;
    s->send(&a,1);

    int nb_lobby[30];
    size_t recv_size;
    //    s->setBlocking(false); /*Non blocking to avoid blocking on partial info */
                            
    sf::Socket::Status st = s->receive(nb_lobby, sizeof(int), recv_size);
                            
    //    s->setBlocking(true);

    Network::RemoteKernel k(p, s);
    std::cout << "connected" << std::endl;
    std::array<int,4> v=k.run();
    for(int i=0; i<4; i++)
        cout << "Player " << i << ": " << v[i] << endl;
    return 0;
}
