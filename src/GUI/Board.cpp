/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Board.hpp"
#include "GUI.hpp"
#include "Constants.hpp"
#include "SoundManager.hpp"
#include <exception>
#include <iostream>
#include <typeinfo>

// _________________ Static _______________________
std::string Board::state_to_string(State const state) {
    switch (state) {
    case State::DISPLAY_SCORE:
        return "Display Score";
    case State::WANT_TO_QUIT:
        return "Want to quit";
    case State::WAIT_READY:
        return "Wait ready";
    case State::CALL_READY:
        return "Call ready";
    case State::PLAY_PHASE:
        return "Play";
    case State::BET_PHASE:
        return "Bet";
    case State::END_ROUND_PHASE:
        return "End round";
    case State::END_GAME:
        return "End game";
    case State::IDLE:
        return "Idle";
    default:
        return "UNKNOWN STATUS";
    }
}

void Board::log_state_changed() {
    std::clog << "[Board] Changing to state "
              << state_to_string(m_state)
              << std::endl;
}

void Board::set_state(State s, bool verbose) {
    m_state = s;
    if (verbose) {
        log_state_changed();
    }
}

// _________________ Constructor __________________

Board::Board(const Core::GameState *kernelState,
             shared_ptr<Core::ParallelPlayer> guiPlayer,
             FourPlayerInfo const pInfo)
:
    m_state(State::IDLE),
    m_previous_state(State::IDLE),
    m_betArray(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 + 100.f*SCALE, 7),
    m_buttonMechoune(WINDOW_WIDTH -120.f*SCALE, WINDOW_HEIGHT-100*SCALE,
                     170*SCALE, 170*SCALE, "buttons/bouton-mechounee.png",
                     L"",0,"GillSans.ttc",WHITE, WHITE, WHITE),
    m_buttonChoune(WINDOW_WIDTH -120.f*SCALE, WINDOW_HEIGHT-100*SCALE,
                   170*SCALE, 170*SCALE, "buttons/bouton-chounee.png",
                   L"",0,"GillSans.ttc",WHITE, WHITE, WHITE),
    m_buttonReady(WINDOW_WIDTH -120.f*SCALE, WINDOW_HEIGHT-100*SCALE,
                  170*SCALE, 170*SCALE,"buttons/bouton-carre-plein.png",
                  "buttons/bouton-carre-vide.png", "ready", 70,
                  "GillSans.ttc",WHITE, WHITE, WHITE),
    m_ready(false),
    m_yesButton(WINDOW_WIDTH/2 - 100.f*SCALE, WINDOW_HEIGHT/2, 150.f*SCALE,
                80.f*SCALE, "buttons/blank.png", "yes", 50, "GillSans.ttc",
                WHITE, BLACK_LIT, BLACK_LIT),
    m_noButton(WINDOW_WIDTH/2 + 100.f*SCALE, WINDOW_HEIGHT/2, 150.f*SCALE,
               80.f*SCALE, "buttons/blank.png", "no", 50, "GillSans.ttc",
               WHITE, BLACK_LIT, BLACK_LIT),
    m_returnToMenu(WINDOW_WIDTH/2, WINDOW_HEIGHT/2-100.f*SCALE, unsigned(40*HALF_SCALE), "backToMenuQ", "GillSans.ttc", WHITE),
    m_yourTurn(false),
    m_mechouneAvailable(false),
    m_chouneAvailable(false),
    m_tuto(false),
    m_drawButtonBottom(true),
    m_lastTrick(sf::Vector2f(120.f*SCALE, 100*SCALE)),
    m_infoTopRight(WINDOW_WIDTH - 100*SCALE, 60*SCALE),
    m_tempsDispo(100.f)
{
    m_buttonReady.set_text_on_button();
    m_yesButton.set_text_on_button();
    m_noButton.set_text_on_button();
    m_gamestate = kernelState;
    m_kernelPlayer = guiPlayer;

    m_player[0] = GraphicPlayer(CardinalPoints::SOUTH, pInfo[0]);
    m_player[1] = GraphicPlayer(CardinalPoints::WEST, pInfo[1]);
    m_player[2] = GraphicPlayer(CardinalPoints::NORTH, pInfo[2]);
    m_player[3] = GraphicPlayer(CardinalPoints::EAST, pInfo[3]);

    m_endRoundDisplay.retrieve_player_info(pInfo);
    m_endGameDisplay.retrieve_player_info(pInfo);

    m_timerDistribution.restart();
    m_timerTrick.restart();

    m_handDistribution = 1;
    m_trickWait = -1;
    m_playerTrick = -1;

    m_nbMiddleCard = 0;

    for (int i = 0; i < 4; i++) {
        m_middleCard[i] = nullptr;
    }

    m_buttonsDownRight.push_back(std::make_unique<HybridButton>(
            WINDOW_WIDTH -165.f*SCALE, WINDOW_HEIGHT-145*SCALE, 80*SCALE,
            80*SCALE, "buttons/lignes.png",L"",0,"GillSans.ttc",
            WHITE, WHITE, WHITE));

    m_buttonsDownRight.push_back(std::make_unique<HybridButton>(
            WINDOW_WIDTH -75.f*SCALE, WINDOW_HEIGHT-145*SCALE, 80*SCALE,
            80*SCALE, "buttons/trophy.png",L"",0,"GillSans.ttc",
            WHITE, WHITE, WHITE));

    m_buttonsDownRight.push_back(std::make_unique<HybridButton>(
            WINDOW_WIDTH -165.f*SCALE, WINDOW_HEIGHT-55*SCALE, 80*SCALE,
            80*SCALE, "buttons/engrenage.png",L"",0,"GillSans.ttc",
            WHITE, WHITE, WHITE));

    m_buttonsDownRight.push_back(std::make_unique<HybridButton>(
            WINDOW_WIDTH -75.f*SCALE, WINDOW_HEIGHT-55*SCALE, 80*SCALE,
            80*SCALE, "buttons/bulle.png",L"",0,"GillSans.ttc",
            WHITE, WHITE, WHITE));
}

// __________________ Update Function ____________________
void Board::update(sf::RenderWindow &window, sf::Vector2i const mousePos) {
    update(window, sf::Vector2f(mousePos));
}

void Board::update(sf::RenderWindow &window, sf::Vector2f const mousePos) {
    update(window, mousePos.x, mousePos.y);
}

void Board::update(sf::RenderWindow &window, float const mouseX,
                   float const mouseY) {

    //______________ Distribution animation _______________________
    if (!(m_cardToDistribute.empty())) { // Distribution time
        if (float(m_timerDistribution.getElapsedTime().asMilliseconds()) >=
            100.f / DISTRIBUTION_SPEED) {
            m_timerDistribution.restart();
            if (m_handDistribution) {
                m_player[m_handDistribution].give_card();
            } else {
                m_player[0].give_card(m_cardToDistribute.back());
                m_cardToDistribute.pop_back();
            }
            m_handDistribution = (m_handDistribution + 1) % 4;
        }
        window.draw(m_deckDistribution);
    }

    m_drawButtonBottom = true;
    switch (m_state) {

    case State::WANT_TO_QUIT: {
      m_yesButton.update(window, mouseX, mouseY);
      m_noButton.update(window, mouseX, mouseY);
      m_returnToMenu.update(window);
      for (int i = 0; i < 4; i++) {
          m_player[i].draw_minimal(window);
      }
      m_lastTrick.update(window);
      m_infoTopRight.update(window);
      return;
    }

    case State::DISPLAY_SCORE: {
      m_endGameDisplay.update(window, mouseX, mouseY);
      return;
    }

    case State::CALL_READY:
    case State::IDLE:
        // nothing to do except waiting for Kernel
        break;

    case State::WAIT_READY:
        m_buttonReady.update(window, mouseX, mouseY);
        m_drawButtonBottom = false;
        m_tempsDispo -= 0.1f;
        m_buttonReady.set_filling(m_tempsDispo);
        break;

    case State::BET_PHASE:
        if (m_mechouneAvailable and not m_tuto) {
            m_buttonMechoune.update(window, mouseX, mouseY);
            m_drawButtonBottom = false;
        }

        if (m_chouneAvailable) {
            m_buttonChoune.update(window, mouseX, mouseY);
            m_drawButtonBottom = false;
        }

        if (!is_animated() && m_kernelPlayer->waiting_bet()) {
            m_betArray.draw(window);
        }
        break;

    case State::PLAY_PHASE:
        //___________ begin animation trick __________
        if (m_trickWait >= 0 &&
            float(m_timerTrick.getElapsedTime().asMilliseconds()) >=
                TIME_WAIT_TRICK) {
            give_trick_to_player(m_trickWait);
            m_trickWait = -1;
        }

        //___________ draw animation trick _____________
        if (m_playerTrick >= 0) {
            if (!is_animated()) {
                m_player[m_playerTrick].draw_new_trick();
                for (unsigned int i = 0; i < 4; i++) {
                    m_middleCard[i] = nullptr;
                }
                m_playerTrick = -1;
            }

            for (unsigned int i = 0; i < 4;
                 i++) { // we draw at begin in this case
                if (m_middleCard[i]) {
                    m_middleCard[i]->continue_move();
                    window.draw(*m_middleCard[i]);
                }
            }
        }
        break;

    case State::END_ROUND_PHASE:
        m_endRoundDisplay.update(window, mouseX, mouseY);
        return;

    case State::END_GAME:
        m_endGameDisplay.update(window, mouseX, mouseY);
        return;

    default:
        std::cerr << "[Board] In update function : status not recognized"
                  << std::endl;
        throw(std::runtime_error("Board in invalid state"));
    } // End switch

    // Actions done whatever the state is :

    for (int i = 1; i < 4; i++) {
        m_player[i].draw(window);
    }
    m_player[0].draw(window);

    if (m_playerTrick < 0) {
        for (unsigned int i = 0; i < 4; i++) {
            if (m_middleCard[i]) {
                m_middleCard[i]->update(window);
            }
        }
    }

    m_lastTrick.update(window);
    m_infoTopRight.update(window);
    if(m_drawButtonBottom and not m_tuto){
      for(auto &button : m_buttonsDownRight){
        button->update(window, mouseX, mouseY);
      }
    }
}

// __________________ Event Function ____________________

bool Board::can_quit(){
  switch (m_state) {
    case State::BET_PHASE:
    // intentionnal fallthrough
    case State::PLAY_PHASE:
    // intentionnal fallthrough
    case State::WAIT_READY:
    // intentionnal fallthrough
    case State::CALL_READY:
      return true;
    default:
      return false;
  }
}

bool Board::can_show_score(){
  switch (m_state) {
    case State::BET_PHASE:
    // intentionnal fallthrough
    case State::PLAY_PHASE:
      return true;
    default:
      return false;
  }
}

bool Board::handle_mouse_click(sf::Vector2i const mousePos) {
    return handle_mouse_click(sf::Vector2f(mousePos));
}

bool Board::handle_mouse_click(sf::Vector2f const mousePos) {
    return handle_mouse_click(mousePos.x, mousePos.y);
}

bool Board::handle_mouse_click(float const mouseX, float const mouseY) {
    if(m_buttonsDownRight[0]->is_lit() and can_quit()){
      m_previous_state = m_state;
      set_state(State::WANT_TO_QUIT);
      return true;
    }

    if (m_buttonsDownRight[1]->is_lit() and can_show_score()) {
      m_previous_state = m_state;
      set_state(State::DISPLAY_SCORE);
      return true;
    }

    if (m_buttonsDownRight[2]->is_lit() and can_quit()) {
      run_ingame_option_loop();
      return true;
    }

    switch (m_state) {
    case State::CALL_READY:
    case State::IDLE:
        break;

    case State::WANT_TO_QUIT: {
      if (m_yesButton.is_lit()){
         throw(EndOfGame());
      }
      if (m_noButton.is_lit()){
        set_state(m_previous_state);
      }
      break;
    }

    case State::WAIT_READY: {
        if (m_buttonReady.is_lit()) {
            // we can say ready or mechoune even if they are other actions
            soundManager.play_sound("click.wav");
            set_state(State::CALL_READY);
            if (m_kernelPlayer->waiting_ready()) {
                m_kernelPlayer->set_ready();
            }
            m_ready = true;
            return true;
        }
        break;
    }

    case State::BET_PHASE: {
        if (m_buttonMechoune.is_lit() and not m_tuto) {
            soundManager.play_sound("click.wav");
            if (m_kernelPlayer->waiting_mechoune()) {
                m_kernelPlayer->set_mechoune(true);
                m_buttonMechoune.lock();
            }
        }

        if (m_kernelPlayer->waiting_choune()) {
            if (m_buttonChoune.is_lit()) {
                m_kernelPlayer->set_choune(true);
                m_buttonChoune.lock();
            }
        }

        if (!m_kernelPlayer->waiting_bet())
            return false;

        std::shared_ptr<Core::Bet> currentBet =
            m_betArray.click(mouseX, mouseY);
        if (currentBet && m_tuto && m_betsAvailable.size() &&
            find(m_betsAvailable.begin(), m_betsAvailable.end(), *currentBet) ==
                m_betsAvailable.end()) {
            return false;
        }

        if (currentBet &&
            m_gamestate->bet_info(*currentBet) == Core::BetInfo::ValidBet) {
            m_kernelPlayer->set_bet(*currentBet);
        }

        break;
    }

    case State::PLAY_PHASE: {
        if (is_animated() or
            m_gamestate->state_changed() or
            !kernel_waiting_action()){
                // in those 3 cases, ignore user input
                return false;
            }

        if (std::shared_ptr<Card> tempCard =
                m_player[0].want_play(mouseX, mouseY)) {
            if (m_yourTurn &&
                find(m_playableCards.begin(), m_playableCards.end(),
                     tempCard->get_card_data()) != m_playableCards.end()) {
                m_kernelPlayer->set_played_card(tempCard->get_card_data());
            }
        }
        break;
    }

    case State::END_ROUND_PHASE: {
        if (m_endRoundDisplay.okButton_lit()) {
            soundManager.play_sound("click.wav");
            set_state(State::IDLE);
            for (auto &gPlayer : m_player){
                gPlayer.show_player_display();
            }
            return true;
        }
        break;
    }

    case State::END_GAME: {
        if (m_endGameDisplay.okButton_lit()) {
            soundManager.play_sound("click.wav");
            throw(EndOfGame());
            return true;
        }
        break;
    }

    case State::DISPLAY_SCORE:{
      if (m_endGameDisplay.okButton_lit()) {
          soundManager.play_sound("click.wav");
          set_state(m_previous_state);
          return true;
      }
      break;
    }

    default:
        std::cerr
            << "[Board] In handle_mouse_click function : status not recognized"
            << std::endl;
        throw std::runtime_error("Board in invalid state");
        break;
    }
    return false;
}

bool Board::handle_key_pressed(sf::Event::KeyEvent const key) {
  if (key.code == sf::Keyboard::Space) {
      if (m_state == State::WAIT_READY){
        soundManager.play_sound("click.wav");
        set_state(State::CALL_READY);
        if (m_kernelPlayer->waiting_ready()) {
            m_kernelPlayer->set_ready();
        }
        m_ready = true;
        return true;
      }
      return false;
   } else if (key.code == sf::Keyboard::Escape) {
      if (m_state == State::WANT_TO_QUIT){
        throw(EndOfGame());
      } else if (m_state == State::DISPLAY_SCORE) {
        set_state(m_previous_state);
        return true;
      } else if (can_quit()){
        m_previous_state = m_state;
        set_state(State::WANT_TO_QUIT);
        return true;
      }
      return false;
    } else {
      // no other key has effect
      return false;
    }
}


// __________________ Kernel Communication ___________________

void Board::wait_action_kernel() {

    if (m_playerTrick < 0 && !is_animated() &&
        m_state != State::END_ROUND_PHASE && m_state != State::WAIT_READY) {
        if (m_gamestate->state_changed()) {

            std::shared_ptr<Core::Event> currentEvent =
                m_gamestate->get_next_event();

            if (auto betEvent = Core::handle<Core::BetEvent>(currentEvent)) {
                for (int i = 0; i < 4; i++) {
                    if (m_player[i].get_bet().trump != betEvent->bet.trump) {
                        m_player[i].restart_bet();
                    }
                }
                m_player[betEvent->player].set_bet(betEvent->bet);
                m_infoTopRight.set_trump(betEvent->bet.trump);
                m_infoTopRight.set_nb_bet(m_gamestate->nb_trick_bet());
                m_infoTopRight.set_nb_card(m_gamestate->nb_cards_round());
            }

            else if (auto playEvent =
                         Core::handle<Core::PlayEvent>(currentEvent)) {
                play_card(playEvent->card, playEvent->player);
            }

            else if (auto mechouneEvent =
                         Core::handle<Core::MechouneEvent>(currentEvent)) {
                m_player[mechouneEvent->player].set_message(L"Méchoune");
                m_buttonMechoune.unlock();
                m_infoTopRight.set_text_mechoune(L"MÉCHOUNÉE");
            }

            else if (auto chouneEvent =
                         Core::handle<Core::ChouneEvent>(currentEvent)) {
                m_player[m_gamestate->id_best_bet()].set_message(L"Choune");
                m_buttonMechoune.unlock();
                m_infoTopRight.set_text_mechoune(L"CHOUNÉE");
            }

            else if (auto endBetPhaseEvent =
                         Core::handle<Core::EndBetPhaseEvent>(currentEvent)) {
                end_bet_moment();
            }

            else if (auto TrickWonByEvent =
                         Core::handle<Core::TrickWonByEvent>(currentEvent)) {
                m_trickWait = TrickWonByEvent->player;
                set_first_player(m_trickWait);
                m_timerTrick.restart();
                statManager.end_trick(*m_gamestate);
                m_lastTrick.update_trick(
                    m_gamestate->last_card_stack().stack(),
                    m_gamestate->last_card_stack().id_first_player());
            }

            else if (auto endCardPhaseEvent =
                         Core::handle<Core::EndCardPhaseEvent>(currentEvent)) {
                end_play_moment();
                m_lastTrick.reset();
                m_infoTopRight.reset();
                m_infoTopRight.set_round(m_gamestate->turn_number());
            }

            else if (auto beginBetPhaseEvent =
                         Core::handle<Core::BeginBetPhaseEvent>(currentEvent)) {
                m_tempsDispo=100.f;
                for (int i = 0; i < 4; i++) {
                    m_player[i].restart_bet();
                }
                set_state(State::BET_PHASE);
                m_infoTopRight.set_nb_bet(0);
                m_infoTopRight.set_nb_card(m_gamestate->nb_cards_round());
            }

            else if (auto gameWonByEvent =
                         Core::handle<Core::GameWonByEvent>(currentEvent)) {
                end_game(gameWonByEvent->player);
            }

            else if (auto ReadyEvent =
                         Core::handle<Core::ReadyEvent>(currentEvent)) {
                m_player[ReadyEvent->player].set_message("ready");
            }

            else if (auto DealCardsEvent =
                         Core::handle<Core::DealCardsEvent>(currentEvent)) {
                distribution();
                if (m_tuto) {
                    m_ready = true;
                    set_state(State::BET_PHASE);
                } else {
                    set_state(State::WAIT_READY);
                    m_ready = false;
                }
                set_first_player(m_gamestate->first_player_round());
            }

            else {
                std::cerr
                    << "[Board] in function wait_action_kernel : Event unknown"
                    << std::endl;
            }
        } else { // Kernel state has not changed
            if (m_kernelPlayer->waiting_ready() && m_ready) {
                m_kernelPlayer->set_ready();
            } else if (m_kernelPlayer->waiting_bet()) {
                auto &best_bet = m_gamestate->best_bet();
                m_betArray.set_playable_bet(best_bet,
                                            m_gamestate->is_mechoune(),
                                            m_gamestate->bet_forbidden());
                if (m_tuto) {
                    m_betArray.set_playable_bet(m_betsAvailable);
                }
                m_player[0].restart_bet();
            }

            if (m_kernelPlayer->waiting_play()) {
                if (m_player[0].size_hand() == 1) {
                    // Automatically play the last trick
                    m_kernelPlayer->set_played_card(
                        m_gamestate->playable_cards()[0]);
                } else {
                    m_yourTurn = true;
                    if (!m_tuto) {
                        m_playableCards = m_gamestate->playable_cards();
                    }
                    m_player[0].set_playable_cards(m_playableCards);
                    m_player[0].your_turn();
                }
            }

            m_mechouneAvailable = m_kernelPlayer->waiting_mechoune();
            m_chouneAvailable = m_kernelPlayer->waiting_choune();
        }
    }
}

void Board::play_card(Core::Card cardPlayed, int playerID) {
    m_player[playerID].end_turn();
    m_middleCard[m_nbMiddleCard] = m_player[playerID].play_card(cardPlayed);
    m_nbMiddleCard = (m_nbMiddleCard + 1) % 4;
}

// ________________________ Private Function ________________________

void Board::distribution() {
    m_timerDistribution.restart();
    m_cardToDistribute = m_gamestate->my_hand(Core::Trump::NoTrump, false);
    m_betArray.set_nb_token(int(m_cardToDistribute.size() + 1));
}

bool Board::is_animated() {
    if (!m_cardToDistribute.empty() or m_trickWait >= 0) {
        return true;
    }
    for (int i = 0; i < 4; i++) {
        if (m_middleCard[i] && m_middleCard[i]->is_animated()) {
            return true;
        }
        if (m_player[i].is_animated()) {
            return true;
        }
    }
    return false;
}

void Board::give_trick_to_player(int playerID) {
    m_player[playerID].give_trick();
    sf::Vector2f destination = m_player[playerID].get_pos_free_token();
    float scaleToken = m_player[playerID].get_scale_token();
    m_playerTrick = playerID;
    for (int i = 0; i < 4; i++) {
        if (m_middleCard[i]) {
            m_middleCard[i]->move(destination, float(360 - playerID * 90),
                                  C_TRICK_TIME);
            m_middleCard[i]->zoom_sprite(scaleToken, C_TRICK_TIME / 3);
            m_middleCard[i]->flip_card(C_TRICK_TIME);
        }
    }
}

void Board::end_game(int winnerID) {
    set_state(State::END_GAME);
    m_endGameDisplay.retrieve_winner(winnerID);
    statManager.end_game(*m_gamestate);
}

void Board::end_bet_moment() {
    set_state(State::PLAY_PHASE);
    m_endRoundDisplay.retrieve_bet_info(
        m_gamestate->trump(), m_gamestate->nb_tricks_objective(),
        m_gamestate->is_mechoune(), m_gamestate->is_choune());
    m_endGameDisplay.retrieve_bet_info(
        m_gamestate->trump(), m_gamestate->nb_tricks_objective(),
        m_gamestate->is_mechoune(), m_gamestate->is_choune());
    for (int i = 0; i < 4; i++) {
        m_player[i].begin_main_phase();
    }
    m_player[0].sort_hand(m_gamestate->trump());
    statManager.begin_round(*m_gamestate);
}

void Board::end_play_moment() {
    set_state(State::END_ROUND_PHASE);
    m_endRoundDisplay.retrieve_score_info(m_gamestate->nb_tricks_current(),
                                            m_gamestate->get_scores());
    m_endGameDisplay.retrieve_score_info(m_gamestate->nb_tricks_current(),
                                            m_gamestate->get_scores());
    auto &score = m_gamestate->get_scores();
    for (int player=0; player<4; player++) {
        m_player[player].end_play_moment();
        m_player[player].set_points(score[player]);
    }
    soundManager.play_sound("card_shuffle.ogg");
    statManager.end_round(*m_gamestate);
}

bool Board::kernel_waiting_action() {
    return (m_kernelPlayer->waiting_bet() or
            m_kernelPlayer->waiting_play() or
            m_kernelPlayer->waiting_mechoune() or
            m_kernelPlayer->waiting_choune());
}

void Board::set_first_player(int first_player) {
    for (int player = 0; player < 4; player++) {
      m_player[player].set_first_player(player==first_player);
    }
}

void Board::run_ingame_option_loop() {

}

//_______________________________ Tuto Functions _______________________________

void Board::enable_tuto_mode() {
    m_tuto = true;
    m_state = State::IDLE;
}

void Board::set_playable_cards(Core::Hand cards) {
    if (not m_tuto){
      cerr << "[Board] Method set_playable_cards can only be called "
           << "when the tutorial mode is on" << endl;
      throw runtime_error("Runtime error from Board");
    }
    m_playableCards = cards;
}

void Board::set_playable_bets(int color, int value) {
    if (not m_tuto){
      cerr << "[Board] Method set_playable_bets can only be called "
           << "when the tutorial mode is on" << endl;
      throw runtime_error("Runtime error from Board");
    }
    m_betsAvailable = std::vector<Core::Bet>();
    if (color != -1) {
        if (value != -1) {
            m_betsAvailable.emplace_back(static_cast<Core::Trump>(color),
                                         value);
        } else {
            for (int val = 0; val <= 9; val++)
                m_betsAvailable.emplace_back(static_cast<Core::Trump>(color),
                                             val);
        }
    } else if (value != -1) {
        for (int trumpId = 0; trumpId < 6; trumpId++)
            m_betsAvailable.emplace_back(static_cast<Core::Trump>(trumpId),
                                         value);
    }
}

void Board::set_allbet(Core::AllBets &bets) {
    if (not m_tuto){
      cerr << "[Board] Method set_allbet can only be called "
           << "when the tutorial mode is on" << endl;
      throw runtime_error("Runtime error from Board");
    }
    for (int i = 0; i < 4; i++) {
        m_player[i].set_bet(bets[i], false);
    }
    m_state = State::PLAY_PHASE;
    m_player[0].sort_hand(bets[0].trump);
    m_infoTopRight.set_trump(bets[0].trump);
}

MessageEvent::MessageEvent(std::string &_text) : text(_text) {}
