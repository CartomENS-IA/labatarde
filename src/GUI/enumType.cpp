/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "enumType.hpp"

std::string language_to_string(Language const l) {
    switch (l) {
        case Language::ENGLISH:
            return "EN";
        case Language::FRENCH:
            return "FR";
        case Language::GERMAN:
            return "GER";
        case Language::SPANISH:
            return "SPA";
        case Language::ITALIAN:
            return "ITA";
        case Language::PIRATE:
            return "PIR";
        case Language::PORTUGUESE:
            return "POR";
        default:
            return "";
    }
}

Language string_to_language(std::string const &str) {
    if (str == "EN")
        return Language::ENGLISH;
    if (str == "GER")
        return Language::GERMAN;
    if (str == "SPA")
        return Language::SPANISH;
    if (str == "ITA")
        return Language::ITALIAN;
    if (str == "POR")
        return Language::PORTUGUESE;
    if (str == "PIR")
        return Language::PIRATE;
    return Language::FRENCH;
}


int language_to_id(Language const l){
    switch (l) {
        case Language::ENGLISH:
            return 0;
        case Language::FRENCH:
            return 1;
        case Language::GERMAN:
            return 2;
        case Language::SPANISH:
            return 3;
        case Language::PORTUGUESE:
            return 4;
        case Language::ITALIAN:
            return 5;
        default:
            return -1;
    }
}

Language id_to_language(int const n){
    switch (n) {
        case 0:
            return Language::ENGLISH;
        case 2:
            return Language::GERMAN;
        case 3:
            return Language::SPANISH;
        case 4:
            return Language::PORTUGUESE;
        case 5:
            return Language::ITALIAN;
        case 1:
        default:
            return Language::FRENCH;
    }
}

std::string difficulty_to_string(Difficulty const d) {
    switch (d) {
    case Difficulty::BEGINNER:
        return "Beginner";
    case Difficulty::VERY_GOOD:
        return "VeryGood";
    case Difficulty::MASTER:
        return "Master";
    case Difficulty::GOOD:
    default:
        return "Good";
    }
}

Difficulty string_to_difficulty(std::string const &str) {
    if (str == "Beginner")
        return Difficulty::BEGINNER;
    if (str == "VeryGood")
        return Difficulty::VERY_GOOD;
    if (str == "Master")
        return Difficulty::MASTER;
    return Difficulty::GOOD;
}


std::string cardType_to_string(CardType const t) {
    switch (t) {
        case CardType::SPECIAL:
            return "special";
        case CardType::CLASSICAL:
        default:
            return "classical";
    }
}

CardType string_to_cardType(std::string const &str) {
    if (str == "special")
        return CardType::SPECIAL;
    return CardType::CLASSICAL;
}
