/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <string>
#include <vector>

#include "AnimatedSprite.hpp"
#include "GUI.hpp"
#include "enumType.hpp"

void GUI::run_credits_loop() {

    float const CONTRIBUTOR_TEXT_SIZE = 18;
    unsigned int const CONTRIBUTOR_FONT_SIZE = unsigned(18);
    unsigned int const CREDIT_FONT_SIZE = unsigned(20);

    TM.load("credits.xml");

    Background theBackground("background/menu.png");
    TextButton backButton(
        WINDOW_WIDTH / 2, WINDOW_HEIGHT -1.5f*MENU_BUTTON_TEXT_SIZE ,
        MENU_BUTTON_FONT_SIZE, "back", "GillSans.ttc", BLACK, BLACK_LIT, BLACK);

    std::vector<Message> allMessages;
    AnimatedSprite cartomLogo(130, 130);
    cartomLogo.set_texture("logoCartomensia.png");
    cartomLogo.set_position(100, 100);
    allMessages.emplace_back(190, 40, CONTRIBUTOR_FONT_SIZE, "creditCartom1",
                             "GillSans.ttc", BLACK, Align::LEFT);
    allMessages.emplace_back(190, 40 + CONTRIBUTOR_TEXT_SIZE + 10,
                             CONTRIBUTOR_FONT_SIZE, "creditCartom2",
                             "GillSans.ttc", BLACK, Align::LEFT);
    allMessages.emplace_back(190, 40 + 2 * (CONTRIBUTOR_TEXT_SIZE + 10),
                             CONTRIBUTOR_FONT_SIZE, "creditCartom3",
                             "GillSans.ttc", BLACK, Align::LEFT);
    TextButton cartomWS(190, 40 + 3 * (CONTRIBUTOR_TEXT_SIZE + 10),
                        CONTRIBUTOR_FONT_SIZE, "creditCartomWeb",
                        "GillSans.ttc", BLACK, BLACK_LIT, BLACK, Align::LEFT);

    AnimatedSprite batardeLogo(100, 150);
    batardeLogo.set_texture("logoBatarde.png");
    batardeLogo.set_position(WINDOW_WIDTH - 130, 60 + 190);
    allMessages.emplace_back(WINDOW_WIDTH - 220, 50 + 150,
                             CONTRIBUTOR_FONT_SIZE, "creditBatarde",
                             "GillSans.ttc", BLACK, Align::RIGHT);
    TextButton batardeWS(WINDOW_WIDTH - 220,
                         50 + 150 + CONTRIBUTOR_TEXT_SIZE + 10,
                         CONTRIBUTOR_FONT_SIZE, "creditBatardeWeb",
                         "GillSans.ttc", BLACK, BLACK_LIT, BLACK, Align::RIGHT);

    // Contributors
    allMessages.emplace_back(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 + 20,
                             CREDIT_FONT_SIZE, "creditTeamMember",
                             "GillSans.ttc", BLACK, Align::CENTER,
                             sf::Text::Bold);
    for (int i = 0; i < 18; i++) { // 18 members of the project :)
        allMessages.emplace_back(WINDOW_WIDTH / 2 + float((i / 6 - 1) * 250),
                                 WINDOW_HEIGHT / 2 + 50 + float((i % 6) * 30),
                                 CONTRIBUTOR_FONT_SIZE,
                                 "creditName" + std::to_string(i),
                                 "GillSans.ttc", BLACK, Align::CENTER);
    }

    // Special Thanks
    allMessages.emplace_back(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 - 10,
                             CONTRIBUTOR_FONT_SIZE, "creditSpecialThanks",
                             "GillSans.ttc", BLACK);

    sf::Event event;
    sf::Vector2i mousePos;

    while (app.isOpen()) {

        mousePos = sf::Mouse::getPosition(app);

        // #1 Event handling
        while (app.pollEvent(event)) {
            // check the type of the event...

            switch (event.type) {
            // window closed
            case sf::Event::Closed:
                app.close();
                break;
            // key pressed
            case sf::Event::KeyPressed: {
                if (event.key.code == sf::Keyboard::Escape)
                    return;
                break;
            }

            case sf::Event::MouseButtonPressed:
                if (event.mouseButton.button == sf::Mouse::Left) {
                    if (backButton.is_lit()) {
                        soundManager.play_sound("click.wav");
                        return;
                    } else if (cartomWS.is_lit()) {
                        soundManager.play_sound("click.wav");
                        open_url("http://graal.ens-lyon.fr/cartomensia/");
                    } else if (batardeWS.is_lit()) {
                        soundManager.play_sound("click.wav");
                        open_url("http://www.labatarde.com/");
                    }
                }
                break;

            // we don't process other types of events
            default:
                break;
            }
        }

        // #2 Various objects update
        theBackground.update(app);
        backButton.update(app, mousePos);
        cartomWS.update(app, mousePos);
        batardeWS.update(app, mousePos);

        cartomLogo.update(app);
        batardeLogo.update(app);

        for (auto &mess : allMessages) {
            mess.update(app);
        }
        

        // #3 Screen display
        app.display();
    }
}
