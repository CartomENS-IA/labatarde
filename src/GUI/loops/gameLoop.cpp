/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <iostream>
#include <thread>

#include "GUI.hpp"
#include "Arrays.hpp"
#include "Board.hpp"

using namespace std;

void GUI::init_game_loop(bool onlineGame, Difficulty const diff){
  shared_ptr<Core::KernelInterface> kernel;
  if (onlineGame) {
    // initialize a RemoteKernel

    /*
    kernel = make_shared<Network::RemoteKernel>();
    shared_ptr<Core::ParallelPlayer> gui_player =
        make_shared<Core::ParallelPlayer>();

    // TODO : probably a lot of other initialization
    run_game_loop(kernel, gui_player, onlineGame, diff);
    */

  } else {
    // game is offline, we initialize three IA of the correct level
    int nbDesc;
    switch (diff) {
    case Difficulty::BEGINNER:
        nbDesc = 50;
        break;
    case Difficulty::VERY_GOOD:
        nbDesc = 1000;
        break;
    case Difficulty::MASTER:
        nbDesc = 2000;
        break;
    case Difficulty::GOOD:
    default:
        nbDesc = 400;
        break;
    }

    shared_ptr<Core::ParallelPlayer> gui_player = make_shared<Core::ParallelPlayer>();
    shared_ptr<IA::IAPlayer> ia_playerWest = make_shared<IA::IAPlayer>(nbDesc);
    shared_ptr<IA::IAPlayer> ia_playerNorth = make_shared<IA::IAPlayer>(nbDesc);
    shared_ptr<IA::IAPlayer> ia_playerEast = make_shared<IA::IAPlayer>(nbDesc);
    kernel = make_shared<Core::Kernel>(gui_player, ia_playerWest,
                                        ia_playerNorth, ia_playerEast);
    run_game_loop(kernel, gui_player, onlineGame, diff);
  }
}

FourPlayerInfo GUI::retrieve_player_info(bool onlineGame, Difficulty const diff) {
  FourPlayerInfo output;
  if (onlineGame) {
    throw std::runtime_error("not implemented yet!");
  } else {
        unsigned int elo;
        std::array<std::string, 4> profile_pic;
        std::array<std::wstring,4> names;

        switch (diff) {
        case Difficulty::BEGINNER:
            elo = 1300;
            names= {TM.get_entry("you"), L"Matrix", L"SKYNET", L"WOPR"};
            profile_pic = {"vous.png", "IAprofile/matrix.png",
                           "IAprofile/skynet.png", "IAprofile/WOPR.png"};
        break;

        case Difficulty::VERY_GOOD:
            elo = 1900;
            names= {TM.get_entry("you"), L"Big Brother", L"Red Queen", L"H.A.L."};
            profile_pic = {"vous.png", "IAprofile/big-brother.png",
                           "IAprofile/red-queen.png", "IAprofile/HAL.png"};
        break;

        case Difficulty::MASTER:
            elo = 2300;
            names= {TM.get_entry("you"),L"Colossus", L"Alpha60", L"Glados"};
            profile_pic = {"vous.png", "IAprofile/colossus.png",
                           "IAprofile/alpha60.png", "IAprofile/glados.png"};
        break;

        case Difficulty::GOOD:
            elo = 1600;
            names= {TM.get_entry("you"), L"Deep Thought", L"MCP", L"Puppet Master"};
            profile_pic = {"vous.png", "IAprofile/deep-thought.png",
                           "IAprofile/MCP.png", "IAprofile/puppet-master.png"};
        break;

        default:
            break;
        }

        for (int i = 0 ; i<4 ; i++){
            output[i].guild_pic = "IAprofile/fibm.png";
            output[i].name = names[i];
            output[i].profile_pic = profile_pic[i];
            output[i].elo = elo;
        }
        output[0].elo = 0;
    }
    return output;
}

void GUI::run_game_loop(shared_ptr<Core::KernelInterface> kernel,
                        shared_ptr<Core::ParallelPlayer> gui_player,
                        bool onlineGame, Difficulty const diff) {

    srand((unsigned int)time(NULL));
    if (optionManager()->is_pirate())
        soundManager.play_music("sea.ogg");
    else
        soundManager.play_music("main_music.wav");

    thread thread_kernel(&Core::KernelInterface::run, kernel);

    FourPlayerInfo info = retrieve_player_info(onlineGame, diff);
    Board gameManager(&kernel->get_game_state(), gui_player, info);

    Background theBackground("background/main_bg.png");

    // Event handling
    sf::Event event;

    // Cursor
    sf::Vector2i mousePos;

    statManager.begin_game();

    try {

      while (app.isOpen()) {
          mousePos = sf::Mouse::getPosition(app);

          // #1 Event handling
          while (app.pollEvent(event)) {
              switch (event.type) {

              case sf::Event::Closed: {
                  thread_kernel.detach();
                  app.close();
                  return;
              }

              case sf::Event::KeyPressed: {
                  gameManager.handle_key_pressed(event.key);
                  break;
              }

              case sf::Event::MouseButtonPressed: {
                  if (event.mouseButton.button == sf::Mouse::Left) {
                      gameManager.handle_mouse_click(mousePos);
                  }
                  break;
              }

              default:
                  break;
              } // end switch (event)
          } // end while (event)

          gameManager.wait_action_kernel();

          // #2 Various objects update. Object in the foreground
          // should be updated last
          theBackground.update(app);
          gameManager.update(app, mousePos);

          // #3 Screen display
          app.display();
      } // end while

      thread_kernel.join();

  } catch (const EndOfGame &e) {
      thread_kernel.detach();
      if (optionManager()->is_pirate())
          soundManager.stop_music("sea.ogg");
      else
          soundManager.stop_music("main_music.wav");
      return;
  }
}
