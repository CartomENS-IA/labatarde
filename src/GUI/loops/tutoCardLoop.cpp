/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Card.hpp"
#include "GUI.hpp"
#include "TutoBubble.hpp"
#include "TutoKernel.hpp"

#include <thread>

void GUI::run_tutorial_card_loop() {

    TutoBubble tutoMessage(
        sf::Vector2f(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2), L"");
    auto tutoKernel = make_shared<TutoKernel>("tuto_cards", nullptr, nullptr,
                                              nullptr, nullptr);
    thread kernelThread(&TutoKernel::run, tutoKernel);

    float const HAND_ANGLE = 1.3f;

    std::array<Hand, 4> hand = {
        Hand(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 - 2 * Card::LENGTH,
            CardinalPoints::SOUTH, HAND_ANGLE),
        Hand(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 - Card::LENGTH,
            CardinalPoints::SOUTH, HAND_ANGLE),
        Hand(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2,
            CardinalPoints::SOUTH, HAND_ANGLE),
        Hand(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2 + Card::LENGTH,
            CardinalPoints::SOUTH, HAND_ANGLE)};

    for (int value = 0; value < 9; value++) {
        for (int suit = 0; suit < 4; suit++) {
            hand[suit].give_card(Core::Card(static_cast<Core::Suit>(suit + 1),
                                            static_cast<Core::Value>(value)));
        }
    }

    for (int suit = 0; suit < 4; suit++)
        hand[suit].replace_card(30);

    Background theBackground("background/main_bg_logo.png");
    sf::Event event;
    sf::Vector2i mousePos;

    bool label2_end = false;

    while (app.isOpen()) {

        mousePos = sf::Mouse::getPosition(app);

        // #1 Event handling
        while (app.pollEvent(event)) {
            // check the type of the event...
            switch (event.type) {
            // window closed
            case sf::Event::Closed:
                app.close();
                return;

            case sf::Event::KeyPressed: {
                if (event.key.code == sf::Keyboard::Escape) {
                    tutoKernel->end_tutorial();
                    kernelThread.detach();
                    return;
                } else if (event.key.code == sf::Keyboard::Space) {
                    if (tutoKernel->no_more_event()) {
                        tutoKernel->end_tutorial();
                        kernelThread.detach();
                        return;
                    }
                    tutoKernel->next_event();
                }
                break;
            }

            case sf::Event::MouseButtonPressed: {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    tutoKernel->next_event();
                    if (tutoKernel->no_more_event()) {
                        tutoKernel->end_tutorial();
                        kernelThread.detach();
                        return;
                    }
                }
                break;
            }

            default:
                break;
            }
        }

        if (tutoKernel->has_new_message()){
          tutoKernel->set_message_on_bubble(&tutoMessage);
        }
        if (tutoKernel->has_new_label()){
          std::string label = tutoKernel->get_last_label();
          if (label == "label2") {
              label2_end = true;

          } else if (label == "label1") {
              hand[0].set_playable_cards(std::vector<Core::Card>(
                  1, Core::Card(Core::Suit::Diamond, Core::Value::Cat)));
              hand[2].set_playable_cards(std::vector<Core::Card>(
                  1, Core::Card(Core::Suit::Diamond, Core::Value::Cat)));
              hand[3].set_playable_cards(std::vector<Core::Card>(
                  1, Core::Card(Core::Suit::Diamond, Core::Value::Cat)));

          } else if (label == "label3") {
              Core::Hand blinkCard;
              blinkCard.emplace_back(Core::Suit::Diamond, Core::Value::King);
              blinkCard.emplace_back(Core::Suit::Diamond, Core::Value::Queen);
              blinkCard.emplace_back(Core::Suit::Diamond,
                                     Core::Value::Knight);
              blinkCard.emplace_back(Core::Suit::Diamond, Core::Value::Knave);
              hand[1].set_playable_cards(blinkCard);

          } else if (label == "label4") {
              Core::Hand blinkCard;
              blinkCard.emplace_back(Core::Suit::Diamond,
                                     Core::Value::Entertainer);
              blinkCard.emplace_back(Core::Suit::Diamond,
                                     Core::Value::Musician);
              blinkCard.emplace_back(Core::Suit::Diamond,
                                     Core::Value::Juggler);
              hand[1].set_playable_cards(blinkCard);

          } else if (label == "label5") {
              Core::Hand blinkCard;
              blinkCard.emplace_back(Core::Suit::Diamond, Core::Value::Dog);
              blinkCard.emplace_back(Core::Suit::Diamond, Core::Value::Cat);
              hand[1].set_playable_cards(blinkCard);

          } else if (label == "label6") {
              hand[1].sort_hand(Core::Trump::Diamond);
              hand[1].set_playable_cards(Core::Hand());

          }
        }

        theBackground.update(app);

        for (int suit = 0; suit < 4; suit++)
            if (!label2_end || suit == 1)
                hand[suit].draw(app);
        tutoMessage.update(app);

        app.display();
    }
}
