#include "GUI.hpp"
#include "Graphics.hpp"
#include "EloChart.hpp"
#include "StatRectangles.hpp"
#include "StatArray.hpp"

enum class StatMode : int {
    MAIN = 0,
    PLACES = 1,
    ELO = 2,
    TRICKS = 3,
    BETS = 4
};

void GUI::run_stat_loop() {

    // WORK IN PROGRESS
    /*
    if (!optionManager()->is_online()){
        run_connect_stats();
    }
    */

    run_work_in_progress();
    return;

    if (true){ //(optionManager()->is_online()){
        TM.load("stats.xml");
        TM.load("elo.xml");

        // _______ common elements
        StatMode loopState = StatMode::MAIN;
        Background theBackground("background/menu.png");

        HybridButton back(BUTTON_SIZE/2 + 50.f*SCALE,
                          (WINDOW_HEIGHT - BUTTON_SIZE/2 - 50.f*SCALE),
                          BUTTON_SIZE, BUTTON_SIZE,
                          "buttons/back.png", L"", 0, "GillSans.ttc",
                          WHITE, WHITE, WHITE);
        back.set_hover_mode(HybridButton::HoverMode::SCALING);

        // Screen selector
        sf::Vector2f const butSelectorPosCenter(WINDOW_WIDTH/2+70.f*SCALE,
                                                WINDOW_HEIGHT-100.f*SCALE);
        sf::Vector2f const butOffset(220.f*SCALE, 0.f);
        sf::Vector2f const buttonSelectorSize(210.f*SCALE, 60.f*SCALE);
        unsigned int const selectorFontSize = unsigned(35.f*HALF_SCALE_TO_MAX);

        std::vector<ButPtr> selector_vect;

        HybridButton mainBut(butSelectorPosCenter-butOffset-butOffset,
                             buttonSelectorSize.x, buttonSelectorSize.y,
                             "buttons/blank_rect.png", "stat_main", selectorFontSize, "GillSans.ttc", GRAY_LIT, WHITE, WHITE,
                             Align::CENTER, sf::Text::Bold);
        mainBut.set_hover_mode(HybridButton::HoverMode::DARKEN);
        mainBut.set_text_on_button();
        selector_vect.push_back(make_unique<HybridButton>(mainBut));

        HybridButton placesBut(butSelectorPosCenter-butOffset,
                             buttonSelectorSize.x, buttonSelectorSize.y,
                             "buttons/blank_rect.png", "stat_places", selectorFontSize, "GillSans.ttc", GRAY_LIT, WHITE, WHITE,
                             Align::CENTER, sf::Text::Bold);
        placesBut.set_hover_mode(HybridButton::HoverMode::DARKEN);
        placesBut.set_text_on_button();
        selector_vect.push_back(make_unique<HybridButton>(placesBut));

        HybridButton eloBut(butSelectorPosCenter,
                             buttonSelectorSize.x, buttonSelectorSize.y,
                             "buttons/blank_rect.png", "stat_elo", selectorFontSize, "GillSans.ttc", GRAY_LIT, WHITE, WHITE,
                             Align::CENTER, sf::Text::Bold);
        eloBut.set_hover_mode(HybridButton::HoverMode::DARKEN);
        eloBut.set_text_on_button();
        selector_vect.push_back(make_unique<HybridButton>(eloBut));

        HybridButton trickBut(butSelectorPosCenter+butOffset,
                             buttonSelectorSize.x, buttonSelectorSize.y,
                             "buttons/blank_rect.png", "stat_trick", selectorFontSize, "GillSans.ttc", GRAY_LIT, WHITE, WHITE,
                             Align::CENTER, sf::Text::Bold);
        trickBut.set_hover_mode(HybridButton::HoverMode::DARKEN);
        trickBut.set_text_on_button();
        selector_vect.push_back(make_unique<HybridButton>(trickBut));

        HybridButton betsBut(butSelectorPosCenter+butOffset+butOffset,
                             buttonSelectorSize.x, buttonSelectorSize.y,
                             "buttons/blank_rect.png", "stat_bets", selectorFontSize, "GillSans.ttc", GRAY_LIT, WHITE, WHITE,
                             Align::CENTER, sf::Text::Bold);
        betsBut.set_hover_mode(HybridButton::HoverMode::DARKEN);
        betsBut.set_text_on_button();
        selector_vect.push_back(make_unique<HybridButton>(betsBut));

        ArraySelector selector(selector_vect);

        // _______ main screen
        MainStatRectangle mainRect;

        // _______ elo screen
        EloStatRectangle eloRect;
        EloChart chart;
        HybridButton infoElo(BUTTON_SIZE/2 + 50.f*SCALE,
                             BUTTON_SIZE/2 + 50.f*SCALE,
                             BUTTON_SIZE, BUTTON_SIZE,
                            "buttons/infoElo.png", L"", 0,
                            "GillSans.ttc", WHITE, WHITE, WHITE);

        // _______ place screen
        PlaceStatRectangle placeRect;

        // _______ trick screen
        TrickStatArray trickArray;
        ChouneStatArray chouneArray;

        // _______ bet screen
        BetStatArray betArray;
        std::vector<AnimatedSprite> trickTrump;
        int i = 0;
        for (auto& key : {"tokens/toutat-noir.png", "tokens/spade.png", "tokens/heart.png", "tokens/diamond.png", "tokens/club.png", "tokens/sansat-noir.png"}) {
            trickTrump.emplace_back(100.f*SCALE_TO_MAX, 100.f*SCALE_TO_MAX);
            trickTrump[i].set_texture(key);
            trickTrump[i].set_position(697.f*SCALE_TO_MAX + float(i)*190.f*SCALE_TO_MAX, 160.f*SCALE_TO_MAX);
            i+=1;
        }

        bool clicked;
        sf::Event event;
        sf::Vector2i mousePos;

        while (app.isOpen()) {

            clicked = false;
            mousePos = sf::Mouse::getPosition(app);

            // #1 Event handling
            while (app.pollEvent(event)) {
                switch (event.type) {

                    case sf::Event::Closed:
                        app.close();
                        break;

                    case sf::Event::KeyPressed: {
                        if (event.key.code == sf::Keyboard::Escape)
                            return;
                        break;
                    }

                    case sf::Event::MouseButtonPressed:{
                        if (event.mouseButton.button == sf::Mouse::Left) {
                            clicked=true;
                            if (back.is_lit()){
                                return;
                            } else if (infoElo.is_lit()){
                                open_url("http://www.labatarde.com/");
                            }
                        }
                    }

                    default:
                        break;
                } // end switch
            } // end while event

            theBackground.update(app);

            switch (loopState) {
                case StatMode::PLACES :
                    placeRect.update(app);
                break;

                case StatMode::ELO :
                    eloRect.update(app);
                    infoElo.update(app, mousePos);
                    chart.update(app);
                break;

                case StatMode::TRICKS :
                    trickArray.update(app);
                    chouneArray.update(app);
                break;

                case StatMode::BETS :
                    betArray.update(app);
                    for (auto &sprite : trickTrump){
                        sprite.update(app);
                    }
                break;

                case StatMode::MAIN :
                default:
                    mainRect.update(app);
                    m_title.update(app);
                break;
            }
            selector.update(app,mousePos,clicked);

            loopState = static_cast<StatMode>(selector.who_is_selected());
            back.update(app,mousePos);

            app.display();
        }
    }
}
