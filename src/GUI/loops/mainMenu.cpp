#include "GUI.hpp"

void GUI::run_main_menu() {

    // load text
    TM.load("main.xml");

    // Background
    Background theBackground("background/menu.png");

    sf::Vector2f const centralButtonPos(WINDOW_WIDTH /2, WINDOW_HEIGHT/2);
    sf::Vector2f const buttonOffset(BETWEEN_BUTTONS, 0);

    HybridButton startGame(centralButtonPos - 2.f*buttonOffset,
                           BUTTON_SIZE, BUTTON_SIZE, "buttons/play.png",
                           "play", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                           BLACK, BLACK_LIT, BLACK);

    HybridButton tuto(centralButtonPos - buttonOffset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/tuto.png",
                      "tuto", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);

    HybridButton results(centralButtonPos,
                         BUTTON_SIZE, BUTTON_SIZE, "buttons/result.png",
                         "stats", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                         BLACK, BLACK_LIT, BLACK);

    HybridButton options(centralButtonPos + buttonOffset,
                         BUTTON_SIZE, BUTTON_SIZE, "buttons/options.png",
                         "option", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                         BLACK, BLACK_LIT, BLACK);

    HybridButton exitGame(centralButtonPos + 2.f*buttonOffset,
                         BUTTON_SIZE, BUTTON_SIZE, "buttons/quit.png",
                         "exit", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                         BLACK, BLACK_LIT, BLACK);

    TextButton credits(WINDOW_WIDTH-10, WINDOW_HEIGHT-25, 20, "credit", "GillSans.ttc",
                       BLACK, BLACK_LIT, BLACK, Align::RIGHT);

    // Language selection
    // WORK IN PROGRESS
    /*
    vector<ButPtr> argLanguage;
    float const FLAG_SIZE_X = 60.f*SCALE;
    float const FLAG_SIZE_Y = 40.f*SCALE;
    float flagCount=-3;
    float const FLAG_POS_X = WINDOW_WIDTH/2.f;
    float const FLAG_POS_Y = WINDOW_HEIGHT*4.f/5.f;
    float const flagOffset = 100.f*SCALE;
    argLanguage.push_back(make_unique<GraphicButton>(FLAG_POS_X + (0.5f+(flagCount++)) * flagOffset, FLAG_POS_Y,
                            "flags/gb_bw.png", "flags/gb.png",
                            "flags/gb.png", FLAG_SIZE_X, FLAG_SIZE_Y));
    argLanguage.push_back(make_unique<GraphicButton>(FLAG_POS_X + (0.5f+(flagCount++)) * flagOffset, FLAG_POS_Y,
                            "flags/fr_bw.png", "flags/fr.png",
                            "flags/fr.png", FLAG_SIZE_X, FLAG_SIZE_Y));
    argLanguage.push_back(make_unique<GraphicButton>(FLAG_POS_X + (0.5f+(flagCount++)) * flagOffset, FLAG_POS_Y,
                            "flags/ger_bw.png", "flags/ger.png",
                            "flags/ger.png", FLAG_SIZE_X, FLAG_SIZE_Y));
    argLanguage.push_back(make_unique<GraphicButton>(FLAG_POS_X + (0.5f+(flagCount++)) * flagOffset, FLAG_POS_Y,
                            "flags/esp_bw.png", "flags/esp.png",
                            "flags/esp.png", FLAG_SIZE_X, FLAG_SIZE_Y));
    argLanguage.push_back(make_unique<GraphicButton>(FLAG_POS_X + (0.5f+(flagCount++)) * flagOffset, FLAG_POS_Y,
                            "flags/por_bw.png", "flags/por.png",
                            "flags/por.png", FLAG_SIZE_X, FLAG_SIZE_Y));
    argLanguage.push_back(make_unique<GraphicButton>(FLAG_POS_X + (0.5f+(flagCount++)) * flagOffset, FLAG_POS_Y,
                              "flags/ita_bw.png", "flags/ita.png",
                              "flags/ita.png", FLAG_SIZE_X, FLAG_SIZE_Y));

    int whichLanguage = optionManager() ->get_language_id();
    ArraySelector chooseLanguage(argLanguage, whichLanguage);
    chooseLanguage.change_selected(whichLanguage);
    */

    sf::Event event;
    sf::Vector2i mousePos;
    bool clicked;
    sf::Clock clk;

    while (app.isOpen()) {

        clicked = false;
        mousePos = sf::Mouse::getPosition(app);

        // #1 Event handling
        while (app.pollEvent(event)) {
            // check the type of the event...
            switch (event.type) {
                case sf::Event::Closed:
                    app.close();
                    break;
                case sf::Event::KeyPressed: {
                    if (event.key.code == sf::Keyboard::Escape)
                        app.close();
                    break;
                }

                case sf::Event::MouseButtonPressed:{
                    if (event.mouseButton.button == sf::Mouse::Left) {
                        clicked=true;
                        if (startGame.is_lit()){
                            run_IA_difficulty_selection();
                            // WORK IN PROGRESS
                            // run_mode_selection_loop();
                        } else if (exitGame.is_lit()) {
                            app.close();
                            return;
                        } else if (tuto.is_lit()) {
                            run_tutorial_menu();
                        } else if (options.is_lit()) {
                            run_options_loop();
                        } else if (results.is_lit()) {
                            run_stat_loop();
                        } else if (credits.is_lit()) {
                            run_credits_loop();
                        }
                    } // end if
                } // end case

                default:
                    break;
            }
        }

        if (m_splashActive) {
            m_splashScreen.fade(0, FADE_SPEED);
            m_splashActive = false;
        }

        // #2 Various objects update
        theBackground.update(app);
        m_title.update(app);
        startGame.update(app,mousePos);
        exitGame.update(app,mousePos);
        tuto.update(app,mousePos);
        results.update(app,mousePos);
        options.update(app,mousePos);
        credits.update(app,mousePos);

        /*
        chooseLanguage.update(app, mousePos, clicked);
        if(chooseLanguage.who_is_selected() != whichLanguage) {
            Language l = id_to_language(chooseLanguage.who_is_selected());
            optionManager()->set_language(l);
            return; // get back to main loop -> main loop will relaunch the menu
        }
        */

        m_splashScreen.update(app);

        // #3 Screen display
        app.display();
    }
}
