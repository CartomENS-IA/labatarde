/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Constants.hpp"
#include "GUI.hpp"
#include "SoundManager.hpp"
#include "LoginInterface.hpp"

using namespace std;

void GUI::choose_functionnality(Function const f) {
    switch (f) {
    case Function::NONE:
        break;
    case Function::AUTHENTICATE:
        break;
    case Function::OPTIONS_LOOP:
        run_options_loop();
        break;
    case Function::CREDITS:
        run_credits_loop();
        break;
    case Function::STATS:
        run_stat_loop();
        break;
    case Function::MODE_SELECTION:
        run_mode_selection_loop();
        break;
    case Function::TUTORIAL_SELECTION:
        run_tutorial_menu();
        break;
    case Function::DIFFICULTY_SELECTION:
        run_IA_difficulty_selection();
        break;
    case Function::INIT_IA_LVL1:
        init_game_loop(false, Difficulty::BEGINNER);
        break;
    case Function::INIT_IA_LVL2:
        init_game_loop(false, Difficulty::GOOD);
        break;
    case Function::INIT_IA_LVL3:
        init_game_loop(false, Difficulty::VERY_GOOD);
        break;
    case Function::INIT_IA_LVL4:
        init_game_loop(false, Difficulty::MASTER);
        break;
    case Function::INIT_ONLINE:
        init_game_loop(true);
        break;
    case Function::TUTORIAL_CARDS_LOOP:
        run_tutorial_card_loop();
        break;
    case Function::TUTORIAL_TRICKS_LOOP:
        run_tutorial_loop("tuto_trick");
        break;
    case Function::TUTORIAL_BET_LOOP:
        run_tutorial_loop("tuto_bet");
        break;
    case Function::TUTORIAL_FLOW_LOOP:
        run_tutorial_loop("tuto_flow");
        break;
    case Function::CREATE_ACCOUNT:
        open_url("http://www.labatarde.com/");
        break;
    case Function::LOGIN_GAME:
        run_connect_game();
        break;
    case Function::LOGIN_STAT:
        run_connect_stats();
        break;
    default:
        break;
    }
}

void GUI::run_loop(vector<MessPtr> &messages,
                   vector<ButPtr> &buttons,
                   vector<Function> &functionnalities,
                   bool display_title,
                   LoginInfo * logInfo)
{
    // Background
    Background theBackground("background/menu.png");

    // Event handling
    sf::Event event;

    // Cursor
    sf::Vector2i mousePos;

    while (app.isOpen()) {
        mousePos = sf::Mouse::getPosition(app);

        // #1 Event handling
        while (app.pollEvent(event)) {

            if (logInfo!=0){
                if (logInfo->login->get_proxy_state() == LoginState::WAITING
                 || logInfo->login->get_proxy_state() == LoginState::LOGIN_FAIL) {
                    logInfo->login->process_GUI_event(event);
                }
            }

            switch (event.type) {

            case sf::Event::Closed:
                app.close();
            break;

            case sf::Event::KeyPressed:
                if (event.key.code == sf::Keyboard::Escape){
                    return;
                } else if (logInfo!= 0 and event.key.code == sf::Keyboard::Tab) {
                    logInfo->login->toggle_focus();
                } else if (logInfo!= 0 and event.key.code == sf::Keyboard::Return) {
                    logInfo->login->authenticate();
                }
            break;

            case sf::Event::MouseButtonPressed:
                if (event.mouseButton.button == sf::Mouse::Left) {
                    for (unsigned int i = 0; i < buttons.size(); i++) {
                        if (buttons[i]->is_lit()) {
                            soundManager.play_sound("click.wav");
                            if (functionnalities[i] == Function::BACK)
                                return;
                            else if (functionnalities[i] == Function::QUIT){
                                app.close();
                                return;
                            }
                            else if (logInfo!=0  and
                                functionnalities[i] ==  Function::AUTHENTICATE){
                                logInfo->login->authenticate();
                            }
                            else if (functionnalities[i] == Function::DISCONNECT){
                                optionManager()->set_online(false);
                                return;
                            }
                            choose_functionnality(functionnalities[i]);
                        }
                    }
                }
                break;

            // we don't process other types of events
            default:
                break;
            }
        }

        // #2 Various objects update
        theBackground.update(app);

        if (display_title) m_title.update(app);

        if (logInfo!=0){
            logInfo->login->update(app);
            if (logInfo->login->process_Proxy_event()){
                choose_functionnality(logInfo->successAction);
                return;
            }
        }

        for (auto &m : messages)
            m->update(app);

        for (auto &b : buttons)
            b->update(app, mousePos);

        // #3 Screen display
        app.display();
    }
}
