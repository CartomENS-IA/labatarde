/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Constants.hpp"
#include "GUI.hpp"
#include "SoundManager.hpp"
#include "TextManager.hpp"
#include <algorithm>

using namespace std;

/** \file
*   \brief Definition of some loops that have been factored using the runLoop engine.
**/

void GUI::run_tutorial_menu() {

    vector<MessPtr> mess;
    vector<ButPtr> buttons;
    vector<Function> func;

    sf::Vector2f const centralButtonPos(WINDOW_WIDTH /2, WINDOW_HEIGHT/2);
    sf::Vector2f const buttonOffset(BETWEEN_BUTTONS, 0);

    mess.push_back(make_unique<MultiLineMessage>(
        WINDOW_WIDTH/2, WINDOW_HEIGHT*3.f/4.f, 30,
        "tutoIntro", ';', "GillSans.ttc", BLACK));

    HybridButton back(centralButtonPos - 2.f*buttonOffset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/back.png",
                      "back", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(back));
    func.push_back(Function::BACK);

    HybridButton cards(centralButtonPos - 1.f*buttonOffset,
                           BUTTON_SIZE, BUTTON_SIZE, "buttons/1.png",
                           "tuto_cards", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                           BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(cards));
    func.push_back(Function::TUTORIAL_CARDS_LOOP);

    HybridButton tricks(centralButtonPos,
                        BUTTON_SIZE, BUTTON_SIZE, "buttons/2.png",
                        "tuto_trick", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                        BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(tricks));
    func.push_back(Function::TUTORIAL_TRICKS_LOOP);

    HybridButton bets(centralButtonPos + 1.f*buttonOffset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/3.png",
                      "tuto_bet", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(bets));
    func.push_back(Function::TUTORIAL_BET_LOOP);

    HybridButton flow(centralButtonPos + 2.f*buttonOffset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/4.png",
                      "tuto_flow", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(flow));
    func.push_back(Function::TUTORIAL_FLOW_LOOP);

    run_loop(mess, buttons, func);
}

void GUI::run_connect_stats(){

    vector<MessPtr> mess;
    vector<ButPtr> buttons;
    vector<Function> func;


    sf::Vector2f const centralPos(WINDOW_WIDTH /2, WINDOW_HEIGHT/2);
    sf::Vector2f const offset(2*BETWEEN_BUTTONS, 0);

    HybridButton back(centralPos - offset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/back.png",
                      "back", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(back));
    func.push_back(Function::BACK);

    HybridButton stat(centralPos + offset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/result.png",
                      "stats", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(stat));
    func.push_back(Function::AUTHENTICATE);

    LoginInterface interf(centralPos, m_serverLink);
    LoginInfo info = {&interf, Function::STATS};

    run_loop(mess, buttons, func, true, &info);
}

void GUI::run_connect_game(){

    vector<MessPtr> mess;
    vector<ButPtr> buttons;
    vector<Function> func;

    sf::Vector2f const centralPos(WINDOW_WIDTH /2, WINDOW_HEIGHT/2);
    sf::Vector2f const offset(2*BETWEEN_BUTTONS, 0);

    HybridButton back(centralPos - offset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/back.png",
                      "back", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(back));
    func.push_back(Function::BACK);

    HybridButton play(centralPos + offset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/play.png",
                      "play", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(play));
    func.push_back(Function::AUTHENTICATE);

    LoginInterface interf(centralPos, m_serverLink);
    LoginInfo info = {&interf, Function::NONE};

    run_loop(mess, buttons, func, true, &info);

}

void GUI::run_mode_selection_loop(){

    vector<MessPtr> mess;
    vector<ButPtr> buttons;
    vector<Function> func;

    sf::Vector2f const centralButtonPos(WINDOW_WIDTH*0.5f, WINDOW_HEIGHT*0.55f);
    sf::Vector2f const buttonOffset(300.f*SCALE, 0);

    if (optionManager()->is_online()){
        Message playerName(WINDOW_WIDTH/2.f, WINDOW_HEIGHT*0.4f,
                  unsigned(40.f*HALF_SCALE), L"PlayerName TODO", "GillSans.ttc",
                  BLACK_LIT, Align::CENTER, sf::Text::Bold);
        mess.push_back(make_unique<Message>(playerName));
    }


    HybridButton back(centralButtonPos - 1.5f*buttonOffset,
                    BUTTON_SIZE, BUTTON_SIZE, "buttons/back.png",
                    "back", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                    BLACK, BLACK_LIT, BLACK);
    back.set_text_under_button();
    buttons.push_back(make_unique<HybridButton>(back));
    func.push_back(Function::BACK);

    HybridButton offline(centralButtonPos - 0.5f*buttonOffset,
                        BUTTON_SIZE, BUTTON_SIZE,
                        "buttons/play.png","single", MENU_BUTTON_FONT_SIZE,
                        "GillSans.ttc", BLACK, BLACK_LIT, BLACK);
    offline.set_text_under_button();
    buttons.push_back(make_unique<HybridButton>(offline));
    func.push_back(Function::DIFFICULTY_SELECTION);

    HybridButton createAccount(centralButtonPos + 0.5f*buttonOffset,
                   BUTTON_SIZE, BUTTON_SIZE,
                   "buttons/bonhomme.png", "createAccount", MENU_BUTTON_FONT_SIZE,
                   "GillSans.ttc", BLACK, BLACK_LIT, BLACK);
    createAccount.set_text_under_button();
    buttons.push_back(make_unique<HybridButton>(createAccount));
    func.push_back(Function::CREATE_ACCOUNT);

    HybridButton play(centralButtonPos + 1.5f*buttonOffset,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/play.png",
                      "multi", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(play));
    func.push_back(Function::LOGIN_GAME);

    run_loop(mess, buttons, func);
}


void GUI::run_IA_difficulty_selection() {
  // _______ Message vector _______
  vector<MessPtr> mess;
  vector<ButPtr> buttons;
  vector<Function> func;

  /*
  mess.push_back(make_unique<Message>(
                  WINDOW_WIDTH / 2, WINDOW_HEIGHT*0.4f,
                  unsigned(30.f*HALF_SCALE), "difficulty",
                  "GillSans.ttc", BLACK));
  */
  float const CARD_ZOOM = 1.2f;
  float const CX = 152.f*SCALE_TO_MAX*CARD_ZOOM;
  float const CY = 304.f*SCALE_TO_MAX*CARD_ZOOM;
  float const TEXT_OFFSET = -30.f*SCALE;

  sf::Vector2f const centralButtonPos(WINDOW_WIDTH /2, WINDOW_HEIGHT/2);
  sf::Vector2f const buttonOffset(250.f*SCALE, 0);

  HybridButton d1(centralButtonPos - 1.5f*buttonOffset,
                     CX, CY, "IAprofile/fiche-joueur-IA-matrix.png",
                     "difficulty1", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
  d1.move_text(TEXT_OFFSET);
  buttons.push_back(make_unique<HybridButton>(d1));
  func.push_back(Function::INIT_IA_LVL1);

  HybridButton d2(centralButtonPos - 0.5f*buttonOffset,
                      CX, CY, "IAprofile/fiche-joueur-IA-deep-thought.png",
                      "difficulty2", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
  d2.move_text(TEXT_OFFSET);
  buttons.push_back(make_unique<HybridButton>(d2));
  func.push_back(Function::INIT_IA_LVL2);

  HybridButton d3(centralButtonPos + 0.5f*buttonOffset,
                    CX, CY, "IAprofile/fiche-joueur-IA-HAL.png",
                    "difficulty3", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                    BLACK, BLACK_LIT, BLACK);
  d3.move_text(TEXT_OFFSET);
  buttons.push_back(make_unique<HybridButton>(d3));
  func.push_back(Function::INIT_IA_LVL3);

  HybridButton d4(centralButtonPos + 1.5f*buttonOffset,
                    CX, CY, "IAprofile/fiche-joueur-IA-glados.png",
                    "difficulty4", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                    BLACK, BLACK_LIT, BLACK);
  d4.move_text(TEXT_OFFSET);
  buttons.push_back(make_unique<HybridButton>(d4));
  func.push_back(Function::INIT_IA_LVL4);

  // _______ Button to return to previous menu  _______
  HybridButton back(centralButtonPos.x, WINDOW_HEIGHT-120.f*SCALE,
                    90.f*SCALE, 90.f*SCALE, "buttons/back.png",
                    "back", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                    BLACK, BLACK_LIT, BLACK);
  buttons.push_back(make_unique<HybridButton>(back));
  func.push_back(Function::BACK);

  run_loop(mess,buttons,func, true);
}

void GUI::run_work_in_progress() {

    vector<MessPtr> mess;
    vector<ButPtr> buttons;
    vector<Function> func;

    mess.push_back(make_unique<MultiLineMessage>(
        WINDOW_WIDTH/2, WINDOW_HEIGHT/2.f, 70u,
        "WIP", ';', "GillSans.ttc", BLACK));

    HybridButton back(WINDOW_WIDTH/2, WINDOW_HEIGHT*0.75f,
                      BUTTON_SIZE, BUTTON_SIZE, "buttons/back.png",
                      "back", MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                      BLACK, BLACK_LIT, BLACK);
    buttons.push_back(make_unique<HybridButton>(back));
    func.push_back(Function::BACK);

    run_loop(mess, buttons, func);
}
