/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GUI.hpp"
#include "Constants.hpp"
#include "LoginInterface.hpp"
#include "Graphics.hpp"
#include <iostream>

using namespace std;

void GUI::run_test() {

    Background theBackground("background/main_bg.png");
    TM.load("main.xml");

    LoginInterface interf(WINDOW_WIDTH/2, WINDOW_HEIGHT/2, m_serverLink);

    // Event handling
    sf::Event event;

    // Cursor
    sf::Vector2i mousePos;
    bool clicked;

    while (app.isOpen()) {

        mousePos = sf::Mouse::getPosition(app);
        clicked = false;

        // #1 Event handling
        while (app.pollEvent(event)) {

            if (interf.get_proxy_state() == LoginState::WAITING
             || interf.get_proxy_state() == LoginState::LOGIN_FAIL) {
                interf.process_GUI_event(event);
            }

            switch (event.type) {
                case sf::Event::Closed: {
                    app.close();
                    break;
                }

                case sf::Event::KeyPressed: {
                    if (event.key.code == sf::Keyboard::Escape){
                        app.close();
                        break;
                    } else if (event.key.code == sf::Keyboard::Tab) {
                        interf.toggle_focus();
                    } else if (event.key.code == sf::Keyboard::Return) {
                        interf.authenticate();
                    }
                break;
                }

                case sf::Event::MouseButtonPressed:
                    if (event.mouseButton.button == sf::Mouse::Left) {
                        clicked = true;

                    }
                    break;

                // we don't process other types of events
                default:
                    break;
            }
        }

        // #2 Various objects update
        theBackground.update(app);
        interf.update(app);


        if (interf.process_Proxy_event()) return;

        // #3 Screen display
        app.display();
    }
}
