/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GUI.hpp"
#include <exception>
#include <iostream>
#include <thread>

using namespace std;

GUI::GUI()
:
    m_splashActive(false),
    m_title(340*SCALE,89*SCALE)
{
    m_serverLink = make_shared<Network::ClientMessenger>(
                     optionManager()->get_default_server_IP(),
                     optionManager()->get_default_server_port());

	// Initializing the Window
	sf::VideoMode videoMode = optionManager()->get_window_settings();
    sf::ContextSettings settings = optionManager()->get_context_settings();

	if (optionManager()->is_full_screen())
		app.create(videoMode, L"La Bâtarde",
            sf::Style::Fullscreen, settings);
	else
		app.create(videoMode, L"La Bâtarde",
            sf::Style::Close | sf::Style::Titlebar, settings);
    app.setFramerateLimit(60);

	// icon of the program
    sf::Image icon;
    if(!icon.loadFromFile("res/img/icon.png")) {
        cout << "[GUI.cpp] ERROR : App's icon couldn't be loaded !\n";
        throw(std::runtime_error("App's icon couldn't be loaded !"));
    }
    app.setIcon(64, 64, icon.getPixelsPtr());

    // Splash screen
    m_splashScreen.set_texture("background/splash_screen.png");
    m_splashScreen.set_transparency(255);

    // Title
    m_title.set_texture("title.png");
    m_title.set_position(WINDOW_WIDTH/2, WINDOW_HEIGHT/5);
}

void GUI::run() {
    sf::Clock clk;
    sf::Event event;

    while (app.isOpen()) {
        m_splashScreen.update(app);

        while (app.pollEvent(event)) {
            switch (event.type) {
                case sf::Event::Closed:
                    app.close();
                    break;

                default:
                    break;
            }
        }

        if (not m_splashActive) {
            m_splashScreen.fade(255, FADE_SPEED);
            m_splashActive = true;
        }

        if (clk.getElapsedTime() >
            sf::seconds(SPLASH_TIME + 1 / FADE_SPEED)) {
            run_main_menu();
        }
        app.display();
    }
}


void open_url(std::string const &url){
  #ifdef _WIN32
        ShellExecute(0, 0, url, 0, 0, SW_SHOW);
  #elif __unix__
        std::string command = "sensible-browser " + url;
        system(command.c_str());
  #endif
}
