/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GUI.hpp"
#include "Constants.hpp"
#include "OptionManager.hpp"

using namespace std;

void GUI::run_options_loop() {

    TM.load("options.xml");
    bool isGonnaStop = false;
    sf::Clock clk;

    //________ Option menu Constants ________
    float const RESOLUTION_X = WINDOW_WIDTH*0.15f;
    float const CARD_CHOICE_X = WINDOW_WIDTH*0.4f;
    float const SOUND_X = WINDOW_WIDTH*0.65f;
    float const RETURN_BUTTON_X = WINDOW_WIDTH*0.85f;
    float const HEADER_Y = WINDOW_HEIGHT*0.35f;

    unsigned int OPTION_FONT_TITLE_SIZE = unsigned(30.f*HALF_SCALE);
    unsigned int OPTION_FONT_SIZE = unsigned(26.f*HALF_SCALE);
    unsigned int OPTION_FONT_WARNING_SIZE = unsigned(14.f*HALF_SCALE);

    Background theBackground("background/menu.png");
    vector<MessPtr> messageVector;

    // _______ Button to return to main menu _______
    messageVector.push_back(make_unique<Message>(RETURN_BUTTON_X, HEADER_Y,
                            OPTION_FONT_TITLE_SIZE, "back", "GillSans.ttc",
                            BLACK_LIT, Align::CENTER, sf::Text::Bold));

    HybridButton backButton(RETURN_BUTTON_X, HEADER_Y+100.f*SCALE,
                            90.f*SCALE, 90.f*SCALE,
                            "buttons/back.png", L"", MENU_BUTTON_FONT_SIZE,
                            "GillSans.ttc",WHITE,WHITE,WHITE);

    // _______ Pirate easter egg button ___________
    GraphicButton secretButton(40.f*SCALE, WINDOW_HEIGHT - 60.f*SCALE,
                               "secret.png", "secretlit.png", "secretlit.png",
                               32, 32, false);

    // _____ Resolution Option ______
    messageVector.push_back(make_unique<Message>(RESOLUTION_X, HEADER_Y,
                               OPTION_FONT_TITLE_SIZE, "resolutionOption",
                               "GillSans.ttc", BLACK_LIT, Align::CENTER,
                               sf::Text::Bold));
    messageVector.push_back(make_unique<MultiLineMessage>(RESOLUTION_X,
                              HEADER_Y + 30.f*SCALE,
                              OPTION_FONT_WARNING_SIZE, "optionWarning", ';',
                              "GillSans.ttc", BLACK_LIT));
    float nButtons=1.f;
    float const RESOL_BUT_OFFSET = 80.f*SCALE;
    vector<unique_ptr<Button>> argResolution;
    argResolution.push_back(make_unique<MultiTextButton>(RESOLUTION_X,
                            HEADER_Y + (nButtons++)*RESOL_BUT_OFFSET,
                            OPTION_FONT_SIZE, "resolutionSmall", ';',
                            "GillSans.ttc", GRAY, BLACK_LIT, BLACK));
    argResolution.push_back(make_unique<MultiTextButton>(RESOLUTION_X,
                            HEADER_Y + (nButtons++)*RESOL_BUT_OFFSET,
                            OPTION_FONT_SIZE, "resolutionMedium", ';',
                            "GillSans.ttc", GRAY, BLACK_LIT, BLACK));
    argResolution.push_back(make_unique<MultiTextButton>(RESOLUTION_X,
                            HEADER_Y + (nButtons++)*RESOL_BUT_OFFSET,
                            OPTION_FONT_SIZE, "resolutionLarge", ';',
                            "GillSans.ttc", GRAY, BLACK_LIT, BLACK));
    argResolution.push_back(make_unique<TextButton>(RESOLUTION_X,
                            HEADER_Y + (nButtons++)*RESOL_BUT_OFFSET,
                            OPTION_FONT_SIZE, "fullscreen",
                            "GillSans.ttc", GRAY, BLACK_LIT, BLACK));
    ArraySelector chooseResolution(argResolution);
    if (optionManager()->is_full_screen()){
        chooseResolution.change_selected(3);
    } else {
        switch ((int)optionManager()->get_screen_width()) {
            case 1600:
                chooseResolution.change_selected(1);
                break;
            case 1920:
                chooseResolution.change_selected(2);
                break;
            case 1280:
            default:
                chooseResolution.change_selected(0);
                break;
        }
    }

    // ________ Card choice option _________
    messageVector.push_back(make_unique<Message>(CARD_CHOICE_X, HEADER_Y,
        OPTION_FONT_TITLE_SIZE,"cardChoice", "GillSans.ttc", BLACK_LIT,
        Align::CENTER, sf::Text::Bold));

    float const CARD_SIZE = 120.f*SCALE;
    float const CARD_RATIO = 1.53f;
    HybridButton specialCard(CARD_CHOICE_X+80.f*SCALE, HEADER_Y+130.f*SCALE,
                            CARD_SIZE, CARD_SIZE*CARD_RATIO,
                            "cards/special/roi-coeur.png", "special",
                            MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                            GRAY,BLACK_LIT,BLACK);
    specialCard.set_hover_mode(HybridButton::HoverMode::DARKEN);
    specialCard.set_text_under_button();

    HybridButton classicalCard(CARD_CHOICE_X-80.f*SCALE, HEADER_Y+130.f*SCALE,
                                CARD_SIZE, CARD_SIZE*CARD_RATIO,
                                "cards/classical/roi-coeur.png", "classical",
                                MENU_BUTTON_FONT_SIZE, "GillSans.ttc",
                                GRAY,BLACK_LIT, BLACK);
    classicalCard.set_hover_mode(HybridButton::HoverMode::DARKEN);
    classicalCard.set_text_under_button();

    std::vector<ButPtr> cardTypes;
    cardTypes.push_back(make_unique<HybridButton>(specialCard));
    cardTypes.push_back(make_unique<HybridButton>(classicalCard));
    ArraySelector chooseCard(cardTypes);
    switch (optionManager()->get_card_type()){
        case CardType::SPECIAL:
            chooseCard.change_selected(0);
            break;
        case CardType::CLASSICAL:
        default:
            chooseCard.change_selected(1);
    }

    // ________ Sound Volume Option ________
    messageVector.push_back(make_unique<Message>(SOUND_X, HEADER_Y,
        OPTION_FONT_TITLE_SIZE,"sound", "GillSans.ttc",
        BLACK_LIT, Align::CENTER, sf::Text::Bold));

    messageVector.push_back(make_unique<Message>(SOUND_X, HEADER_Y+50.f*SCALE,
        OPTION_FONT_SIZE,"soundOption", "GillSans.ttc",
        BLACK, Align::CENTER, sf::Text::Bold));

    OnOffSelector chooseSound(SOUND_X, HEADER_Y+80.f*SCALE, OPTION_FONT_SIZE,
        "GillSans.ttc", GRAY, BLACK_LIT, BLACK);

    chooseSound.set(optionManager()->is_sound_effect_on());

    // ________ Music Volume Option ________
    messageVector.push_back(make_unique<Message>(SOUND_X, HEADER_Y+150.f*SCALE,
                        OPTION_FONT_SIZE, "musicOption","GillSans.ttc",
                        BLACK, Align::CENTER, sf::Text::Bold));

    OnOffSelector chooseMusic(SOUND_X, HEADER_Y+180.f*SCALE, OPTION_FONT_SIZE,
       "GillSans.ttc", GRAY, BLACK_LIT, BLACK);
   chooseMusic.set(optionManager()->is_music_on());

    // _____ Export procedure _______

    auto exportAll = [&](){
        int resol = chooseResolution.who_is_selected();
        if (resol==3){
            optionManager()->set_full_screen(true);
        } else {
            optionManager()->set_full_screen(false);
            optionManager()->set_screen_width(
                RESOLUTION_WIDTH[resol]);
            optionManager()->set_screen_height(
                RESOLUTION_HEIGHT[resol]);
        }

        optionManager()->toggle_sound_effects(chooseSound.is_on());
        optionManager()->toggle_music(chooseMusic.is_on());

        int card = chooseCard.who_is_selected();
        if (card==1){
            optionManager()->set_card_type(CardType::CLASSICAL);
        } else {
            optionManager()->set_card_type(CardType::SPECIAL);
        }
    }; // End of export procedure

    sf::Event event;
    sf::Vector2i mousePos;
    bool clicked;

    while (app.isOpen()) {

        clicked = false;
        mousePos = sf::Mouse::getPosition(app);

        // #1 Event handling
        while (app.pollEvent(event)) {

            switch (event.type) {
            case sf::Event::Closed:{
                exportAll();
                app.close();
                return;
            }

            case sf::Event::KeyPressed: {
                if (event.key.code == sf::Keyboard::Escape) {
                    exportAll();
                    return;
                }
                break;
            }

            case sf::Event::MouseButtonPressed:
                if (event.mouseButton.button == sf::Mouse::Left) {
                    clicked = true;
                    if (secretButton.is_lit()) {
                        std::cout << "MOUHAHAHAHA : BIENVENUE SUR LE NAVIRE, "
                                     "MARIN D'EAU DOUCE !"
                                  << std::endl;
                        optionManager()->set_pirate(true);
                        isGonnaStop = true;
                        clk.restart();
                    }
                    if (backButton.is_lit()) {
                        soundManager.play_sound("click.wav");
                        exportAll();
                        return;
                    }
                }
                break;

            default:
                break;
            }
        }

        // #2 Various objects update
        theBackground.update(app);
        m_title.update(app);
        backButton.update(app, mousePos);

        //secretButton.update(app, mousePos);
        for (auto &message : messageVector) {
            message->update(app);
        }

        chooseResolution.update(app, mousePos, clicked);
        chooseCard.update(app, mousePos, clicked);
        chooseSound.update(app, mousePos, clicked);
        chooseMusic.update(app, mousePos, clicked);



        if (isGonnaStop && clk.getElapsedTime() > sf::seconds(4.f))
            app.close();

        // #3 Screen display
        app.display();
    }
}
