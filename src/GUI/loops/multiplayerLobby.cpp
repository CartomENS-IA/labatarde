/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Core.hpp"
#include "GUI.hpp"

void GUI::run_multiplayer_lobby(char const serverID) {

    /* OFFLINE BETA CHANGES
    shared_ptr<Network::RemoteKernel> kernel;
    Core::ParallelPlayer *gui_player = new Core::ParallelPlayer();

    switch (conType) {
    case ConnectionType::CREATE: {
        kernel.reset(m_serverLink.create_game(gui_player));
        break;
    }
    case ConnectionType::JOIN: {
        kernel.reset(m_serverLink.join_game(gui_player, serverID));
        break;
    }
    case ConnectionType::MATCHMAKING: {
        kernel.reset(m_serverLink.join_matchmaking(gui_player));
        break;
    }
    default:
        throw std::string(
            "[multiplayerLobby.cpp] ERROR : ConnectionType not recognized");
        break;
    }

    Core::GameState gameState = kernel->get_game_state();

    // Background
    Background theBackground("background/menu.png");

    // Messages
    Message connected(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2,
                      MENU_BUTTON_FONT_SIZE, L"1/4", "GillSans.ttc", WHITE);
    Message idMess(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 3, MENU_BUTTON_FONT_SIZE,
                   L"ID : " + to_wstring(kernel->lobbyId), "GillSans.ttc",
                   WHITE);

    // Buttons of main menu
    TextButton startGame(WINDOW_WIDTH / 3, WINDOW_HEIGHT * 4.f / 5,
                         MENU_BUTTON_FONT_SIZE, "play", "GillSans.ttc", BLACK,
                         BLACK_LIT, BLACK, Align::CENTER_TOP);
    TextButton exitGame(2 * WINDOW_WIDTH / 3, WINDOW_HEIGHT * 4.f / 5,
                        MENU_BUTTON_FONT_SIZE, "exit", "GillSans.ttc", BLACK,
                        BLACK_LIT, BLACK, Align::CENTER_TOP);

    // Event handling
    sf::Event event;

    // Cursor
    sf::Vector2i mousePos;

    int nbPlayers = 0;

    while (app.isOpen()) {

        mousePos = sf::Mouse::getPosition(app);

        if (nbPlayers != kernel->nbPlayers) {
            nbPlayers = kernel->nbPlayers;
            connected.set_text(to_wstring(nbPlayers) + L"/4");
        }

        // #1 Event handling
        while (app.pollEvent(event)) {

            switch (event.type) {
            case sf::Event::Closed:
                app.close();
                return;

            case sf::Event::KeyPressed: {
                if (event.key.code == sf::Keyboard::Escape) {
                    app.close();
                }
                break;
            }

            case sf::Event::MouseButtonPressed:
                if (event.mouseButton.button == sf::Mouse::Left) {
                    if (startGame.is_lit()) {
                        soundManager.play_sound("click.wav");
                        init_game_loop(true);
                        return;
                    }

                    else if (exitGame.is_lit()) {
                        soundManager.play_sound("click.wav");
                        return;
                    }
                }
                break;

            // we don't process other types of events
            default:
                break;
            }
        }

        if (gameState.game_part() != Core::GamePart::NotBeganYet) {
            // someone started the game
            init_game_loop(true);
        }

        // #2 Various objects update
        theBackground.update(app);

        m_title.update(app);
        connected.update(app);
        idMess.update(app);

        startGame.update(app, mousePos);
        exitGame.update(app, mousePos);


        // #3 Screen display
        app.display();
    }
    */
}
