/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GUI.hpp"
#include "TutoBubble.hpp"
#include "TutoKernel.hpp"
#include <thread>

using namespace std;

FourPlayerInfo GUI::retrieve_player_info_tuto() {
  FourPlayerInfo output;
  output[0].name  = TM.get_entry("you");
  output[0].profile_pic = "vous.png";
  output[0].elo = 0;
  output[0].guild_pic = "IAprofile/fibm.png";
  for (unsigned i = 1 ; i <4 ; i++){
      output[i].name  = L"TutoIA "+std::to_wstring(i);
      output[i].profile_pic = "IAprofile/matrix.png";
      output[i].elo = 0;
      output[i].guild_pic = "IAprofile/fibm.png";
  }
  return output;
}

void GUI::run_tutorial_loop(std::string name) {

    Core::GameState gamestate(0);
    shared_ptr<Core::ParallelPlayer> player =
        make_shared<Core::ParallelPlayer>();

    Core::BetQueue *betQ = new Core::BetQueue();
    player->set_game_state(gamestate);
    Core::BetBox *betBox = new Core::BetBox(0, betQ);
    player->set_bet_box(betBox);

    TutoBubble tutoMessage(
        sf::Vector2f(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2), L"");

    Board board(&gamestate, player, retrieve_player_info_tuto());

    auto tutoKernel = make_shared<TutoKernel>(name, &gamestate, player, betQ, &board);
    thread kernelThread(&TutoKernel::run, tutoKernel);

    Background theBackground("background/tuto.png");
    sf::Event event;
    sf::Vector2i mousePos;

    while (app.isOpen()) {

        mousePos = sf::Mouse::getPosition(app);

        while (app.pollEvent(event)) {
            switch (event.type) {
            case sf::Event::Closed:
                app.close();
                return;

            case sf::Event::KeyPressed: {
                if (event.key.code == sf::Keyboard::Escape) {
                    tutoKernel->end_tutorial();
                    kernelThread.detach();
                    return;
                } else if (event.key.code == sf::Keyboard::Space) {
                    if (tutoKernel->no_more_event()) {
                        tutoKernel->end_tutorial();
                        kernelThread.detach();
                        return;
                    }
                    tutoKernel->next_event();
                }
                break;
            }

            case sf::Event::MouseButtonPressed: {
                if (event.mouseButton.button == sf::Mouse::Left) {
                    board.handle_mouse_click(mousePos);
                    tutoKernel->next_event();
                    if (tutoKernel->no_more_event()) {
                        tutoKernel->end_tutorial();
                        kernelThread.detach();
                        return;
                    }
                }
                break;
            }
            default:
                break;
            }
        }

        if (tutoKernel->has_new_message()) {
            tutoKernel->set_message_on_bubble(&tutoMessage);
        }

        board.wait_action_kernel();

        theBackground.update(app);
        board.update(app, mousePos);
        tutoMessage.update(app);

        app.display();
    }
}
