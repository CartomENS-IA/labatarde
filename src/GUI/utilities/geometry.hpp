/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef GEOMETRY
#define GEOMETRY

#include <SFML/Graphics.hpp>
#include <cmath>

#include "Constants.hpp"

/** \file geometry.hpp
 *    \author Leo.V
 *    \brief Some rotation functions for vectors
 **/

/** \brief Apply a rotation on a SFML vector object. **/
sf::Vector2f rotateVector(sf::Vector2f const input, float const angle);
sf::Vector2f rotateVector(float const x, float const y, float const angle);

/** \brief multiplication by a scalar **/
sf::Vector2f operator*(float, const sf::Vector2f&);

#endif
