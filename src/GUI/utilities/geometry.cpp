/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "geometry.hpp"

sf::Vector2f rotateVector(sf::Vector2f const input, float const angle) {
    using namespace std;
    sf::Vector2f output;
    output.x =
        cos(angle * PI / 180.f) * input.x + sin(angle * PI / 180.f) * input.y;
    output.y =
        -sin(angle * PI / 180.f) * input.x + cos(angle * PI / 180.f) * input.y;
    return output;
}
sf::Vector2f rotateVector(float const x, float const y, float const angle) {
    using namespace std;
    sf::Vector2f output;
    output.x = cos(angle * PI / 180.f) * x + sin(angle * PI / 180.f) * y;
    output.y = -sin(angle * PI / 180.f) * x + cos(angle * PI / 180.f) * y;
    return output;
}

sf::Vector2f operator*(float a, const sf::Vector2f& v){
    return sf::Vector2f(a*v.x, a*v.y);
}
