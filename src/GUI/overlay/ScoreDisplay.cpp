/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "ScoreDisplay.hpp"
#include "OptionManager.hpp"
#include "Constants.hpp"
#include <string>
#include <iostream>

float const ScoreDisplay::CELL_SIZE = 70.f*optionManager()->get_screen_scale();
float const ScoreDisplay::CELL_WIDTH_PLAYER = 240.f*optionManager()->get_screen_scale();
sf::Vector2f const ScoreDisplay::CELL_OFFSET(6.f*optionManager()->get_screen_scale(), 12.f*optionManager()->get_screen_scale());
float const ScoreDisplay::OK_BUTTON_OFFSET = 30.f*optionManager()->get_screen_scale();

unsigned int const ScoreDisplay::FONT_SIZE = unsigned(26*optionManager()->get_screen_half_scale());
unsigned int const ScoreDisplay::SCORE_FONT_SIZE = unsigned(42*optionManager()->get_screen_half_scale());

sf::Color const ScoreDisplay::BLACK(17,37,90);

using namespace std;

ScoreDisplay::ScoreDisplay()
:   m_nRound(1), m_currentMechoune(false), m_currentChoune(false),
    m_okButton(WINDOW_WIDTH/2,
               OK_BUTTON_OFFSET + WINDOW_HEIGHT/2 + 150.f*SCALE,
               SCORE_FONT_SIZE, L"", "GillSans.ttc",
               WHITE, GRAY, GRAY),
    m_roundAnnounced{-1, -1, -1, -1},
    m_roundResult{-1, -1, -1, -1},
    m_roundError{-1, -1, -1, -1},
    m_roundScore{-1, -1, -1, -1},
    m_totalScore{0, 0, 0, 0}
{}

void ScoreDisplay::retrieve_player_info(FourPlayerInfo const &pInfo){
  float i=0;
  sf::Vector2f posPlayerSouth(0.f, (i++)*(CELL_SIZE+CELL_OFFSET.y));
  sf::Vector2f playerDisplaySize(CELL_WIDTH_PLAYER, CELL_SIZE);
  std::wstring yourName;
  if (optionManager()->is_online()) {
      yourName =  optionManager()->get_player_name().toWideString();
  } else {
      yourName = L"You";
  }
  m_playerDisplays.emplace_back(posPlayerSouth, playerDisplaySize, pInfo[0]);

  sf::Vector2f posPlayerWest(0.f, (i++)*(CELL_SIZE+CELL_OFFSET.y));
  m_playerDisplays.emplace_back(posPlayerWest, playerDisplaySize, pInfo[1]);

  sf::Vector2f posPlayerNorth(0.f, (i++)*(CELL_SIZE+CELL_OFFSET.y));
  m_playerDisplays.emplace_back(posPlayerNorth, playerDisplaySize, pInfo[2]);

  sf::Vector2f posPlayerEast(0.f, (i++)*(CELL_SIZE+CELL_OFFSET.y));
  m_playerDisplays.emplace_back(posPlayerEast, playerDisplaySize, pInfo[3]);
}

void ScoreDisplay::draw(sf::RenderWindow &window) {
    for (auto &message : m_infoMessages) {
        message->update(window);
    }
    for (auto &disp : m_playerDisplays){
        disp.update(window);
    }
    for (auto &cell : m_cells) {
        window.draw(cell);
    }
}

std::array<int, 4>
ScoreDisplay::correct_array_offset(int const n, std::array<int, 4> const &src) {
    std::array<int, 4> result = {0, 0, 0, 0};
    for (int i = 0; i < 4; i++) {
        result[i] = src[(i + n) % 4];
    }
    return result;
}

void ScoreDisplay::retrieve_bet_info(Core::Trump const trump,
                                     array<int, 4> const &bets,
                                     bool const isMechoune,
                                     bool const isChoune) {
    m_roundAnnounced = bets; // nb_tricks_objective is not messed up
    m_currentMechoune = isMechoune;
    m_currentChoune = isChoune;
    m_historyTrumps.push_back(trump);
    m_historyMechoune.push_back(isMechoune);
    m_historyChoune.push_back(isChoune);
}

void ScoreDisplay::retrieve_score_info(array<int, 4> const &tricks,
                                       array<int, 4> const &scores) {
    // scores are cumulative in GameState
    for (unsigned int i = 0; i < m_roundScore.size(); i++) {
        int score = scores[i] - m_totalScore[i];
        m_totalScore[i] = scores[i];
        m_roundScore[i] = score;
        if (m_currentChoune) {
            m_roundError[i] = score/4;
        } else if (m_currentMechoune) {
            m_roundError[i] = score/2;
        } else {
            m_roundError[i] = score;
        }
        m_roundResult[i] = tricks[i];
    }
    m_historyScore.push_back(m_roundScore);
    m_nRound++;
}

bool ScoreDisplay::okButton_lit() {
    return m_okButton.is_lit();
}
