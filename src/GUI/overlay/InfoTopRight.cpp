/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "InfoTopRight.hpp"
#include "Constants.hpp"
#include "OptionManager.hpp"
#include "TextManager.hpp"

using namespace std;

//________________________Constructor____________________________

InfoTopRight::InfoTopRight(float const posX, float const posY)
:
    m_roundTextOffset( float((TM.get_entry("roundCAP",true).length())*11)/2.f ),
    m_trump(75*SCALE, 75*SCALE),
    m_fix_trick_left(posX, posY + 85*SCALE, 22,
                     L"  "+TM.get_entry("trick", true),
                     "GillSans.ttc", WHITE, Align::LEFT),
    m_fix_trick_right(posX, posY + 85*SCALE, 22,
                      L"    "+TM.get_entry("cards", true),
                      "GillSans.ttc", WHITE, Align::LEFT),
    m_fix_manche_left(posX-m_roundTextOffset, posY - 50*SCALE, 22,
                      TM.get_entry("roundCAP", true)+L"  ",
                      "GillSans.ttc", WHITE, Align::CENTER),
    m_fix_manche_right(posX-m_roundTextOffset+m_fix_manche_left.get_width()/2+22*SCALE,
                       posY - 50*SCALE, 22, L"/10", "GillSans.ttc", WHITE, Align::CENTER),
    m_mechoune(posX, posY + 60*SCALE, 22, L"Mechoune", "GillSans.ttc", WHITE,
               Align::CENTER, sf::Text::Bold),
    m_manche(posX-m_roundTextOffset+m_fix_manche_left.get_width()/2, posY - 50*SCALE, 22,
             L"1", "GillSans.ttc", WHITE, Align::CENTER, sf::Text::Bold),
    m_trick_left(posX, posY + 85*SCALE, 22,
                 L"0", "GillSans.ttc", WHITE, Align::LEFT, sf::Text::Bold),
    m_trick_right(posX, posY + 85*SCALE, 22,
                  L"5", "GillSans.ttc", WHITE, Align::LEFT, sf::Text::Bold),
    m_nb_card(5), m_nb_bet(0), m_fullDraw(false)
{
    m_trump.set_position(posX, posY+5*SCALE);
    float width = m_fix_trick_left.get_width() + m_fix_trick_right.get_width();
    m_fix_trick_left.set_position(posX-width/2, posY + 85*SCALE);
    m_fix_trick_right.set_position(posX-width/2+m_fix_trick_left.get_width(), posY + 85*SCALE);
    m_trick_left.set_position(posX-width/2, posY + 85*SCALE);
    m_trick_right.set_position(posX-width/2+m_fix_trick_left.get_width(), posY + 85*SCALE);
    reset();
}

InfoTopRight::InfoTopRight(sf::Vector2f const pos)
    : InfoTopRight(pos.x, pos.y) {}

void InfoTopRight::update(sf::RenderWindow &window) {
    m_fix_manche_left.update(window);
    m_fix_manche_right.update(window);
    m_manche.update(window);
    if (!m_fullDraw)
        return;
    m_fix_trick_left.update(window);
    m_fix_trick_right.update(window);
    m_trick_left.update(window);
    m_trick_right.update(window);
    m_trump.update(window);
    m_mechoune.update(window);

}

void InfoTopRight::set_trump(Core::Trump trump) {
    m_trump.set_texture("tokens/" + NAME_TRUMP[int(trump)] + ".png");
}

void InfoTopRight::set_nb_bet(int nb_bet) {
    m_fullDraw = true;
    m_trick_left.set_text( to_wstring(nb_bet) );
}

void InfoTopRight::set_nb_card(int nb_card) {
    m_fullDraw = true;
    m_nb_card = nb_card;
    m_trick_right.set_text( L" "+ to_wstring(nb_card) );
}

void InfoTopRight::set_text_mechoune(wstring text) {
    m_mechoune.set_text(text);
}

void InfoTopRight::reset() {
    m_fullDraw = false;
    m_trump.set_texture("tokens/without_trump.png");
    m_mechoune.set_text(L"");
}

void InfoTopRight::set_round(int manche) {
    m_manche.set_text(to_wstring(manche+1));
}
