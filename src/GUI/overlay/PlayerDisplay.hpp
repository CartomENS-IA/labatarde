/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Graphics.hpp>
#include <string>

#include "Constants.hpp"
#include "Graphics.hpp"
#include "enumType.hpp"
#include "PlayerInfo.hpp"

/**	\file
 * \author Julien.D , GUIllaume.C
 *	\brief Definition of classes PlayerDisplay
 **/

 /** \class **/
 class PlayerDisplay {
 public:
    virtual void update(sf::RenderWindow &);

    void your_turn();
    void end_turn();

protected:
    /** \brief Class constructor. Is not meant to be called by anything else
        than its daughter's classes constructors. Just here for factoring **/
     PlayerDisplay();
     PlayerDisplay(PlayerInfo const &info);
     PlayerInfo m_info;
     AnimatedSprite m_avatar;
     Message m_playerName;
     bool m_lit;
     bool m_firstPlayer;
 };

class PlayerDisplayBoard : public PlayerDisplay {
public:
    PlayerDisplayBoard();
    PlayerDisplayBoard(sf::Vector2f pos, PlayerInfo const& info,
                       bool horizontal=false);
    void update(sf::RenderWindow &);
    void set_points(int);
private:
    AnimatedSprite m_overlay;
    AnimatedSprite m_aura;
    AnimatedSprite m_guild;
    AnimatedSprite m_country;
    sf::RectangleShape m_playerRect;
    sf::RectangleShape m_timerRect;
    sf::RectangleShape m_eloRect;
    sf::RectangleShape m_eloRectBg;
    sf::RectangleShape m_pointRect;
    Message m_eloMessage;
    Message m_pointsMessage;
};

class PlayerDisplayScore : public PlayerDisplay {
public:
    PlayerDisplayScore(sf::Vector2f pos, sf::Vector2f size, PlayerInfo const& info);
    void update(sf::RenderWindow &);
    void move_offset(sf::Vector2f const offset);
private:
    sf::RectangleShape m_bgRect;
};
