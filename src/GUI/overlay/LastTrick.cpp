/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "LastTrick.hpp"
#include "Constants.hpp"
#include "OptionManager.hpp"

float const WIDTH = 50;
float const LENGTH = 70;

//________________________Constructor____________________________

LastTrick::LastTrick(sf::Vector2f position)
    : m_background(30 + LENGTH * 2, 30 + LENGTH * 2), m_draw(false) {
    m_background.set_position(position);
    m_background.set_texture("background/wood_last_trick.png");
    for (int i = 0; i < 4; i++) {
        auto pos =
            position +
            rotateVector(0, ((i % 2) ? WIDTH : LENGTH / 2) + 5, float(-90 * i));
        m_card[i] = AnimatedSprite(WIDTH, LENGTH);
        m_card[i].set_position(pos);
    }
}

void LastTrick::update(sf::RenderWindow &window) {
    if (m_draw) {
        m_background.update(window);
        for (int i = 0; i < 4; i++) {
            m_card[i].update(window);
        }
    }
}

void LastTrick::update_trick(const std::vector<Core::Card> &Cards,
                             int first_player) {
    for (int i = 0; i < 4; i++) {
        int j = (i + first_player) % 4;
        m_card[j].set_texture("cards/mini/" +
                              FNAME_VALUE[int(Cards[i].value())] + "-" +
                              FNAME_TRUMP[int(Cards[i].suit())] + ".png");
    }
    m_draw = true;
}

void LastTrick::reset() { m_draw = false; }
