/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "PlayerDisplay.hpp"
#include "Constants.hpp"

using namespace sf;

// ___________________ PlayerDisplay __________________________________________

PlayerDisplay::PlayerDisplay(){}

PlayerDisplay::PlayerDisplay(PlayerInfo const& info) :
    m_info(info), m_lit(false), m_firstPlayer(false)
 {}

void PlayerDisplay::your_turn(){
    m_lit = true;
}

void PlayerDisplay::end_turn(){
    m_lit = false;
}



void PlayerDisplay::update(sf::RenderWindow &app){
    m_avatar.update(app);
    m_playerName.update(app);
}


// ___________________ PlayerDisplayBoard ______________________________________

PlayerDisplayBoard::PlayerDisplayBoard(){}

PlayerDisplayBoard::PlayerDisplayBoard(sf::Vector2f pos, PlayerInfo const& info, bool horizontal) :
    PlayerDisplay(info)
{
    // define constants
    sf::Vector2f const SIZE = horizontal ?
        sf::Vector2f(300.f*SCALE_TO_MAX, 152.f*SCALE_TO_MAX)
      : sf::Vector2f(152.f*SCALE_TO_MAX, 304.f*SCALE_TO_MAX);

    float const SIDE_OFFSET = 4.f*SCALE_TO_MAX;
    float const AVATAR_SIZE = 144.f*SCALE_TO_MAX;
    float const SMALL_AVATAR_SIZE = 70.f*SCALE_TO_MAX;
    unsigned const FONT_SIZE = unsigned(15.f*(3.f+SCALE_TO_MAX)/4.f);
    unsigned const SCORE_FONT_SIZE = unsigned(28.f*(3.f+SCALE_TO_MAX)/4.f);
    float const RECT_HEIGHT = horizontal ?
        33.f*SCALE_TO_MAX : 35.f*SCALE_TO_MAX;

    // define all sizes and variables
    sf::Color main_color = get_elo_color_main(info.elo);
    sf::Color bg_color = get_elo_color_bg(info.elo);

    m_avatar = AnimatedSprite(AVATAR_SIZE, AVATAR_SIZE, false);
    m_avatar.set_texture(m_info.profile_pic);
    m_guild = AnimatedSprite(SMALL_AVATAR_SIZE, SMALL_AVATAR_SIZE, false);
    m_guild.set_texture(m_info.guild_pic);

    sf::Vector2f avatarPos(pos);

    sf::Vector2f namePos(pos);
    sf::Vector2f eloPos(pos);
    sf::Vector2f pointPos(pos);
    sf::Vector2f playerRectPos(pos);
    sf::Vector2f eloRectPos(pos);
    sf::Vector2f guildPos(pos);
    sf::Vector2f pointRectPos(pos);
    m_playerRect = sf::RectangleShape(sf::Vector2f(AVATAR_SIZE,RECT_HEIGHT));
    m_timerRect = sf::RectangleShape(sf::Vector2f(AVATAR_SIZE/2.f, RECT_HEIGHT));
    m_eloRectBg = sf::RectangleShape(sf::Vector2f(AVATAR_SIZE, RECT_HEIGHT));
    m_eloRect = sf::RectangleShape(sf::Vector2f(AVATAR_SIZE/3.f,RECT_HEIGHT));
    m_pointRect = sf::RectangleShape(sf::Vector2f(SMALL_AVATAR_SIZE, SMALL_AVATAR_SIZE));

    m_overlay = AnimatedSprite(SIZE.x, SIZE.y, false);
    m_aura = AnimatedSprite(SIZE.x, SIZE.y, false);

    // define position of elements depending on horizontal mode or not
    if (horizontal){
        avatarPos.x += SIDE_OFFSET;
        avatarPos.y += SIDE_OFFSET;
        m_overlay.set_texture("fiche-joueur-horizontale-blanc.png");
        m_aura.set_texture("fiche-joueur-horizontale-dore.png");
        playerRectPos += sf::Vector2f(AVATAR_SIZE+2.f*SIDE_OFFSET, SIDE_OFFSET);
        eloRectPos = playerRectPos;
        eloRectPos.y += RECT_HEIGHT + SIDE_OFFSET;
        namePos += sf::Vector2f(1.5f*AVATAR_SIZE+2.f*SIDE_OFFSET, 0.2f*RECT_HEIGHT);
        eloPos = namePos;
        eloPos.y += RECT_HEIGHT + SIDE_OFFSET;
        guildPos = eloRectPos;
        guildPos.y += SIDE_OFFSET + RECT_HEIGHT;
        pointRectPos = guildPos;
        pointRectPos.x += SIDE_OFFSET + SMALL_AVATAR_SIZE;
        pointPos = pointRectPos + sf::Vector2f(35.f*SCALE_TO_MAX,10.f*SCALE_TO_MAX);
    } else {
        m_overlay.set_texture("fiche-joueur-verticale-blanc.png");
        m_aura.set_texture("fiche-joueur-verticale-dore.png");
        avatarPos.x += SIDE_OFFSET;
        avatarPos.y += SIDE_OFFSET;
        playerRectPos.x += SIDE_OFFSET;
        playerRectPos.y += AVATAR_SIZE + 2.f*SIDE_OFFSET;
        eloRectPos = playerRectPos;
        eloRectPos.y += RECT_HEIGHT + SIDE_OFFSET;
        namePos += sf::Vector2f(AVATAR_SIZE/2.f+SIDE_OFFSET, SIDE_OFFSET+AVATAR_SIZE+0.2f*RECT_HEIGHT);
        eloPos = namePos;
        eloPos.y += RECT_HEIGHT + SIDE_OFFSET;
        guildPos = eloRectPos;
        guildPos.y += SIDE_OFFSET + RECT_HEIGHT;
        pointRectPos = guildPos;
        pointRectPos.x += SIDE_OFFSET + SMALL_AVATAR_SIZE;
        pointPos = pointRectPos +sf::Vector2f(35.f*SCALE_TO_MAX,5.f*SCALE_TO_MAX);
    }
    m_overlay.set_position(pos);
    m_aura.set_position(pos);
    m_avatar.set_position(avatarPos);
    m_guild.set_position(guildPos);
    m_playerRect.setPosition(playerRectPos);
    m_playerRect.setFillColor(bg_color);
    m_timerRect.setPosition(playerRectPos);
    m_timerRect.setFillColor(main_color);
    m_eloRect.setPosition(eloRectPos);
    m_eloRect.setFillColor(main_color);
    m_eloRectBg.setPosition(eloRectPos);
    m_eloRectBg.setFillColor(bg_color);
    m_pointRect.setPosition(pointRectPos);
    m_pointRect.setFillColor(main_color);
    m_playerName = Message(namePos, FONT_SIZE, m_info.name,
                        "GillSans.ttc", WHITE, Align::CENTER_TOP);
    m_eloMessage = Message(eloPos, FONT_SIZE, std::to_wstring(m_info.elo),
                        "GillSans.ttc", WHITE, Align::CENTER_TOP);
    m_pointsMessage = Message(pointPos, SCORE_FONT_SIZE, L"0",
                        "GillSans.ttc", WHITE, Align::CENTER_TOP, sf::Text::Bold);
}

void PlayerDisplayBoard::update(sf::RenderWindow &app){
    app.draw(m_playerRect);
    app.draw(m_eloRectBg);
    app.draw(m_timerRect);
    app.draw(m_eloRect);
    app.draw(m_pointRect);
    PlayerDisplay::update(app);
    m_eloMessage.update(app);
    m_pointsMessage.update(app);
    m_guild.update(app);
    m_lit ? m_aura.update(app) : m_overlay.update(app);
}

void PlayerDisplayBoard::set_points(int point){
    m_pointsMessage.set_text(std::to_wstring(point));
}

// ___________________ PlayerDisplayScore ______________________________________

PlayerDisplayScore::PlayerDisplayScore(sf::Vector2f pos, sf::Vector2f size,
    PlayerInfo const& info) :
    PlayerDisplay(info), m_bgRect(size)
{
    float const AVATAR_SIZE = 60.f*SCALE;
    unsigned const FONT_SIZE = unsigned(24.f*HALF_SCALE);

    m_bgRect.setPosition(pos);
    sf::Color main_color = get_elo_color_main(info.elo);
    m_bgRect.setFillColor(main_color);
    m_avatar = AnimatedSprite(AVATAR_SIZE, AVATAR_SIZE);
    m_avatar.set_texture(m_info.profile_pic);
    m_avatar.set_position(pos + sf::Vector2f(AVATAR_SIZE/2.f, AVATAR_SIZE/2.f)
                              + sf::Vector2f(10.f*SCALE,5.f*SCALE));
    sf::Vector2f namePos(pos);
    namePos += sf::Vector2f(AVATAR_SIZE+20.f*SCALE,25.f*SCALE);
    m_playerName = Message(namePos, FONT_SIZE, m_info.name,
                            "GillSans.ttc", WHITE, Align::LEFT);
}

void PlayerDisplayScore::update(sf::RenderWindow &app){
    app.draw(m_bgRect);
    PlayerDisplay::update(app);
}

void PlayerDisplayScore::move_offset(sf::Vector2f const offset){
    m_bgRect.move(offset);
    m_avatar.move_relative(offset, 0.f, 0.f);
    m_playerName.move_relative(offset, 0.f, 0.f);
}
