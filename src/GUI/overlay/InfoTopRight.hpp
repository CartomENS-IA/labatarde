/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_CLASS_INFOTOPRIGHT
#define DEF_CLASS_INFOTOPRIGHT

/** \file
 *	\author Léo.V
 *	\date 28 MAY
 *	\brief Definition of class InfoTopRight
 **/

#include "Core.hpp"
#include "Graphics.hpp"
#include <cmath>
#include <string>

/** \class **/
class InfoTopRight {

  public:
    InfoTopRight(float const, float const);
    InfoTopRight(sf::Vector2f const);
    void update(sf::RenderWindow &window);
    void set_trump(Core::Trump);
    void set_nb_bet(int nb_bet);
    void set_nb_card(int nb_card);
    void set_text_mechoune(std::wstring text);
    void set_round(int);
    void reset();

  private:
    float m_roundTextOffset;

    AnimatedSprite m_trump;
    Message m_fix_trick_left;
    Message m_fix_trick_right;
    Message m_fix_manche_left;
    Message m_fix_manche_right;

    Message m_mechoune;
    Message m_manche;
    Message m_trick_left;
    Message m_trick_right;

    int m_nb_card;
    int m_nb_bet;
    bool m_fullDraw;
};

#endif
