/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "ScoreDisplay.hpp"
#include "stringSplit.hpp"
#include <algorithm>

sf::Vector2f const EndRoundDisplay::POSITION(
    optionManager()->get_screen_width() / 2 - 470.f*optionManager()->get_screen_scale(),
    optionManager()->get_screen_height() / 2 - 200.f*optionManager()->get_screen_scale()); // top left corner

unsigned int const EndRoundDisplay::HEADER_FONT_SIZE = unsigned(18*optionManager()->get_screen_half_scale());

EndRoundDisplay::EndRoundDisplay()
:   ScoreDisplay::ScoreDisplay()
{
    m_okButton.set_text("nextRound");

    /** /!\ Position of every object is given in relation to the ScoreDisplay
       position. It is shifted at the end to fit in the background **/

    // ________ Build the cells ________
    sf::Vector2f const cellSizeVector(110.f*SCALE, ScoreDisplay::CELL_SIZE);
    sf::Vector2f const cellPadding(cellSizeVector+ScoreDisplay::CELL_OFFSET);

    // coordinates of the top left corners of the cells
    std::array<float,6> cellPosX;
    std::array<float,4> cellPosY;
    for (int x = 0; x < 6; x++) {
        cellPosX[x] = CELL_WIDTH_PLAYER + CELL_OFFSET.x*float(x+1)
                           + cellSizeVector.x*float(x);
    }
    for (int y = 0 ; y < 4 ; y++) {
        cellPosY[y] = (CELL_OFFSET.y + cellSizeVector.y)*float(y);
    }

    for (int y = 0; y < 4; y++) {
        for (int x = 0 ; x < 5 ; x++) {
            sf::RectangleShape cell(cellSizeVector);
            cell.setFillColor(ScoreDisplay::BLACK);
            cell.setPosition(sf::Vector2f(cellPosX[x],cellPosY[y]));
            m_cells.push_back(cell);
        }
        sf::RectangleShape totalCell(cellSizeVector);
        totalCell.setFillColor(WHITE);
        totalCell.setPosition(sf::Vector2f(cellPosX[5],cellPosY[y]));
        m_cells.push_back(totalCell);
    }

    // ________ build headers of table ________
    vector<string> headerKeys = {"score_announced","score_result",
                                 "score_error","score_mechoune",
                                 "score_round","score_total"};
    int cnt = 0;
    // load the string for each header, and split it to fit the width of a cell
    std::for_each(headerKeys.begin(), headerKeys.end(),
        [&](string &s){
            m_infoMessages.push_back(make_unique<MultiLineMessage>(
                cellPosX[cnt++] + 0.5f*cellSizeVector.x, -40.f*SCALE,
                HEADER_FONT_SIZE, s, ';', "GillSans.ttc",
                WHITE, Align::CENTER));
        });

    // ________ Build every score message ________
    // Initialize a message "0" in a given cell coordinate
    auto initCellMessage = [&](int x, int y, sf::Color col=WHITE){
        return make_unique<Message>(cellPosX[x]+0.5f*cellSizeVector.x,
                       cellPosY[y]+0.33f*cellSizeVector.y,
                       SCORE_FONT_SIZE, L"0", "GillSans.ttc", col,
                       Align::CENTER, sf::Text::Bold);
    };

    for (int y = 0; y < 4; y++) {
        int x = 0;
        m_announcedMessages.push_back(initCellMessage(x++,y));
        m_resultMessages.push_back(initCellMessage(x++,y));
        m_errorMessages.push_back(initCellMessage(x++,y));
        m_multipliers.push_back(initCellMessage(x++,y));
        m_scoreMessages.push_back(initCellMessage(x++,y));
        m_totalMessages.push_back(initCellMessage(x++,y,BLACK));
    }

    // _______ Apply change of origin to every object built so far ________
    for (auto &cell : m_cells) {
        cell.move(POSITION);
    }

    auto moveMessages = [&](std::vector<MessPtr> &v){
        for (auto &mess :v) mess->move_offset(POSITION);
    };
    moveMessages(m_infoMessages);
    moveMessages(m_announcedMessages);
    moveMessages(m_resultMessages);
    moveMessages(m_errorMessages);
    moveMessages(m_scoreMessages);
    moveMessages(m_totalMessages);
    moveMessages(m_multipliers);
}

void EndRoundDisplay::retrieve_player_info(FourPlayerInfo const &pInfo) {
  ScoreDisplay::retrieve_player_info(pInfo);
  for (auto &disp : m_playerDisplays) {
      disp.move_offset(POSITION);
  }
}

void EndRoundDisplay::update(sf::RenderWindow &window,
                             sf::Vector2f const mousePos) {
    update(window, mousePos.x, mousePos.y);
}

void EndRoundDisplay::update(sf::RenderWindow &window,
                             sf::Vector2i const mousePos) {
    update(window, sf::Vector2f(mousePos));
}

void EndRoundDisplay::update(sf::RenderWindow &window, float const mouseX,
                             float const mouseY) {
    ScoreDisplay::draw(window);
    auto updateMessages = [&window](std::vector<MessPtr>& v){
        for (auto &mess : v) mess->update(window);
    };
    updateMessages(m_announcedMessages);
    updateMessages(m_resultMessages);
    updateMessages(m_errorMessages);
    updateMessages(m_multipliers);
    updateMessages(m_scoreMessages);
    updateMessages(m_totalMessages);
    m_okButton.update(window, mouseX, mouseY);
}

void EndRoundDisplay::retrieve_bet_info(Core::Trump const trump,
                                        std::array<int, 4> const &bets,
                                        bool const isMechoune,
                                        bool const isChoune) {
    ScoreDisplay::retrieve_bet_info(trump, bets, isMechoune, isChoune);
    for (auto &message : m_multipliers){
        if (isChoune) {
            message->set_text(L"x4");
        } else if (isMechoune && !isChoune) {
            message->set_text(L"x2");
        } else {
            message->set_text(L"x1");
        }
    }
    if (m_nRound == 10) {
        m_okButton.set_text("next");
    }
    for (int i = 0; i < 4; i++) {
        m_announcedMessages[i]->set_text(std::to_wstring(m_roundAnnounced[i]));
    }
}

void EndRoundDisplay::retrieve_score_info(
    std::array<int, 4> const &tricks,
    std::array<int, 4> const &scores)
{
    ScoreDisplay::retrieve_score_info(tricks,scores);
    for (int p = 0; p < 4; p++) {
        m_resultMessages[p]->set_text(std::to_wstring(m_roundResult[p]));
        m_errorMessages[p]->set_text(std::to_wstring(m_roundError[p]));
        m_scoreMessages[p]->set_text(std::to_wstring(m_roundScore[p]));
        m_totalMessages[p]->set_text(std::to_wstring(m_totalScore[p]));
    }
}
