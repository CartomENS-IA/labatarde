/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "ScoreDisplay.hpp"

#include <iostream> // DEBUG

sf::Vector2f const EndGameDisplay::POSITION(
    optionManager()->get_screen_width() / 2 - 550.f*optionManager()->get_screen_scale(),
    optionManager()->get_screen_height() / 2 - 150.f*optionManager()->get_screen_scale()); // top left corner

unsigned int const EndGameDisplay::HEADER_FONT_SIZE = unsigned(34*optionManager()->get_screen_half_scale());

EndGameDisplay::EndGameDisplay()
:   ScoreDisplay::ScoreDisplay(),
    m_winnerSprite(CELL_SIZE, CELL_SIZE, false)
{
    m_okButton.set_text("backToGame");
    m_winnerSprite.set_texture("vide.png");
    unsigned int nbRound = Core::nbCardsPerTurn.size();

    sf::Vector2f cellSize(ScoreDisplay::CELL_SIZE, ScoreDisplay::CELL_SIZE);
    sf::Vector2f const cellOffset(cellSize+ScoreDisplay::CELL_OFFSET);
    float const initialCellX = ScoreDisplay::CELL_WIDTH_PLAYER
                               + ScoreDisplay::CELL_OFFSET.x ; // width of player display
    for (unsigned int i = 0; i < 4; i++) {
        for (unsigned int j = 0 ; j < nbRound ; j++) {
            sf::RectangleShape cell(cellSize);
            cell.setFillColor(ScoreDisplay::BLACK);
            sf::Vector2f pos;
            pos.x = initialCellX + cellOffset.x*float(j);
            pos.y = cellOffset.y*float(i);
            cell.setPosition(pos);
            m_cells.push_back(cell);
        }
        sf::RectangleShape totalCell(cellSize);
        totalCell.setFillColor(WHITE);
        sf::Vector2f pos;
        pos.x = ScoreDisplay::CELL_WIDTH_PLAYER + ScoreDisplay::CELL_OFFSET.x
                + (cellSize.x + ScoreDisplay::CELL_OFFSET.x)
                * float(Core::nbCardsPerTurn.size());
        pos.y = (cellSize.y + ScoreDisplay::CELL_OFFSET.y)*float(i);
        totalCell.setPosition(pos);
        m_cells.push_back(totalCell);
    }

    // headers of table
    for (unsigned int j = 0; j < nbRound; j++) {
        // number of cards per turn
        sf::Vector2f pos((float(j)+0.5f)*(CELL_SIZE+CELL_OFFSET.x)
                         + CELL_WIDTH_PLAYER + CELL_OFFSET.x, -120.f*SCALE);
        m_infoMessages.push_back(make_unique<Message>(pos.x, pos.y,
            HEADER_FONT_SIZE+6, std::to_wstring(Core::nbCardsPerTurn[j]),
            "GillSans.ttc", WHITE, Align::CENTER, sf::Text::Bold));

        // Trump of turns
        sf::Vector2f posTrump(pos);
        posTrump.y += 60.f*SCALE;
        m_trumps.emplace_back(CELL_SIZE*0.5f, CELL_SIZE*0.5f);
        m_trumps[j].set_position(posTrump+POSITION);
        m_trumps[j].set_texture("vide.png");

        // Mechoune-Choune indicator
        sf::Vector2f posChoune(pos);
        posChoune.y += 90.f*SCALE;
        m_chouneMessages.push_back(make_unique<Message>(posChoune.x,
            posChoune.y, HEADER_FONT_SIZE+3, L" ", "GillSans.ttc", WHITE,
            Align::CENTER, sf::Text::Bold));
    }


    // Round score messages
    for (unsigned int rnd = 0; rnd < nbRound; rnd++) {
        for (int i = 0; i < 4; i++) {
            sf::Vector2f pos;
            pos.x = (float(rnd)+0.5f) * (CELL_SIZE+CELL_OFFSET.x)
                    + CELL_WIDTH_PLAYER + CELL_OFFSET.x;
            pos.y = (float(i)+0.333f) * (CELL_SIZE+CELL_OFFSET.y);
            m_scoreMessages.push_back(make_unique<Message>(pos.x, pos.y,
                SCORE_FONT_SIZE, L"", "GillSans.ttc", WHITE, Align::CENTER));
        }
    }

    // Total score messages
    float posX = (float(nbRound)+0.5f) * (CELL_SIZE+CELL_OFFSET.x)
                  + CELL_WIDTH_PLAYER + CELL_OFFSET.x;

    m_infoMessages.push_back(make_unique<MultiLineMessage>(
        posX-8.f*SCALE, -64.f*SCALE, FONT_SIZE,
        "score_total", ';', "GillSans.ttc", WHITE, Align::CENTER_TOP));

    for (int i = 0; i < 4; i++) {
        m_totalMessages.push_back(make_unique<Message>(
            posX, (float(i) + 1.f/3.f) * (CELL_SIZE+CELL_OFFSET.y),
            SCORE_FONT_SIZE, L"0", "GillSans.ttc",
            BLACK, Align::CENTER));
    }

    // _______ Apply change of origin to every object built so far ________
    for (auto &cell : m_cells) {
        cell.move(POSITION);
    }
    auto moveMessages = [&](std::vector<MessPtr> &v){
        for (auto &mess :v) mess->move_offset(POSITION);
    };
    moveMessages(m_infoMessages);
    moveMessages(m_scoreMessages);
    moveMessages(m_totalMessages);
    moveMessages(m_chouneMessages);
}

void EndGameDisplay::retrieve_player_info(FourPlayerInfo const &pInfo) {
  ScoreDisplay::retrieve_player_info(pInfo);
  for (auto &disp : m_playerDisplays) {
      disp.move_offset(POSITION);
  }
}

void EndGameDisplay::update(sf::RenderWindow &window,
                            sf::Vector2f const mousePos) {
    update(window, mousePos.x, mousePos.y);
}

void EndGameDisplay::update(sf::RenderWindow &window,
                            sf::Vector2i const mousePos) {
    update(window, sf::Vector2f(mousePos));
}

void EndGameDisplay::update(sf::RenderWindow &window, float const mouseX,
                            float const mouseY) {
    ScoreDisplay::draw(window);
    auto updateMessages = [&](std::vector<MessPtr> &v){
        for (auto &mess :v) mess->update(window);
    };
    updateMessages(m_totalMessages);
    updateMessages(m_chouneMessages);
    updateMessages(m_scoreMessages);
    for (auto &sprite : m_trumps) {
        window.draw(sprite);
    }
    m_okButton.update(window, mouseX, mouseY);
    m_winnerSprite.update(window);
}

void EndGameDisplay::retrieve_bet_info(Core::Trump const trump,
                                       std::array<int, 4> const &bets,
                                       bool const isMechoune,
                                       bool const isChoune) {
    ScoreDisplay::retrieve_bet_info(trump, bets, isMechoune, isChoune);
    if (isChoune) {
      m_chouneMessages[m_nRound-1]->set_text(L"x4");
    } else if (isMechoune && !isChoune) {
      m_chouneMessages[m_nRound-1]->set_text(L"x2");
    }
    switch (trump) {
      case Core::Trump::NoTrump :
        m_trumps[m_nRound-1].set_texture("tokens/without_trump.png");
        break;
      case Core::Trump::Club :
        m_trumps[m_nRound-1].set_texture("tokens/club.png");
        break;
      case Core::Trump::Diamond :
        m_trumps[m_nRound-1].set_texture("tokens/diamond.png");
        break;
      case Core::Trump::Heart :
        m_trumps[m_nRound-1].set_texture("tokens/heart.png");
        break;
      case Core::Trump::Spade :
        m_trumps[m_nRound-1].set_texture("tokens/spade.png");
        break;
      case Core::Trump::AllTrump :
      default:
        m_trumps[m_nRound-1].set_texture("tokens/all_trump.png");
        break;
    }
}

void EndGameDisplay::retrieve_score_info(
    std::array<int, 4> const &tricks,
    std::array<int, 4> const &scores) {
    ScoreDisplay::retrieve_score_info(tricks,scores);
    for (int p = 0; p < 4; p++) {
        m_totalMessages[p]->set_text(std::to_wstring(m_totalScore[p]));
    }
    for (int p = 0; p < 4; p++) {
        m_scoreMessages[4 * (m_nRound - 2) + p]->set_text(
            std::to_wstring(m_roundScore[p]));
    }
}

void EndGameDisplay::retrieve_winner(int const winner) {
    m_winnerSprite.set_texture("cup.png");
    m_winnerSprite.set_position(POSITION.x +(11.f)*(CELL_SIZE+CELL_OFFSET.x)
                                + CELL_WIDTH_PLAYER + 2.f*CELL_OFFSET.x,
                     POSITION.y + (CELL_SIZE+CELL_OFFSET.y)*float(winner));
    m_okButton.set_text("backToMenu");
}
