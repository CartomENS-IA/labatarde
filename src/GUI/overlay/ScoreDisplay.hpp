/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <array>
#include <string>
#include <vector>
#include <memory>

#include "Constants.hpp"
#include "Graphics.hpp"
#include "PlayerDisplay.hpp"

#include "Core.hpp"

/**  \file
 *    \author GUIllaume.C
 *    \brief Definition of classes ScoreDisplay, EndRoundDisplay and
 *EndGameDisplay
 *
 *    The two classes EndRoundDisplay and EndGameDisplay are very similar, so we
 *factorized what they have in common in a parent class ScoreDisplay
 **/

/** /class
*   /brief This is a utility class specifically designed for inheritance.
            It should not be instanciated
**/
class ScoreDisplay {
  public:
    ScoreDisplay();
    /** \brief Draws elements on the screen**/
    virtual void draw(sf::RenderWindow &window);

    virtual void retrieve_player_info(FourPlayerInfo const &);

    /** \brief retrieves everything there is to know about the bets during a
    round. Should be called at the end of betPhase
    **/
    virtual void retrieve_score_info(std::array<int, 4> const &tricks,
                                     std::array<int, 4> const &scores);

    /** \brief retrieves everything there is to know about the results of a
    round. Should be called at the end of a round.
        /!\ increments the m_nRound attribute
    **/
    virtual void retrieve_bet_info(Core::Trump const trump,
                                   std::array<int, 4> const &,
                                   bool const isMechoune, bool const isChoune);

    bool okButton_lit();

  private:
    /** \brief When given a bet or a score from the Kernel, the index of the
       first element is the player that played first. This player changes during
       the game. This function applies a permutation in order to retrieve the
       order :**/
    static std::array<int, 4> correct_array_offset(int const,
                                                   std::array<int, 4> const &);

  protected:
    // ______Constants_________
    static float const CELL_WIDTH_PLAYER;
    static float const CELL_SIZE;
    static sf::Vector2f const CELL_OFFSET; // space between cells

    static unsigned int const FONT_SIZE;
    static unsigned int const SCORE_FONT_SIZE;
    static float const OK_BUTTON_OFFSET;
    static float const WIDTH_AVATAR;

    static sf::Color const BLACK;

    int m_nRound; // in which round we are
    bool m_currentMechoune;
    bool m_currentChoune;
    TextButton m_okButton;

    std::vector<sf::RectangleShape> m_cells;
    std::vector<MessPtr> m_infoMessages;
    std::vector<PlayerDisplayScore> m_playerDisplays;
    std::array<int, 4> m_roundAnnounced; // Bets given by players
    std::array<int, 4> m_roundResult; // Number of tricks taken
    std::array<int, 4> m_roundError; // Difference between bet and result
    std::array<int, 4> m_roundScore; // Score (tricks*mechoune/choune)
    std::array<int, 4> m_totalScore; // Cumulative score
    std::vector<bool> m_historyMechoune;
    std::vector<bool> m_historyChoune;
    std::vector<Core::Trump> m_historyTrumps;
    std::vector<std::array<int, 4>> m_historyScore;
};


/** /class
 *   /brief A subwindows that is displayed at the end of a round. Displays
 *results of the different players
 **/
class EndRoundDisplay : public ScoreDisplay {
  public:
    /**  \brief Constructor **/
    EndRoundDisplay();

    /** \brief Update the scores of the screen and draw them **/
    void update(sf::RenderWindow &window, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &window, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &window, float const mouseX = -1.,
                float const mouseY = -1.);

    void retrieve_player_info(FourPlayerInfo const &);

    void retrieve_score_info(std::array<int, 4> const &tricks,
                             std::array<int, 4> const &scores);
    void retrieve_bet_info(Core::Trump const trump, std::array<int, 4> const &,
                           bool const isMechoune, bool const isChoune);

  private:
    static sf::Vector2f const POSITION; // top left corner
    static unsigned int const HEADER_FONT_SIZE;

    std::vector<MessPtr> m_announcedMessages;
    std::vector<MessPtr> m_resultMessages;
    std::vector<MessPtr> m_errorMessages;
    std::vector<MessPtr> m_multipliers;
    std::vector<MessPtr> m_scoreMessages;
    std::vector<MessPtr> m_totalMessages;
};


/** /class
 *   /brief A subwindows that is displayed at the end of a round. Displays
 *results of the different players
 **/
class EndGameDisplay : public ScoreDisplay {
  public:
    EndGameDisplay();

    void update(sf::RenderWindow &window, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &window, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &window, float const mouseX = -1.,
                float const mouseY = -1.);

    void retrieve_player_info(FourPlayerInfo const &);

    void retrieve_bet_info(Core::Trump const trump,
                           std::array<int, 4> const &bets,
                           bool const isMechoune, bool const isChoune);
    void retrieve_score_info(std::array<int, 4> const &tricks,
                             std::array<int, 4> const &scores);
    void retrieve_winner(int const);

  private:
    static sf::Vector2f const POSITION; // top left corner
    static unsigned int const HEADER_FONT_SIZE;

    std::vector<MessPtr> m_scoreMessages;
    std::vector<MessPtr> m_totalMessages;
    std::vector<MessPtr> m_chouneMessages;
    std::vector<AnimatedSprite> m_trumps;
    AnimatedSprite m_winnerSprite;
};
