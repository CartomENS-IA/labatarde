/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_CLASS_LASTTRICK
#define DEF_CLASS_LASTTRICK

/** \file
 *	\author Léo.V
 *	\date 28 MAY
 *	\brief Definition of class LastTrick
 **/

#include "Core.hpp"
#include "Graphics.hpp"
#include <cmath>

class LastTrick {

  public:
    LastTrick(sf::Vector2f position);
    void update(sf::RenderWindow &window);
    void update_trick(const std::vector<Core::Card> &Cards, int first_player);
    void reset();

  private:
    std::array<AnimatedSprite, 4> m_card;
    AnimatedSprite m_background;
    bool m_draw;
};

#endif
