/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "TutoBubble.hpp"
#include "Constants.hpp"
#include "TextManager.hpp"
#include "geometry.hpp"
#include "stringSplit.hpp"

unsigned int const TutoBubble::FONTSIZE_TUTOBUBBLE = 25;

TutoBubble::TutoBubble(sf::Vector2f const position,
                       std::string const &key,
                       float const width)
: TutoBubble(position, TM.get_entry(key), width) {}

TutoBubble::TutoBubble(sf::Vector2f const position,
                       std::wstring const &text,
                       float const width)
:
    m_textwidth(width),
    m_text(text),
    m_position(position),
    m_show(true)
{
    int nb_lines = split_string_width(m_text, FONTSIZE_TUTOBUBBLE, m_textwidth);
    m_height = compute_height(nb_lines);

    m_bubbleSprite = TutoBubbleSprite(position, width, m_height);

    m_message = Message(position.x, position.y, FONTSIZE_TUTOBUBBLE, m_text,
                        "GillSans.ttc", BLACK, Align::LEFT);

    m_message.set_position(position + MESSAGE_OFFSET +
                           sf::Vector2f(0, m_height / 2));
}

float TutoBubble::compute_height(int nb_lines) {
    return BOTTOM_TEXT_OFFSET + float(nb_lines * (FONTSIZE_TUTOBUBBLE + 10));
}

void TutoBubble::set_text(std::string const &key) {
    set_text(TM.get_entry(key));
}

void TutoBubble::set_text(std::wstring const &text) {
    m_text = text;
    int nb_lines = split_string_width(m_text, FONTSIZE_TUTOBUBBLE, m_textwidth);
    m_height = compute_height(nb_lines);
    m_bubbleSprite = TutoBubbleSprite(m_position, m_textwidth, m_height);
    m_message.set_text(m_text);
    m_message.set_position(m_position.x + LEFT_TEXT_OFFSET,
                           m_position.y + m_height / 2 - 10.f*SCALE);
}

void TutoBubble::set_position(sf::Vector2f const pos) {
    set_position(pos.x, pos.y);
}

void TutoBubble::set_position(sf::Vector2i const pos) {
    set_position(sf::Vector2f(pos));
}

void TutoBubble::set_position(float posX, float posY) {
    m_message.set_position(posX + LEFT_TEXT_OFFSET,
                           posY + SCALE * m_height / 2 - 10.f*SCALE);
    m_bubbleSprite.set_position(posX, posY);
}

sf::Text TutoBubble::get_message_text() { return m_message.get_text(); }

std::wstring &TutoBubble::get_text() { return m_text; }

void TutoBubble::update(sf::RenderWindow &window) {
    if (m_show) {
        m_bubbleSprite.update(window);
        m_message.update(window);
    }
}

bool TutoBubble::is_animated() { return m_bubbleSprite.is_animated(); }

void TutoBubble::show() { m_show = true; }

void TutoBubble::hide() { m_show = false; }
