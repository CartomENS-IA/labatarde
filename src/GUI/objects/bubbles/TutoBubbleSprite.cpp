/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "TutoBubbleSprite.hpp"
#include "Constants.hpp"
#include "RessourceManager.hpp"
#include "geometry.hpp"

// ________ Tuto Bubble Constants ________

float const LEFT_SPRITE_WIDTH = 50.f * optionManager()->get_screen_scale();
float const RIGHT_SPRITE_WIDTH = 100.f * optionManager()->get_screen_scale();
float const LEFT_TEXT_OFFSET = 30.f * optionManager()->get_screen_scale();
float const BOTTOM_TEXT_OFFSET = 30.f * optionManager()->get_screen_scale();
float const ENTERTAINER_SIZE = 80.f * optionManager()->get_screen_scale();
float const ENTERTAINER_OFFSET = 70.f * optionManager()->get_screen_scale();

sf::Vector2f const MESSAGE_OFFSET(LEFT_TEXT_OFFSET, -10.f);
// _______________________________________

TutoBubbleSprite::TutoBubbleSprite(sf::Vector2f const position,
                                   float const width, float height)
    : m_width(width*SCALE), m_height(height*SCALE), m_origin(position),
      m_midOffset(LEFT_SPRITE_WIDTH, 0.f) {
    m_spriteBubbleLeft = AnimatedSprite(LEFT_SPRITE_WIDTH, m_height, false);
    m_spriteBubbleRight = AnimatedSprite(RIGHT_SPRITE_WIDTH, m_height, false);

    float middleSpriteWidth =
        m_width - (LEFT_SPRITE_WIDTH + RIGHT_SPRITE_WIDTH) / 2;
    m_spriteBubbleMiddle = AnimatedSprite(middleSpriteWidth, m_height, false);

    m_spriteEntertainer =
        AnimatedSprite(ENTERTAINER_SIZE, ENTERTAINER_SIZE, false);
    m_spriteEntertainer.set_texture("fou.png");
    m_centerBubble = position + sf::Vector2f(m_width / 2, m_height / 2);

    m_rightOffset = sf::Vector2f(LEFT_SPRITE_WIDTH + middleSpriteWidth, 0);
    m_entertainerOffset =
        sf::Vector2f(m_width + ENTERTAINER_OFFSET, m_height / 2);

    m_spriteBubbleMiddle.set_texture("bubbles/fou_milieu.png");
    m_spriteBubbleMiddle.set_position(m_origin + m_midOffset);

    m_spriteBubbleLeft.set_texture("bubbles/fou_gauche.png");
    m_spriteBubbleLeft.set_position(m_origin);

    m_spriteBubbleRight.set_texture("bubbles/fou_droite.png");
    m_spriteBubbleRight.set_position(m_origin + m_rightOffset);

    m_spriteEntertainer.set_position(m_origin + m_entertainerOffset);
}

sf::Vector2f TutoBubbleSprite::get_dimensions() {
    return sf::Vector2f(m_width, m_height);
}

void TutoBubbleSprite::set_position(sf::Vector2f const pos) {
    set_position(pos.x, pos.y);
}

void TutoBubbleSprite::set_position(float posX, float posY) {
    sf::Vector2f pos(posX, posY);
    m_spriteBubbleLeft.set_position(pos);
    m_spriteBubbleMiddle.set_position(pos + m_midOffset);
    m_spriteBubbleRight.set_position(pos + m_rightOffset);
    m_spriteEntertainer.set_position(pos + m_entertainerOffset);
}

void TutoBubbleSprite::update(sf::RenderWindow &window) {
    m_spriteBubbleLeft.update(window);
    m_spriteBubbleRight.update(window);
    m_spriteBubbleMiddle.update(window);
    m_spriteEntertainer.update(window);
}

bool TutoBubbleSprite::is_animated() {
    return m_spriteBubbleMiddle.is_animated();
}
