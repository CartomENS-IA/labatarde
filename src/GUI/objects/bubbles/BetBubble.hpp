/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file BetBubble.hpp
 *   \author Leo.V, GUIllaume.C
 *   \brief Definition of class BetBubble
 **/

#include "Core.hpp"
#include "Graphics.hpp"
#include "enumType.hpp"

/** \brief BetBubble is the object to be displayed on screen when an adversary
 * player makes a bet **/
class BetBubble {
  public:
    BetBubble() = default;

    /** \brief Non default constructor **/
    BetBubble(CardinalPoints const cardi, float const scaleBubble = 1.f);

    /** \brief Update method. Should update the object and display it on the
     * RenderWindow **/
    void update(sf::RenderWindow &window);

    /** \brief Getter method. Says if the bubble is currently moving**/
    bool is_animated();

    /** \brief Setter for the private attribute m_bet **/
    void set_bet(Core::Bet bet, bool draw = true);
    /** \brief Getter for the private attribute m_bet **/
    Core::Bet get_bet();

    /** \brief Sets the message in the bubble**/
    void set_message(std::wstring const &text);

    /** \brief Sets the message in the bubble**/
    void set_message(std::string const &textname);

    /** \brief Resets the bubble and its message **/
    void restart();

  private:
    /** \brief make the bubble appear and grow **/
    void animate();

    bool m_onlyText;
    bool m_draw;

    Core::Bet m_bet; // Value of the bet to be displayed
    sf::Vector2f m_positionBubble;
    sf::Vector2f m_startPosition;
    sf::Vector2f m_centerBubble;
    
    AnimatedSprite m_spriteBubble;
    AnimatedSprite m_iconTrump;
    Message m_messageBubble;

    int m_sizeFont;
    float m_angleBubble;
    float m_scaleBubble;
};
