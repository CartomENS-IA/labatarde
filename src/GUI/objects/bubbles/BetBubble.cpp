/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "BetBubble.hpp"
#include "Constants.hpp"
#include "TextManager.hpp"
#include "geometry.hpp"

BetBubble::BetBubble(CardinalPoints const cardi, float const scaleBubble)
:
    m_bet(Core::Trump::NoTrump, 0),
    m_spriteBubble(WIDTH_BETBUBBLE + ((cardi%2)?SIZE_ARROW_BETBUBBLE:0), HEIGHT_BETBUBBLE + ((cardi%2)?0:SIZE_ARROW_BETBUBBLE)),
    m_iconTrump(SIZE_TEXT_BETBUBBLE, SIZE_TEXT_BETBUBBLE),
    m_sizeFont(unsigned(SIZE_FONT_BETBUBBLE*HALF_SCALE_TO_MAX)),
    m_angleBubble( float(cardi) * 90.f),
    m_scaleBubble(scaleBubble)
{
    switch (cardi) {
    case CardinalPoints::EAST:
        m_spriteBubble.set_texture("bubbles/east.png");
        m_positionBubble = sf::Vector2f(WINDOW_WIDTH-270.f*SCALE, WINDOW_HEIGHT/4.f+50.f*SCALE);
        break;
    case CardinalPoints::WEST:
        m_spriteBubble.set_texture("bubbles/west.png");
        m_positionBubble = sf::Vector2f(270.f*SCALE, WINDOW_HEIGHT/4.f+50.f*SCALE);
        break;
    case CardinalPoints::NORTH:
        m_spriteBubble.set_texture("bubbles/north.png");
        m_positionBubble = sf::Vector2f(WINDOW_WIDTH/2.f, 175.f*SCALE);
        break;
    case CardinalPoints::SOUTH:
    default:
        m_spriteBubble.set_texture("bubbles/south.png");
        m_positionBubble = sf::Vector2f(WINDOW_WIDTH/2.f, WINDOW_HEIGHT-300.f*SCALE);
        break;
    }
    m_messageBubble = Message(m_positionBubble.x, m_positionBubble.y, SIZE_FONT_BETBUBBLE,
                      L"", "GillSans.ttc", BLACK, Align::CENTER, sf::Text::Bold);

    m_centerBubble =
        m_positionBubble - rotateVector(0.f, SIZE_ARROW_BETBUBBLE/2.f,m_angleBubble);
    m_startPosition =
        m_positionBubble + rotateVector(0.f, HEIGHT_BETBUBBLE / 2.f, m_angleBubble);

    m_spriteBubble.set_position(m_startPosition);
    m_iconTrump.set_position(m_startPosition);
    m_messageBubble.set_position(m_startPosition);
    m_spriteBubble.set_scale(0);
    m_iconTrump.set_scale(0);
    m_messageBubble.set_scale(0);
    m_onlyText = true;
    m_draw = false;
}

void BetBubble::set_bet(Core::Bet bet, bool draw) {
    m_bet = bet;
    m_sizeFont = (int) SIZE_TEXT_BETBUBBLE;
    m_messageBubble.set_font_size(m_sizeFont);
    if (!draw)
        return;
    if (m_bet.trump == Core::Trump::NoTrump) {
        m_iconTrump.set_texture("tokens/sansat-noir.png");
    } else if (m_bet.trump == Core::Trump::AllTrump) {
        m_iconTrump.set_texture("tokens/toutat-noir.png");
    } else {
        m_iconTrump.set_texture("tokens/" +
                                trump_to_string(m_bet.trump) + ".png");
    }
    m_messageBubble.set_text(std::to_wstring(m_bet.value)+L"   ");
    m_onlyText = false;
    m_draw = true;

    animate();
}

void BetBubble::set_message(std::wstring const &text) {
    m_onlyText = true;
    m_draw = true;
    if(text.size()>6){
        m_sizeFont = (int) SIZE_TEXT_BETBUBBLE*6/(int) text.size();
    } else {
        m_sizeFont = (int) SIZE_TEXT_BETBUBBLE;
    }
    m_messageBubble.set_font_size(m_sizeFont);
    m_messageBubble.set_text(text); // Attention, il faut une espace en français
                                    // et pas en englais devant un '!'.
    animate();
}

void BetBubble::set_message(std::string const &textname) {
    set_message(TM.get_entry(textname));
}

void BetBubble::update(sf::RenderWindow &window) {
    m_spriteBubble.continue_move();
    if (m_draw) {
        window.draw(m_spriteBubble);
        m_messageBubble.update(window);
        if (!m_onlyText) {
            m_iconTrump.update(window);
        }
    }
}

bool BetBubble::is_animated() { return m_spriteBubble.is_animated(); }

void BetBubble::animate() {
    m_messageBubble.set_position(m_startPosition);
    m_spriteBubble.set_position(m_startPosition);
    m_iconTrump.set_position(m_startPosition);
    m_messageBubble.set_scale(0);
    m_spriteBubble.set_scale(0);
    m_iconTrump.set_scale(0);

    m_messageBubble.move(m_centerBubble- sf::Vector2f(0.f,25.f*SCALE), 0.f,
                         TIME_BETBUBBLE_ANIMATION);
    m_spriteBubble.move(m_positionBubble, 0, TIME_BETBUBBLE_ANIMATION);
    m_iconTrump.move(m_centerBubble+sf::Vector2f((float)m_sizeFont/2.f,0.f), 0.f,
                     TIME_BETBUBBLE_ANIMATION);
    m_messageBubble.zoom_sprite(1, TIME_BETBUBBLE_ANIMATION);
    m_spriteBubble.zoom_sprite(1, TIME_BETBUBBLE_ANIMATION);
    m_iconTrump.zoom_sprite(1, TIME_BETBUBBLE_ANIMATION);
}

Core::Bet BetBubble::get_bet() { return m_bet; }

void BetBubble::restart() {
    sf::Vector2f move(rotateVector(0, -HEIGHT_BETBUBBLE / 2, m_angleBubble));

    m_messageBubble.move(m_startPosition, 0, TIME_BETBUBBLE_ANIMATION);
    m_spriteBubble.move(m_startPosition, 0, TIME_BETBUBBLE_ANIMATION);
    m_iconTrump.move(m_startPosition, 0, TIME_BETBUBBLE_ANIMATION);
    m_messageBubble.zoom_sprite(0, TIME_BETBUBBLE_ANIMATION);
    m_spriteBubble.zoom_sprite(0, TIME_BETBUBBLE_ANIMATION);
    m_iconTrump.zoom_sprite(0, TIME_BETBUBBLE_ANIMATION);
}
