/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 *   \author GUIllaume.C
 *   \brief Definition of class TutoBubble
 **/

#include "Graphics.hpp"
#include "TutoBubbleSprite.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <string>

/** \class
 *   \brief A bubble used in the tutorials to display messages.
 *          Contains a TutoBubbleSprite, which is a set of sprites used to
 *          represent the bubble, a message and some buttons
 **/
class TutoBubble {
  public:
    TutoBubble() = default;

    /** \brief Non default constructor. Calls TextManager in order to retrieve
    its text \param position The position of the top-left corner of the bubble
        \param width is the wanted width of the window. Height is no parameter
    because it will depend on the length of the text and the width \param
    orientation is of enum type PointPosition defined in enumType.hpp It tells
    in which corner the entertainer's face is going to be displayed
    **/
    TutoBubble(sf::Vector2f const position,
               std::string const &key,
               float const width = 300.f);

    /** \brief Non default constructor.
        \param position The position of the top-left corner of the bubble
        \param width is the wanted width of the window. Height is no parameter
    because it will depend on the length of the text and the width \param
    orientation is of enum type PointPosition defined in enumType.hpp
    **/
    TutoBubble(sf::Vector2f const position,
               std::wstring const &text,
               float const width = 300.f);

    /** \brief Update method. Updates the object and display it on the
            RenderWindow
    **/
    void update(sf::RenderWindow &window);

    /** \brief Getter method. Says if the bubble is currently moving**/
    bool is_animated();

    /** \brief sets text by calling the TextManager **/
    void set_text(std::string const &key);

    /** \brief sets text directly (no call to TextManager) **/
    void set_text(std::wstring const &text);

    void set_position(sf::Vector2f const pos);
    void set_position(sf::Vector2i const pos);
    void set_position(float const posX, float const posY);

    sf::Text get_message_text();
    std::wstring &get_text();

    void show();
    void hide();

  private:
    static unsigned int const FONTSIZE_TUTOBUBBLE;

    /** \brief Given the number of lines of text in the bubble, computes its
     * total height in pixels **/
    float compute_height(int nb_lines);

    float m_textwidth;
    float m_height;

    std::wstring m_text;
    sf::Vector2f m_position;

    TutoBubbleSprite m_bubbleSprite;

    bool m_show;

    Message m_message;
};
