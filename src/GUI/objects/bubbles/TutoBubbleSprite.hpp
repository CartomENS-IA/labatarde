/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 *   \author GUIllaume.C
 *   \brief Definition of class TutoBubbleSprite
 **/

#include "Graphics.hpp"
#include "enumType.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <string>

// ________ Text Bubble Constant ________

extern float const OK_BUTTON_SIZE_TUTOBUBBLE;
extern float const LEFT_SPRITE_WIDTH;
extern float const RIGHT_SPRITE_WIDTH;
extern float const LEFT_TEXT_OFFSET;
extern float const TOP_TEXT_OFFSET;
extern float const BOTTOM_TEXT_OFFSET;
extern float const ENTERTAINER_SIZE;
extern float const ENTERTAINER_OFFSET;

extern sf::Vector2f const OK_OFFSET;
extern sf::Vector2f const MESSAGE_OFFSET;
// _______________________________________

class TutoBubble;

/** \class
*   \brief A set of sprites representing a bubble of customizable size.
           Mainly used in TutoBubble class, which is used in the tutorial
section
**/
class TutoBubbleSprite {

    friend class TutoBubble;
    /* This class represents some factoring of various objects. Its only use is
    to be an attribute of the TutoBubble class. Therefore, TutoBubble is a
    friend class, and TutoBubbleSprite's constructor is private. */

  public:
    TutoBubbleSprite() = default;

  private:
    /** \brief Non default constructor.
        \param position The position of the top-left corner of the bubble
        \param width is the wanted width of the window. Height is no parameter
    because it will depend on the length of the text and the width \param
    orientation is of enum type PointPosition defined in enumType.hpp It tells
    in which corner the entertainer's face is going to be displayed
    **/
    TutoBubbleSprite(
        sf::Vector2f const position,
        float const width = 300.f, float const height = 200.f);

    void set_position(float const posX, float const posY);
    void set_position(sf::Vector2f const pos);

    void update(sf::RenderWindow &window);

    /** \brief Getter method. Says if the bubble is currently moving**/
    bool is_animated();

    sf::Vector2f get_dimensions();

    float m_width;
    float m_height;

    sf::Vector2f m_origin;
    sf::Vector2f m_centerBubble;
    sf::Vector2f m_midOffset;
    sf::Vector2f m_rightOffset;
    sf::Vector2f m_entertainerOffset;

    AnimatedSprite m_spriteBubbleLeft;
    AnimatedSprite m_spriteBubbleRight;
    AnimatedSprite m_spriteBubbleMiddle;
    AnimatedSprite m_spriteEntertainer;
};
