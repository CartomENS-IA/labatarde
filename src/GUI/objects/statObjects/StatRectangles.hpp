/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

 /** \file
     \author GUIllaume.C
     \brief Definition of classes StatRectangle, MainStatRectangle,
     EloStatRectangle and PlaceStatRectangle
**/

#include <SFML/Graphics.hpp>
#include <unordered_map>
#include <string>

#include "Graphics.hpp"

/** \class **/
class StatRectangle {
public:

    template<class charT>
    void set_text_of_field(std::string const key, std::basic_string<charT>& value){
        auto found = m_fields.find(key);
        if (found == m_fields.end()){
            return;
        }
        found->second->set_text(value);
    }

    void set_field(std::string const key, std::shared_ptr<IMessage> value);
    std::shared_ptr<IMessage> get_field(std::string const key);

    virtual void update(sf::RenderWindow &) = 0;

protected:
    AnimatedSprite m_background;
    std::vector<MessPtr> m_fieldsNames;
    std::unordered_map<std::string, std::shared_ptr<IMessage>> m_fields;
};

/** \class
 *  \brief One particular instantiation of the StatRectangle class for the
 *   'main' screen of the statistics menu
 **/
class MainStatRectangle : public StatRectangle {
public:
    MainStatRectangle();

    void update(sf::RenderWindow&);
private:
    AnimatedSprite m_profilePic;
};

/** \class
 *  \brief One particular instantiation of the StatRectangle class for the 'elo'
 *   screen of the statistics menu
 **/
class EloStatRectangle : public StatRectangle {
public:
    EloStatRectangle();

    void update(sf::RenderWindow&);
private:
    AnimatedSprite m_eloMaxRect;
    AnimatedSprite m_eloCurrentRect;
};


/** \class
 *  \brief One particular instantiation of the StatRectangle class for the
 *   'place' screen of the statistics menu
 **/
class PlaceStatRectangle : public StatRectangle {
public:
    PlaceStatRectangle();

    void update(sf::RenderWindow&);
};
