/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "StatRectangles.hpp"
#include "StatManager.hpp"
#include "Constants.hpp"

MainStatRectangle::MainStatRectangle() {
    m_background = AnimatedSprite(1830.f*SCALE_TO_MAX, 381.f*SCALE_TO_MAX);
    m_background.set_texture("stats/statRect_red.png");
    m_background.set_position(WINDOW_WIDTH/2, WINDOW_HEIGHT/2);

    m_profilePic = AnimatedSprite(295.f*SCALE_TO_MAX, 295.f*SCALE_TO_MAX);
    m_profilePic.set_texture("vous.png");
    m_profilePic.set_position(WINDOW_WIDTH/2-720.f*SCALE_TO_MAX, WINDOW_HEIGHT/2);

    PlayerInfo info = statManager.get_player_info();

    // Create messages
    sf::Vector2f const leftPos(650.f*SCALE_TO_MAX, 400.f*SCALE_TO_MAX);
    sf::Vector2f const leftPosIn(700.f*SCALE_TO_MAX, 403.f*SCALE_TO_MAX);
    sf::Vector2f const rightPos(1570.f*SCALE_TO_MAX, 400.f*SCALE_TO_MAX);
    sf::Vector2f const rightPosIn(1620.f*SCALE_TO_MAX, 403.f*SCALE_TO_MAX);
    sf::Vector2f const offset(0.f, 75.f*SCALE_TO_MAX);
    unsigned const fontSize = unsigned(30.f*HALF_SCALE_TO_MAX);
    unsigned const fontSizeIn = unsigned(45.f*HALF_SCALE_TO_MAX);

    Message nameField(leftPos, fontSize, "main_name",
                      "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(nameField));
    Message name(leftPosIn, fontSizeIn, info.name,
                      "GillSans.ttc", BLACK, Align::LEFT);
    set_field("name", make_shared<Message>(name));

    Message eloField(leftPos+offset, fontSize, "main_elo",
                    "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(eloField));
    Message scoreElo(leftPosIn+offset, fontSizeIn, std::to_wstring(info.elo),
                    "GillSans.ttc", BLACK, Align::LEFT);
    set_field("score_elo", make_shared<Message>(scoreElo));

    Message titleField(leftPos+2*offset, fontSize, "main_title",
                      "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(titleField));
    Message titleElo(leftPosIn+2*offset, fontSizeIn, info.elo_title,
                      "GillSans.ttc", BLACK, Align::LEFT);
    set_field("title_elo", make_shared<Message>(titleElo));

    Message nationField(leftPos+3*offset, fontSize, "main_nation",
                        "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(nationField));
    Message nationality(leftPosIn+3*offset, fontSizeIn, info.nationality,
                        "GillSans.ttc", BLACK, Align::LEFT);
    set_field("nationality", make_shared<Message>(nationality));

    Message nbPlayedField(rightPos, fontSize, "main_nbplayed",
                          "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(nbPlayedField));
    Message nbPlayed(rightPosIn, fontSizeIn, std::to_wstring(info.nb_played),
                        "GillSans.ttc", BLACK, Align::LEFT);
    set_field("nb_played", make_shared<Message>(nbPlayed));

    Message nbWonField(rightPos+offset, fontSize, "main_nbwon",
                      "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(nbWonField));
    Message nbWon(rightPosIn+offset, fontSizeIn, std::to_wstring(info.ranks[0]),
                        "GillSans.ttc", BLACK, Align::LEFT);
    set_field("nb_won", make_shared<Message>(nbWon));

    Message percentField(rightPos+2*offset, fontSize, "main_percent",
                      "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(percentField));
    int percent = (info.nb_played>0) ?
        int(float(info.ranks[0])/float(info.nb_played)) : 0;
    Message percentMess(rightPosIn+2*offset, fontSizeIn, std::to_wstring(percent)+L"%",
                        "GillSans.ttc", BLACK, Align::LEFT);
    set_field("percent_won", make_shared<Message>(percentMess));

    Message bestField(rightPos+3*offset, fontSize, "main_best",
                      "GillSans.ttc", WHITE, Align::RIGHT);
    m_fieldsNames.push_back(make_unique<Message>(bestField));
    Message best(rightPosIn+3*offset, fontSizeIn, std::to_wstring(info.best_score),
                 "GillSans.ttc", BLACK, Align::LEFT);
    set_field("best_score", make_shared<Message>(best));
}

 void MainStatRectangle::update(sf::RenderWindow &app){
     app.draw(m_background);
     app.draw(m_profilePic);
     for (auto &mess : m_fieldsNames){
         mess->update(app);
     }
     for (auto &it : m_fields){
         it.second->update(app);
     }
 }
