/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Graphics.hpp>
#include <unordered_map>
#include <string>
#include "Message.hpp"

/** \file
 *  \author GUIllaume.C
 *  \brief Definition of class StatArray, BetStatArray, TrickStatArray
 *   and ChouneStatArray
 **/

/** \class **/
class StatArray{
public:
    void set_field(std::string const &key, std::shared_ptr<IMessage>);
    virtual void update(sf::RenderWindow &app);
protected:

    static sf::Color const BG_DARK;
    static sf::Color const BG_LIGHT;

    void set_global_position(float, float);

    std::vector<sf::RectangleShape> m_cells;
    std::unordered_map<std::string, std::shared_ptr<IMessage>> m_fields;
};

class BetStatArray : public StatArray {
public:
    BetStatArray();
};

class TrickStatArray : public StatArray {
public:
    TrickStatArray();
};

class ChouneStatArray : public StatArray {
public:
    ChouneStatArray();
};
