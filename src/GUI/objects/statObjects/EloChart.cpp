/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "EloChart.hpp"
#include "PlayerInfo.hpp"
#include "Constants.hpp"

EloChart::EloChart(){
    m_eloScale = AnimatedSprite(29.f*SCALE_TO_MAX, 739.f*SCALE_TO_MAX, false);
    m_eloScale.set_texture("stats/eloChart.png");
    m_eloScale.set_position(1310.f*SCALE_TO_MAX, 100.f*SCALE_TO_MAX);

    float const leftPos1 = 1350.f*SCALE_TO_MAX;
    float const leftPos2 = 1450.f*SCALE_TO_MAX;
    float const topPos  = 110.f*SCALE_TO_MAX;

    unsigned int const fontSize = unsigned(28*HALF_SCALE_TO_MAX);

    std::array<float,11> offsets = {0.f, 42.f, 84.f, 126.f, 168.f, 260.f, 340.f, 440.f, 520.f, 620.f, 700.f};

    std::array<std::wstring,11> scores= {L"2600", L"2500", L"2400", L"2300", L"2200", L"2000", L"1800", L"1600", L"1400", L"1200", L"1000"};

    std::array<std::string,11> titles= {"elo_top", "elo_GMI", "elo_MI", "elo_M", "elo_CM", "elo_NN", "elo_Vgood", "elo_good", "elo_casual", "elo_beginner", "elo_nov"};

    std::array<sf::Color,11> colors = {ELO_BLUE, ELO_BLUE, ELO_BLUE, ELO_BLUE, ELO_BLUE, ELO_PURPLE, ELO_PURPLE, ELO_RED, ELO_RED, ELO_GREEN, ELO_GREEN};


    for (float i = 0; i<11 ; i++){

        sf::Text::Style style;
        if (i==0 || i==4 || i==6 || i==8 || i==10){
            style = sf::Text::Bold;
        } else {
            style = sf::Text::Regular;
        }
        Message mVal(leftPos1, topPos+offsets[i]*SCALE_TO_MAX,
                     fontSize, scores[i], "GillSans.ttc",
                     colors[i], Align::LEFT, style);
        Message mTitle(leftPos2, topPos+offsets[i]*SCALE_TO_MAX,
                     fontSize, titles[i], "GillSans.ttc",
                     colors[i], Align::LEFT, style);

        m_messages.push_back(make_unique<Message>(mVal));
        m_messages.push_back(make_unique<Message>(mTitle));
    }

}

void EloChart::update(sf::RenderWindow &app) {
    app.draw(m_eloScale);
    for (auto& mess : m_messages)
        mess->update(app);
}
