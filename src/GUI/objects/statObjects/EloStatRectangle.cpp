/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "StatRectangles.hpp"
#include "StatManager.hpp"

EloStatRectangle::EloStatRectangle(){
    float const width = 264.f*SCALE_TO_MAX;
    float const height = 507.f*SCALE_TO_MAX;
    sf::Vector2f offset(50.f*SCALE_TO_MAX, 270.f*SCALE_TO_MAX);
    sf::Vector2f const centerTopPos(width/2, 0.f);

    unsigned int fontSize = unsigned(25*HALF_SCALE_TO_MAX);
    unsigned int fontSizeBig = unsigned(30*HALF_SCALE_TO_MAX);
    unsigned int fontSizeElo = unsigned(50*HALF_SCALE_TO_MAX);

    m_background = AnimatedSprite(width, height, false);
    m_background.set_texture("stats/eloRect.png");
    m_background.set_position(offset.x, offset.y);

    PlayerInfo info = statManager.get_player_info();

    // Create messages

    Message mRegister(centerTopPos+offset+sf::Vector2f(0.f,20.f*SCALE_TO_MAX),
                        fontSize, "elo_register", "GillSans.ttc",
                        WHITE, Align::CENTER);
    m_fieldsNames.push_back(make_unique<Message>(mRegister));
    Message mRegisterDate(centerTopPos+offset+sf::Vector2f(0.f,70.f*SCALE_TO_MAX),
                        fontSizeBig, info.registered_date, "GillSans.ttc",
                        WHITE, Align::CENTER, sf::Text::Bold);
    set_field("registered_date", make_shared<Message>(mRegisterDate));

    Message mRanking(centerTopPos+offset+sf::Vector2f(0.f,120.f*SCALE_TO_MAX),
                        fontSize, "elo_first_rank", "GillSans.ttc",
                        WHITE, Align::CENTER);
    m_fieldsNames.push_back(make_unique<Message>(mRanking));
    Message mRankingDate(centerTopPos+offset+sf::Vector2f(0.f,160.f*SCALE_TO_MAX),
                        fontSizeBig, info.first_ranking_date, "GillSans.ttc",
                        WHITE, Align::CENTER, sf::Text::Bold);
    set_field("first_ranking_date", make_shared<Message>(mRankingDate));

    Message mEloMax(centerTopPos+offset+sf::Vector2f(0.f,230.f*SCALE_TO_MAX),
                        fontSize, "elo_max", "GillSans.ttc",
                        WHITE, Align::CENTER);
    m_fieldsNames.push_back(make_unique<Message>(mEloMax));
    Message mEloMaxDate(centerTopPos+offset+sf::Vector2f(0.f,270.f*SCALE_TO_MAX),
                        fontSizeBig, info.elo_max_date, "GillSans.ttc",
                        WHITE, Align::CENTER, sf::Text::Bold);
    Message mEloMaxValue(centerTopPos+offset+sf::Vector2f(-10.f*SCALE_TO_MAX,320.f*SCALE_TO_MAX),
                        fontSizeElo, std::to_wstring(info.elo_max), "GillSans.ttc",
                        WHITE, Align::CENTER, sf::Text::Bold);
    set_field("elo_max_date", make_shared<Message>(mEloMaxDate));
    set_field("elo_max", make_shared<Message>(mEloMaxValue));
    m_eloMaxRect = AnimatedSprite(width-30.f*SCALE_TO_MAX, 60.f*SCALE_TO_MAX);
    m_eloMaxRect.set_position(offset.x + width/2, offset.y + 340.f*SCALE_TO_MAX);
    m_eloMaxRect.set_color(get_elo_color_main(info.elo_max));

    Message mEloCurrent(centerTopPos+offset+sf::Vector2f(0.f,400.f*SCALE_TO_MAX),
                        fontSize, "elo_current", "GillSans.ttc",
                        WHITE, Align::CENTER);
    m_fieldsNames.push_back(make_unique<Message>(mEloCurrent));
    Message mEloCurrentValue(centerTopPos+offset+sf::Vector2f(-10.f*SCALE_TO_MAX ,440.f*SCALE_TO_MAX),
                        fontSizeElo, std::to_wstring(info.elo), "GillSans.ttc",
                        WHITE, Align::CENTER, sf::Text::Bold);
    set_field("elo", make_shared<Message>(mEloCurrentValue));
    m_eloCurrentRect = AnimatedSprite(width-30.f*SCALE_TO_MAX, 60.f*SCALE_TO_MAX);
    m_eloCurrentRect.set_position(offset.x + width/2, offset.y + 460.f*SCALE_TO_MAX);
    m_eloCurrentRect.set_color(get_elo_color_main(info.elo));
}

void EloStatRectangle::update(sf::RenderWindow &app){
    app.draw(m_background);
    app.draw(m_eloMaxRect);
    app.draw(m_eloCurrentRect);
    for (auto &mess : m_fieldsNames){
        mess->update(app);
    }
    for (auto &it : m_fields){
        it.second->update(app);
    }
}
