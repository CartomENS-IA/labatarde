/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Graphics.hpp>
#include "Graphics.hpp"
#include <vector>

/** \file
    \author GUIllaume.C
    \brief Definition of class EloChart
**/

/** \class **/
class EloChart{
public:
    EloChart();

    void update(sf::RenderWindow&);
private:
    AnimatedSprite m_eloScale;
    std::vector<MessPtr> m_messages;
};
