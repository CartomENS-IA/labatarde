/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "StatRectangles.hpp"
#include <memory>

using namespace std;

void StatRectangle::set_field(string const key, shared_ptr<IMessage> value){
    auto found = m_fields.find(key);
    if (found == m_fields.end()){
        m_fields.erase(key);
    }
    m_fields.insert(make_pair(key, value));
}

shared_ptr<IMessage> StatRectangle::get_field(string const key){
    auto found = m_fields.find(key);
    if (found == m_fields.end()){
        throw(runtime_error("Key not found"));
    }
    return found->second;
}
