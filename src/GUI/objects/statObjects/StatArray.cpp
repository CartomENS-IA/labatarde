/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "StatArray.hpp"
#include "StatManager.hpp"

using namespace std;

// ____________________________ Basic StatArray ________________________________

sf::Color const StatArray::BG_DARK(18,37,90);
sf::Color const StatArray::BG_LIGHT(86,91,161);

void StatArray::set_field(string const &key, std::shared_ptr<IMessage> mess){
    m_fields[key] = mess;
}

void StatArray::update(sf::RenderWindow &app){
    for (auto &cell : m_cells){
        app.draw(cell);
    }
    for (auto &mess : m_fields){
        mess.second->update(app);
    }
}

void StatArray::set_global_position(float x, float y){
    // apply global offset
    for (auto &cell : m_cells){
        cell.move(x,y);
    }
    for (auto &mess : m_fields){
        mess.second->move_offset(x,y);
    }
}

// ________________________________ BetStatArray _______________________________

BetStatArray::BetStatArray(){

    sf::Vector2f const header_cell_size(394.f*SCALE_TO_MAX, 65.f*SCALE_TO_MAX);
    sf::Vector2f const cell_size(180.f*SCALE_TO_MAX, 65.f*SCALE_TO_MAX);
    sf::Vector2f const cell_offset(10.f*SCALE_TO_MAX, 3.f*SCALE_TO_MAX);
    sf::Vector2f const textPos(370.f*SCALE_TO_MAX, 15.f*SCALE_TO_MAX);

    unsigned int const fontSize = unsigned(35.f*HALF_SCALE_TO_MAX);

    float row = 0;
    float column = 0;
    for (auto key : {string("bet_played"), string("bet_points"), string("bet_success")}){

        Message header(textPos.x, textPos.y + row*(cell_size.y + cell_offset.y),
            fontSize, key, "GillSans.ttc", WHITE, Align::RIGHT);
        sf::RectangleShape header_box(header_cell_size);
        header_box.setFillColor(StatArray::BG_DARK);
        header_box.setPosition(sf::Vector2f(0,
            row*(header_cell_size.y+cell_offset.y)));

        Message header_percent(textPos.x, textPos.y + (row+1)*(cell_size.y +
            cell_offset.y), fontSize, "percent",
            "GillSans.ttc", WHITE, Align::RIGHT);
        sf::RectangleShape header_box_percent(header_cell_size);
        header_box_percent.setFillColor(StatArray::BG_LIGHT);
        header_box_percent.setPosition(sf::Vector2f(0,
            (row+1)*(header_cell_size.y+cell_offset.y)));

        m_cells.push_back(header_box);
        m_cells.push_back(header_box_percent);
        set_field(key+string("_header"), make_shared<Message>(header));
        set_field(key+string("_percent_header"),
            make_shared<Message>(header_percent));

        column = 0;
        for (auto &trump : {string("alltrump"), string("spade"),
                            string("heart"), string("diamond"),
                            string("club"), string("notrump")}){


            sf::Vector2f const text_offset(cell_size.x/2, cell_size.y/3);
            sf::Vector2f const cell_position(
                header_cell_size.x + cell_offset.x +
                column*(cell_size.x + cell_offset.x),
                row*(cell_size.y+cell_offset.y));
            Message value(cell_position + text_offset,
                          fontSize, L"0", "GillSans.ttc", WHITE);
            sf::RectangleShape value_box(cell_size);
            value_box.setFillColor(StatArray::BG_DARK);
            value_box.setPosition(cell_position);

            sf::Vector2f const cell_position_percent(
                header_cell_size.x + cell_offset.x +
                column*(cell_size.x + cell_offset.x),
                (row+1)*(cell_size.y+cell_offset.y));
            Message value_percent(cell_position_percent + text_offset,
                          fontSize, L"0", "GillSans.ttc", WHITE);
            sf::RectangleShape value_box_percent(cell_size);
            value_box_percent.setFillColor(StatArray::BG_LIGHT);
            value_box_percent.setPosition(cell_position_percent);

            column +=1;
            m_cells.push_back(value_box);
            m_cells.push_back(value_box_percent);
            set_field(key+string("_")+trump, make_shared<Message>(value));
            set_field(key+string("_")+trump+string("_percent"),
                make_shared<Message>(value_percent));
        }
        row+=2;
    }

    set_global_position(200.f*SCALE_TO_MAX, 220.f*SCALE_TO_MAX);

    /* TODO : retrieve player info and fill the array
    PlayerInfo info = statManager.get_player_info();
    */
}

// ________________________________ TrickStatArray _____________________________

TrickStatArray::TrickStatArray(){

    sf::Vector2f const header_cell_size(394.f*SCALE_TO_MAX, 65.f*SCALE_TO_MAX);
    sf::Vector2f const cell_size(180.f*SCALE_TO_MAX, 65.f*SCALE_TO_MAX);
    sf::Vector2f const cell_offset(10.f*SCALE_TO_MAX, 3.f*SCALE_TO_MAX);
    sf::Vector2f const textPos(370.f*SCALE_TO_MAX, 15.f*SCALE_TO_MAX);

    unsigned int const fontSize = unsigned(35.f*HALF_SCALE_TO_MAX);

    float row = 0;
    float column = 0;
    for (auto key : {string("bet_points"), string("bet_success")}){

        Message header(textPos.x, textPos.y + row*(cell_size.y + cell_offset.y),
            fontSize, key, "GillSans.ttc", WHITE, Align::RIGHT);
        sf::RectangleShape header_box(header_cell_size);
        header_box.setFillColor(StatArray::BG_DARK);
        header_box.setPosition(sf::Vector2f(0,
            row*(header_cell_size.y+cell_offset.y)));

        Message header_percent(textPos.x, textPos.y + (row+1)*(cell_size.y +
            cell_offset.y), fontSize, "percent",
            "GillSans.ttc", WHITE, Align::RIGHT);
        sf::RectangleShape header_box_percent(header_cell_size);
        header_box_percent.setFillColor(StatArray::BG_LIGHT);
        header_box_percent.setPosition(sf::Vector2f(0,
            (row+1)*(header_cell_size.y+cell_offset.y)));

        m_cells.push_back(header_box);
        m_cells.push_back(header_box_percent);
        set_field(key+string("_header"), make_shared<Message>(header));
        set_field(key+string("_percent_header"),
            make_shared<Message>(header_percent));

        column = 0;
        for (int i = 5 ; i<10 ; i++) {

            sf::Vector2f const text_offset(cell_size.x/2, cell_size.y/3);
            sf::Vector2f const cell_position(
                header_cell_size.x + cell_offset.x +
                column*(cell_size.x + cell_offset.x),
                row*(cell_size.y+cell_offset.y));
            Message value(cell_position + text_offset,
                          fontSize, L"0", "GillSans.ttc", WHITE);
            sf::RectangleShape value_box(cell_size);
            value_box.setFillColor(StatArray::BG_DARK);
            value_box.setPosition(cell_position);

            sf::Vector2f const cell_position_percent(
                header_cell_size.x + cell_offset.x +
                column*(cell_size.x + cell_offset.x),
                (row+1)*(cell_size.y+cell_offset.y));
            Message value_percent(cell_position_percent + text_offset,
                          fontSize, L"0", "GillSans.ttc", WHITE);
            sf::RectangleShape value_box_percent(cell_size);
            value_box_percent.setFillColor(StatArray::BG_LIGHT);
            value_box_percent.setPosition(cell_position_percent);

            column +=1;
            m_cells.push_back(value_box);
            m_cells.push_back(value_box_percent);
            set_field(key+string("_")+to_string(i), make_shared<Message>(value));
            set_field(key+string("_")+string("_percent_")+to_string(i),
                make_shared<Message>(value_percent));
        }
        row+=2;
    }
    set_global_position(220.f*SCALE_TO_MAX, 220.f*SCALE_TO_MAX);
}

// ________________________________ ChouneStatArray ____________________________

ChouneStatArray::ChouneStatArray() {

    float const CellHeight = 65.f*SCALE_TO_MAX;
    float const HeaderCellWidth = 726.f*SCALE_TO_MAX;
    float const ValueCellWidth = 417.f*SCALE_TO_MAX;
    float const PercentCellWidth = 180.f*SCALE_TO_MAX;

    sf::Vector2f const cell_offset(10.f*SCALE_TO_MAX, 3.f*SCALE_TO_MAX);

    sf::Vector2f const text_offset_header(705.f*SCALE_TO_MAX, CellHeight*0.3f);
    sf::Vector2f const text_offset_value(ValueCellWidth/2, CellHeight*0.3f);
    sf::Vector2f const text_offset_percent(PercentCellWidth/2, CellHeight*0.3f);

    unsigned int const fontSize = unsigned(35.f*HALF_SCALE_TO_MAX);

    float row = 0;
    for (auto key : {string("trick_mechoune"), string("trick_choune"),
                     string("trick_mechoune_you"), string("trick_choune_you")}){

        sf::Color rowColor = int(row)%2==0 ? StatArray::BG_DARK : StatArray::BG_LIGHT;
        Message header(text_offset_header.x, text_offset_header.y + row*(CellHeight+cell_offset.y),
            fontSize, key, "GillSans.ttc", WHITE, Align::RIGHT);
        sf::RectangleShape header_box(sf::Vector2f(HeaderCellWidth, CellHeight));
        header_box.setFillColor(rowColor);
        header_box.setPosition(sf::Vector2f(0, row*(CellHeight+cell_offset.y)));

        sf::Vector2f const cell_position_value(
            HeaderCellWidth + cell_offset.x,
            row*(CellHeight + cell_offset.y));
        Message value(cell_position_value + text_offset_value,
                      fontSize, L"0/0", "GillSans.ttc", WHITE);
        sf::RectangleShape value_box(sf::Vector2f(ValueCellWidth, CellHeight));
        value_box.setFillColor(rowColor);
        value_box.setPosition(cell_position_value);

        sf::Vector2f const cell_position_percent(
            HeaderCellWidth + ValueCellWidth + 2*cell_offset.x,
            row*(CellHeight + cell_offset.y));
        Message value_percent(cell_position_percent + text_offset_percent,
                      fontSize, L"0%", "GillSans.ttc", WHITE);
        sf::RectangleShape value_box_percent(sf::Vector2f(PercentCellWidth, CellHeight));
        value_box_percent.setFillColor(rowColor);
        value_box_percent.setPosition(cell_position_percent);

        m_cells.push_back(header_box);
        m_cells.push_back(value_box);
        m_cells.push_back(value_box_percent);
        set_field(key+string("_header"), make_shared<Message>(header));
        set_field(key, make_shared<Message>(value));
        set_field(key+string("_percent"),
                   make_shared<Message>(value_percent));
        row+=1;
    }

    set_global_position(220.f*SCALE_TO_MAX, 500.f*SCALE_TO_MAX);
}
