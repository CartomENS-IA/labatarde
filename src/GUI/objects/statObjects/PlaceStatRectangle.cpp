/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */
#include "StatRectangles.hpp"
#include "StatManager.hpp"
#include <array>

PlaceStatRectangle::PlaceStatRectangle(){
    m_background = AnimatedSprite(960.f*SCALE_TO_MAX, 496.f*SCALE_TO_MAX);
    m_background.set_texture("stats/placeRect_red.png");
    m_background.set_position(WINDOW_WIDTH/2, WINDOW_HEIGHT/2.5f);

    PlayerInfo info = statManager.get_player_info();

    // Create messages
    sf::Vector2f const leftPos(950.f*SCALE_TO_MAX, 250.f*SCALE_TO_MAX);
    sf::Vector2f const leftPosIn(1020.f*SCALE_TO_MAX, 250.f*SCALE_TO_MAX);
    sf::Vector2f const rightPosIn(1250.f*SCALE_TO_MAX, 250.f*SCALE_TO_MAX);
    sf::Vector2f const offset(0.f, 75.f*SCALE_TO_MAX);
    unsigned const fontSize = unsigned(35.f*HALF_SCALE_TO_MAX);
    unsigned const fontSizeIn = unsigned(45.f*HALF_SCALE_TO_MAX);

    std::array<std::string,5> keys = {"place_victory", "place_second",
                        "place_third", "place_fourth","place_left"};
    for (float i = 0 ; i<5; i++){
        sf::Text::Style style = (i==0) ? sf::Text::Bold : sf::Text::Regular;
        Message field(leftPos+i*offset, fontSize, keys[i],
                      "GillSans.ttc", WHITE, Align::RIGHT, style);
        m_fieldsNames.push_back(make_unique<Message>(field));
        unsigned value = (i<4) ? info.ranks[i] : info.nb_left;
        Message valueM(leftPosIn+i*offset, fontSizeIn, std::to_wstring(value),
                       "GillSans.ttc", BLACK, Align::LEFT, style);
        int percent = (info.nb_played>0) ?
            int( float(value)/float(info.nb_played)) : 0;
        Message percentM(rightPosIn+i*offset, fontSizeIn, std::to_wstring(percent),
                     "GillSans.ttc", BLACK, Align::LEFT, style);
        set_field("rank_"+std::to_string(i+1), make_shared<Message>(valueM));
        set_field("percent_"+std::to_string(i+1), make_shared<Message>(percentM));
    }
}

void PlaceStatRectangle::update(sf::RenderWindow &app){
    app.draw(m_background);
    for (auto &mess : m_fieldsNames){
        mess->update(app);
    }
    for (auto &it : m_fields){
        it.second->update(app);
    }
}
