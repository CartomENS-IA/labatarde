/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Loading.hpp"
#include "Constants.hpp"
#include "RessourceManager.hpp"
#include "TextManager.hpp"

using namespace std;

wstring Loading::onePt = L".";
wstring Loading::twoPt = L"..";
wstring Loading::threePt = L"...";

float const Loading::SPRITE_OFFSET = 40.f;

Loading::Loading(){}

Loading::Loading(float posX, float posY)
:
    m_loadingPhase(0), m_loadStr(TM.get_entry("loading")),
    m_loadingMessage(posX, posY, MENU_BUTTON_FONT_SIZE, L"", "GillSans.ttc",
                     BLACK, Align::LEFT),
    m_loadingAnimation(posX - SPRITE_OFFSET, posY+MENU_BUTTON_TEXT_SIZE/3, 20, 10, BLACK, BLUE, true)
{}

Loading::Loading(sf::Vector2f position) : Loading(position.x, position.y) {}
Loading::Loading(sf::Vector2i position) : Loading(sf::Vector2f(position)) {}

void Loading::update(sf::RenderWindow &window) {
    if (m_clock.getElapsedTime()> sf::seconds(0.5f)) {
        m_clock.restart();
        m_loadingPhase = (m_loadingPhase + 1) % 4;
        switch (m_loadingPhase) {
        case 1:
            m_loadingMessage.set_text(m_loadStr + onePt);
            break;
        case 2:
            m_loadingMessage.set_text(m_loadStr + twoPt);
            break;
        case 3:
            m_loadingMessage.set_text(m_loadStr + threePt);
            break;
        default:
            m_loadingMessage.set_text(m_loadStr);
            break;
        }
    }
    m_loadingMessage.update(window);
    m_loadingAnimation.update(window);
}
