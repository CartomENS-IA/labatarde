/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Button.hpp"
#include <iostream>

using namespace sf;

GraphicButton::GraphicButton(float posX, float posY,
                             std::string const &textureBasePath,
                             std::string const &textureLitPath,
                             std::string const &textureClickedPath, float width,
                             float height, bool originAtCenter)
:
    Button(),
    m_baseTexture(textureBasePath),
    m_litTexture(textureLitPath),
    m_clickedTexture(textureClickedPath),
    m_buttonSprite(width, height, originAtCenter) {
    m_buttonSprite.set_texture(textureBasePath);
    m_buttonSprite.set_position(posX, posY);
}

void GraphicButton::lock() {
    m_locked = true;
    m_buttonSprite.set_texture(RM.get_texture(m_clickedTexture));
}

void GraphicButton::unlock() {
    m_locked = false;
    m_buttonSprite.set_texture(RM.get_texture(m_baseTexture));
}

void GraphicButton::update(sf::RenderWindow &window, float const mouseX,
                           float const mouseY) {
    if (not is_locked()) {
        if ((m_buttonSprite.pos_on_sprite(mouseX, mouseY) ^ is_lit())) {
            if (is_lit()) {
                m_buttonSprite.set_texture(RM.get_texture(m_baseTexture));
            } else {
                m_buttonSprite.set_texture(RM.get_texture(m_litTexture));
            }
            m_isLit = !m_isLit;
        }
    } else {
        m_buttonSprite.set_texture(RM.get_texture(m_clickedTexture));
    }
    m_buttonSprite.update(window);
}

void GraphicButton::update(sf::RenderWindow &window,
                           sf::Vector2f const mousePos) {
    update(window, mousePos.x, mousePos.y);
}

void GraphicButton::update(sf::RenderWindow &window,
                           sf::Vector2i const mousePos) {
    update(window, sf::Vector2f(mousePos));
}

// ______ Movement of sprites ______

void GraphicButton::set_position(float const posX, float const posY) {
    m_buttonSprite.set_position(posX, posY);
}

void GraphicButton::set_position(sf::Vector2f const pos) {
    set_position(pos.x, pos.y);
}

void GraphicButton::set_position(sf::Vector2i const pos) {
    set_position(sf::Vector2f(pos));
}

void GraphicButton::move(sf::Vector2i const position, float angle,
                         float const timing) {
    m_buttonSprite.move(Vector2f(position), angle, timing);
}

void GraphicButton::move_offset(float const x, float const y, float angle,
                                  float const speed) {
    m_buttonSprite.move_relative(x, y, angle, speed);
}

void GraphicButton::move_offset(sf::Vector2f offset, float angle,
                                  float const speed) {
    m_buttonSprite.move_relative(offset, angle, speed);
}

void GraphicButton::move_offset(sf::Vector2i offset, float angle,
                                  float const speed) {
    move_offset(sf::Vector2f(offset), angle, speed);
}

void GraphicButton::set_angle(float angle) { m_buttonSprite.set_angle(angle); }
