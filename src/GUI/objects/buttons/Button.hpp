/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "AnimatedSprite.hpp"
#include "TimerSprite.hpp"
#include "Message.hpp"
#include <memory>

/** \file Button.hpp
 * \author Julien.D , GUIllaume.C
 *  \brief Definition of the three button classes (Button, TextButton,
 * GraphicButton)
 **/

/** \class
 * \brief Implementation of a simple clickable button in SFML
 *
 * This class is supposed to be abstract :
 * it contains code that has been factorized between different types of Buttons
 **/
class Button {
  public:
    Button();

    /** \brief Getter for the private attribute m_isLit.
     *      A button is lit if the mouse's position intersects its rect.
     **/
    bool is_lit() const;

    /** \brief getter for the private attribute m_locked. **/
    bool is_locked() const;

    /** \brief Locks the button into its clicked mode.
     * This allows to make button selector (see Selector.hpp for more
     *information)
     **/
    virtual void lock();

    /** \brief unlocks a button. Gives back its normal behavior. **/
    virtual void unlock();

    virtual void update(sf::RenderWindow &, sf::Vector2f const ) = 0;
    virtual void update(sf::RenderWindow &, sf::Vector2i const ) = 0;
    virtual void update(sf::RenderWindow &, float const, float const) = 0;
    virtual void set_position(float const posX, float const posY) = 0;
    virtual void set_position(sf::Vector2f const pos) = 0;
    virtual void set_position(sf::Vector2i const pos) = 0;

  protected:
    bool m_isLit;
    bool m_locked;
};

typedef std::unique_ptr<Button> ButPtr;

/** \class
 * \brief Implementation of a Button whose hitbox is only text
 *
 * TextButton inherits from Button. It also inherits from Message, thus it
 * shares its constructor's parameters (see Message.hpp for a more detailed
 * description)
 **/
class TextButton : public Button, public Message {
  public:
    /** \brief TextButton's constructor.
     *    \param text The key for looking up the TextManager data container in
     *order to retrieve the button text
     **/
    TextButton(float posX, float posY, unsigned int charSize, std::string const &textKey,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);

    TextButton(sf::Vector2f pos, unsigned int charSize, std::string const &textKey,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);



    /** \brief TextButton's constructor.
     *    \param text The button text. No access to TextManager in that case
     **/
    TextButton(float posX, float posY, unsigned int charSize, std::wstring const &text,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);

    TextButton(sf::Vector2f pos, unsigned int charSize, std::wstring const &text,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);

    /** \brief update the state of the button (lit , not lit, selected, not
    selected) and draws it on screen
    **/
    void update(sf::RenderWindow &app, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &app, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1.);


    void set_position(float const posX, float const posY);
    void set_position(sf::Vector2f const pos);
    void set_position(sf::Vector2i const pos);

    void lock();
    void unlock();

    /** \brief Allow the user to change the text of the button. Calls
     * TextManager **/
    void set_text(std::string const &);
    /** \brief Allow the user to change the text of the button. Does not call
     * TextManager **/
    void set_text(std::wstring const &);

  private:
    sf::Color m_color;
    sf::Color m_litColor;
    sf::Color m_clickedColor;
    sf::FloatRect m_buttonRect;
};


/** \class
 * \brief Implementation of a Button whose hitbox is only text
 *
 * Unlike TextButton, MultiTextButton inherits from MultiMessage. This allows
 *  to display the text over several resolutions
 **/
class MultiTextButton : public Button, public MultiLineMessage {
  public:
    /** \brief MultiTextButton's constructor.
     *    \param text The key for looking up the TextManager data container in
     *order to retrieve the button text
     **/
    MultiTextButton(float posX, float posYTop, unsigned int charSize,
               std::string const &textKey, char delimiter,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);

    MultiTextButton(sf::Vector2f pos, unsigned int charSize,
               std::string const &textKey, char delimiter,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);

    /** \brief MultiTextButton's constructor.
     *    \param text The button text. No access to TextManager in that case
     **/
    MultiTextButton(float posX, float posYTop, unsigned int charSize,
               std::vector<std::wstring> const &texts,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);

    MultiTextButton(sf::Vector2f pos, unsigned int charSize,
               std::vector<std::wstring> const &text,
               std::string const &fontname, sf::Color const &col,
               sf::Color const &litCol, sf::Color const &clickedCol,
               Align align = Align::CENTER,
               sf::Text::Style style = sf::Text::Regular);

    /** \brief update the state of the button (lit , not lit, selected, not
    selected) and draws it on screen
    **/
    void update(sf::RenderWindow &app, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &app, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1.);

    void set_position(float const posX, float const posY);
    void set_position(sf::Vector2f const pos);
    void set_position(sf::Vector2i const pos);

    void lock();
    void unlock();

    /** \brief Allow the user to change the text of the button. Calls
     * TextManager
     * \TODO
     **/
    void set_text(std::string const &, char delimiter=';');
    /** \brief Allow the user to change the text of the button. Does not call
     * TextManager
     * \TODO
     **/
    void set_text(std::vector<std::wstring> const &);

  private:
    std::vector<sf::FloatRect> m_buttonRectVector;

    /** To be called when the position has changed **/
    void update_rects();

    /** Returns true if and only if the given position intersects
     * one of the rects **/
    bool contains(float const x, float const y);

    sf::Color m_color;
    sf::Color m_litColor;
    sf::Color m_clickedColor;
};

/** \class
* \brief Implementation of a Button whose hitbox is a texture
*
* GraphicButton inherits from Button
**/
class GraphicButton : public Button {
  public:
    GraphicButton() = default;

    /** \brief GraphicButton's constructor **/
    GraphicButton(float posX, float posY, std::string const &textureBasePath,
                  std::string const &textureLitPath,
                  std::string const &textureClickedPath, float width,
                  float height, bool originAtCenter = true);

    void set_position(float const posX, float const posY);
    void set_position(sf::Vector2f const pos);
    void set_position(sf::Vector2i const pos);

    /** \brief Make the button move in an animated way towards (x,y) **/
    void move(float const x, float const y, float angle = 0,
              float const speed = 1);
    void move(sf::Vector2f const position, float angle = 0,
              float const speed = 1);
    void move(sf::Vector2i const position, float angle = 0,
              float const speed = 1);

    /** \brief same as move but gives an offset instead of an absolute
     * destination position **/
    void move_offset(float const x, float const y, float angle = 0,
                       float const speed = 1);
    void move_offset(sf::Vector2f const position, float angle = 0,
                       float const speed = 1);
    void move_offset(sf::Vector2i const position, float angle = 0,
                       float const speed = 1);

    /** \brief implementation of the lock method of Button for GraphicButton **/
    void lock();
    void unlock();

    /** \brief update the state of the button (lit , not lit, selected, not
     *selected) and draws it on screen
     **/
    void update(sf::RenderWindow &app, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &app, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1.);

    void set_angle(float angle);

  private:
    std::string m_baseTexture;
    std::string m_litTexture;
    std::string m_clickedTexture;
    AnimatedSprite m_buttonSprite;
};

/**
 * \class
 * \brief Implementation of a Button which has a sprite and a test underneath or on it
 *
 * HydribButton inherits from Button
 **/
class HybridButton : public Button {
  public:

     enum class HoverMode {
         SCALING,
         DARKEN
     };

    /** \brief Constructor.
     *  \param text The key for looking up the TextManager data container in
     *         order to retrieve the button text
     **/
    HybridButton(float const posX, float const posY,
                 float const width, float const height,
                 std::string const &texturePath,
                 std::string const &textKey, unsigned int charSize,
                 std::string const &fontname, sf::Color const &col,
                 sf::Color const &litCol, sf::Color const &clickedCol,
                 Align align = Align::CENTER,
                 sf::Text::Style style = sf::Text::Regular);

    /** \brief Constructor's variant where the position is given as a sf::Vector2f **/
    HybridButton(sf::Vector2f const pos,
                 float const width, float const height,
                 std::string const &texturePath,
                 std::string const &textKey, unsigned int charSize,
                 std::string const &fontname, sf::Color const &col,
                 sf::Color const &litCol, sf::Color const &clickedCol,
                 Align align = Align::CENTER,
                 sf::Text::Style style = sf::Text::Regular);

    /** \brief Constructor.
     *  \param text The button text. No access to TextManager in that case
     **/
     HybridButton(float const posX, float const posY,
                  float const width, float const height,
                  std::string const &texturePath,
                  std::wstring const &text, unsigned int charSize,
                  std::string const &fontname, sf::Color const &col,
                  sf::Color const &litCol, sf::Color const &clickedCol,
                  Align align = Align::CENTER,
                  sf::Text::Style style = sf::Text::Regular);

     /** \brief Constructor's variant where the position is given as a sf::Vector2f **/
     HybridButton(sf::Vector2f const pos,
                  float const width, float const height,
                  std::string const &texturePath,
                  std::wstring const &text, unsigned int charSize,
                  std::string const &fontname, sf::Color const &col,
                  sf::Color const &litCol, sf::Color const &clickedCol,
                  Align align = Align::CENTER,
                  sf::Text::Style style = sf::Text::Regular);


    /** \brief update the state of the button (lit , not lit, selected, not
    selected) and draws it on screen
    **/
    void update(sf::RenderWindow &app, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &app, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1.);

    void set_position(float const posX, float const posY);
    void set_position(sf::Vector2f const pos);
    void set_position(sf::Vector2i const pos);

    void set_text_on_button();
    void set_text_under_button();
    void move_text(float const offsetY);
    void adjust_width_to_text();

    void set_hover_mode(HoverMode const);
    void set_darken_color(sf::Color const);

    /** \brief Implementation of the lock method of class Button **/
    void lock();
    void unlock();

  private:
    bool m_textOnButton;
    float m_width;
    float m_height;
    sf::Color m_color;
    sf::Color m_litColor;
    sf::Color m_clickedColor;
    sf::Color m_darkenColor;
    Message m_message;
    AnimatedSprite m_buttonSprite;
    HoverMode m_hoverMode;
};

/**
 * \class
 * \brief Implementation of a Button which has a timer sprite and a test
 *  underneath or on it
 *
 * TimerButton inherits from Button
 **/
class TimerButton : public Button {
  public:

    /** \brief Constructor.
     *  \param text The key for looking up the TextManager data container in
     *         order to retrieve the button text
     **/
    TimerButton(float const posX, float const posY,
                 float const width, float const height,
                 std::string const &textureFull,
                 std::string const &textureEmpty,
                 std::string const &textKey, unsigned int charSize,
                 std::string const &fontname, sf::Color const &col,
                 sf::Color const &litCol, sf::Color const &clickedCol,
                 Align align = Align::CENTER,
                 sf::Text::Style style = sf::Text::Regular);

    /** \brief Constructor's variant where the position is given as a sf::Vector2f **/
    TimerButton(sf::Vector2f const pos,
                 float const width, float const height,
                 std::string const &textureFull,
                 std::string const &textureEmpty,
                 std::string const &textKey, unsigned int charSize,
                 std::string const &fontname, sf::Color const &col,
                 sf::Color const &litCol, sf::Color const &clickedCol,
                 Align align = Align::CENTER,
                 sf::Text::Style style = sf::Text::Regular);

    /** \brief Constructor.
     *  \param text The button text. No access to TextManager in that case
     **/
     TimerButton(float const posX, float const posY,
                  float const width, float const height,
                  std::string const &textureFull,
                  std::string const &textureEmpty,
                  std::wstring const &text, unsigned int charSize,
                  std::string const &fontname, sf::Color const &col,
                  sf::Color const &litCol, sf::Color const &clickedCol,
                  Align align = Align::CENTER,
                  sf::Text::Style style = sf::Text::Regular);

     /** \brief Constructor's variant where the position is given as a sf::Vector2f **/
     TimerButton(sf::Vector2f const pos,
                  float const width, float const height,
                  std::string const &textureFull,
                  std::string const &textureEmpty,
                  std::wstring const &text, unsigned int charSize,
                  std::string const &fontname, sf::Color const &col,
                  sf::Color const &litCol, sf::Color const &clickedCol,
                  Align align = Align::CENTER,
                  sf::Text::Style style = sf::Text::Regular);


    /** \brief update the state of the button (lit , not lit, selected, not
    selected) and draws it on screen
    **/
    void update(sf::RenderWindow &app, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &app, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1.);

    void set_position(float const posX, float const posY);
    void set_position(sf::Vector2f const pos);
    void set_position(sf::Vector2i const pos);

    void set_text_on_button();
    void set_text_under_button();
    void adjust_width_to_text();

    /** \brief Implementation of the lock method of class Button **/
    void lock();
    void unlock();

    void set_filling(float fill);

  private:
    bool m_textOnButton;
    float m_width;
    float m_height;
    sf::Color m_color;
    sf::Color m_litColor;
    sf::Color m_clickedColor;
    Message m_message;
    TimerSprite m_buttonSprite;
};
