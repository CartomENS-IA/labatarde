#include "Button.hpp"
#include "TextManager.hpp"
#include "stringSplit.hpp"

using namespace std;

MultiTextButton::MultiTextButton(float posX, float posY, unsigned int charSize,
                       string const &textKey, char delimiter,
                       string const &fontname,
                       sf::Color const &col, sf::Color const &litCol,
                       sf::Color const &clickedCol, Align align,
                       sf::Text::Style style)
: MultiTextButton(posX, posY, charSize,
             split_string_separator(TM.get_entry(textKey), delimiter),
             fontname, col, litCol, clickedCol, align, style)
{}

MultiTextButton::MultiTextButton(sf::Vector2f pos, unsigned int charSize,
                       string const &textKey, char delimiter,
                       string const &fontname,
                       sf::Color const &col, sf::Color const &litCol,
                       sf::Color const &clickedCol, Align align,
                       sf::Text::Style style)
: MultiTextButton(pos.x, pos.y, charSize,
             split_string_separator(TM.get_entry(textKey), delimiter),
             fontname, col, litCol, clickedCol, align, style)
{}


MultiTextButton::MultiTextButton(float posX, float posY, unsigned int charSize,
                       vector<wstring> const &textKey,
                       string const &fontname, sf::Color const &col,
                       sf::Color const &litCol, sf::Color const &clickedCol,
                       Align align, sf::Text::Style style)
:
    Button(),
    MultiLineMessage(posX, posY, charSize, textKey, fontname, col, align, style),
    m_color(col),
    m_litColor(litCol),
    m_clickedColor(clickedCol)
{
    update_rects();
}

MultiTextButton::MultiTextButton(sf::Vector2f pos, unsigned int charSize,
                       vector<wstring> const &text,
                       string const &fontname, sf::Color const &col,
                       sf::Color const &litCol, sf::Color const &clickedCol,
                       Align align, sf::Text::Style style)
    : MultiTextButton(pos.x, pos.y, charSize, text, fontname, col, litCol, clickedCol,
                 align, style) {}

void MultiTextButton::lock() {
    m_locked = true;
    set_color(m_clickedColor);
}

void MultiTextButton::unlock() {
    m_locked = false;
    set_color(m_color);
}

void MultiTextButton::update(sf::RenderWindow &window, float const mouseX,
                        float const mouseY) {
    if (not is_locked()) {
        if (contains(mouseX, mouseY) ^ is_lit()) {
            if (is_lit()) {
                set_color(m_color);
            } else {
                set_color(m_litColor);
            }
            m_isLit = !m_isLit;
        }
    } else {
      set_color(m_clickedColor);
    }
    MultiLineMessage::update(window);
}

void MultiTextButton::update(sf::RenderWindow &window, sf::Vector2f const mousePos) {
    update(window, mousePos.x, mousePos.y);
}

void MultiTextButton::update(sf::RenderWindow &window, sf::Vector2i const mousePos) {
    update(window, sf::Vector2f(mousePos));
}

void MultiTextButton::set_position(float const posX, float const posY) {
    MultiLineMessage::set_position(posX, posY);
    update_rects();
}

void MultiTextButton::set_position(sf::Vector2f const pos) {
    set_position(pos.x, pos.y);
}

void MultiTextButton::set_position(sf::Vector2i const pos) {
    set_position(float(pos.x), float(pos.y));
}

void MultiTextButton::update_rects(){
    m_buttonRectVector.clear();
    for (auto &m : m_messages){
        m_buttonRectVector.push_back(m.get_text().getGlobalBounds());
    }
}

bool MultiTextButton::contains(float const x, float const y){
    for (auto &r : m_buttonRectVector){
        if (r.contains(x,y)){
            return true;
        }
    }
    return false;
}
