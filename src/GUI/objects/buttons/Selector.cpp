/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Selectors.hpp"

Selector::Selector(int size, int selected)
: m_size(size), m_selected(selected) {}

void Selector::change_selected(int new_selected) {
    m_selected = new_selected%m_size;
}

int Selector::who_is_selected() const {
    return m_selected;
}

int Selector::get_size() const {
    return m_size;
}
