/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Selectors.hpp"

using namespace std;

CycleSelector::CycleSelector(float const posX, float const posY,
                            vector<unique_ptr<Button>> &buttons,
                            int selected)
: Selector(int(buttons.size()), selected) {
    for (unsigned int i = 0 ; i < buttons.size() ; i++) {
        m_buttons.push_back(move(buttons[i]));
        m_buttons[i]->set_position(posX,posY);
    }
}

/** \brief Critical method: changes the currently selected button **/
void CycleSelector::change_selected(int i) {
    m_selected = i>=0 ? i : (m_selected+1)%m_size;
}


void CycleSelector::update(sf::RenderWindow &app, sf::Vector2f const pos, bool clicked) {
    update(app, pos.x, pos.y, clicked);
}

void CycleSelector::update(sf::RenderWindow &app, sf::Vector2i const pos, bool clicked) {
    update(app, float(pos.x), float(pos.y), clicked);
}

void CycleSelector::update(sf::RenderWindow &app, float const mouseX,
                           float const mouseY, bool clicked)
{
    m_buttons[who_is_selected()]->update(app, mouseX, mouseY);
    if (m_buttons[who_is_selected()]->is_lit() and clicked)
        change_selected();
}
