/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Button.hpp"
#include "TextManager.hpp"

TextButton::TextButton(float posX, float posY, unsigned int charSize,
                       std::string const &textKey, std::string const &fontname,
                       sf::Color const &col, sf::Color const &litCol,
                       sf::Color const &clickedCol, Align align,
                       sf::Text::Style style)
: TextButton(posX, posY, charSize,
             TM.get_entry(textKey), fontname,
             col, litCol, clickedCol, align, style)
{}

TextButton::TextButton(sf::Vector2f pos, unsigned int charSize,
                       std::string const &textKey, std::string const &fontname,
                       sf::Color const &col, sf::Color const &litCol,
                       sf::Color const &clickedCol, Align align,
                       sf::Text::Style style)
: TextButton(pos.x, pos.y, charSize,
             TM.get_entry(textKey), fontname,
             col, litCol, clickedCol, align, style)
{}


TextButton::TextButton(float posX, float posY, unsigned int charSize,
                       std::wstring const &textKey,
                       std::string const &fontname, sf::Color const &col,
                       sf::Color const &litCol, sf::Color const &clickedCol,
                       Align align, sf::Text::Style style)
:
    Button(),
    Message(posX, posY, charSize, textKey, fontname, col, align, style),
    m_color(col),
    m_litColor(litCol),
    m_clickedColor(clickedCol)
{
    m_buttonRect = m_messageText.getGlobalBounds();
}

TextButton::TextButton(sf::Vector2f pos, unsigned int charSize, std::wstring const &text,
                       std::string const &fontname, sf::Color const &col,
                       sf::Color const &litCol, sf::Color const &clickedCol,
                       Align align, sf::Text::Style style)
    : TextButton(pos.x, pos.y, charSize, text, fontname, col, litCol, clickedCol,
                 align, style) {}

void TextButton::lock() {
    m_locked = true;
    set_color(m_clickedColor);
}

void TextButton::unlock() {
    m_locked = false;
    set_color(m_color);
}

void TextButton::update(sf::RenderWindow &window, float const mouseX,
                        float const mouseY) {
    if (not is_locked()) {
        if ((m_buttonRect.contains(mouseX, mouseY)) ^ is_lit()) {
            if (is_lit()) {
                set_color(m_color);
            } else {
                set_color(m_litColor);
            }
            m_isLit = !m_isLit;
        }
    }
    Message::update(window);
}

void TextButton::update(sf::RenderWindow &window, sf::Vector2f const mousePos) {
    update(window, mousePos.x, mousePos.y);
}

void TextButton::update(sf::RenderWindow &window, sf::Vector2i const mousePos) {
    update(window, sf::Vector2f(mousePos));
}

void TextButton::set_position(float const posX, float const posY) {
    Message::set_position(posX, posY);
    m_buttonRect = m_messageText.getGlobalBounds();
}

void TextButton::set_position(sf::Vector2f const pos) {
    set_position(pos.x, pos.y);
}

void TextButton::set_position(sf::Vector2i const pos) {
    set_position(float(pos.x), float(pos.y));
}


void TextButton::set_text(std::string const &key) {
    Message::set_text(key);
    m_buttonRect = m_messageText.getGlobalBounds();
}

void TextButton::set_text(std::wstring const &text) {
    Message::set_text(text);
    m_buttonRect = m_messageText.getGlobalBounds();
}
