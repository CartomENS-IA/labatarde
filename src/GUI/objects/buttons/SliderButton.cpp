/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "SliderButton.hpp"
#include "Constants.hpp"
#include "SoundManager.hpp"
#include <iostream>

using namespace sf;

SliderButton::SliderButton(float posX, float posY, float disk_size,
                           float length, sf::Color color,
                           std::string const &fontname, float base_value)
:
    m_x(posX),  m_y(posY), m_value(base_value),
    m_color(color), m_litColor(BLACK_LIT), m_selectColor(GRAY),
    m_radius(disk_size), m_circle(sf::CircleShape(disk_size)),
    m_moving(false)
{
    m_length = length* SCALE;
    m_line[0] = sf::Vertex(sf::Vector2f(m_x, m_y));
    m_line[0].color = m_color;
    m_line[1] = sf::Vertex(sf::Vector2f(m_x + m_length, m_y));
    m_line[1].color = m_color;
    m_circle.setPosition(m_x + m_value * m_length - m_radius, m_y - m_radius);
    m_circle.setFillColor(m_color);
    m_text = Message(posX + m_length + 4.f*m_radius, posY - m_radius,
                     (int)(3.f * (10.f + m_radius) /2.f),
                     std::to_wstring((int)(m_value * 100)),
                     fontname, color);
}

float SliderButton::get_value() { return m_value; }

void SliderButton::update(sf::RenderWindow &app, bool mouseLeftClick,
                          float const mouseX, float const mouseY)
{
    bool mouseInBounds = m_circle.getGlobalBounds().contains(mouseX, mouseY);
    if (not m_moving and mouseInBounds) {
        if (mouseLeftClick) {
            soundManager.play_sound("click.wav");
            m_moving = true;
            m_circle.setFillColor(m_selectColor);
        } else {
            m_circle.setFillColor(m_litColor);
        }
    } else if (m_moving and not mouseLeftClick) {
        soundManager.play_sound("click.wav");
        m_moving = false;
        m_circle.setFillColor(m_color);
    } else if (not mouseInBounds and not m_moving) {
        m_circle.setFillColor(m_color);
    }
    if (m_moving) {
        if (mouseX >= m_x and mouseX <= m_x + m_length) {
            m_value = (mouseX - m_x) / m_length;
            m_circle.setPosition(mouseX - m_radius, m_y - m_radius);
            m_text.set_text(std::to_wstring((int)(m_value * 100)));
        } else if (mouseX < m_x and m_value != 0) {
            m_value = 0;
            m_circle.setPosition(m_x - m_radius, m_y - m_radius);
            m_text.set_text(std::to_wstring(0));
        } else if (mouseX > m_x + m_length and m_value != 1) {
            m_value = 1;
            m_circle.setPosition(m_x + m_length - m_radius, m_y - m_radius);
            m_text.set_text(std::to_wstring(100));
        }
    }
    m_text.update(app);
    app.draw(m_line, 2, sf::Lines);
    app.draw(m_circle);
}

void SliderButton::update(sf::RenderWindow &app, bool mouseLeftClick,
                          sf::Vector2f mousePos) {
    update(app, mouseLeftClick, mousePos.x, mousePos.y);
}

void SliderButton::update(sf::RenderWindow &app, bool mouseLeftClick,
                          sf::Vector2i mousePos) {
    update(app, mouseLeftClick, sf::Vector2f(mousePos));
}
