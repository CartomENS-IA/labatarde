/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_SLIDER_BUTTON
#define DEF_SLIDER_BUTTON

#include "Button.hpp"
#include "Message.hpp"

/** \file SliderButton.hpp
 * \author Julien D.
 *  \brief Definition of the SliderButton class
 **/

/** \class
 * \brief The goal of this class is to make Buttons that will slide following an
 *axis. It can be useful for example to set the volume of the sound. \brief
 *Implementation of a sliding button in SFML
 **/

class SliderButton {
  public:
    /** \brief construtor of SliderButton **/
    SliderButton(float posX, float posY, float disk_size, float length,
                 sf::Color color, std::string const &fontname,
                 float baseValue = 0);

    /** \brief returns the value currently taken by the button. Between 0 (full
     * left) and 1 (full right) **/
    float get_value();

    void update(sf::RenderWindow &app, bool mouseLeftClick,
                float const mouseX = -1., float const mouseY = -1.);

    void update(sf::RenderWindow &app, bool mouseLeftClick,
                sf::Vector2f const mousePos);

    void update(sf::RenderWindow &app, bool mouseLeftClick,
                sf::Vector2i const mousePos);

  private:
    float m_x;
    float m_y;
    float m_value;
    sf::Color m_color;
    sf::Color m_litColor;
    sf::Color m_selectColor;

    float m_radius;
    sf::CircleShape m_circle;

    float m_length;
    sf::Vertex m_line[2];

    bool m_moving;
    Message m_text;
};

#endif
