#include "Selectors.hpp"
#include "SoundManager.hpp"

OnOffSelector::OnOffSelector(sf::Vector2f const pos, unsigned const fontSize,
    std::string const &fontname, sf::Color col, sf::Color litCol,
    sf::Color clickedCol, bool selected)
:
    m_isOn(false),
    m_on(pos.x - float(fontSize), pos.y, fontSize, L"On", fontname, col, litCol, clickedCol, Align::RIGHT),
    m_off(pos.x + float(fontSize), pos.y, fontSize, L"Off", fontname, col, litCol, clickedCol, Align::LEFT),
    m_sep(pos,fontSize, L"/", fontname, clickedCol)
{
    set(selected);
}

OnOffSelector::OnOffSelector(float const posX, float const posY,
    unsigned const fontSize, std::string const &fontname, sf::Color col,
    sf::Color litCol, sf::Color clickedCol, bool selected)
: OnOffSelector(sf::Vector2f(posX,posY), fontSize, fontname, col,
                litCol, clickedCol, selected) {}

bool OnOffSelector::is_on() {
    return m_isOn;
}

void OnOffSelector::set(bool pos){
    m_isOn = pos;
    if (m_isOn){
        m_on.lock();
        m_off.unlock();
    } else {
        m_on.unlock();
        m_off.lock();
    }
}

void OnOffSelector::toggle(){
    set(!m_isOn);
}

void OnOffSelector::update(sf::RenderWindow & app, sf::Vector2f const mouse, bool clicked) {
    update(app, mouse.x, mouse.y, clicked);
}

void OnOffSelector::update(sf::RenderWindow &app, sf::Vector2i const mouse, bool clicked) {
    update(app, float(mouse.x), float(mouse.y), clicked);
}

void OnOffSelector::update(sf::RenderWindow &app, float const mouseX,
                float const mouseY, bool clicked){
    m_on.update(app,mouseX, mouseY);
    m_off.update(app, mouseX, mouseY);
    if (!m_isOn && m_on.is_lit() && clicked){
        soundManager.play_sound("click.wav");
        set(true);
    } else if (m_isOn && m_off.is_lit() && clicked){
        soundManager.play_sound("click.wav");
        set(false);
    }
    m_sep.update(app);
}
