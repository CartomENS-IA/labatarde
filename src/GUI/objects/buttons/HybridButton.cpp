/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Button.hpp"
#include "TextManager.hpp"
#include "Constants.hpp"

HybridButton::HybridButton(float const posX, float const posY,
             float const width, float const height,
             std::string const &texturePath,
             std::string const &textKey, unsigned int charSize,
             std::string const &fontname, sf::Color const &col,
             sf::Color const &litCol, sf::Color const &clickedCol,
             Align align, sf::Text::Style style)
:
HybridButton(posX, posY, width, height, texturePath, TM.get_entry(textKey),
             charSize, fontname, col, litCol, clickedCol,
             align, style)
{}

HybridButton::HybridButton(sf::Vector2f const pos,
             float const width, float const height,
             std::string const &texturePath,
             std::string const &textKey, unsigned int charSize,
             std::string const &fontname, sf::Color const &col,
             sf::Color const &litCol, sf::Color const &clickedCol,
             Align align, sf::Text::Style style)
: HybridButton(pos.x, pos.y, width, height, texturePath, textKey,
               charSize, fontname, col, litCol,
               clickedCol, align, style)
{}


 HybridButton::HybridButton(float const posX, float const posY,
              float const width, float const height,
              std::string const &texturePath,
              std::wstring const &text, unsigned int charSize,
              std::string const &fontname, sf::Color const &col,
              sf::Color const &litCol, sf::Color const &clickedCol,
              Align align, sf::Text::Style style)
:
    Button(),
    m_textOnButton(false),
    m_width(width),
    m_height(height),
    m_color(col),
    m_litColor(litCol),
    m_clickedColor(clickedCol),
    m_darkenColor(CARD_GRAY),
    m_message(posX, posY+3.f*height/4.f, charSize, text, fontname, col, align, style),
    m_buttonSprite(width,height),
    m_hoverMode(HoverMode::SCALING)
{
    m_buttonSprite.set_texture(texturePath);
    m_buttonSprite.set_position(posX,posY);
}


HybridButton::HybridButton(sf::Vector2f const pos,
             float const width, float const height,
             std::string const &texturePath,
             std::wstring const &text, unsigned int charSize,
             std::string const &fontname, sf::Color const &col,
             sf::Color const &litCol, sf::Color const &clickedCol,
             Align align, sf::Text::Style style)
: HybridButton(pos.x, pos.y, width, height, texturePath, text,
             charSize, fontname, col, litCol,
             clickedCol, align, style)
{}

void HybridButton::update(sf::RenderWindow &app, sf::Vector2f const mousePos) {
    update(app, mousePos.x, mousePos.y);
}

void HybridButton::update(sf::RenderWindow &app, sf::Vector2i const mousePos) {
    update(app, float(mousePos.x), float(mousePos.y));
}

void HybridButton::update(sf::RenderWindow &app,
        float const mouseX, float const mouseY)
{
    if (not is_locked()) {
        if (m_buttonSprite.pos_on_sprite(mouseX, mouseY)^is_lit())
        {
            if (is_lit()) {
                if (m_hoverMode== HoverMode::SCALING){
                    m_buttonSprite.set_scale(1.f);
                } else {
                    m_buttonSprite.set_color(m_darkenColor);
                }
                m_message.set_color(m_color);
            } else {
                if (m_hoverMode== HoverMode::SCALING){
                    m_buttonSprite.set_scale(1.15f);
                } else {
                    m_buttonSprite.set_color(WHITE);
                }
                m_message.set_color(m_litColor);
            }
            m_isLit = !m_isLit;
        }
    }
    m_buttonSprite.update(app);
    m_message.update(app);
}

void HybridButton::set_position(float const posX, float const posY) {
    m_message.set_position(posX, posY);
}

void HybridButton::set_position(sf::Vector2f const pos) {
    set_position(pos.x, pos.y);
}

void HybridButton::set_position(sf::Vector2i const pos) {
    set_position(sf::Vector2f(pos));
}


void HybridButton::set_text_on_button() {
    if (not m_textOnButton){
        m_message.move_offset(0.f, -m_height);
        m_textOnButton = true;
    }
}

void HybridButton::set_text_under_button() {
    if (m_textOnButton) {
        m_message.move_offset(0.f, m_height);
        m_textOnButton = false;
    }
}

void HybridButton::set_darken_color(sf::Color const col){
    m_darkenColor = col;
}

void HybridButton::move_text(float const offsetY) {
  m_message.move_offset(0.f, offsetY);
}

/** \brief Implementation of the virtual lock method of class Button **/
void HybridButton::lock() {
    m_locked = true;
    m_message.set_color(m_clickedColor);
    if (m_hoverMode==HoverMode::SCALING){
        m_buttonSprite.set_scale(1.15f);
    } else {
        m_buttonSprite.set_color(WHITE);
    }
}

void HybridButton::unlock(){
    m_locked = false;
    m_message.set_color(m_color);
    if (m_hoverMode==HoverMode::SCALING){
        m_buttonSprite.set_scale(1.f);
    } else {
        m_buttonSprite.set_color(m_darkenColor);
    }
}


void HybridButton::set_hover_mode(HybridButton::HoverMode const mode){
    m_hoverMode = mode;
}
