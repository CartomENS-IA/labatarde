/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Selectors.hpp"
#include "SoundManager.hpp"

ArraySelector::ArraySelector(std::vector<ButPtr> &buttons, int selected)
:
    Selector(int(buttons.size()),selected)
{
    m_buttons.clear();
    for (unsigned i = 0 ; i<buttons.size() ; i++){
        m_buttons.push_back(move(buttons[i]));
        m_buttons[i]->unlock();
    }
    m_size = (int)buttons.size();
    change_selected(selected);
}

void ArraySelector::change_selected(int new_selected) {
    m_buttons[m_selected]->unlock();
    m_buttons[new_selected]->lock();
    m_selected = new_selected;
}

void ArraySelector::update(sf::RenderWindow &app, sf::Vector2f const pos, bool clicked) {
    update(app, pos.x, pos.y, clicked);
}

void ArraySelector::update(sf::RenderWindow &app, sf::Vector2i const pos, bool clicked) {
    update(app, float(pos.x), float(pos.y), clicked);
}

void ArraySelector::update(sf::RenderWindow &app, float const mouseX,
                          float const mouseY, bool clicked) {
    int oldSelected = who_is_selected();
    for (unsigned i = 0 ; i < m_buttons.size() ; i++) {
        m_buttons[i]->update(app, mouseX, mouseY);
        if (!m_buttons[i]->is_locked() && m_buttons[i]->is_lit() && clicked) {
            soundManager.play_sound("click.wav");
            change_selected(i);
            m_buttons[oldSelected]->update(app, mouseX, mouseY);
            m_buttons[i]->update(app, mouseX, mouseY);
        }
    }
}
