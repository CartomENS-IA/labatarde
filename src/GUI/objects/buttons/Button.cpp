/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Button.hpp"

Button::Button()
: m_isLit(false), m_locked(false) {}

bool Button::is_lit() const { return m_isLit; }

bool Button::is_locked() const { return m_locked; }

void Button::lock() {m_locked = true;}

void Button::unlock() { m_locked = false; m_isLit = false; }
