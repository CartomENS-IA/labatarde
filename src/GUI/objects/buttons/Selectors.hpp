/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Button.hpp"
#include <string>
#include <vector>
#include <memory>

/** \file
 * \author Julien.D , GUIllaume.C
 *  \brief Definition of classes Selector, ArraySelector and CycleSelector
 **/
class Selector {
public:
    Selector() = default;

    Selector(int size, int selected);

    /** \brief Critical method: changes the currently selected button **/
    virtual void change_selected(int);

    /** \brief Returns the number of the currently selected button **/
    int who_is_selected() const;
    int get_size() const;

    /** \brief First update function, that automatically detects when a click is
     * made and changes the selected button accordingly **/
    virtual void update(sf::RenderWindow &, sf::Vector2f const, bool) = 0;
    virtual void update(sf::RenderWindow &, sf::Vector2i const, bool) = 0;
    virtual void update(sf::RenderWindow &, float const, float const, bool) = 0;
protected:
    int m_size;
    int m_selected;
};


/**
* \class
* An array selector lets you choose between several buttons.
* Only one button can be pressed at the same time.
*  Clicking on another button releases the first one.
* \brief Selector that displays all possibilities
*
* Inherits from abstract class Selector
 **/
class ArraySelector : public Selector {
  public:
     ArraySelector(std::vector<std::unique_ptr<Button>> &buttons,
                   int selected = 0);

    /** \brief Critical method: changes the currently selected button **/
    void change_selected(int new_selected);

    /** \brief First update function, that automatically detects when a click is
     * made and changes the selected button accordingly **/
    virtual void update(sf::RenderWindow &, sf::Vector2f const, bool);
    virtual void update(sf::RenderWindow &app, sf::Vector2i const, bool);
    virtual void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1., bool clicked = false);

  private:
    std::vector<ButPtr> m_buttons;
};


/**
* \class
* A cycle selector lets you choose between several options.
* Only the current option is visible at each time.
*  Clicking on it will cycle through the possibilities.
* \brief Selector that displays only the current choice
*
* Inherits from abstract class Selector
**/
class CycleSelector : public Selector {
  public:
    CycleSelector(float const posX, float const posY,
                  std::vector<std::unique_ptr<Button>> &buttons,
                  int selected = 0);

    /** \brief Increments the cycle **/
    void change_selected(int i = -1);

    /** \brief First update function, that automatically detects when a click is
     * made and changes the selected button accordingly **/
    virtual void update(sf::RenderWindow &, sf::Vector2f const, bool);
    virtual void update(sf::RenderWindow &app, sf::Vector2i const, bool);
    virtual void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1., bool clicked = false);

  private:
    std::vector<std::unique_ptr<Button>> m_buttons;
};

/**
* \class
* A cycle selector that let you choose only between ON and OFF.
*  Clicking on it will toggle the result.
* \brief Selector that only toggles an option ON and OFF
*
**/
class OnOffSelector {
public:
    OnOffSelector(sf::Vector2f const pos, unsigned const fontSize,
            std::string const &fontname, sf::Color col, sf::Color litCol,
            sf::Color clickedCol, bool selected=false);

    OnOffSelector(float const posX, float const posY, unsigned const fontSize,
            std::string const &fontname, sf::Color col, sf::Color litCol,
            sf::Color clickedCol, bool selected=false);

    bool is_on();
    void set(bool);
    void toggle();

    void update(sf::RenderWindow &, sf::Vector2f const, bool);
    void update(sf::RenderWindow &app, sf::Vector2i const, bool);
    void update(sf::RenderWindow &app, float const mouseX = -1.,
                float const mouseY = -1., bool clicked = false);
private:
    bool m_isOn;
    TextButton m_on;
    TextButton m_off;
    Message m_sep; // separator is a "/"
};
