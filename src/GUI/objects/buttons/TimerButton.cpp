#include "Button.hpp"
#include "TextManager.hpp"
#include "Constants.hpp"

TimerButton::TimerButton(float const posX, float const posY,
             float const width, float const height,
             std::string const &textureFull,
             std::string const &textureEmpty,
             std::string const &textKey, unsigned int charSize,
             std::string const &fontname, sf::Color const &col,
             sf::Color const &litCol, sf::Color const &clickedCol,
             Align align, sf::Text::Style style)
:
TimerButton(posX, posY, width, height, textureFull, textureEmpty, TM.get_entry(textKey),
             charSize, fontname, col, litCol, clickedCol,
             align, style)
{}

TimerButton::TimerButton(sf::Vector2f const pos,
             float const width, float const height,
             std::string const &textureFull,
             std::string const &textureEmpty,
             std::string const &textKey, unsigned int charSize,
             std::string const &fontname, sf::Color const &col,
             sf::Color const &litCol, sf::Color const &clickedCol,
             Align align, sf::Text::Style style)
: TimerButton(pos.x, pos.y, width, height, textureFull, textureEmpty, textKey,
               charSize, fontname, col, litCol,
               clickedCol, align, style)
{}


 TimerButton::TimerButton(float const posX, float const posY,
              float const width, float const height,
              std::string const &textureFull,
              std::string const &textureEmpty,
              std::wstring const &text, unsigned int charSize,
              std::string const &fontname, sf::Color const &col,
              sf::Color const &litCol, sf::Color const &clickedCol,
              Align align, sf::Text::Style style)
:
    Button(),
    m_textOnButton(false),
    m_width(width),
    m_height(height),
    m_color(col),
    m_litColor(litCol),
    m_clickedColor(clickedCol),
    m_message(posX, posY+3.f*height/4.f, charSize, text, fontname, col, align, style),
    m_buttonSprite(sf::Vector2f(posX,posY),width,height)
{
    m_buttonSprite.set_texture(textureFull, textureEmpty);
}


TimerButton::TimerButton(sf::Vector2f const pos,
             float const width, float const height,
             std::string const &textureFull,
             std::string const &textureEmpty,
             std::wstring const &text, unsigned int charSize,
             std::string const &fontname, sf::Color const &col,
             sf::Color const &litCol, sf::Color const &clickedCol,
             Align align, sf::Text::Style style)
: TimerButton(pos.x, pos.y, width, height, textureFull, textureEmpty, text,
             charSize, fontname, col, litCol,
             clickedCol, align, style)
{}

void TimerButton::update(sf::RenderWindow &app, sf::Vector2f const mousePos) {
    update(app, mousePos.x, mousePos.y);
}

void TimerButton::update(sf::RenderWindow &app, sf::Vector2i const mousePos) {
    update(app, float(mousePos.x), float(mousePos.y));
}

void TimerButton::update(sf::RenderWindow &app,
        float const mouseX, float const mouseY)
{
    if (not is_locked()) {
        if (m_buttonSprite.pos_on_sprite(mouseX, mouseY)^ is_lit())
        {
            if (is_lit()) {
                m_buttonSprite.set_scale(1.f);
                m_message.set_color(m_color);
            } else {
                m_buttonSprite.set_scale(1.15f);
                m_message.set_color(m_litColor);
            }
            m_isLit = !m_isLit;
        }
    }
    m_buttonSprite.update(app);
    m_message.update(app);
}

void TimerButton::set_position(float const posX, float const posY) {
    m_message.set_position(posX, posY);
}

void TimerButton::set_position(sf::Vector2f const pos) {
    set_position(pos.x, pos.y);
}

void TimerButton::set_position(sf::Vector2i const pos) {
    set_position(sf::Vector2f(pos));
}


void TimerButton::set_text_on_button() {
    if (not m_textOnButton){
        m_message.move_offset(0.f, -m_height);
        m_textOnButton = true;
    }
}

void TimerButton::set_text_under_button() {
    if (m_textOnButton) {
        m_message.move_offset(0.f, m_height);
        m_textOnButton = false;
    }
}

void TimerButton::lock() {
    m_locked = true;
    m_message.set_color(m_clickedColor);
}

void TimerButton::unlock() {
  m_locked = false;
  m_message.set_color(m_color);
}

void TimerButton::set_filling(float fill){
    m_buttonSprite.set_filling(fill);
}
