/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Message.hpp"
#include "TextManager.hpp"

void Message::init(float x, float y, unsigned int size, std::string const &fontname,
                   sf::Color const &col, Align align, sf::Text::Style style) {
    m_messageText.setFont(RM.get_font(fontname));
    m_size = static_cast<unsigned int>((float(size) * SCALE));
    m_messageText.setCharacterSize(m_size);
    m_messageText.setColor(col); // /!\ Deprecated in SFML >= 2.4
    m_messageText.setStyle(style);
    m_position.x = 0;
    m_position.y = 0;
    m_align = align;

    reset_origin();
    m_position.x = x;
    m_position.y = y;
    set_position(m_position);
    m_messageText.setPosition(m_position);
}

Message::Message(float x, float y, unsigned int size, std::string const &textname,
                 std::string const &fontname, sf::Color const &col, Align align,
                 sf::Text::Style style) {
    init(x, y, size, fontname, col, align, style);
    set_text(TM.get_entry(textname));
}

Message::Message(float x, float y, unsigned int size, std::wstring const &text,
                 std::string const &fontname, sf::Color const &col, Align align,
                 sf::Text::Style style) {
    init(x, y, size, fontname, col, align, style);
    set_text(text);
}

Message::Message(sf::Vector2f pos, unsigned int size, std::string const &key,
                 std::string const &fontname, sf::Color const &col, Align align,
                 sf::Text::Style style) {
    init(pos.x, pos.y, size, fontname, col, align, style);
    set_text(TM.get_entry(key));
}

Message::Message(sf::Vector2f pos, unsigned int size, std::wstring const &textname,
                 std::string const &fontname, sf::Color const &col, Align align,
                 sf::Text::Style style) {
    init(pos.x, pos.y, size, fontname, col, align, style);
    set_text(textname);
}

// ______ Rewriting of continue_move ________

void Message::continue_move() {
    AnimatedTransformable::continue_move();
    m_messageText.setCharacterSize((int)((float)m_size * getScale().x));
    reset_origin();
    m_messageText.setPosition(
        getPosition()); // movement of Message is transmitted to its text
}

// ______ Displaying methods _______

void Message::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    states.transform *= getTransform();
    target.draw(m_messageText, states);
}

void Message::update(sf::RenderWindow &bliton) {
    continue_move();
    bliton.draw(m_messageText);
}

// ______ Getters and setters ______

void Message::set_text(std::wstring const &newText) {
    m_messageText.setString(newText);
    reset_origin();
}

void Message::set_text(std::string const &textKey) {
    m_messageText.setString(TM.get_entry(textKey));
    reset_origin();
}

void Message::set_color(sf::Color const &coul) { m_messageText.setColor(coul); }

sf::Text Message::get_text() { return m_messageText; }

void Message::set_font_size(unsigned size) {
    m_size = unsigned(float(size) * SCALE);
    m_messageText.setCharacterSize(size);
}

void Message::reset_origin() {
    float width = m_messageText.getGlobalBounds().width;
    float height = m_messageText.getGlobalBounds().height;
    switch (m_align) {
    case Align::LEFT_TOP:
        m_messageText.setOrigin(0, 0);
        break;
    case Align::LEFT:
        m_messageText.setOrigin(0, height / 2);
        break;

    case Align::RIGHT_TOP:
        m_messageText.setOrigin(width, 0);
        break;

    case Align::RIGHT:
        m_messageText.setOrigin(width, height / 2);
        break;

    case Align::CENTER_TOP:
        m_messageText.setOrigin(width / 2, 0);
        break;

    case Align::CENTER:
    default:
        m_messageText.setOrigin(width / 2, height / 2);
        break;
    }
    setOrigin(m_messageText.getOrigin());
}

void Message::set_position(float const x, float const y, float const angle) {
    set_position(sf::Vector2f(x, y), angle);
}

void Message::set_position(sf::Vector2f const position, float const angle) {
    AnimatedTransformable::set_position(position, angle);
    m_messageText.setPosition(getPosition());
}

void Message::set_position(sf::Vector2i const position, float const angle) {
    set_position(sf::Vector2f(position), angle);
}

void Message::move_offset(sf::Vector2f const offset) {
    set_position(m_position + offset);
}

void Message::move_offset(sf::Vector2i const offset) {
    move_offset(sf::Vector2f(offset));
}

void Message::move_offset(float const x, float const y) {
    move_offset(sf::Vector2f(x, y));
}

float Message::get_width() { return m_messageText.getGlobalBounds().width; }
