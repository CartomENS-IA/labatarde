/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "AnimatedTransformable.hpp"
#include "enumType.hpp"
#include <memory>

/** \file Message.hpp
    \author GUIllaume.C
    \brief Definition of class Message, MultiMessage
**/


/**
 * \class
 * \brief empty class to factorize Message and MultiLineMessage
 *        as daugther classes for storage in containers
 **/
class IMessage {
  public:
    virtual ~IMessage() {};
    virtual void continue_move() = 0;
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const = 0;
    virtual void update(sf::RenderWindow &window) = 0;
    virtual void set_text(std::wstring const &newText) = 0;
    virtual void set_text(std::string const &textKey) = 0;
    virtual void set_color(sf::Color const &coul) = 0;
    virtual void set_font_size(unsigned int) = 0;
    virtual void set_position(float const x, float const y, float const angle = 0) = 0;
    virtual void set_position(sf::Vector2f const position, float const angle = 0) = 0;
    virtual void set_position(sf::Vector2i const position, float const angle = 0) = 0;
    virtual void move_offset(sf::Vector2f const) = 0;
    virtual void move_offset(sf::Vector2i const) = 0;
    virtual void move_offset(float const, float const) = 0;
};


typedef std::unique_ptr<IMessage> MessPtr;

/**
 * A class that displays a message on screen.
 *
 * Messages are displayed using their `update` method. They can be moved in an
 *animation as every object that inherits from AnimatedTransformable. See
 *AnimatedTransformable class documentation for more information.
 *
 * \brief A class that displays a message on screen.
 *
 **/
class Message : public sf::Drawable,
                public AnimatedTransformable,
                public IMessage {
  public:
    Message() = default;

    /** \brief Message class constructor
     * \param posX specify the position of the center of the message
     * \param posY specify the position of the center of the message
     * \param size the font size
     * \param text the key in order to retrieve the content of the message.
     *         Content of the message is stored in TextManager.
     *
     * \param fontname the font in which the message is rendered (is a string
     *specifying the font file name)
     *
     * \param col a sf::Color in which the message is rendered
     *
     * \param align align is part of the Alignment enum. It specify at which
     *point of the text does (posX,posY) refers to. It can be the center of
     *the message, its left extremity (centered vertically) or its right's. By
     *default, align is Center.
     *
     * \param style a sf::Text::Style object that specify is the text is
     *regular, bold, italic, ... style is Regular by default.
     **/
    Message(float posX, float posY, unsigned int size, std::string const &key,
            std::string const &fontname, sf::Color const &col,
            Align align = Align::CENTER,
            sf::Text::Style style = sf::Text::Regular);

    Message(sf::Vector2f pos, unsigned int size, std::string const &key,
            std::string const &fontname, sf::Color const &col,
            Align align = Align::CENTER,
            sf::Text::Style style = sf::Text::Regular);

    /** \brief Variant of the message constructor. Does not look up the
     * TextManager container **/
    Message(float posX, float posY, unsigned int size, std::wstring const &text,
            std::string const &fontname, sf::Color const &col,
            Align align = Align::CENTER,
            sf::Text::Style style = sf::Text::Regular);

    Message(sf::Vector2f pos, unsigned int size, std::wstring const &text,
            std::string const &fontname, sf::Color const &col,
            Align align = Align::CENTER,
            sf::Text::Style style = sf::Text::Regular);

    /** \brief overload of method continue_move from class AnimatedTransformable
     * in order to set the font size **/
    void continue_move();

    /** \brief draw the message on the screen **/
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;

    /** \brief update method : update the object's position and call draw **/
    void update(sf::RenderWindow &window);

    /** \brief setter for the sf::Text private attribute m_messageText
     *           Does not call the TextManager to retrive the text message
     **/
    void set_text(std::wstring const &newText);

    /** \brief setter for the sf::Text private attribute m_messageText
     *           Calls the TextManager to get the text message
     **/
    void set_text(std::string const &textKey);

    /** \brief getter for the sf::Text private attribute m_messageText **/
    sf::Text get_text();

    /**  \brief setter for the sf::Color private attribute**/
    void set_color(sf::Color const &coul);

    /** \brief Accesses the length, in pixels, of the Message object**/
    float get_width();

    void set_font_size(unsigned int);

    /**
    Calls to setPosition of sf::Transformable. This function has been rewritten
    to also move the m_destination point. \brief 	Sets the position of the
    origin of the sprite \param angle an optional argument to rotate the sprite.
    /!\ this doesn't trigger any animation
    */
    void set_position(float const x, float const y,
                              float const angle = 0);
    /** \brief Variant of set_position that takes a sf::Vector2f as an argument
     * **/
    void set_position(sf::Vector2f const position,
                              float const angle = 0);
    /** \brief Variant of set_position that takes a sf::Vector2i as an argument
     * **/
    void set_position(sf::Vector2i const position,
                              float const angle = 0);

    /** \brief Sets position using an offset, and not a flat position **/
    void move_offset(sf::Vector2f const);
    void move_offset(sf::Vector2i const);
    void move_offset(float const, float const);

  protected:

     /** \brief recomputes the origin depending on the alignment and the current
     * text **/
    void reset_origin();

    /** \brief End of constructor. Helps for code factorisation.
     *
     *   This is not a constructor because it has to be called after that the
     *text has been set.
     **/
    void init(float posX, float posY, unsigned int size, std::string const &fontname,
              sf::Color const &col, Align align, sf::Text::Style style);

    sf::Text m_messageText;
    unsigned int m_size;
    Align m_align;
    sf::Vector2f m_position;
};


/**
 * \class
 * \brief A Message that splits its text onto several lines
 **/
class MultiLineMessage : public IMessage {
  public:

  /** \brief MultiLineMessage class constructor
   * \param posX specify the position of the center of the message
   * \param posYTop specify the position of the center of the first message
   * \param size the font size
   * \param text the key in order to retrieve the content of the message.
   *         Content of the message is stored in TextManager.
   *
   * \param fontname the font in which the message is rendered (is a string
   *specifying the font file name)
   *
   * \param col a sf::Color in which the message is rendered
   *
   * \param align align is part of the Alignment enum. It specify at which
   *point of the text does (posX,posY) refers to. It can be the center of
   *the message, its left extremity (centered vertically) or its right's. By
   *default, align is Center.
   *
   * \param style a sf::Text::Style object that specify is the text is
   *regular, bold, italic, ... style is Regular by default.
   **/
    MultiLineMessage(float posX, float posYTop, unsigned int size,
                     std::vector<std::string> const &keys,
                     std::string const &fontname,
                     sf::Color const &col,
                     Align align = Align::CENTER,
                     sf::Text::Style style = sf::Text::Regular);

     /** MultiLineMessage constructor.
      *    \param texts the texts of the messages
      *    \param delimiter The delimiter character to split the string
      **/
    MultiLineMessage(float posX, float posYTop, unsigned int size,
                      std::vector<std::wstring> const &texts,
                      std::string const &fontname,
                      sf::Color const &col,
                      Align align = Align::CENTER,
                      sf::Text::Style style = sf::Text::Regular);

    /** MultiLineMessage constructor.
     *    \param key the key to lookup in the textManager.
     *    \param delimiter The delimiter character to split the string
     **/
    MultiLineMessage(float posX, float posYTop, unsigned int size,
                      std::string const &key, char delimiter,
                      std::string const &fontname,
                      sf::Color const &col,
                      Align align = Align::CENTER,
                      sf::Text::Style style = sf::Text::Regular);

  /** MultiLineMessage constructor.
   *    \param textt the text of the message
   *    \param delimiter The delimiter character to split the string
   **/
    MultiLineMessage(float posX, float posYTop, unsigned int size,
                        std::wstring const &text, char delimiter,
                        std::string const &fontname,
                        sf::Color const &col,
                        Align align = Align::CENTER,
                        sf::Text::Style style = sf::Text::Regular);

    /** \brief overload of method continue_move from class AnimatedTransformable
     * in order to set the font size **/
    void continue_move();

    /** \brief draw the message on the screen **/
    void draw(sf::RenderTarget &target, sf::RenderStates states) const;

    /** \brief update method : update the object's position and call draw **/
    void update(sf::RenderWindow &window);


    /** \brief setter for the sf::Text private attribute m_messageText
     *           Does not call the TextManager to retrive the text message.
     *           Plot the message on a single line.
     **/
    void set_text(std::wstring const &newText);

    /** \brief setter for the sf::Text private attribute m_messageText
     *           Does not call the TextManager to retrive the text message
     **/
    void set_text(std::vector<std::wstring> const &newText);

    /** \brief setter for the sf::Text private attribute m_messageText
     *           Calls the TextManager to get the text message
     **/
    void set_text(std::string const &textKey);

    /**  \brief Setter for the sf::Color private attribute**/
    void set_color(sf::Color const &coul);

    /** \brief Setter for the font size of the text **/
    void set_font_size(unsigned int);

    /**
    Calls to setPosition of sf::Transformable. This function has been rewritten
    to also move the m_destination point. \brief 	Sets the position of the
    origin of the sprite \param angle an optional argument to rotate the sprite.
    /!\ this doesn't trigger any animation
    */
    void set_position(float const x, float const y,
                              float const angle = 0);
    /** \brief Variant of set_position that takes a sf::Vector2f as an argument
     * **/
    void set_position(sf::Vector2f const position,
                              float const angle = 0);
    /** \brief Variant of set_position that takes a sf::Vector2i as an argument
     * **/
    void set_position(sf::Vector2i const position,
                              float const angle = 0);

    void move_offset(sf::Vector2f const);
    void move_offset(sf::Vector2i const);
    void move_offset(float const, float const);
protected:
    std::vector<Message> m_messages;
};
