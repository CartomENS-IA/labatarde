/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
    \author Louis B., GUIllaume C.
    \brief Definition of class TextEntry
**/

#include <SFML/Graphics.hpp>
#include <string>

/** \class **/
class TextEntry : public sf::Drawable, public sf::Transformable {
  public:
    TextEntry(float posX = 0.f, float posY = 0.f, unsigned int lengthMax = 18,
              const std::string &fontName = "GillSans.ttc",
              unsigned int characterSize = 30,
              sf::Color fontColor = sf::Color::Black,
              sf::Color fillColor = sf::Color(0, 0, 0, 32),
              sf::Color borderColor = sf::Color::Black,
              sf::Time timer = sf::seconds(0.35f));

    TextEntry(sf::Vector2i position, unsigned int lengthMax = 18,
              const std::string &fontName = "GillSans.ttc",
              unsigned int characterSize = 30,
              sf::Color fontColor = sf::Color::Black,
              sf::Color fillColor = sf::Color(0, 0, 0, 32),
              sf::Color borderColor = sf::Color::Black,
              sf::Time timer = sf::seconds(0.35f));

    TextEntry(sf::Vector2f position, unsigned int lengthMax = 18,
              const std::string &fontName = "GillSans.ttc",
              unsigned int characterSize = 30,
              sf::Color fontColor = sf::Color::Black,
              sf::Color fillColor = sf::Color(0, 0, 0, 32),
              sf::Color borderColor = sf::Color::Black,
              sf::Time timer = sf::seconds(0.35f));

    /** \brief Getter for the m_realText private attribute (ie the current
     * string that is stored) **/
    sf::String get_text() const;
    bool is_selected() const;
    void set_focus(bool);
    void toggle_focus();

    /** \brief update method. To be called in GUI loop **/
    void update(sf::RenderWindow &);

    /** \brief updates the TextEntry according to a received event. Should be
    placed in the event subloop of a game loop. \return True if event handling
    was a success
    **/
    bool process_event(const sf::Event &);

    /** \brief Adds a character at the end of the current string m_realText **/
    bool add_char(sf::Uint32);

    /** \brief Erases the last character of the current string m_realText **/
    bool remove_last_char();

    /** \brief Forces the current string m_realText to be some custom string **/
    bool set_text(const sf::String &);

    /** \brief Overload of the sf::Drawable draw function. It is better to use
     * the update method to display the TextEntry **/
    void draw(sf::RenderTarget &, sf::RenderStates) const;

    /** To move the TextEntry, use the move method from sf::Transformable **/

    /** \brief setter to the m_hidden private attribute. Sets it to true, so
     * that characters are replaced by a *. **/
    void hide_text();

    /** \brief setter to the m_hidden private attribute. Sets it to false, so
     * that characters are visible **/
    void show_text();

    /** \brief sets the m_drawBox attribute to false. The background box
    * won't be drawn onto the screen when calling the draw method.
    **/
    void hide_box();

    /** \brief sets the m_drawBox attribute to true. The background box
    * will be drawn onto the screen when calling the draw method.
    **/
    void show_box();

  private:
    void update_visible_text();

    bool m_isSelected;
    bool m_cursor;
    bool m_hidden;
    bool m_drawBox;
    unsigned int m_lengthMax;
    sf::Time m_timer;
    unsigned int m_characterSize;
    sf::String m_realText;
    sf::Text m_visibleText;
    sf::RectangleShape m_entryBar;
    sf::Clock m_clock;

    sf::Color m_fillColor;
    sf::Color m_fillColorSelected;
};
