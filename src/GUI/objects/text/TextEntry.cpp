/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "TextEntry.hpp"
#include "Constants.hpp"
#include "RessourceManager.hpp"
#include "SoundManager.hpp"

#include <iostream>

using namespace std;
using namespace sf;

TextEntry::TextEntry(float posX, float posY, unsigned int lengthMax,
                     const string &fontName, unsigned int characterSize,
                     Color fontColor, Color fillColor, Color borderColor,
                     Time timer)
:
    m_isSelected(false), m_cursor(false), m_hidden(false), m_drawBox(true),
    m_lengthMax(lengthMax), m_timer(timer), m_characterSize(characterSize),
    m_entryBar(
          Vector2f(float(m_lengthMax * characterSize - 110) * SCALE,
                   float(characterSize+5) * SCALE)),
    m_fillColor(fillColor),
    m_fillColorSelected(fillColor + sf::Color(0, 0, 0, 30))
{
    m_visibleText.setFont(RM.get_font(fontName));
    m_visibleText.setCharacterSize(unsigned(float(characterSize) * SCALE));
    m_visibleText.setColor(fontColor);
    m_visibleText.move(float(characterSize) * 0.4f * SCALE, -10.f*SCALE);
    m_entryBar.setFillColor(m_fillColor);
    m_entryBar.setOutlineColor(borderColor);
    m_entryBar.setOutlineThickness(2.f * SCALE);
    move(posX, posY);
}

TextEntry::TextEntry(Vector2i position, unsigned int lengthMax,
                     const string &fontName, unsigned int characterSize,
                     Color fontColor, Color fillColor, Color borderColor,
                     Time timer)
: TextEntry(sf::Vector2f(position), lengthMax, fontName, characterSize,
            fontColor, fillColor, borderColor, timer) {}

TextEntry::TextEntry(Vector2f position, unsigned int lengthMax,
                     const string &fontName, unsigned int characterSize,
                     Color fontColor, Color fillColor, Color borderColor,
                     Time timer)
: TextEntry(position.x, position.y, lengthMax, fontName, characterSize,
            fontColor, fillColor, borderColor, timer) {}


void TextEntry::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    states.transform *= getTransform();
    if (m_drawBox) target.draw(m_entryBar, states);
    target.draw(m_visibleText, states);
}

String TextEntry::get_text() const { return m_realText; }

void TextEntry::update(sf::RenderWindow &window) {
    if (m_clock.getElapsedTime() > m_timer) {
        m_cursor = !m_cursor;
        m_clock.restart();
        update_visible_text();
    }
    window.draw(*this);
}

bool TextEntry::process_event(const Event &event) {
    switch (event.type) {
    case Event::TextEntered:
        if (!m_isSelected)
            return true;
        if (event.text.unicode == 0x08)
            return remove_last_char();
        else if (event.text.unicode != 0xD and
                 event.text.unicode != 0x9)
            // don't type tabulations and carriage return
            return add_char(static_cast<wchar_t>(event.text.unicode));
        break;

    case Event::MouseButtonPressed:
        if (event.mouseButton.button == Mouse::Button::Left) {
            FloatRect bounds = m_entryBar.getGlobalBounds();
            bounds = getTransform().transformRect(bounds);
            bool isContain = bounds.contains((float)event.mouseButton.x,
                                             (float)event.mouseButton.y);
            if (isContain) {
                m_cursor = true;
                m_clock.restart();
            }
            m_isSelected = isContain;
            m_isSelected ? m_entryBar.setFillColor(m_fillColorSelected)
                         : m_entryBar.setFillColor(m_fillColor);
            update_visible_text();
        }

    default:
        break;
    }
    return true;
}

bool TextEntry::add_char(Uint32 charac) {
    if (m_realText.getSize() >= m_lengthMax)
        return false;
    m_realText += String(charac);
    update_visible_text();
    //m_visibleText.move(-float(m_characterSize) * 0.18f * SCALE, 0);
    return true;
}

bool TextEntry::remove_last_char() {
    if (m_realText.isEmpty())
        return false;
    m_realText.erase(m_realText.getSize() - 1, 1);
    update_visible_text();
    //m_visibleText.move(float(m_characterSize) * 0.18f * SCALE, 0);
    return true;
}

bool TextEntry::set_text(const sf::String &newText) {
    if (newText.getSize() > m_lengthMax)
        return false;
    m_realText = newText;
    update_visible_text();
    return true;
}

bool TextEntry::is_selected() const { return m_isSelected; }

void TextEntry::set_focus(bool focus) {
    m_isSelected = focus;
    m_isSelected ? m_entryBar.setFillColor(m_fillColorSelected)
                 : m_entryBar.setFillColor(m_fillColor);
    m_clock.restart();
    update_visible_text();
}

void TextEntry::toggle_focus() {
    soundManager.play_sound("click.wav");
    set_focus(not m_isSelected);
}

void TextEntry::update_visible_text() {
    if (m_hidden) {
        String hiddenString = "";
        for (unsigned int i = 0; i < m_realText.getSize(); i++)
            hiddenString += "*";
        if (m_cursor && m_isSelected) {
            m_visibleText.setString(hiddenString + String("|"));
        } else {
            m_visibleText.setString(hiddenString);
        }
    } else {
        if (m_cursor && m_isSelected) {
            m_visibleText.setString(m_realText + String("|"));
        } else {
            m_visibleText.setString(m_realText);
        }
    }
}

void TextEntry::hide_text() { m_hidden = true; }
void TextEntry::show_text() { m_hidden = false; }

void TextEntry::hide_box() { m_drawBox = false;}
void TextEntry::show_box() { m_drawBox = true;}
