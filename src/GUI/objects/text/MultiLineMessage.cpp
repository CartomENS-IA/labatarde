/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Message.hpp"
#include "TextManager.hpp"
#include "stringSplit.hpp"
#include <iterator>
#include <iostream>

using namespace std;

MultiLineMessage::MultiLineMessage(float posX, float posYTop,
    unsigned int size, vector<string> const &keys,
    string const &fontname, sf::Color const &col,
    Align align, sf::Text::Style style) {
    for (unsigned int i = 0 ; i < keys.size() ; i++){
        float posY = posYTop + float(i*(size+30))*SCALE_TO_MAX;
        m_messages.emplace_back(posX, posY, size, keys[i],
                                fontname, col, align, style);
    }
}

MultiLineMessage::MultiLineMessage(float posX, float posYTop,
    unsigned int size, vector<wstring> const &texts,
    string const &fontname, sf::Color const &col,
    Align align, sf::Text::Style style) {
    for (unsigned int i = 0 ; i < texts.size() ; i++){
        float posY = posYTop + float(i*(size+10))*SCALE_TO_MAX;
        m_messages.emplace_back(posX, posY, size, texts[i],
                                fontname, col, align, style);
    }
}

MultiLineMessage::MultiLineMessage(float posX, float posYTop, unsigned int size,
                  string const &key, char delimiter,
                  string const &fontname, sf::Color const &col,
                  Align align, sf::Text::Style style) :
MultiLineMessage(posX, posYTop, size,
                 split_string_separator(TM.get_entry(key),delimiter),
                 fontname, col, align, style) {}

MultiLineMessage::MultiLineMessage(float posX, float posYTop, unsigned int size,
                    wstring const &text, char delimiter,
                    string const &fontname, sf::Color const &col,
                    Align align, sf::Text::Style style) :
MultiLineMessage(posX, posYTop, size,
                split_string_separator(text,delimiter),
                fontname, col, align, style) {}

/** \brief overload of method continue_move from class AnimatedTransformable
 * in order to set the font size **/
void MultiLineMessage::continue_move() {
    for (auto &mess : m_messages){
        mess.continue_move();
    }
}

/** \brief draw the message on the screen **/
void MultiLineMessage::draw(sf::RenderTarget &target, sf::RenderStates states) const {
    for (auto &mess : m_messages){
        mess.draw(target, states);
    }
}

/** \brief update method : update the object's position and call draw **/
void MultiLineMessage::update(sf::RenderWindow &window) {
    for (auto &mess : m_messages){
        mess.update(window);
    }
}


void MultiLineMessage::set_text(wstring const &newText) {
  std::vector<wstring> textVect;
  textVect.push_back(newText);
  set_text(textVect);
}

void MultiLineMessage::set_text(vector<wstring> const &newText) {
  for (unsigned int i = 0 ; i < m_messages.size() ; i++) {
    m_messages[i].set_text(newText[i]);
  }
}


void MultiLineMessage::set_text(string const &textKey) {
    set_text(split_string_separator(TM.get_entry(textKey),';'));
}


void MultiLineMessage::set_color(sf::Color const &coul) {
    for (auto &mess : m_messages){
        mess.set_color(coul);
    }
}

void MultiLineMessage::set_font_size(unsigned int fontSize){
    for (auto &mess : m_messages){
        mess.set_font_size(fontSize);
    }
}

void MultiLineMessage::set_position(float const x, float const y,
    float const angle){
        set_position(sf::Vector2f(x,y), angle);
}

void MultiLineMessage::set_position(sf::Vector2f const pos,
    float const angle){
        for (auto& mess : m_messages){
            mess.set_position(pos.x,pos.y,angle);
        }
}

void MultiLineMessage::set_position(sf::Vector2i const pos,
    float const angle){
        set_position(sf::Vector2f(pos), angle);
}

void MultiLineMessage::move_offset(sf::Vector2f const offset) {
    for (auto &mess : m_messages){
        mess.move_offset(offset);
    }
}

void MultiLineMessage::move_offset(sf::Vector2i const offset) {
    move_offset(sf::Vector2f(offset));
}

void MultiLineMessage::move_offset(float const offsetX, float const offsetY) {
    move_offset(sf::Vector2f(offsetX,offsetY));
}
