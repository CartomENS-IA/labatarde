/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once
#include <string>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>

/** \file
 * \author GUIllaume C.
 * \brief Definition of the function that splits strings onto several lines
 **/

/** \brief Splits the string into several lines
 *   \param width the width of the zone where the text should be displayed. Act
 *          as a constraint
 *   \param fontSize size of the font of the given text. Determines
 *          the width of the characters.
 *   \param src the input string to split. Works in place
 * \return the number of lines created
 **/
 template<class charT>
 int split_string_width(std::basic_string<charT>& src, int fontSize, float width) {
     int nbLines = 1;
     float curWidth = 0;
     int curInd;
     sf::Font font = RM.get_font("GillSans.ttc");
     for (int i = 0; i < int(src.size()); i++) {
         curWidth += font.getGlyph(src[i], fontSize, true).advance;
         if (curWidth >= width) { // End of a line. Go back to last beginned word
                                  // and add a \n
             curWidth = 0;
             curInd = i;
             while ((src[curInd] != 32) and curInd >= 0) {
                 // Go back to where last 32 occured
                 // 32 = ASCII code for space character
                 curInd--;
                 curWidth += font.getGlyph(src[curInd], fontSize, true).advance;
             }
             if (curInd == -1) {
                 std::cout
                     << "Warning : You used a text zone which width is not";
                 std::cout << " large enough to fit one word ! Please increase "
                              "the width\n";
             } else {
                 src[curInd] = '\n';
                 nbLines++;
             }
         }
     }
     return nbLines;
 }

template<class charT>
std::vector<std::basic_string<charT>> split_string_separator(
                                        std::basic_string<charT> const& src,
                                        char delim=';')
{
    std::vector<std::basic_string<charT>> output;
    std::size_t current, previous = 0;
    current = src.find(delim);
    while (current != std::basic_string<charT>::npos) {
        output.push_back(src.substr(previous, current - previous));
        previous = current + 1;
        current = src.find(delim, previous);
    }
    output.push_back(src.substr(previous, current - previous));
    return output;
}
