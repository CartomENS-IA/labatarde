/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Graphics.hpp>
#include <string>

#include "GraphicTimer.hpp"
#include "Message.hpp"
#include "enumType.hpp"

/** \file
    \author GUIllaume C.
    \brief Definition of class Loading
**/

/** \class
 *   \brief A loading message that is animated
 **/
class Loading {

  public:
    Loading();

    Loading(sf::Vector2f const position);
    Loading(sf::Vector2i const position);
    Loading(float const posX, float const posY);

    void update(sf::RenderWindow &app);

    static float const SPRITE_OFFSET;

  private:
    static std::wstring onePt;   // "."
    static std::wstring twoPt;   // ".."
    static std::wstring threePt; // "..."

    int m_loadingPhase; // from 0 to 3
    std::wstring m_loadStr;

    sf::Clock m_clock;
    Message m_loadingMessage;
    GraphicTimer m_loadingAnimation;
};
