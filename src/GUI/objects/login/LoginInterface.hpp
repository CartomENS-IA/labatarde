/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <thread>
#include <exception>
#include <mutex>
#include <memory>

#include "Loading.hpp"
#include "LoginEntry.hpp"
#include "ClientMessenger.hpp"

/** \file
*   \author GUIllaume.C
*   \brief Definition of classes LoginProxy and LoginInterface
**/

enum class LoginState : int {
    WAITING,
    CONNECTING,
    LOGIN_FAIL,
    LOGIN_SUCCESS
};

enum class ProxyEvent : int {
    NOTHING,
    LOGIN_SUCCESS,
    LOGIN_FAIL
};

class LoginProxy {
private:
    std::shared_ptr<Network::ClientMessenger> m_serverLink;
    LoginState m_state;
    ProxyEvent m_last_event;
    std::mutex mtx;
    sf::Clock m_clock;
    bool m_initialized;
public:
    static sf::Time const LOGIN_TIMEOUT;

    LoginProxy(std::shared_ptr<Network::ClientMessenger> const);
    void set_offline();
    ProxyEvent pop_last_event();
    LoginState get_state() const;

    void init();
    void authenticate(std::string const& name, std::string const& pswd);
    void update();
};


class LoginInterface {
public:
    LoginInterface(sf::Vector2f const,
        std::shared_ptr<Network::ClientMessenger> const);

    LoginInterface(float const, float const,
        std::shared_ptr<Network::ClientMessenger> const);

    void process_GUI_event(const sf::Event &);

    /** \brief Process the proxy event on top of the stack.
        \returns True if the login was successfull**/
    bool process_Proxy_event();

    void toggle_focus();
    void authenticate();
    void update(sf::RenderWindow &);

    LoginState get_proxy_state();

private:
    LoginProxy m_proxy;
    LoginEntry m_userEntry;
    LoginEntry m_pswdEntry;
    Message m_failMessage;
    Loading m_loadingAnim;
};
