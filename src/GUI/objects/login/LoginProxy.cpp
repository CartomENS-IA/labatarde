#include "LoginInterface.hpp"
#include <thread>
#include <exception>
#include <mutex>

sf::Time const LoginProxy::LOGIN_TIMEOUT = sf::seconds(8.f);

LoginProxy::LoginProxy(std::shared_ptr<Network::ClientMessenger> const _serverLink)
:
    m_serverLink(_serverLink),
    m_state(LoginState::WAITING),
    m_last_event(ProxyEvent::NOTHING),
    m_initialized(false)
{}

void LoginProxy::set_offline() {
    m_state = LoginState::LOGIN_SUCCESS;
}

ProxyEvent LoginProxy::pop_last_event() {
    std::unique_lock<decltype(mtx)> lock(mtx);
    ProxyEvent last_event = m_last_event;
    m_last_event = ProxyEvent::NOTHING;
    return last_event;
}

LoginState LoginProxy::get_state() const {
    return m_state;
}

void LoginProxy::init(){
  using namespace std;
  std::thread try_init([&](){
      Network::ServerMessageStatus status;
      cout << "[SERVER INIT] Try to resolve server's name" << endl;
      try {
          m_serverLink->init();
      } catch (const std::exception e) {
          cout << "[SERVER INIT] Failed to connect to server" << endl;
          cout << e.what() << std::endl;
          status = Network::ServerMessageStatus::Error;
      }
      std::unique_lock<decltype(mtx)> lock(mtx);
      if (status == Network::ServerMessageStatus::Done) {
          m_initialized = true;
      }
  });
  try_init.detach();
}

void LoginProxy::authenticate(std::string const& name, std::string const& pswd) {
    std::cout << "[SERVER LINK] Attempting to connect" << std::endl;
    if (not m_initialized || m_state == LoginState::CONNECTING){
      std::cout << "[SERVER LINK] Connection failed : bad state" << std::endl;
      return ;
    }
    if (name.size() == 0 || pswd.size() == 0) {
        std::cout << "[SERVER LINK] Connection failed :"
                  << "empty username or password" << std::endl;
        m_state = LoginState::LOGIN_FAIL;
        return ;
    }
    m_state = LoginState::CONNECTING;
    m_clock.restart();
    std::thread try_authenticate([&](){
        Network::ServerMessageStatus status;
        try {
            status = m_serverLink->authenticate(name.c_str(), pswd.c_str());
        } catch(const std::exception e) {
            cout << e.what() << std::endl;
            status = Network::ServerMessageStatus::Error;
        }
        std::unique_lock<decltype(mtx)> lock(mtx);
        if (status == Network::ServerMessageStatus::Done) {
            m_state = LoginState::LOGIN_SUCCESS;
            m_last_event = ProxyEvent::LOGIN_SUCCESS;
            std::cout << "[SERVER LINK] Connection success as "
                      << name << std::endl;
        } else {
            m_state = LoginState::LOGIN_FAIL;
            m_last_event = ProxyEvent::LOGIN_FAIL;
            std::cout << "[SERVER LINK] Connection failed" << std::endl;
        }
    });
    try_authenticate.detach();
}

void LoginProxy::update() {
    if (m_state == LoginState::CONNECTING &&
        m_clock.getElapsedTime() > LOGIN_TIMEOUT) {
        m_state = LoginState::LOGIN_FAIL;
    }
}
