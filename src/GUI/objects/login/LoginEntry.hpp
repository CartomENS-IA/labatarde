/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "TextEntry.hpp"
#include "Message.hpp"
#include "RessourceManager.hpp"
#include "AnimatedSprite.hpp"
#include <string>

/** \file
    \author Guillaume.C
    \brief Definition of class LoginEntry
**/

class LoginEntry : public TextEntry {

public:
    LoginEntry();

    /**
    *  \param posX x position of the center of the object
    *  \param poxY y position of the center of the object
    **/
    LoginEntry(std::string const &name,
              float posX = 0.f, float posY = 0.f,
              const std::string &fontName = "GillSans.ttc",
              sf::Color fontColor = sf::Color::Black);

    LoginEntry(std::string const &name,
                sf::Vector2f const,
                const std::string &fontName = "GillSans.ttc",
                sf::Color fontColor = sf::Color::Black);

    /** \brief update method. To be called in GUI loop **/
    void update(sf::RenderWindow &);

private:
    static float const WIDTH;
    static float const HEIGHT;
    static float const TITLE_OFFSET;
    Message m_title;
    AnimatedSprite m_background;
};
