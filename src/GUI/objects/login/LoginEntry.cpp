/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "LoginEntry.hpp"
#include "TextManager.hpp"
#include "Constants.hpp"

using namespace std;
using namespace sf;

float const LoginEntry::WIDTH = 380.f*optionManager()->get_screen_scale();
float const LoginEntry::HEIGHT = 50.f*optionManager()->get_screen_scale();
float const LoginEntry::TITLE_OFFSET = 50.f*optionManager()->get_screen_scale();

LoginEntry::LoginEntry(){}

LoginEntry::LoginEntry(const string &name,
                     float posX, float posY,
                     const string &fontName,
                     Color fontColor)
:
    TextEntry(posX-WIDTH/2, posY-HEIGHT/3+5.f*SCALE, 18, fontName, 35,
              fontColor),
    m_title(posX, posY-TITLE_OFFSET, 28, name, fontName, BLACK),
    m_background(WIDTH,HEIGHT)
{
    m_background.set_texture("loginbar.png");
    m_background.set_position(posX,posY);
    hide_box();
}

LoginEntry::LoginEntry(std::string const &name,
                     sf::Vector2f const pos,
                     const string &fontName,
                     Color fontColor)
: LoginEntry(name, pos.x, pos.y, fontName, fontColor) {}


void LoginEntry::update(sf::RenderWindow &window) {
    m_background.update(window);
    m_title.update(window);
    TextEntry::update(window);
}
