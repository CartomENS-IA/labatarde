/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "LoginInterface.hpp"
#include "Constants.hpp"
#include "StatManager.hpp"

LoginInterface::LoginInterface(sf::Vector2f const pos,
    std::shared_ptr<Network::ClientMessenger> const server_link):
    LoginInterface(pos.x, pos.y, server_link){}


LoginInterface::LoginInterface(float const posX, float const posY,
    std::shared_ptr<Network::ClientMessenger> const server_link)
:
    m_proxy(server_link)
{
    m_proxy.init();

    float const LOGIN_FAIL_TEXT_SIZE = 25.f*HALF_SCALE;
    unsigned int const LOGIN_FAIL_FONT_SIZE = unsigned(LOGIN_FAIL_TEXT_SIZE);

    m_failMessage = Message(posX, posY +  100.f *SCALE,
                            LOGIN_FAIL_FONT_SIZE, "loginFail", "GillSans.ttc",
                            RED_LIT, Align::CENTER);

    m_loadingAnim = Loading(posX - 100.f*SCALE, posY +  170.f *SCALE);

    sf::Vector2f userEntryPos(posX, posY - 50.f*SCALE);
    sf::Vector2f entryOffset(0.f, 110.f*SCALE);

    m_userEntry = LoginEntry("username", userEntryPos, "GillSans.ttc", BLACK);
    m_userEntry.set_focus(true);

    m_pswdEntry = LoginEntry("password", userEntryPos+entryOffset, "GillSans.ttc", BLACK);
    m_pswdEntry.hide_text(); // Password characters replaced with *
}

void LoginInterface::process_GUI_event(const sf::Event &e){
    m_userEntry.process_event(e);
    m_pswdEntry.process_event(e);
}

bool LoginInterface::process_Proxy_event(){
    switch (m_proxy.pop_last_event()) {
        case ProxyEvent::LOGIN_SUCCESS:
            optionManager()->set_player_name(m_userEntry.get_text());
            optionManager()->set_online(true);
            statManager.load_profile();
            std::clog << "[LOGIN] Logged in with player name '"
                      << m_userEntry.get_text().toAnsiString()
                      << "'" << std::endl;
            return true;
        break;

        case ProxyEvent::LOGIN_FAIL:
            std::clog << "[LOGIN] Failed to login" << std::endl;
        break;

        default: break;
    }
}

void LoginInterface::toggle_focus(){
    m_userEntry.toggle_focus();
    m_pswdEntry.toggle_focus();
}

LoginState LoginInterface::get_proxy_state(){
    return m_proxy.get_state();
}

void LoginInterface::authenticate(){
    m_proxy.authenticate(m_userEntry.get_text(), m_pswdEntry.get_text());
}

void LoginInterface::update(sf::RenderWindow &app){
    m_userEntry.update(app);
    m_pswdEntry.update(app);
    m_proxy.update();
    switch (m_proxy.get_state()) {
        case LoginState::CONNECTING:
            m_loadingAnim.update(app);
        break;

        case LoginState::LOGIN_FAIL:
            m_failMessage.update(app);
        break;

        case LoginState::LOGIN_SUCCESS:
        case LoginState::WAITING:
        default:
            break;
    }
}
