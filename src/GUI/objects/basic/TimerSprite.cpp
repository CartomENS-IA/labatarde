#include "TimerSprite.hpp"

TimerSprite::TimerSprite(sf::Vector2f position, float width, float height) : m_spriteFull(), m_spriteEmpty(), m_width(width), m_height(height), m_scale(1.f){
    set_position(position);
    m_spriteFull.setOrigin(width/2.f, height/2.f);
    m_spriteEmpty.setOrigin(width/2.f, height/2.f);
    
    m_spriteFull.setPosition(position);
    m_spriteEmpty.setPosition(position+sf::Vector2f(float(width),0.f));
}

void TimerSprite::set_texture(std::string const &textureFull, std::string const &textureEmpty){

    m_spriteFull.setTexture(RM.get_texture(textureFull));
    m_spriteEmpty.setTexture(RM.get_texture(textureEmpty));
    
    m_sizeFull = RM.get_texture(textureFull).getSize();
    m_sizeEmpty = RM.get_texture(textureFull).getSize();
    
    //To have an image of the good size
    m_spriteFull.setScale( float(m_width) / float(m_sizeFull.x), float(m_height) / float(m_sizeFull.y));
    m_spriteEmpty.setScale( float(m_width) / float(m_sizeEmpty.x), float(m_height) / float(m_sizeEmpty.y));
    
    set_filling(100.f);
}

void TimerSprite::set_filling(float fill){
    if(fill < 0.f) fill = 0.f;
    if(fill > 100.f) fill = 100.f;
    m_percentFill = fill;
    
    float width = float(m_width) * m_scale;
    m_spriteEmpty.setPosition(m_spriteFull.getPosition()+sf::Vector2f(float(width*fill)/100.f,0.f));
    
    m_spriteFull.setTextureRect(sf::IntRect(0, 0, int(209.f*fill/100.f), 209));
    m_spriteEmpty.setTextureRect(sf::IntRect(int(209.f*fill/100), 0, int(209.f - 209.f*fill/100.f), 209));
}

void TimerSprite::update(sf::RenderWindow &window){
    window.draw(m_spriteFull);
    window.draw(m_spriteEmpty);
}

void TimerSprite::set_scale(float scale){
    m_scale = scale;
    m_spriteFull.setScale( scale * float(m_width) / float(m_sizeFull.x), scale * float(m_height) / float(m_sizeFull.y));
    m_spriteEmpty.setScale( scale * float(m_width) / float(m_sizeEmpty.x), scale * float(m_height) / float(m_sizeEmpty.y));
    set_filling(m_percentFill);
}

//_________________________Event_Function___________________________

bool TimerSprite::pos_on_sprite(sf::Vector2f const _mousePos) {
    auto mousePos = sf::Vector2f(_mousePos.x + m_width/2.f, _mousePos.y + m_height/2.f);
    return m_spriteEmpty.getLocalBounds().contains(
        getTransform().getInverse().transformPoint(mousePos)) 
          ||
           m_spriteFull.getLocalBounds().contains(
        getTransform().getInverse().transformPoint(mousePos));
}

bool TimerSprite::pos_on_sprite(sf::Vector2i const mousePos) {
    return pos_on_sprite(sf::Vector2f(mousePos));
}

bool TimerSprite::pos_on_sprite(float const mouseX, float const mouseY) {
    return pos_on_sprite(sf::Vector2f(mouseX, mouseY));
}
