/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_BACKGROUND
#define DEF_BACKGROUND

#include "Constants.hpp"
#include "RessourceManager.hpp"

/** \file Background.hpp
 *   \author Guillaume.C
 *   \brief Definition of the Background class
 **/

/** \brief A texture implemented with sf::VertexArray that is meant to be the
 * background of a SFML's window **/
class Background {
  public:
    /** \brief Default Constructor **/
    Background();

    /** \brief Background's constructor
     *   \param name the relative path to the ressource file of the background
     *(call to global RM)
     **/
    Background(std::string const &name);

    /** \brief Setter for the texture of the m_sprite attribute **/
    void set_texture(std::string const &name);

    /** \brief Sets the transparency of the object. No animation**/
    void set_transparency(int const alpha);

    /** \brief Sets the transparency of the object. It will slowly fade in/out
     * at speed `speed` **/
    void fade(int const alpha, float const speed);

    /** \brief continues the animation **/
    void continue_fade();

    /** \brief Returns true iff the object is currently in the middle of an
     * animation **/
    bool is_fading() const;

    /** \brief Update method : alias for continue_fade + draw method **/
    void update(sf::RenderWindow &bliton);

    /** \brief Implementation of the draw method of sf::Drawable **/
    void draw(sf::RenderWindow &bliton) const;

  private:
    int m_alpha;
    int m_goal_alpha;
    float m_speed;
    sf::Sprite m_sprite;
};

#endif
