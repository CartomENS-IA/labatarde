/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_ANIMATED_TRANSFORMABLE
#define DEF_ANIMATED_TRANSFORMABLE

#include "RessourceManager.hpp"
#include "geometry.hpp"

/** \file AnimatedTransformable.hpp
 *	\author Léo.V
 *	\date 14 October
 *	\brief Definition of AnimatedTransformable class
 **/

/**
AnimatedTransformable inherits from sf::Transformable.
It implements methods that allow SFML objects to move according to an animation.
This class is mostly virtual and is meant to be the mother class of other
objects \brief An object that can move according to an animation
**/
class AnimatedTransformable : public sf::Transformable {

  public:
    /** \brief AnimatedTransformable's constructor **/
    AnimatedTransformable();

    /** \brief update the destination position of an object
     *	\param angle an angle between 0 and 360 if you want to do rotations
     *	\param timing the duration of the animation
     **/
    virtual void move(float const x, float const y, float angle = 0,
              float const timing = 10);
    /** \brief variant of move that takes as sf::Vector2f as a position **/
    virtual void move(sf::Vector2f const pos, float angle = 0, float const timing = 10);
    /** \brief variant of move that takes as sf::Vector2i as a position **/
    virtual void move(sf::Vector2i const pos, float angle = 0, float const timing = 10);

    /** \brief Same as move, but takes an offset instead of the final position
     * **/
    void move_relative(float const x, float const y, float angle = 0,
                       float const timing = 1);
    /** \brief Same as move, but takes an offset instead of the final position
     * **/
    void move_relative(sf::Vector2f const position, float angle = 0,
                       float const timing = 1);
    /** \brief Same as move, but takes an offset instead of the final position
     * **/
    void move_relative(sf::Vector2i const position, float angle = 0,
                       float const timing = 1);

    /**
    Calls to setPosition of sf::Transformable. This function has been rewritten
    to also move the m_destination point. \brief 	Sets the position of the
    origin of the sprite \param angle an optional argument to rotate the sprite.
    /!\ this doesn't trigger any animation
    */
    virtual void set_position(float const x, float const y, float angle = 0);
    /** \brief Variant of set_position that takes a sf::Vector2f as an argument
     * **/
    virtual void set_position(sf::Vector2f const position, float angle = 0);
    /** \brief Variant of set_position that takes a sf::Vector2i as an argument
     * **/
    virtual void set_position(sf::Vector2i const position, float angle = 0);

    /** \brief Zooms on the sprite with animation
     *	\param scale the amplitude of the zoom (if scale <1, then the object
     *shrinks) \param speed the speed at which the animation will occur
     **/
    void zoom_sprite(float const scale, float const time = 15);

    void set_scale(float const scale);

    void set_angle(float angle);

    /** \brief an update function that continues the animation of an object.
     * Call this at each render loop **/
    virtual void continue_move();

    /** \brief Returns true iff the object is currently rendered in the middle
     * of an animation (has not finished to move) **/
    virtual bool is_animated();

    sf::Vector2f get_position();

    /** NOTE : this method is not written with underscores to stick with the
     *  SFML coding convention.
     * \brief setter for the m_speed and m_speedAngle
     *        private attributes
     **/
    void setSpeed(float const speed);

    /* draw attributes */
    void debug();

  protected:
    float m_angleDestination;
    float m_scaleDestination;
    float m_speed;
    float m_speedScale;
    float m_speedAngle;
    sf::Vector2f m_destination;
};

#endif
