/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "AnimatedSprite.hpp"

using namespace sf;

//________________________Constructor____________________________

AnimatedSprite::AnimatedSprite() : AnimatedTransformable() {
    m_textureSprite.reset();
}

AnimatedSprite::AnimatedSprite(float const w, float const h,
                               bool originAtCenter)
    : width(w), height(h) {
    m_textureSprite.reset();
    sf::VertexArray vArray(sf::Quads, 4);

    if (originAtCenter) {
        vArray[0].position = sf::Vector2f(-width / 2, -height / 2);
        vArray[1].position = sf::Vector2f(width / 2, -height / 2);
        vArray[2].position = sf::Vector2f(width / 2, height / 2);
        vArray[3].position = sf::Vector2f(-width / 2, height / 2);
    } else {
        vArray[0].position = sf::Vector2f(0, 0);
        vArray[1].position = sf::Vector2f(width, 0);
        vArray[2].position = sf::Vector2f(width, height);
        vArray[3].position = sf::Vector2f(0, height);
    }
    m_verticeSprite = vArray;
}

AnimatedSprite::AnimatedSprite(const sf::VertexArray &vertice)
    : AnimatedSprite() {
    m_verticeSprite = vertice;
}

//_________________________Draw_Function___________________________

void AnimatedSprite::update(sf::RenderWindow &window) {
    continue_move();
    window.draw(*this);
}

void AnimatedSprite::draw(sf::RenderTarget &target,
                          sf::RenderStates states) const {
    states.transform *= getTransform();
    states.texture = m_textureSprite.get();
    target.draw(m_verticeSprite, states);
}

//_________________________Setter_Function___________________________

void AnimatedSprite::set_texture(sf::Texture &texture) {
    m_textureSprite = std::make_shared<sf::Texture>(texture);
    if (!m_textureSprite) {
        std::cout << "failed load texture\n";
        return;
    }
    sf::Vector2f textureSize = sf::Vector2f(texture.getSize());

    m_verticeSprite[0].texCoords = sf::Vector2f(0, 0);
    m_verticeSprite[1].texCoords = sf::Vector2f(textureSize.x, 0);
    m_verticeSprite[2].texCoords = sf::Vector2f(textureSize.x, textureSize.y);
    m_verticeSprite[3].texCoords = sf::Vector2f(0, textureSize.y);

    m_textureSprite->setSmooth(true);
}

void AnimatedSprite::set_texture(std::string const &name) {
    set_texture(RM.get_texture(name));
}

void AnimatedSprite::set_vertex_array(const sf::VertexArray &vArray) {
    m_verticeSprite = vArray;
}

void AnimatedSprite::set_color(sf::Color color) {
    for (unsigned int i = 0; i < 4; i++) {
        m_verticeSprite[i].color = color;
    }
}

//_________________________Event_Function___________________________

bool AnimatedSprite::pos_on_sprite(sf::Vector2f const _mousePos) {
    auto mousePos = sf::Vector2f(_mousePos.x, _mousePos.y);
    return m_verticeSprite.getBounds().contains(
        getTransform().getInverse().transformPoint(mousePos));
}

bool AnimatedSprite::pos_on_sprite(sf::Vector2i const mousePos) {
    return pos_on_sprite(Vector2f(mousePos));
}

bool AnimatedSprite::pos_on_sprite(float const mouseX, float const mouseY) {
    return pos_on_sprite(Vector2f(mouseX, mouseY));
}
