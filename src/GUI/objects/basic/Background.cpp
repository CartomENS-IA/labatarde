/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Background.hpp"

Background::Background() : m_alpha(255), m_goal_alpha(255), m_speed(0.f) {}

Background::Background(std::string const &name) : Background::Background() {
    set_texture(name);
}

void Background::set_texture(std::string const &name) {
    m_sprite.setTexture(RM.get_texture(name));
    sf::Vector2u size = m_sprite.getTexture()->getSize();
    sf::Vector2f fsize(size);
    m_sprite.setScale(WINDOW_WIDTH / fsize.x,
                      WINDOW_HEIGHT / fsize.y);
}

void Background::set_transparency(int const alpha) {
    m_alpha = alpha;
    m_goal_alpha = alpha;
    m_sprite.setColor(sf::Color(255, 255, 255, sf::Uint8(m_alpha)));
}

void Background::fade(int const alpha, float const speed) {
    m_goal_alpha = alpha;
    m_speed = speed;
}

void Background::continue_fade() {
    float delta = float(m_goal_alpha - m_alpha);
    if (std::abs(delta) > m_speed) {
        int scaling = (int)(delta / m_speed);
        m_alpha += scaling;
    } else if (delta != 0) {
        m_alpha = m_goal_alpha;
    }
    m_sprite.setColor(sf::Color(255, 255, 255, sf::Uint8(m_alpha)));
}

bool Background::is_fading() const { return (m_alpha != m_goal_alpha); }

void Background::draw(sf::RenderWindow &bliton) const {
    if (m_alpha > 0) {
        bliton.draw(m_sprite);
    }
}

void Background::update(sf::RenderWindow &bliton) {
    continue_fade();
    if (m_alpha > 0) {
        bliton.draw(m_sprite);
    }
}
