#ifndef DEF_TIMER_SPRITE
#define DEF_TIMER_SPRITE

#include "AnimatedSprite.hpp"
#include "RessourceManager.hpp"

/** \file TimerSprite.hpp
 *	\author Léo.V
 *	\date 15 September
 *	\brief Definition of class TimerSprite
 **/
class TimerSprite : public AnimatedTransformable {
  public:
    TimerSprite();
    TimerSprite(sf::Vector2f m_position, float width, float height);
    void set_texture(std::string const &textureFull, std::string const &textureEmpty);
    void update(sf::RenderWindow &window);
    void set_filling(float fill);
    void set_scale(float scale);
    
     /** \brief returns true if the given position intersects the hitbox of the
     *sprite. Usually used with the mouse's position.
     **/
    bool pos_on_sprite(float const x, float const y);
    /** \brief variant of pos_on_sprite using sf::Vector2f **/
    bool pos_on_sprite(sf::Vector2f const position);
    /** \brief variant of pos_on_sprite using sf::Vector2i **/
    bool pos_on_sprite(sf::Vector2i const mposition);
    
  private:
    sf::Sprite m_spriteFull;
    sf::Sprite m_spriteEmpty;
    
    std::string m_textureFull;
    std::string m_textureEmpty;
    
    sf::Vector2u m_sizeFull;
    sf::Vector2u m_sizeEmpty;
    
    float m_width;
    float m_height;
    float m_percentFill;
    float m_scale;
};

#endif
