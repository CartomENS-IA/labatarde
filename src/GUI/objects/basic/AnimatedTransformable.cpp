/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "AnimatedTransformable.hpp"
#include <cmath>
#include <iostream> // DEBUG

using namespace sf;

//________________________Constructor____________________________

AnimatedTransformable::AnimatedTransformable()
    : m_angleDestination(0), m_scaleDestination(1), m_speed(1),
      m_speedScale(0.01f), m_speedAngle(1) {
    m_destination = getPosition();
}

//________________________Animation_Function_____________________

void AnimatedTransformable::continue_move() {
    sf::Vector2f currentPos = getPosition();
    float delta_x = m_destination.x - currentPos.x;
    float delta_y = m_destination.y - currentPos.y;
    float distance = std::abs(delta_x) + std::abs(delta_y);

    float delta_angle = m_angleDestination - getRotation();
    if (delta_angle > 0) {
        delta_angle -= 360;
    }
    if (delta_angle <= -180) {
        delta_angle += 360; // on tourne dans l'autre sens
    }

    if (distance >= 1) {
        if (distance >= m_speed) {
            float facteur = m_speed / distance;
            delta_x *= facteur;
            delta_y *= facteur;
        }
        setPosition(currentPos.x + delta_x, currentPos.y + delta_y);
    }

    if (delta_angle > m_speedAngle) {
        delta_angle = m_speedAngle;
    } else if (delta_angle < -m_speedAngle) {
        delta_angle = -m_speedAngle;
    }
    rotate(delta_angle);

    float delta_scale = m_scaleDestination - getScale().y;
    if (std::abs(delta_scale) > 1E-2) {
        if (delta_scale > 0) {
            delta_scale = std::min(delta_scale, m_speedScale);
        } else {
            delta_scale = std::max(delta_scale, -m_speedScale);
        }
        setScale(getScale().x + delta_scale, getScale().y + delta_scale);
    }
}

//_________________________Move_Function___________________________

void AnimatedTransformable::move(float const x, float const y, float angle,
                                 float const timing) {
    move(Vector2f(x, y), angle, timing);
}

void AnimatedTransformable::move(sf::Vector2f const _position, float angle,
                                 float const timing) {

    auto position = sf::Vector2f(_position.x,
                                 _position.y);

    float delta_x = position.x - getPosition().x;
    float delta_y = position.y - getPosition().y;
    float distance = std::abs(delta_x) + std::abs(delta_y);

    if (angle < 0) {
        angle += 360;
    }

    float angleDist = getRotation() - angle;
    if (angleDist < 0) {
        angleDist += 360;
    }

    m_destination = position;
    m_speed = distance / timing;
    m_speedAngle = angleDist / timing;

    if (angle < 0) {
        angle += 360;
    }
    m_angleDestination = angle;
}

void AnimatedTransformable::move(sf::Vector2i const position, float angle,
                                 float const timing) {
    move(Vector2f(position), angle, timing);
}

//_________Move_Relative__________

void AnimatedTransformable::move_relative(float const x, float const y,
                                          float angle, float const timing) {
    move(x + getPosition().x, y + getPosition().y, angle + getRotation(),
         timing);
}

void AnimatedTransformable::move_relative(sf::Vector2f const position,
                                          float angle, float const timing) {
    move(position + getPosition(), angle + getRotation(), timing);
}

void AnimatedTransformable::move_relative(sf::Vector2i const position,
                                          float angle, float const timing) {
    move(sf::Vector2f(position) + getPosition(), angle + getRotation(), timing);
}

//_________Other_move_________

void AnimatedTransformable::set_position(float const x, float const y,
                                         float angle) {
    set_position(sf::Vector2f(x, y), angle);
}

void AnimatedTransformable::set_position(sf::Vector2f const _position,
                                         float angle) {
    auto position = sf::Vector2f(_position.x ,
                                 _position.y );
    if (angle < 0) {
        angle += 360;
    }
    setPosition(position);
    setRotation(angle);
    m_destination = position;
    m_angleDestination = angle;
}

void AnimatedTransformable::set_position(sf::Vector2i const position,
                                         float angle) {
    set_position(sf::Vector2f(position), angle);
}

void AnimatedTransformable::zoom_sprite(float const scale, float const timing) {
    m_scaleDestination = scale;
    if (scale - getScale().y > 0) {
        m_speedScale = (scale - getScale().y) / timing;
    } else {
        m_speedScale = (getScale().y - scale) / timing;
    }
}

void AnimatedTransformable::set_scale(float const scale) {
    m_scaleDestination = scale;
    setScale(scale, scale);
}

void AnimatedTransformable::set_angle(float angle) {
    m_angleDestination = angle;
    setRotation(angle);
}

void AnimatedTransformable::setSpeed(float const speed) {
    m_speed = speed;
    if (m_speed < 1) {
        m_speed = 1;
    }
    m_speedAngle = speed;
}

sf::Vector2f AnimatedTransformable::get_position() {
    auto temp = sf::Transformable::getPosition();
    return sf::Vector2f(temp.x , temp.y );
}

//_________________________Event_Function___________________________

bool AnimatedTransformable::is_animated() {
    float delta_x = m_destination.x - getPosition().x;
    float delta_y = m_destination.y - getPosition().y;
    float distance = std::abs(delta_x) + std::abs(delta_y);

    float delta_angle = m_angleDestination - getRotation();
    if (std::abs(delta_angle - 360) < 1E-7) {
        delta_angle = 0;
    }
    float delta_scale = m_scaleDestination - getScale().x;

    if (distance >= 1 || std::abs(delta_angle) >= 1 || delta_scale > 0.01 ||
        delta_scale < -0.01) {
        // if the sprite moves, don't touch
        return true;
    }
    return false;
}

//_________________________Debug_Function____________________________

void AnimatedTransformable::debug() {
    std::cout << m_destination.x << " " << m_destination.y << " "
              << m_angleDestination << " " << m_scaleDestination << " "
              << m_speed << " " << m_speedScale << " " << m_speedAngle << " "
              << std::endl;
    std::cout << getPosition().x << " " << getPosition().y << " "
              << getRotation() << " " << getScale().x << " " << getScale().y
              << std::endl;
}
