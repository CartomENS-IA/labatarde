/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_ANIMATED_SPRITE
#define DEF_ANIMATED_SPRITE

#include "AnimatedTransformable.hpp"
#include "RessourceManager.hpp"

/** \file AnimatedSprite.hpp
 *	\author Léo.V
 *	\date 13 October
 *	\brief Definition of class AnimatedSprite
 **/

/** Animated Sprite provides a class that can draw, move and transform sprites
on screen. However, thoses movements can be displayed as an animation over time
(whereas movements of the class sf::Sprite are instantaneous)

You can store an AnimatedSprite as an attribute of your own class and plot is on
screen.

You can inherit from the class AnimatedSprite in order to make your own object
move like a boss ! In that case, you will have to rewrite draw in your child
class. The typical form of the draw function is :

void Card::draw(sf::RenderTarget& target, sf::RenderStates states) const{
        states.transform *= getTransform();
    states.texture = m_texture_of_your_class;
    target.draw(m_verticeSprite, states);
}

but you can do more complicated and fancy stuff.
classical attributes of your class is textures

-- HOW TO USE --
you can use all functions of transformable (see SFML doc)

mouse_on_sprite return true if coordinates given in argument is in the sprite.
It is called like that because it is often called with the mouse coordinates
(it always return false if the sprite is moving)

The `update`  method draws the sprite with respect to the animation.

if you want more control you can use the `draw` method to draw without
continuing the animation or use `continue_move` to continue the animation
without drawing.

\brief A drawable sprite that inherits from animation methods of
AnimatedTransformable
**/
class AnimatedSprite : public sf::Drawable, public AnimatedTransformable {

  public:
    /**
     * Default constructor. This constructor does not provide any VertexArray to
     *draw the sprite. You will have to define your VertexArray yourself and
     *specify it with the `set_vertex_array` method \brief Default constructor
     **/
    AnimatedSprite();

    /** Use this constructor to initialize your sprite with a rectangular
     *VertexArray.
     *
     * \brief Constructor with a rectangular VertexArray
     * \param originAtCenter specify the position of the origin of the sprite.
     *The sprite is set to coordinates
     * (-width/2,-heigth/2) , (-width/2,height/2) , (width/2, height/2),
     *(width/2, -height/2) if originAtCenter is set to true, and (0,0) ,
     *(width,0) , (width,height), (0,height) if set to false
     * /!\ If you plan to do rotations with your sprite, ALWAYS set the origin
     *at the center
     **/
    AnimatedSprite(float const width, float const height,
                   bool originAtCenter = true);

    /** Use this constructor to directly specify a VertexArray
     *   if you do you need to redefine set_texture
     *	\brief The constructor that uses a predefined VertexArray.
     **/
    AnimatedSprite(const sf::VertexArray &vertice);

    /** \brief Update method. Is a shortcut to call continue_move and draw at
     * the same time **/
    void update(sf::RenderWindow &window);

    /** Purely virtual function. Same signature as the draw function of
     *sf::Drawable. You can to re-implement this in every class that inherit
     *from AnimatedSprite \brief reinplementation of the draw method of
     *sf::Drawable
     **/
    virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const;

    /** \brief setter the texture of the sprite. Without texture, the sprite
     * draws a white square **/
    virtual void set_texture(sf::Texture &texture);
    /** \brief variant of set_texture. Will do a search in the RessourceManager
     *on its own. \param name The path of the texture to load. It should be a
     *path relative to the ressources folder
     **/
    virtual void set_texture(std::string const &name);

    /** \brief Setter for set a color to vertices**/
    void set_color(sf::Color color);

    /** \brief Setter for the VertexArray of the sprite **/
    void set_vertex_array(const sf::VertexArray &vArray);

    /** \brief returns true if the given position intersects the hitbox of the
     *sprite. Usually used with the mouse's position.
     **/
    bool pos_on_sprite(float const x, float const y);
    /** \brief variant of pos_on_sprite using sf::Vector2f **/
    bool pos_on_sprite(sf::Vector2f const position);
    /** \brief variant of pos_on_sprite using sf::Vector2i **/
    bool pos_on_sprite(sf::Vector2i const mposition);

    float width;
    float height;

  protected:
    sf::Vector2f m_destination;
    float m_angleDestination;
    float m_scaleDestination;
    float m_speed;
    float m_speedScale;

    std::shared_ptr<sf::Texture> m_textureSprite;

    sf::VertexArray m_verticeSprite;
};

#endif
