/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 * \author Léo.V , Guillaume.C
 * \brief Definition of class GraphicTimer
 **/

#include <SFML/Graphics.hpp>
#include <cmath>
#include "Constants.hpp"

/** \class **/
class GraphicTimer {
  public:
    GraphicTimer();
    
    GraphicTimer(sf::Vector2f const center, float const size,
                 float const sizeOutline = 10, sf::Color const begin = RED,
                 sf::Color const end = GREEN, bool cycle = false);
    GraphicTimer(float const centerX, float const centerY, float const size,
                 float const sizeOutline = 10, sf::Color const begin = RED,
                 sf::Color const end = GREEN, bool cycle = false);

    /** \brief make the timer advance one step **/
    void tick();
    void update(sf::RenderWindow &);
    void draw(sf::RenderWindow &);

  private:
    static sf::Time const TICK_TIME;

    sf::Vector2f getPoint(std::size_t index, float const size,
                          float const sizeOutline) const;
    bool m_cycle;
    float m_size;
    sf::Clock m_clock;
    unsigned int m_nbPointsDrawn;
    unsigned int m_nbPoints;
    sf::Vector2f m_center;
    std::vector<sf::Vertex> m_vertices;
};
