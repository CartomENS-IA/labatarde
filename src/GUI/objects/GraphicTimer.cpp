/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GraphicTimer.hpp"

sf::Color mix_color(sf::Color const A, sf::Color const B, sf::Uint8 i) {
    sf::Color res(0, 0, 0, 0);
    res.r = sf::Uint8((A.r * (255 - i) + B.r * i) / 255);
    res.g = sf::Uint8((A.g * (255 - i) + B.g * i) / 255);
    res.b = sf::Uint8((A.b * (255 - i) + B.b * i) / 255);
    res.a = sf::Uint8((A.a * (255 - i) + B.a * i) / 255);
    return res;
}

sf::Time const GraphicTimer::TICK_TIME = sf::milliseconds(4);

GraphicTimer::GraphicTimer(){}

GraphicTimer::GraphicTimer(sf::Vector2f const center, float const size,
                           float const sizeOutline, sf::Color const begin,
                           sf::Color const end, bool cycle)
:   m_cycle(cycle), m_size(size), m_nbPointsDrawn(2),
    m_nbPoints(256), m_center(center)
{
    for (unsigned int i = 0; i < m_nbPoints; i++) {
        m_vertices.emplace_back(getPoint(i, m_size, sizeOutline),
                                mix_color(begin, end, sf::Uint8(i)));
    }
}

GraphicTimer::GraphicTimer(float const x, float const y, float const size,
                           float const sizeOutline, sf::Color const begin,
                           sf::Color const end, bool cycle)
: GraphicTimer(sf::Vector2f(x, y), size, sizeOutline, begin, end, cycle)
{}

sf::Vector2f GraphicTimer::getPoint(std::size_t index, float const size,
                                    float const sizeOutline) const
{
    using namespace std;
    if (index % 2) {
        return getPoint(index - 1, size - sizeOutline, 0);
    }
    index /= 2;
    float angle = 4 * PI / float(m_nbPoints - 1);
    float y = -cos(angle * float(index)) * size;
    float x = sin(angle * float(index)) * size;
    return sf::Vector2f(x, y) + m_center;
}

void GraphicTimer::draw(sf::RenderWindow &app) {
    app.draw(&m_vertices[0], m_nbPointsDrawn, sf::TrianglesStrip);
}

void GraphicTimer::update(sf::RenderWindow &app) {
    if (m_clock.getElapsedTime() > TICK_TIME) {
        m_clock.restart();
        tick();
    }
    draw(app);
}

void GraphicTimer::tick() {
    m_nbPointsDrawn += 2;
    if (m_nbPointsDrawn > m_nbPoints)
        m_nbPointsDrawn = 2;
}
