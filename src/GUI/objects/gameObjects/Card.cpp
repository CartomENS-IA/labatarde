/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GUI/objects/gameObjects/Card.hpp" // /!\ Important to distinguish this from Core/Card.hpp
#include "Constants.hpp"
#include "OptionManager.hpp"

std::string name_value[9] = {"chat",  "chien",    "jongleur", "musicien", "fou",
                             "valet", "cavalier", "dame",     "roi"};
std::string name_color[5] = {"", "trefle", "carreau", "coeur", "pique"};

float const Card::WIDTH = 100*optionManager()->get_screen_scale();
float const Card::LENGTH = 141*optionManager()->get_screen_scale();

//________________________Constructor____________________________

Card::Card()
    : AnimatedSprite(Card::WIDTH, Card::LENGTH), m_isVisible(false),
      m_advancementReturn(1), m_speedReturn(0.1f) {
    m_speed = 15;
    set_position(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
    if (optionManager()->get_card_type() == CardType::SPECIAL){
        set_texture("cards/special/base-dos.png");
    } else {
        set_texture("cards/classical/base-dos.png");
    }

    m_textureSprite->setSmooth(true);
}

Card::Card(Core::Value value, Core::Suit color) : Card() {
    m_cardData = Core::Card(color, value);
}

Card::Card(Core::Card typeCard) : Card() { m_cardData = typeCard; }

//_________________________Move_Function___________________________

void Card::flip_card(float timing) {
    m_speedReturn = 2 / timing;
    m_isVisible = !m_isVisible;
    m_advancementReturn = -1;
}

void Card::flip_card(Core::Card typeCard, float timing) {
    m_isVisible = true;
    m_cardData = typeCard;
    m_advancementReturn = -1;
    m_speedReturn = 2 / timing;
}

void Card::continue_move() {
    AnimatedTransformable::continue_move();
    if (m_advancementReturn < 1) {

        m_advancementReturn += m_speedReturn;
        if (m_advancementReturn >= 1) {
            m_advancementReturn = 1;
        }
        if (m_advancementReturn < m_speedReturn && m_advancementReturn >= 0) {
            std::string adress = "cards/";
            if (optionManager()->get_card_type() == CardType::SPECIAL){
                adress += "special/";
            } else {
                adress += "classical/";
            }

            if (m_isVisible) {
                set_texture(adress +
                            name_value[int(m_cardData.value())] + "-" +
                            name_color[int(m_cardData.suit())] + ".png");
            } else {
                set_texture(adress+"base-dos.png");
            }
        }
        setScale((float)sin(PI / 2 * m_advancementReturn) * getScale().y,
                 getScale().y);
    }
}

void Card::update(sf::RenderWindow &window) {
    continue_move();
    window.draw(*this);
}

bool Card::is_animated() {
    return (m_advancementReturn < 1 || AnimatedTransformable::is_animated());
}

//_________________________Setter_Getter____________________________

bool Card::is_visible() { return m_isVisible; }

Core::Suit Card::get_color() { return m_cardData.suit(); }

Core::Value Card::get_value() { return m_cardData.value(); }

Core::Card Card::get_card_data() { return m_cardData; }
