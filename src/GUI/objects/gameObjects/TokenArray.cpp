/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Arrays.hpp"
#include "Constants.hpp"
#include "Card.hpp"

#include <iostream>

using namespace sf;

// ________ Token ______________________
float const Token::SIZE = 75.f * optionManager()->get_screen_scale();
float const Token::SCALE = 0.6f;

Token::Token() : AnimatedSprite(SIZE, SIZE), m_isCompleted(false) {
    set_texture("tokens/token_white.png");
}

void Token::set_black() { set_texture("tokens/token_black.png"); }

void Token::set_white() { set_texture("tokens/token_white.png"); }

void Token::set_gray() { set_texture("tokens/token_gray.png"); }

void Token::set_number(int i) {
    std::string number = std::to_string(i);
    set_texture("tokens/token_" + number + ".png");
}



// ____________________ TokenArray ____________________

TokenArray::TokenArray(sf::Vector2f const position, float const orientation)
    : m_nbToken(0), m_nbTrick(0), m_nbTrickDraw(0), m_centerArray(position),
      m_scaleArray(Token::SCALE), m_orientationArray(orientation) {}

void TokenArray::set_nb_token(int nb) {
    m_nbToken = nb;
    m_nbTrick = 0;
    m_nbTrickDraw = 0;
    m_tokenPlayer = std::vector<Token>(std::max(1, nb));
    for (unsigned int i = 0; i < m_tokenPlayer.size(); i++) {
        m_tokenPlayer[i].set_scale(Token::SCALE);
        m_tokenPlayer[i].set_position(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
    }

    m_cardToken = std::vector<Card>(0);
    if (nb == 0) {
        m_tokenPlayer[0].set_black();
    }

    replace_token(30);
}

// ___________________ Draw Function ____________________

void TokenArray::draw(sf::RenderWindow &window) {
    if (m_nbToken == 0) { // particular case
        m_tokenPlayer[0].continue_move();
        window.draw(m_tokenPlayer[0]);
        for (int i = 0; i < m_nbTrickDraw; i++) {
            m_cardToken[i].continue_move();
            window.draw(m_cardToken[i]);
        }
    } else {
        for (int i = 0; i < m_nbTrickDraw; i++) {
            m_cardToken[i].continue_move();
            window.draw(m_cardToken[i]);
        }
        for (int i = 0; i < m_nbToken; i++) {
            m_tokenPlayer[i].continue_move();
            window.draw(m_tokenPlayer[i]);
        }
    }
}

// ____________________ Move Function ____________________

sf::Vector2f TokenArray::compute_position(int i, int n) {
    return m_centerArray +
           rotateVector(m_scaleArray * (0.5f + float(i) - float(n) / 2.0f) *
                            Card::WIDTH,
                        0, m_orientationArray);
}

void TokenArray::replace_token(float const timing) { // replace token

    int n = m_nbToken == 0 ? 1 + m_nbTrick : std::max(m_nbToken, m_nbTrick);
    m_scaleArray = Token::SCALE;
    if (n > 4) {
        m_scaleArray *= 4.0f / float(n);
    }

    if (m_nbToken == 0) { // particular case
        m_tokenPlayer[0].zoom_sprite(m_scaleArray, timing);
        m_tokenPlayer[0].move(compute_position(0, n), 360 - m_orientationArray,
                              timing);
        for (int i = 0; i < m_nbTrickDraw; i++) {
            m_cardToken[i].move(compute_position(i + 1, n),
                                360 - m_orientationArray, timing);
            m_cardToken[i].zoom_sprite(m_scaleArray, timing);
        }
    } else {
        for (int i = 0; i < m_nbTrickDraw; i++) {
            m_cardToken[i].move(compute_position(i, n),
                                360 - m_orientationArray, timing);
            m_cardToken[i].zoom_sprite(m_scaleArray, timing);
        }
        for (int i = 0; i < m_nbToken; i++) {
            m_tokenPlayer[i].move(compute_position(i, n),
                                  360 - m_orientationArray, timing);
            m_tokenPlayer[i].zoom_sprite(m_scaleArray, timing);
        }
    }
}

// _____________________ Trick Function _________________

void TokenArray::new_trick() {
    m_nbTrick++;
    m_cardToken.push_back(Card());
    replace_token(C_TRICK_TIME);
}

void TokenArray::draw_new_trick() {
    int n = std::max(m_nbToken, m_nbTrick);
    int i = m_nbTrickDraw;
    if (!m_nbToken) {
        n++;
        i++;
    }
    m_cardToken[m_nbTrickDraw].set_position(compute_position(i, n),
                                            360 - m_orientationArray);
    m_cardToken[m_nbTrickDraw].set_scale(m_scaleArray);
    m_nbTrickDraw++;
}

sf::Vector2f TokenArray::get_pos_free_token() {
    int n = std::max(m_nbToken, m_nbTrick);
    int i = m_nbTrick - 1;
    if (!m_nbToken) {
        n++;
        i++;
    }
    return compute_position(i, n);
}

float TokenArray::get_scale() { return m_scaleArray; }

bool TokenArray::is_animated() {
    for (int i = 0; i < m_nbToken; i++) {
        if (m_tokenPlayer[i].is_animated()) {
            return true;
        }
    }
    return false;
}
