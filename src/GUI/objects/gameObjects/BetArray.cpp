/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Arrays.hpp"
#include "AnimatedSprite.hpp"
#include "OptionManager.hpp"

#include <iostream> // DEBUG

using namespace Core;

// _________ Bet Array Constants _______________________________________________

float const BetArray::REAL_WIDTH_AURA_TOKEN = 700*optionManager()->get_screen_scale();
float const BetArray::REAL_HEIGHT_TRUMP = 183*optionManager()->get_screen_scale();
float const BetArray::REAL_HEIGHT_AURA_TRUMP = 263*optionManager()->get_screen_scale();
float const BetArray::REAL_WIDTH_ALL_WITHOUT_TRUMP = 227*optionManager()->get_screen_scale();
float const BetArray::REAL_WIDTH_AURA_TRUMP = 307*optionManager()->get_screen_scale();

float const BetArray::TOKEN_WIDTH =
    75*optionManager()->get_screen_scale(); // /!\ Ensure that this has the same value as Token::SIZE
float const BetArray::TOKEN_REAL_WIDTH = 450*optionManager()->get_screen_scale();

float const BetArray::WIDTH_AURA =
    (TOKEN_WIDTH * REAL_WIDTH_AURA_TOKEN) / TOKEN_REAL_WIDTH;

float const BetArray::DIST_BETWEEN_TOKEN = 8*optionManager()->get_screen_scale();
float const BetArray::DIST_BETWEEN_TRUMP = 20*optionManager()->get_screen_scale();
float const BetArray::DIST_BETWEEN_TOKEN_TRUMP = 20*optionManager()->get_screen_scale();

// _____________ Constructor ___________________________________________________
BetArray::BetArray(float const x, float const y, int nb_token)
    : m_mechoune(-1), m_nb_forbidden(-1), m_trump_clicked(-1),
      m_token_clicked(-1) {
    m_center = sf::Vector2f(x, y);
    m_current_bet = Core::Bet(Core::Trump::NoTrump, 0);
    m_allTokens = std::vector<Token>();
    for (unsigned int i = 0; i <= 9; i++) {
        Token i_th_token;
        i_th_token.set_gray();
        m_allTokens.push_back(i_th_token);
    }
    m_allTokens[0].set_black();
    set_nb_token(nb_token);

    // aura of the token (selected or not)
    m_aura_token = AnimatedSprite(WIDTH_AURA, WIDTH_AURA);
    m_aura_token.set_texture("tokens/token_aura.png");
    m_aura_token_selected = AnimatedSprite(WIDTH_AURA, WIDTH_AURA);
    m_aura_token_selected.set_texture("tokens/token_aura_selected.png");

    float const widthAllWithoutTrump =
        (REAL_WIDTH_ALL_WITHOUT_TRUMP * WIDTH_TRUMP) / REAL_HEIGHT_TRUMP;
    float const widthAuraTrump = widthAllWithoutTrump * REAL_WIDTH_AURA_TRUMP /
                                 REAL_WIDTH_ALL_WITHOUT_TRUMP;
    float const heightAuraTrump =
        REAL_HEIGHT_AURA_TRUMP * WIDTH_TRUMP / REAL_HEIGHT_TRUMP;

    // The trumps and their sprites
    for (int i = 0; i < 6; i++) {
        float width_conditionned;
        if (i == 0 || i == 5) {
            width_conditionned = widthAllWithoutTrump;
        } else {
            width_conditionned = WIDTH_TRUMP;
        }
        AnimatedSprite current_trump(width_conditionned, WIDTH_TRUMP);
        AnimatedSprite current_trump_aura(widthAuraTrump, heightAuraTrump);
        AnimatedSprite current_trump_aura_selected(widthAuraTrump,
                                                   heightAuraTrump);

        current_trump.set_texture("tokens/" + trump_to_string((Trump)i) +
                                  ".png");
        current_trump_aura.set_texture("tokens/" +
                                       trump_to_string((Trump)i) + "_aura.png");
        current_trump_aura_selected.set_texture("tokens/" +
                                                trump_to_string((Trump)i) +
                                                "_aura_selected.png");

        m_allTrumps.push_back(current_trump);
        m_allTrumps_aura.push_back(current_trump_aura);
        m_allTrumps_aura_selected.push_back(current_trump_aura_selected);
    }

    // Setting the position of the trumps and the aura
    m_allTrumps[0].set_position(m_center.x - 5 * DIST_BETWEEN_TRUMP / 2 -
                                    2 * WIDTH_TRUMP - widthAllWithoutTrump / 2,
                                m_center.y);
    m_allTrumps_aura[0].set_position(m_allTrumps[0].get_position());
    m_allTrumps_aura_selected[0].set_position(m_allTrumps[0].get_position());

    m_allTrumps[5].set_position(m_center.x + 5 * DIST_BETWEEN_TRUMP / 2 +
                                    2 * WIDTH_TRUMP + widthAllWithoutTrump / 2,
                                m_center.y);
    m_allTrumps_aura[5].set_position(m_allTrumps[5].get_position());
    m_allTrumps_aura_selected[5].set_position(m_allTrumps[5].get_position());

    float most_left = 3 * (DIST_BETWEEN_TRUMP + WIDTH_TRUMP) / 2;
    for (int i = 0; i < 4; i++) {
        m_allTrumps[i + 1].set_position(
            m_center.x - most_left +
                (float)i * (DIST_BETWEEN_TRUMP + WIDTH_TRUMP),
            m_center.y);
        m_allTrumps_aura[i + 1].set_position(m_allTrumps[i + 1].get_position());
        m_allTrumps_aura_selected[i + 1].set_position(
            m_allTrumps[i + 1].get_position());
    }
}

void BetArray::set_nb_token(int nb_token) {
    m_nb_token = nb_token;
    float most_left =
        float(m_nb_token - 1) * (TOKEN_WIDTH + DIST_BETWEEN_TOKEN) / 2;
    float y_center_tokens = m_center.y - WIDTH_TRUMP / 2 -
                            DIST_BETWEEN_TOKEN_TRUMP - TOKEN_WIDTH / 2;
    for (int i = 0; i < m_nb_token; i++) {
        m_allTokens[i].set_position(m_center.x - most_left +
                                        (float)i *
                                            (TOKEN_WIDTH + DIST_BETWEEN_TOKEN),
                                    y_center_tokens);
    }
}

void BetArray::set_playable_bet(std::vector<Core::Bet> bets_available) {
    m_betsAvailable = bets_available;
}

void BetArray::set_playable_bet(Core::Bet current_bet, bool mechoune,
                                int nb_forbidden) {
    m_current_bet = current_bet;
    m_mechoune = mechoune;
    m_nb_forbidden = nb_forbidden;

    // We start by choosing the tokens allowed
    for (int token = 0; token < 10; token++) {
        if (m_trump_clicked != -1) {
            if (m_mechoune && m_trump_clicked != (int)current_bet.trump) {
                m_allTokens[token].set_color(TRANSPARENT);
            } else if (m_trump_clicked == (int)m_current_bet.trump) {
                if (token != nb_forbidden) {
                    m_allTokens[token].set_color(WHITE);
                } else {
                    m_allTokens[token].set_color(TRANSPARENT);
                }
            } else {
                if ((token > m_current_bet.value ||
                     (token == m_current_bet.value &&
                      m_trump_clicked > (int)m_current_bet.trump))) {
                    m_allTokens[token].set_color(WHITE);
                } else {
                    m_allTokens[token].set_color(TRANSPARENT);
                }
            }

            if (m_betsAvailable.size() &&
                find(m_betsAvailable.begin(), m_betsAvailable.end(),
                     Core::Bet(static_cast<Core::Trump>(m_trump_clicked),
                               (token))) == m_betsAvailable.end()) {
                m_allTokens[token].set_color(TRANSPARENT);
            }

        } else {
            m_allTokens[token].set_color(WHITE);
        }
    }

    // Now the bets
    for (int trump = 0; trump < 6; trump++) {
        // Fix the color if the game is mechouned
        if (m_mechoune) {
            if ((int)current_bet.trump == trump) {
                m_allTrumps[trump].set_color(WHITE);
            } else {
                m_allTrumps[trump].set_color(TRANSPARENT);
            }
        } else {
            // If a token is selected
            if (m_token_clicked != -1) {
                if (m_token_clicked < m_current_bet.value) {
                    if (trump == (int)m_current_bet.trump &&
                        m_token_clicked != m_nb_forbidden) {
                        m_allTrumps[trump].set_color(WHITE);
                    } else {
                        m_allTrumps[trump].set_color(TRANSPARENT);
                    }
                } else {
                    if (m_token_clicked > m_current_bet.value) {
                        if (trump == (int)m_current_bet.trump &&
                            m_token_clicked == m_nb_forbidden) {
                            m_allTrumps[trump].set_color(TRANSPARENT);
                        } else {
                            m_allTrumps[trump].set_color(WHITE);
                        }
                    } else { // m_token_clicked == current_bet.value
                        if (trump > (int)m_current_bet.trump ||
                            (trump == (int)m_current_bet.trump &&
                             m_token_clicked != m_nb_forbidden)) {
                            m_allTrumps[trump].set_color(WHITE);
                        } else {
                            m_allTrumps[trump].set_color(TRANSPARENT);
                        }
                    }
                }

                if (m_betsAvailable.size() &&
                    find(m_betsAvailable.begin(), m_betsAvailable.end(),
                         Core::Bet(static_cast<Core::Trump>(trump),
                                   (m_token_clicked))) ==
                        m_betsAvailable.end()) {
                    m_allTrumps[trump].set_color(TRANSPARENT);
                }
            } else {
                m_allTrumps[trump].set_color(WHITE);
            }
        }
    }
}

std::shared_ptr<Core::Bet> BetArray::click(sf::Vector2i const mousePos) {
    return click(sf::Vector2f(mousePos));
}

std::shared_ptr<Core::Bet> BetArray::click(float const mouseX,
                                           float const mouseY) {
    return click(sf::Vector2f(mouseX, mouseY));
}

std::shared_ptr<Core::Bet> BetArray::click(sf::Vector2f const mousePos) {
    int i_token = get_token_at_pos(mousePos);
    int i_trump = get_trump_at_pos(mousePos);
    if (i_trump != -1) {
        m_trump_clicked = i_trump;
        set_playable_bet(m_current_bet, m_mechoune, m_nb_forbidden);
    }
    if (i_token != -1) {
        m_token_clicked = i_token;
        set_playable_bet(m_current_bet, m_mechoune, m_nb_forbidden);
    }
    if (m_token_clicked != -1 and m_trump_clicked != -1) {
        int return_token = m_token_clicked;
        int return_trump = m_trump_clicked;
        m_token_clicked = -1;
        m_trump_clicked = -1;
        set_playable_bet(m_current_bet, m_mechoune, m_nb_forbidden);
        return std::make_shared<Core::Bet>(
            static_cast<Core::Trump>(return_trump), return_token);
    }
    return nullptr;
}

void BetArray::draw(sf::RenderWindow &window) {
    int i_token = get_token_at_pos(sf::Mouse::getPosition(window));
    int i_trump = get_trump_at_pos(sf::Mouse::getPosition(window));

    // print the aura for the token
    if (i_token == -1) {
        i_token = m_token_clicked;
        if (m_token_clicked != -1) {
            m_aura_token_selected.set_position(
                m_allTokens[m_token_clicked].get_position());
            m_aura_token_selected.update(window);
        }
    } else {
        m_aura_token.set_position(m_allTokens[i_token].get_position());
        m_aura_token_selected.set_position(
            m_allTokens[m_token_clicked].get_position());
        if (m_token_clicked != i_token) {
            m_aura_token.update(window);
        }
        if (m_token_clicked != -1) {
            m_aura_token_selected.update(window);
        }
    }

    // print the aura for the trumps
    if (i_trump == -1) {
        i_trump = m_trump_clicked;
        if (m_trump_clicked != -1) {
            m_allTrumps_aura_selected[i_trump].update(window);
        }
    } else {
        if (m_trump_clicked != i_trump) {
            m_allTrumps_aura[i_trump].update(window);
        }
        if (m_trump_clicked != -1) {
            m_allTrumps_aura_selected[m_trump_clicked].update(window);
        }
    }

    if (i_token == 0) {
        m_allTokens[0].set_number(0);
    } else {
        m_allTokens[0].set_black();
    }
    m_allTokens[0].update(window);
    for (int i = 1; i < m_nb_token; i++) {
        if (i_token != -1) {
            if (i < i_token) {
                m_allTokens[i].set_white();
            } else {
                if (i == i_token) {
                    m_allTokens[i].set_number(i_token);
                } else {
                    m_allTokens[i].set_gray();
                }
            }
        } else {
            m_allTokens[i].set_gray();
        }
        m_allTokens[i].update(window);
    }
    for (int i = 0; i < 6; i++) {
        m_allTrumps[i].update(window);
    }
}

int BetArray::get_token_at_pos(sf::Vector2f const mousePos) {
    float length_token_array =
        (TOKEN_WIDTH + DIST_BETWEEN_TOKEN) * float(m_nb_token);
    float rightmost = m_center.x + length_token_array / 2;
    float leftmost = m_center.x - length_token_array / 2;
    // to say that the mouse is over a token
    if (mousePos.x <= leftmost or mousePos.x >= rightmost or
        mousePos.y <= m_center.y - WIDTH_TRUMP / 2 - DIST_BETWEEN_TOKEN_TRUMP -
                          TOKEN_WIDTH or
        mousePos.y >= m_center.y - WIDTH_TRUMP / 2 - DIST_BETWEEN_TOKEN_TRUMP) {
        return -1;
    } else {
        return int((mousePos.x - leftmost) /
                   (TOKEN_WIDTH + DIST_BETWEEN_TOKEN));
    }
}

int BetArray::get_token_at_pos(float const mouseX, float const mouseY) {
    return BetArray::get_token_at_pos(sf::Vector2f(mouseX, mouseY));
}

int BetArray::get_token_at_pos(sf::Vector2i const mousePos) {
    return BetArray::get_token_at_pos(sf::Vector2f(mousePos));
}

int BetArray::get_trump_at_pos(sf::Vector2f const mousePos) {
    int trumpReturn = -1;
    for (int i = 0; i < 6; i++) {
        if (trumpReturn == -1 && m_allTrumps[i].pos_on_sprite(mousePos)) {
            trumpReturn = i;
        }
    }
    return trumpReturn;
}

int BetArray::get_trump_at_pos(sf::Vector2i const mousePos) {
    return BetArray::get_trump_at_pos(sf::Vector2f(mousePos));
}
