/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Hand.hpp"
#include <iostream>

// ______ Hand Constant ________

// zooming factor when the mouse is hovering a card
float const Hand::ZOOM = 1.1f;

//__________________Constructor__________________

Hand::Hand(sf::Vector2f const position, CardinalPoints const cardi,
           float const scale, bool const mainPlayer, float const angleFan)
    : Hand(position.x, position.y, cardi, scale, mainPlayer, angleFan) {}

Hand::Hand(float const x, float const y, CardinalPoints const cardi,
           float const scale, bool const mainPlayer, float const angleFan)
    : m_scaleHand(scale), m_orientationHand(90.f*(float) cardi),
      m_mainPlayer(mainPlayer), m_cardMain(-1),
      m_cardSpacing((mainPlayer) ? 45*SCALE : 60*SCALE), m_angleFan(angleFan),
      m_distance_circle_center(m_scaleHand * m_cardSpacing * 180 /
                               (angleFan * PI)) {
    m_center = sf::Vector2f(x, y);

    switch (cardi) {
    case CardinalPoints::EAST:
        set_play_zone(WINDOW_WIDTH /2+180*SCALE, WINDOW_HEIGHT/2-50*SCALE);
        break;
    case CardinalPoints::WEST:
        set_play_zone(WINDOW_WIDTH /2-180*SCALE, WINDOW_HEIGHT/2-50*SCALE);
        break;
    case CardinalPoints::NORTH:
        set_play_zone(WINDOW_WIDTH /2+40*SCALE, WINDOW_HEIGHT/2-80*SCALE);
        break;
    case CardinalPoints::SOUTH:
    default:
        set_play_zone(WINDOW_WIDTH /2-40*SCALE, WINDOW_HEIGHT/2-20*SCALE);
        break;
    }

    m_handPlayer = std::vector<std::shared_ptr<Card>>(0, nullptr);
}

void Hand::set_play_zone(sf::Vector2f const position) { m_playZone = position; }

void Hand::set_play_zone(float const x, float const y) {
    m_playZone = sf::Vector2f(x, y);
}

//__________________Draw_Function_______________

void Hand::draw(sf::RenderWindow &window) {
    std::shared_ptr<Card> mainCard(nullptr);

    int i_card = get_card_at_pos(sf::Mouse::getPosition(window));

    if (i_card >= 0) {
        mainCard = m_handPlayer[i_card];
    }

    for (unsigned int i = 0; i < m_handPlayer.size(); i++) {
        m_handPlayer[i]->continue_move();
        if (!m_playableCards.empty() &&
            find(m_playableCards.begin(), m_playableCards.end(),
                 m_handPlayer[i]->get_card_data()) == m_playableCards.end()) {
            m_handPlayer[i]->set_color(CARD_GRAY);
        } else {
            m_handPlayer[i]->set_color(WHITE);
        }

        if (m_handPlayer[i] != mainCard) {
            window.draw(*m_handPlayer[i]);
        }
    }

    if (mainCard) {
        window.draw(*mainCard);
    }
}

// _________________ Move Function ________________

void Hand::replace_card(float const timing) {
    for (unsigned int i = 0; i < m_handPlayer.size(); i++) {
        m_handPlayer[i]->move(compute_position(i), compute_angle(i), timing);
        m_handPlayer[i]->zoom_sprite(m_scaleHand, timing);
    }
}

void Hand::sort_hand(Core::Trump trump) {
    sort(m_handPlayer.begin(), m_handPlayer.end(),
         [trump](const std::shared_ptr<Card> &left_card,
                 const std::shared_ptr<Card> &right_card) {
             auto left = left_card->get_card_data();
             auto right = right_card->get_card_data();
             if (left.suit() == trump && right.suit() != trump)
                 return false;
             if (left.suit() != trump && left.suit() != right.suit())
                 return right.suit() == trump || (left.suit() > right.suit());
             return stronger_than(right, left, trump);
         });
    replace_card(40);
}

sf::Vector2f Hand::compute_position(unsigned int const i) {
    float n = (float)m_handPlayer.size();
    float spread_center = (1 + 2 * (float)i - n) / 2;
    sf::Vector2f newPos =
        m_center +
        rotateVector(
            m_scaleHand * (std::sin(spread_center * m_angleFan * PI / 180)) *
                m_distance_circle_center, // m_scaleHand*spread_center*m_cardSpacing
            m_scaleHand *
                (1 - std::cos(spread_center * m_angleFan * PI / 180)) *
                m_distance_circle_center,
            m_orientationHand);
    return newPos;
}

float Hand::compute_angle(unsigned int const i) {
    float n = (float)m_handPlayer.size();
    return (1 + 2 * (float)i - n) / 2 * m_angleFan - m_orientationHand;
}

void Hand::reveal_hand() {
    for (unsigned int i = 0; i < m_handPlayer.size(); i++) {
        m_handPlayer[i]->flip_card();
    }
}

//__________________Event_Function______________

unsigned int Hand::get_card_at_pos(sf::Vector2f const mousePos) {
    if (!m_mainPlayer) {
        return -1;
    }

    if (m_cardMain > 0 && m_handPlayer[m_cardMain]->pos_on_sprite(mousePos)) {
        return m_cardMain;
    }

    m_cardMain = -1;
    for (unsigned int i = 0; i < m_handPlayer.size(); i++) {
        if (m_cardMain == -1 && m_handPlayer[i]->pos_on_sprite(mousePos)) {
            m_cardMain = i;
            m_handPlayer[i]->zoom_sprite(m_scaleHand * ZOOM, 20);
        } else {
            m_handPlayer[i]->zoom_sprite(m_scaleHand, 20);
        }
    }
    return m_cardMain;
}

unsigned int Hand::get_card_at_pos(float const posX, float const posY) {
    return Hand::get_card_at_pos(sf::Vector2f(posX, posY));
}

unsigned int Hand::get_card_at_pos(sf::Vector2i const mousePos) {
    return Hand::get_card_at_pos(sf::Vector2f(mousePos));
}

std::shared_ptr<Card> Hand::want_play(float const mouseX, float const mouseY) {
    return want_play(sf::Vector2f(mouseX, mouseY));
}

std::shared_ptr<Card> Hand::want_play(sf::Vector2i const mousePos) {
    return want_play(sf::Vector2f(mousePos));
}

std::shared_ptr<Card> Hand::want_play(sf::Vector2f const mousePos) {
    int i = get_card_at_pos(mousePos);
    if (i < 0) {
        return nullptr;
    }
    return m_handPlayer[i];
}

std::shared_ptr<Card> Hand::play_card(sf::Vector2f const mousePos) {
    int i_card = get_card_at_pos(mousePos);
    std::shared_ptr<Card> cardReturn(nullptr);
    if (i_card >= 0) {
        cardReturn = m_handPlayer[i_card];

        cardReturn->move(m_playZone, float((rand() % 11) - 5), C_PLAY_TIME);
        cardReturn->zoom_sprite(1.2f, C_PLAY_TIME);

        m_handPlayer.erase(m_handPlayer.begin() + i_card);
        replace_card(C_REPLACE_TIME);
        m_playableCards = Core::Hand(); // delete old playable hand
    }
    return cardReturn;
}

std::shared_ptr<Card> Hand::play_card(sf::Vector2i const mousePos) {
    return play_card(sf::Vector2f(mousePos));
}

std::shared_ptr<Card> Hand::play_card(float const mouseX, float const mouseY) {
    return play_card(sf::Vector2f(mouseX, mouseY));
}

std::shared_ptr<Card> Hand::play_card(Core::Card typeCard) {
    unsigned int i_card = unsigned(rand() % m_handPlayer.size());
    if (m_mainPlayer) {
        for (unsigned int i = 0; i < m_handPlayer.size(); i++) {
            if (m_handPlayer[i]->get_card_data() == typeCard) {
                i_card = i;
            }
        }
        m_playableCards = Core::Hand();
    }
    std::shared_ptr<Card> cardReturn = m_handPlayer[i_card];
    if (!m_mainPlayer)
        cardReturn->flip_card(typeCard, C_PLAY_TIME);
    cardReturn->move(m_playZone, float((rand() % 11) - 5), C_PLAY_TIME);
    cardReturn->zoom_sprite(1.2f, C_PLAY_TIME);

    m_handPlayer.erase(m_handPlayer.begin() + i_card);
    replace_card(C_REPLACE_TIME);
    return cardReturn;
}

//__________________Give_Card______________________

void Hand::set_playable_cards(Core::Hand cards) { m_playableCards = cards; }

void Hand::give_card() {
    std::shared_ptr<Card> newCard = std::make_shared<Card>(Card());
    m_handPlayer.push_back(newCard);
    replace_card(C_DISTRIB_TIME);
}

void Hand::give_card(Core::Card cardGive) {
    std::shared_ptr<Card> newCard(std::make_shared<Card>(Card(cardGive)));
    newCard->flip_card(C_DISTRIB_TIME);
    m_handPlayer.push_back(newCard);
    replace_card(C_DISTRIB_TIME);
}

int Hand::size() { return (int)m_handPlayer.size(); }
