/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <vector>
#include "Core.hpp"
#include "Card.hpp"
#include "Graphics.hpp"

/** \file
    \author Leo.V, Mathieu.V
    \brief Definition of class Token, TokenArray
**/

/** \class **/
class Token : public AnimatedSprite {

  public:
    Token();

    void set_black();
    void set_white();
    void set_gray();
    void set_number(int i);

    // ________ Token constant ______________________
    static float const SIZE;
    static float const SCALE;

  private:
    bool m_isCompleted;
};


/** \class
 *	\brief DOCUMENTATION TODO
 **/
class TokenArray {
  public:
    TokenArray() = default;
    /** \brief Non default constructor
            \param position position of the center of the hand
    **/
    TokenArray(sf::Vector2f const position, float const orientation = 0);

    void set_nb_token(int nb); // reset the nb_plis to 0;

    void draw(sf::RenderWindow &window);

    void replace_token(float const time = 15);

    sf::Vector2f get_position_token_free();

    void new_trick();
    void draw_new_trick();
    sf::Vector2f get_pos_free_token();

    float get_scale();
    bool is_animated();

  private:
    sf::Vector2f compute_position(int i, int n); // call by replace to make fan

    std::vector<Token> m_tokenPlayer;
    std::vector<Card> m_cardToken;

    int m_nbToken;
    int m_nbTrick;
    int m_nbTrickDraw;
    Card m_card;

    sf::Vector2f m_centerArray; // position of the center of the hand
    float m_scaleArray;         // the scale of the card
    float m_orientationArray;   // orientation of the hand in degree
};




/** \file Arrays.hpp
 *    \author Mathieu.V
 *    \brief Definition of class BetArray
 **/

/** \brief The object that permits the player to choose his bet **/
class BetArray {
  public:
    BetArray() = default;

    BetArray(float const x, float const y, int nb_token);
    void draw(sf::RenderWindow &window);

    /** \brief Sets the number of visible tokens. Setter for the m_nb_tokens
     * attribute **/
    void set_nb_token(int newNbToken);
    /** \brief Sets values used to know what are the playable bets **/
    void set_playable_bet(Core::Bet current_bet, bool mechoune,
                          int nb_forbidden);
    /** \brief Sets bets want by tuto **/
    void set_playable_bet(std::vector<Core::Bet> bets_available);

    /** \brief Gets the token whose hitbox is intersected by the given position
     *   \return The id of the token. Returns -1 if no token is intersected
     **/
    int get_token_at_pos(float const, float const);
    int get_token_at_pos(sf::Vector2f const);
    int get_token_at_pos(sf::Vector2i const);

    /** \brief Gets the trump whose hitbox is intersected by the given position
     *   \return The id of the token. Returns -1 if no token is intersected
     **/
    int get_trump_at_pos(float const, float const);
    int get_trump_at_pos(sf::Vector2f const);
    int get_trump_at_pos(sf::Vector2i const);

    /** \brief what to do when someone clicked **/
    std::shared_ptr<Core::Bet> click(sf::Vector2f const mousePos);
    std::shared_ptr<Core::Bet> click(sf::Vector2i const mousePos);
    std::shared_ptr<Core::Bet> click(float const mouseX, float const mouseY);

  private:
    //_________BetArray Constant _________
    /** Real sizes of variables, the size of trumps' aura is such that it is the
       same for all trumps (and without and all trumps) */
    static float const REAL_WIDTH_AURA_TOKEN;
    static float const REAL_HEIGHT_TRUMP;
    static float const REAL_HEIGHT_AURA_TRUMP;
    static float const REAL_WIDTH_ALL_WITHOUT_TRUMP;
    static float const REAL_WIDTH_AURA_TRUMP;
    static float const WIDTH_AURA;

    // distances between elements
    static float const DIST_BETWEEN_TOKEN;
    static float const DIST_BETWEEN_TRUMP;
    static float const DIST_BETWEEN_TOKEN_TRUMP;

    static float const TOKEN_WIDTH;
    static float const TOKEN_REAL_WIDTH;

    // a general convention is that a non initialized integer is equal to -1.
    std::vector<Token> m_allTokens; // bet tokens, draw only m_nb_tokens of
                                    // them, but there are the 10 tokens
    AnimatedSprite m_aura_token;    // The aura sprite
    AnimatedSprite m_aura_token_selected;    // The aura sprite
    std::vector<AnimatedSprite> m_allTrumps; // the trumps vector

    // the auras linked to the trumps
    std::vector<AnimatedSprite>m_allTrumps_aura;
    std::vector<AnimatedSprite> m_allTrumps_aura_selected;

    Core::Bet m_current_bet;

    sf::Vector2f m_center; // position of the center of the array of trumps, and
                           // the tokens are above

    int m_nbMax;
    int m_mechoune;
    int m_nb_forbidden;
    int m_nb_token; // The number of tokens including the black token that are
                    // drawn
    int m_trump_clicked; // the current trump clicked
    int m_token_clicked; // the current token clicked

    std::vector<Core::Bet> m_betsAvailable;
};
