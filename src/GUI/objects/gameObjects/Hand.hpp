/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_CLASS_HAND
#define DEF_CLASS_HAND

#include "Card.hpp"

/** \file Hand.hpp
 *	\author Léo.V
 *	\date 10 October
 *	\brief Definition of class Hand
 **/

/** \class **/
class Hand {
  public:
    Hand() = default;

    Hand(float const x, float const y, CardinalPoints const cardi,
         float const scale = 1, bool const mainPlayer = false,
         float const angleFan = 12);
    Hand(sf::Vector2f const position, CardinalPoints const cardi,
         float const scale = 1, bool const mainPlayer = false,
         float const angleFan = 12);

    /** \brief adds a card in the hand. No argument -> the card is facing down
     * **/
    void give_card();
    /** \brief adds a card in the hand. Argument -> the card is facing up and
     * has a value **/
    void give_card(Core::Card cardGive);

    void set_play_zone(sf::Vector2f const position);
    void set_play_zone(float const, float const);

    void draw(sf::RenderWindow &window);

    /** \brief **/
    unsigned int get_card_at_pos(float const, float const);
    unsigned int get_card_at_pos(sf::Vector2f const mousePos);
    unsigned int get_card_at_pos(sf::Vector2i const mousePos);

    /** \brief apply a specific sorting of the card and place them in correct
     * order **/
    void replace_card(float const speed = 1);

    /** \brief sort hand according to the trump of the round : trump cards are
     * now stronger than others. **/
    void sort_hand(Core::Trump);

    /** Flip all cards of the hand and reveal them**/
    void reveal_hand();

    // void new_hand(core) // get the hand in the begin of turn (need core)
    std::shared_ptr<Card> play_card(float const mouseX, float const mouseY);
    std::shared_ptr<Card> play_card(
        sf::Vector2f const mousePos); // return NULL if they don't play card
    std::shared_ptr<Card> play_card(sf::Vector2i const mousePos);

    /** \brief play a random card of the hand and give it a value (for the hand
     * of the opponent) **/
    std::shared_ptr<Card> play_card(Core::Card typeCard);

    std::shared_ptr<Card> want_play(float const mouseX, float const mouseY);
    std::shared_ptr<Card> want_play(sf::Vector2f const mousePos);
    std::shared_ptr<Card> want_play(sf::Vector2i const mousePos);

    void set_playable_cards(Core::Hand);

    int size();

    // ______ Hand Constant ________
    static float const ANGLE_FAN;
    static float const CARD_SPACING;
    static float const DISTANCE_CIRCLE_CENTER;
    static float const ZOOM;

  private:
    /** \brief computes the position of the i-th card of the hand **/
    sf::Vector2f compute_position(unsigned int const i);

    /** \brief computes the angle of the i-th card of the hand **/
    float compute_angle(unsigned int const i);

    std::vector<std::shared_ptr<Card>> m_handPlayer;
    sf::Vector2f m_center; // Position of the center of the hand
    float m_scaleHand;

    float m_orientationHand; // orientation of the hand in degree
    Core::Hand m_playableCards;

    bool m_mainPlayer;

    int m_cardMain;
    float m_cardSpacing;

    float m_angleFan;
    float m_distance_circle_center;

    sf::Vector2f m_playZone; // the position where the card when is played
};

#endif
