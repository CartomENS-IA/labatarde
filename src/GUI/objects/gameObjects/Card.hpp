/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 *	\author Léo.V
 *	\date 10 October
 *	\brief Definition of class Card
 **/

#include "Core.hpp"
#include "Graphics.hpp"
#include <cmath>

/**
 * Class Card inherits from AnimatedSprite. It can be moved with animation on
 *the screen. Cards usually contain two textures : one for each side \brief A
 *class representing a "Bâtarde" Card in the game
 **/

class Card : public AnimatedSprite {

  public:
    /** \brief Default constructor : creates a card without a recto **/
    Card();

    /** \brief Constructor : Will look in the RessourceManager to retrieve the
     * correct recto texture **/
    Card(Core::Value value, Core::Suit color);
    /** \brief Constructor : Variant that uses directly the Core::Card class.
     * See Core/Card.hpp for more information **/
    Card(Core::Card typeCard);

    /** \brief animation method. Flips the card and reveal its recto **/
    void flip_card(float time = 25);

    /** \brief variant of flip_card : reveal a card and give it a value
     *
     *    This is useful for the opponent's cards.
     **/
    void flip_card(Core::Card typeCard, float time = 25);

    /** Re-implementation of AnimatedTransformable::continue_move **/
    void continue_move();

    /** \brief update method.shortcut for calling both continue_move and draw
     * methods **/
    void update(sf::RenderWindow &window);

    /** \brief Getter on m_isVisible private attribute **/
    bool is_visible();

    /** \return true iff the card is currently in the middle of an animation **/
    bool is_animated();

    /** \brief Getter on the m_cardData private attribute **/
    Core::Value get_value();
    /** \brief Getter on the m_cardData private attribute **/
    Core::Suit get_color();
    /** \brief Getter on the m_cardData private attribute **/
    Core::Card get_card_data();

    // ________ Card size constants __________
    static float const WIDTH;
    static float const LENGTH;
    static float const SCALE;

  private:
    Core::Card m_cardData;
    bool m_isVisible;
    float m_advancementReturn;
    float m_speedReturn;
};
