/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <string>

/**
    \file enumType.hpp
    \author GUIllaume.C
    \brief Definition of enum classes Language, Align
**/

// _____________________ Enum types ____________________________________________
enum class Language : int {
    /**
        \enum Language
        \brief Language enum used by the option manager
    **/
    ENGLISH,
    FRENCH,
    GERMAN,
    SPANISH,
    ITALIAN,
    PORTUGUESE,
    PIRATE
};

std::string language_to_string(Language const);
Language string_to_language(std::string const &);

int language_to_id(Language const);
Language id_to_language(int const);

// _____________________________________________________________________________
enum class Align {
    /** For message and button alignment **/
    CENTER_TOP,
    LEFT_TOP,
    RIGHT_TOP,
    CENTER,
    LEFT,
    RIGHT
};

// _____________________________________________________________________________
enum class Difficulty {
    /** AI Difficulty **/
    BEGINNER,
    GOOD,
    VERY_GOOD,
    MASTER
};
std::string difficulty_to_string(Difficulty const);
Difficulty string_to_difficulty(std::string const &);

// _____________________________________________________________________________
enum CardinalPoints {
    SOUTH,
    EAST,
    NORTH,
    WEST
};

// _____________________________________________________________________________
enum class CardType {
    CLASSICAL,
    SPECIAL
};
std::string cardType_to_string(CardType const);
CardType string_to_cardType(std::string const &);
