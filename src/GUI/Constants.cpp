/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Constants.hpp"

// _________ String Trump ___________

float const ANIMATION_TIME = 1.f;

std::vector<std::string> const NAME_TRUMP = {
    "without_trump", "club", "diamond", "heart", "spade", "all_trump"};
std::vector<std::wstring> const NAME_VALUE = {L"1", L"2", L"J", L"M", L"F",
                                              L"V", L"C", L"D", L"R"};

std::vector<std::string> FNAME_VALUE = {"chat",     "chien", "jongleur",
                                        "musicien", "fou",   "valet",
                                        "cavalier", "dame",  "roi"};
std::vector<std::string> FNAME_TRUMP = {"sans-atout", "trefle", "carreau",
                                        "coeur",      "pique",  "tout-atout"};

// ________ Splash Screen ___________

float const SPLASH_TIME = 2*ANIMATION_TIME;
float const FADE_SPEED = 6 + (1 - ANIMATION_TIME) * 100;

// ________ Window Resolutions ________
std::vector<std::wstring> const RESOLUTION =
    {L"1280x720", L"1600x900", L"1920x1080"};

std::vector<float> const RESOLUTION_WIDTH = {1280.f, 1600.f, 1920.f};
std::vector<float> const RESOLUTION_HEIGHT = {720.f, 900.f, 1080.f};

int const NB_RESOLUTION = (int)RESOLUTION.size();

// ________ Window Size ________
float const WINDOW_WIDTH = optionManager()->get_screen_width();
float const WINDOW_HEIGHT = optionManager()->get_screen_height();

float const SCALE= optionManager()->get_screen_scale();
float const SCALE_TO_MAX = optionManager()->get_screen_scale()/1.5f;
float const HALF_SCALE = (1.f+optionManager()->get_screen_scale())/2.f;
extern float const HALF_SCALE_TO_MAX = (1.f+optionManager()->get_screen_scale()/1.5f)/2.f;

// ______ Colors ______

sf::Color const WHITE(255, 255, 255);
sf::Color const YELLOW(255, 255, 0);
sf::Color const BLUE(0, 0, 255);
sf::Color const BLACK(0, 0, 0);
sf::Color const BLACK_LIT(6,53,138);
sf::Color const GRAY(120, 120, 120);
sf::Color const CARD_GRAY(190, 190, 190);
sf::Color const GRAY_LIT(170, 170, 170);
sf::Color const LIGHT_GRAY(232,233,244);
sf::Color const GREEN(0, 180, 0);
sf::Color const GREEN_LIT(50, 230, 50);
sf::Color const RED(180, 0, 0);
sf::Color const RED_LIT(230, 50, 50);
sf::Color const TRANSPARENT(255, 255, 255, 64);
sf::Color const BACKGROUND(0, 74, 153);

// ______ Font size constants ______
float const TITLE_TEXT_SIZE = 80;
unsigned int const TITLE_FONT_SIZE = unsigned(TITLE_TEXT_SIZE);

float const MENU_BUTTON_TEXT_SIZE = 32 ;
unsigned int const MENU_BUTTON_FONT_SIZE = unsigned(MENU_BUTTON_TEXT_SIZE);

float const STAT_TEXT_SIZE = 20;
unsigned int const STAT_FONT_SIZE = unsigned(STAT_TEXT_SIZE);

// ______ Math constants ______
float const PI = 3.14159265f;

// _______ Main menu Constants _______

float const BUTTON_SIZE = 100.f*optionManager()->get_screen_scale();
float const BETWEEN_BUTTONS = 220.f*optionManager()->get_screen_scale();
float const MENU_OFFSET = 120.f;
float const MENU_OFFSET_SMALL = 50.f;

//_________ Center Play Zone ________
float const CENTER_PLAY_X = WINDOW_WIDTH / 2;
float const CENTER_PLAY_Y = WINDOW_HEIGHT / 2 - 10;

// ______ Player Mark Size _______
float const PLAYER_MARK_SIZE = 200;

// ______ Card Size _______
float const TIME_WAIT_TRICK = 500;
float const TIME_TROPHY_APPEAR = 4000;

// _______ BETBUBBLE Constant _______

float const HEIGHT_BETBUBBLE = 180;
float const WIDTH_BETBUBBLE = HEIGHT_BETBUBBLE * 40.f / 25;
float const SIZE_ARROW_BETBUBBLE = 52 * HEIGHT_BETBUBBLE / 223;

float const SIZE_TEXT_BETBUBBLE = 55;
unsigned const SIZE_FONT_BETBUBBLE = unsigned(SIZE_TEXT_BETBUBBLE);
float const DIST_HAND_BETBUBBLE = 120;
float const TIME_BETBUBBLE_ANIMATION = 1 + 29 * ANIMATION_TIME;

// _____ Card Speed _______

float const C_DISTRIB_TIME = 1 + 9 * ANIMATION_TIME;
float const C_REPLACE_TIME = 1 + 9 * ANIMATION_TIME;
float const C_PLAY_TIME = 1 + 29 * ANIMATION_TIME;
float const C_TRICK_TIME = 1 + 19 * ANIMATION_TIME;

// ______ Game Constant ________
float const DISTRIBUTION_SPEED = 1.5f + (1 - ANIMATION_TIME) * 150;

// ________ Player Icon Constant _________
float const ICON_SIZE = 80;

// printed size of trumps
float const WIDTH_TRUMP = 70 * SCALE; // = HEIGHT_TRUMP for real trumps
