/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Arrays.hpp"
#include "GraphicPlayer.hpp"
#include "Graphics.hpp"
#include "InfoTopRight.hpp"
#include "LastTrick.hpp"
#include "ScoreDisplay.hpp"
#include "StatManager.hpp"

#include "Core.hpp"

/** \author Léo.V
    \file Board.hpp
    \brief Definition of class Board

    Documentation by GUIllaume.C
**/

/** \brief small Event for TutoGame **/
class MessageEvent : public Core::Event {
  public:
    MessageEvent(std::string &_text);
    std::string text;
};

/** \brief The main event and object handler of the GUI
 *
 *   Board handles every communication made with the kernel.
 *   It also contains the hands of the players, in order for them to be
 *displayed and updated
 **/
class Board {
  public:
    Board() = default;
    /**  \brief Constructor
     * \param kernelState a Core::GameState corresponding to everything the
     *player has to know about the current game \param kernelAction the double
     *of the player in the kernel. Used to update the kernel's state after The
     *constructor creates 4 GraphicPlayer instances. See the GraphicPlayer
     *class' documentation for more information
     **/
    Board(const Core::GameState *kernelState,
          std::shared_ptr<Core::ParallelPlayer> guiPlayer,
          FourPlayerInfo const pInfo);

    /** /brief Update method. Calls update on the attributes **/
    void update(sf::RenderWindow &window, sf::Vector2f const mousePos);
    void update(sf::RenderWindow &window, sf::Vector2i const mousePos);
    void update(sf::RenderWindow &window, float const mouseX = -1.,
                float const mouseY = -1.);

    /** /brief is called when a mouse click is recorded by the game loop
     *   /return true iff the event has triggered something on the screen
     **/
    bool handle_mouse_click(sf::Vector2f const mousePos);
    bool handle_mouse_click(sf::Vector2i const mousePos);
    bool handle_mouse_click(float const mouseX, float const mouseY);

    /** /brief is called when a key of the keyboard is pushed **/
    bool handle_key_pressed(sf::Event::KeyEvent const key);

    /** \brief asks the kernel if something has changed in the game state **/
    void wait_action_kernel();

    /** \brief returns true iff an object is currently in the middle of an
     * animation on the screen **/
    bool is_animated();

    /** \brief return true iff the kernel is currently waiting an action from
     * the player **/
    bool kernel_waiting_action();

    /** \brief inform board that it's a tutoGame **/
    void enable_tuto_mode();

    /** \brief set the playable cards (only for tuto) **/
    void set_playable_cards(Core::Hand);

    /** \brief set the playable bets (only for tuto), -1 if you want not specify
     * **/
    void set_playable_bets(int color, int value);

    /** \brief set bets of all players (only for tuto) **/
    void set_allbet(Core::AllBets &bets);

  private:
    enum class State {
        WANT_TO_QUIT,
        WAIT_READY,
        CALL_READY,
        PLAY_PHASE,
        BET_PHASE,
        END_ROUND_PHASE,
        END_GAME,
        DISPLAY_SCORE,
        IDLE
    };

    void set_state(State s, bool verbose = true);

    static std::string state_to_string(State const);

    State m_state;
    State m_previous_state;

    void log_state_changed();

    /** \brief returns true if the player is allowed to leave the game
        depending of the current state of the Board **/
    bool can_quit();

    /** \brief returns true if the player is allowed to see the score menu
        depending of the current state of the Board **/
    bool can_show_score();

    /** \brief Dispenses cards to every player **/
    void distribution();

    /** \brief Make the player playerID play one card of his hand **/
    void play_card(Core::Card cardPlayed, int playerID);

    /** \brief attributes a trick to a player **/
    void give_trick_to_player(int playerID);

    /** \brief calls the transition animation between bet moment and gaming
     * moment **/
    void end_bet_moment();

    /** \brief calls the transition animation between gaming moment and the next
     * bet moment **/
    void end_play_moment();

    /** \brief calls the transition animation at the end of the game to display
     * scores **/
    void end_game(int winnerID);

    /** \brief set the first_player to drawing it **/
    void set_first_player(int);

    void run_ingame_option_loop();

    // The four GraphicPlayer objects instanciated by Board
    std::array<GraphicPlayer, 4> m_player;

    // The cards of the current trick. Displayed in the middle of the screen
    std::array<std::shared_ptr<Card>, 4> m_middleCard;
    BetArray m_betArray;

    HybridButton m_buttonMechoune;
    HybridButton m_buttonChoune;
    TimerButton m_buttonReady;
    bool m_ready;

    EndRoundDisplay m_endRoundDisplay;
    EndGameDisplay m_endGameDisplay;

    HybridButton m_yesButton;
    HybridButton m_noButton;
    Message m_returnToMenu;

    int m_nbMiddleCard;

    sf::Clock m_timerDistribution;

    sf::Clock m_timerTrick; // time to wait before giving trick
    int m_trickWait;        // contains the id of the player who waits

    Core::Hand m_cardToDistribute; // The card that is being distributed

    int m_handDistribution;

    int m_playerTrick;

    bool m_yourTurn;
    bool m_bufferMechoune;
    bool m_mechouneAvailable;
    bool m_chouneAvailable;
    bool m_tuto;
    bool m_drawButtonBottom;

    std::vector<Core::Bet> m_betsAvailable;

    Card m_deckDistribution;

    Core::Hand m_playableCards;
    const Core::GameState *m_gamestate;
    std::shared_ptr<Core::ParallelPlayer> m_kernelPlayer;

    LastTrick m_lastTrick;
    InfoTopRight m_infoTopRight;

    std::vector<std::unique_ptr<HybridButton>> m_buttonsDownRight;

    float m_tempsDispo;
};
