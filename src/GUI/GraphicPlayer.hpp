/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file GraphicPlayer.hpp
 *   \author Léo.V
 *   \brief Definition of class GraphicPlayer
 *
 **/

#include <SFML/Graphics.hpp>

#include "BetBubble.hpp"
#include "Hand.hpp"
#include "PlayerDisplay.hpp"
#include "Arrays.hpp"

#include "Core.hpp"

/** \class
 *   \brief Contains every element that defines a player in the GUI
 *
 * Contains a hand of card (class Hand), a tokenArray instance, an icon and a
 *bubble for bets
 **/
class GraphicPlayer {
  public:
    GraphicPlayer() = default;

    /** \TODO **/
    GraphicPlayer(CardinalPoints cardi, PlayerInfo const &pinfo);

    void draw(sf::RenderWindow &window);

    /** \brief Minimal draw. Does not render the bubble and the bet array **/
    void draw_minimal(sf::RenderWindow &window);

    void your_turn();
    void end_turn();

    /** \brief trigger event of the left click
            returns a card if trigger a play_card
            returns null if not **/
    std::shared_ptr<Card> handle_mouse_click(float const mouseX,
                                             float const mouseY);

    std::shared_ptr<Card> want_play(float const mouseX, float const mouseY);
    std::shared_ptr<Card> want_play(sf::Vector2f const mousePos);
    std::shared_ptr<Card> want_play(sf::Vector2i const mousePos);

    // play card of an opponent
    std::shared_ptr<Card> play_card(Core::Card cardPlayed);

    void set_play_zone(sf::Vector2f const position);

    /** \brief Adds a card in the hand **/
    void give_card();
    void give_card(Core::Card cardGive);

    void set_playable_cards(Core::Hand);
    void sort_hand(Core::Trump);

    void give_trick();
    void draw_new_trick();
    sf::Vector2f get_pos_free_token();

    void begin_main_phase();
    void end_play_moment();

    void hide_player_display();
    void show_player_display();
    void lock_player_display();

    void set_bet(Core::Bet bet, bool draw = true);
    void set_message(std::wstring text);
    void set_message(std::string text);
    void restart_bet();
    void set_first_player(bool);
    void set_points(int);
    Core::Bet get_bet();

    int size_hand();

    float get_scale_token();
    bool is_animated();

  private:
    Hand m_handPlayer;
    TokenArray m_tokenPlayer;
    BetBubble m_betBubble;

    PlayerDisplayBoard m_playerDisplay;

    bool m_mainPhase;
    bool m_mainPlayer;
    bool m_isActive;
    bool m_showPlayerDisplay;
    bool m_lockPlayerDisplay;
};
