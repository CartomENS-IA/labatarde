/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 *  \brief Definition of enum PlayerInfo
 **/

#include <array>
#include <SFML/Graphics.hpp>

 struct PlayerInfo {
     std::wstring name;
     std::wstring registered_date;
     std::wstring first_ranking_date;
     unsigned int elo;
     unsigned int elo_max;
     std::wstring elo_max_date;
     std::wstring elo_title;
     std::wstring guild_name;
     std::wstring nationality;
     unsigned int nb_played;
     unsigned int nb_left;
     unsigned int nb_mechoune;
     unsigned int nb_choune;
     unsigned int best_score;
     std::array<int,4> ranks;

     std::string profile_pic;
     std::string guild_pic;
 };

typedef std::array<PlayerInfo,4> FourPlayerInfo;

extern sf::Color const ELO_GREEN;
extern sf::Color const ELO_GREEN_LIGHT;
extern sf::Color const ELO_RED;
extern sf::Color const ELO_RED_LIGHT;
extern sf::Color const ELO_BLUE;
extern sf::Color const ELO_BLUE_LIGHT;
extern sf::Color const ELO_PURPLE;
extern sf::Color const ELO_PURPLE_LIGHT;

sf::Color get_elo_color_main(unsigned elo);
sf::Color get_elo_color_bg(unsigned elo);
