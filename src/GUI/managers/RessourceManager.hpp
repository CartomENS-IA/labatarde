/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_RESSOURCE_MANAGER
#define DEF_RESSOURCE_MANAGER

#include <SFML/Audio.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include <unordered_map>

/** \file RessourceManager.hpp
 *   \author GUIllaume.C
 *   \brief Definition of the RessourceManager class
 */

/** The RessourceManager is meant to store all external ressources of the game
(pictures, fonts, sounds, ...)

    Those datas are stored in unordered maps, from where they can be accessed by
their file name. When calling a method "get" from this class, the
RessourceManager will first look up in the correct map if the asked object is
present. If it is, it returns it. Otherwise, it loads it in the map before
returning it. This prevents similar objects from being loaded several times,
which is a big gain of memory

NB : this class is instanciated in a global variable RM. It should not be
instanciated elsewhere

HOW TO USE : if you want to load any texture , write for instance :
    sf::Texture myTexture = RM.get_texture(texture_name);

The texture file should be in the 'res' folder : adresses of files has to be
given in relation to the res/ folder \brief Data container for every object
imported in the game (image, sound, font, ...)
**/
class RessourceManager {
  public:
    /** \brief RessourceManager's constructor **/
    RessourceManager();

    /** \brief Look up the map in search of the objects of same name. If the
     *object isnt in the map, it loads it in \param name the relative path of
     *the object to be found. Path is relative to the ressources folder
     **/
    sf::Texture &get_texture(std::string const &name);

    /** \brief Look up the map in search of the objects of same name. If the
     *object isnt in the map, it loads it in \param name the relative path of
     *the object to be found. Path is relative to the ressources folder
     **/
    sf::Font &get_font(std::string const &name);

    /** \brief Look up the map in search of the objects of same name. If the
     *object isnt in the map, it loads it in \param name the relative path of
     *the object to be found. Path is relative to the ressources folder
     **/
    sf::SoundBuffer &get_sound(std::string const &name);

  private:
    /* The containers */
    std::unordered_map<std::string, sf::Texture> textures;
    std::unordered_map<std::string, sf::Font> fonts;
    std::unordered_map<std::string, sf::SoundBuffer> sounds;
};

extern RessourceManager RM; // The ressource manager is global for now

#endif
