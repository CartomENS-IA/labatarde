/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/** \file
 *   \author Guillaume.C
 *   \brief defition of StatManager class
 **/

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <fstream>
#include <iostream>
#include <memory>

#include "Core.hpp"
#include "OptionManager.hpp"
#include "PlayerInfo.hpp"
#include "ClientMessenger.hpp"
#include "enumType.hpp"
#include <iostream>
#include <queue>

/** \file
 *   \author Léo.V, Guillaume.C
 *   \brief defition of STAT enum, and of StatManager class
 **/

/** \class
 *   \brief Handles everything related to statistics of games
 **/
class StatManager {
  public:
    StatManager();

    /**
     *  \brief Reads and loads a profile from the server
     **/
    void load_profile();
    void save_profile();

    void begin_game();
    void begin_round(const Core::GameState &gs);

    void end_trick(const Core::GameState &gs);
    void end_round(const Core::GameState &gs);
    void end_game(const Core::GameState &gs);

    PlayerInfo get_player_info() const;

  private:
    PlayerInfo m_info;
    bool m_hasLoaded;
    int m_bufMechoune;
    int m_bufChoune;
    std::array<int, 4> m_bufScores;
};


// Global StatManager. Should be the only instance.
extern StatManager statManager;
