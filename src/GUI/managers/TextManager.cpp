/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "TextManager.hpp"

#include "pugixml.hpp"
#include <cwctype>
#include <iostream>
#include <locale>

using namespace std;

TextManager TM; // Global TextManager

void TextManager::load(std::string const &fileName) {
    using namespace pugi;

#if __APPLE__
  #define LOCALE_FR "fr_FR.UTF-8"
  #define LOCALE_US "en_US.UTF-8"
#else
  #define LOCALE_FR "fr_FR.utf8"
  #define LOCALE_US "en_US.utf8"
#endif

    try {
        std::locale::global(std::locale(LOCALE_FR));
    } catch (const std::exception &e) {
        cerr << "[TextManager] Warning : locale " << LOCALE_FR << " not found"
             << endl;
        std::locale::global(std::locale(LOCALE_US));
    }

    wcout.imbue(locale());

    clog << "[TextManager] Loading strings from file " << fileName << endl;

    xml_document textfile;
    xml_parse_result result =
        textfile.load_file(("res/text/" + fileName).c_str());
    if (!result)
        throw runtime_error(result.description());
    xml_node root = textfile.child("text");

    for (xml_node entry = root.first_child(); entry;
         entry = entry.next_sibling()) {
        sf::String key = entry.name();
        sf::String value;
        switch (optionManager()->get_language()) {
        case Language::ENGLISH:
            value = as_wide(entry.child("EN").first_child().value());
            break;
        case Language::FRENCH:
            value = as_wide(entry.child("FR").first_child().value());
            break;
        case Language::GERMAN:
            value = as_wide(entry.child("GER").first_child().value());
            break;
        case Language::SPANISH:
            value = as_wide(entry.child("SPA").first_child().value());
            break;
        case Language::ITALIAN:
            value = as_wide(entry.child("ITA").first_child().value());
            break;
        case Language::PIRATE:
            value = as_wide(entry.child("PIR").first_child().value());
            break;
        case Language::PORTUGUESE:
            value = as_wide(entry.child("POR").first_child().value());
            break;
        default:
            value = L"";
            break;
        }
        if (value != L"") {
            m_dataContainer[key.toAnsiString()] = value.toWideString();
        }
    }

    std::locale::global(std::locale("C"));
}

wstring TextManager::get_entry(string const &key, bool toUpper) {
    if (m_dataContainer.find(key) == m_dataContainer.end()) {
        cerr << "[TextManager] " << key << " : Invalid key for TextManager.\n";
        return L"STRING NOT FOUND";
    }
    wstring found = m_dataContainer[key];
    if (toUpper)
        transform(found.begin(), found.end(), found.begin(), towupper);
    return found;
}
