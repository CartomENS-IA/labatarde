/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "pugixml.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Network.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <memory>
#include <string>

#include "enumType.hpp"

/** \file OptionManager.hpp
 *   \author GUIllaume.C
 *   \brief Definition of OptionManager class
 **/

/**
 * The option manager loads every options of the game. Options are written in a
 *txt file at the root of the project. This file should not be modified by
 *another part of the code (or by hand).
 *
 * Options are loaded at start of the program in optionManager : this object
 *should be the unique object of class OptionManager instanciated in this
 *program.
 *
 * the txt file options.txt is rewritten during destruction of optionManager, at
 *the end of the program's execution.
 *
 * \brief The option Manager handles everything related to user parameters and
 *preferences
 **/
class OptionManager {

  public:
    /** The OptionManager's constructor reads the user defined options from the
     *txt file options.txt and loads them into its memory. \brief
     *OptionManager's constructor
     **/
    OptionManager();

    /**
     *  The OptionManager's destructor writes back every parameters he has
     *    (that might have been changed by the user dureing the execution of the
     *program) into the options.txt file \brief OptionManager's destructor
     **/
    ~OptionManager();

    /** \brief getter for screen height.
     * If fullscreen mode is on, return the value of m_fullScreenVideoMode
     *instead of m_normalVideoMode
     **/
    float get_screen_width() const;
    /** \brief setter for screen width. Only sets the width of m_normalVideoMode
     * **/
    void set_screen_width(float const);

    /** \brief getter for screen height.
     * If fullscreen mode is on, return the value of m_fullScreenVideoMode
     *instead of m_normalVideoMode
     **/
    float get_screen_height() const;
    /** \brief setter for screen height. Only sets the height of
     * m_normalVideoMode **/
    void set_screen_height(float const);

    /** \brief Computes scale of the screen, according to default screen
     * resolution width, which is 1280x720
     **/
    float get_screen_scale() const;

    /** \brief computes (1+screen_scale)/2 **/
    float get_screen_half_scale() const;

    /** \brief Getter on m_isFullScreen private attribute **/
    bool is_full_screen() const;
    /** \brief Setter on m_isFullScreen private attribute**/
    void set_full_screen(bool);

    /** \return If fullscreen mode is active, return m_fullScreenVideoMode ,
     * otherwise return m_normalVideoMode **/
    sf::VideoMode get_window_settings() const;

    /** \brief Returns the OpenGL contex settings, including anti-aliasing **/
    sf::ContextSettings get_context_settings() const;

    /** Language is an enum defined in enumType.hpp
     * \brief getter for m_gameLanguage private attribute
     **/
    Language get_language() const;

    /**
     \brief getter the id of the current language with string form
    **/
    std::string get_language_string() const;

    /**
     \brief getter the id of the current language with int form
    **/
    int get_language_id() const;

    /** Language is an enum defined in enumType.hpp
     * \brief setter for m_gameLanguage private attribute
     **/
    void set_language(Language l);

    bool is_pirate() const;
    void set_pirate(bool);

    /** \brief Getter for the m_soundEffects attribute **/
    bool is_sound_effect_on() const;

    /** \brief Setter for the m_soundEffects attribute.
     * /!\ Also modify the volume attribute of the SoundManager class **/
    void toggle_sound_effects(bool);

    /** \brief Getter for the m_music attribute **/
    bool is_music_on() const;

    /** \brief Setter for the m_music attribute.
    *   /!\ Also modify the volume attribute of the SoundManager class
    **/
    void toggle_music(bool);

    sf::IpAddress get_default_server_IP();
    void set_server_IP(sf::IpAddress const);

    short unsigned int get_default_server_port();
    void set_server_port(short unsigned int const);

    /** \brief getter for the m_online attribute **/
    bool is_online() const;
    /** \brief setter for the m_online attribute. /!\ Should only be called at
     * login **/
    void set_online(bool);

    /** \brief getter for the m_cardType attribute **/
    CardType get_card_type() const;
    /** \brief setter for the m_cardType attribute**/
    void set_card_type(CardType const);

    /** \brief getter for the m_playerName attribute **/
    sf::String get_player_name() const;
    /** \brief setter for the m_playerName attribute. /!\ Should only be called
     * at login **/
    void set_player_name(std::string const &);

  private:
    void load_config(std::string const fileName);
    pugi::xml_document m_options;
    bool m_defaultOption;

    sf::VideoMode m_normalVideoMode;
    sf::VideoMode m_fullScreenVideoMode;
    Language m_gameLanguage;
    bool m_soundEffects;
    bool m_music;
    bool m_isFullScreen;
    CardType m_cardType;
    bool m_online;
    bool m_nextLaunchInPirate;
    bool m_isPirate;
    sf::String m_playerName;
    sf::IpAddress m_defaultIP;
    short unsigned int m_defaultPort; // server port
};

/** \brief Global option manager. This should be the only instance of
 * OptionManager class **/
OptionManager *optionManager();
