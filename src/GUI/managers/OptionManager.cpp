/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "OptionManager.hpp"
#include "SoundManager.hpp"
#include <algorithm>

using namespace std;

// Global option manager
OptionManager *optionManager() {
    static unique_ptr<OptionManager> x{new OptionManager()};
    return x.get();
}

// _____ Constructor and destructor ______
OptionManager::OptionManager() {
    try {
        m_defaultOption = false;
        load_config("res/config/options.xml");
    } catch (runtime_error &e) {
        cerr << "[OptionManager] ERROR : Failed to load options from file "
                "res/config/options.xml\n";
        cerr << "                Reason : " << e.what() << "\n";
        cerr << "                Loading default options..." << endl;
        m_defaultOption = true;
        load_config("res/config/default_options.xml");
    }
}

void OptionManager::load_config(std::string const fileName) {
    using namespace pugi;
    xml_parse_result result = m_options.load_file(fileName.c_str());
    xml_node options = m_options.child("options");
    xml_node config;

    if (!result)
        throw runtime_error(result.description());

    clog << "[OptionManager] Loading options from file " << fileName << " :\n";
    auto get_value = [&](const char *attr) {
        return config.child(attr).first_child().value();
    };

    // __________ GRAPHICAL OPTIONS __________
    config = options.child("graphics");
    m_isFullScreen = (bool)stoi(get_value("fullscreen"));
    m_fullScreenVideoMode = sf::VideoMode::getFullscreenModes()[0];
    unsigned int screenWidth = stoi(get_value("width"));
    unsigned int screenHeight = stoi(get_value("height"));
    sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
    m_normalVideoMode =
        sf::VideoMode(screenWidth, screenHeight, desktop.bitsPerPixel);

    clog << "  Fullscreen " << (m_isFullScreen ? "On\n" : "Off\n");
    clog << "  Resolution : " << screenWidth << " " << screenHeight << "\n";

    // __________ LANGUAGE OPTIONS ___________
    config = options.child("language");
    m_gameLanguage = string_to_language(get_value("lang")); // default is FRENCH
    m_isPirate = (bool)stoi(get_value("isPir"));

    clog << "  Language : " << get_value("lang") << "\n";
    if (m_isPirate)
        clog << "  Pirate mode !\n";

    // __________ SOUND OPTIONS ___________
    config = options.child("sound");
    m_soundEffects = bool(stoi(get_value("noise")));
    m_music= bool(stoi(get_value("music")));

    clog << "  Sound Effects : " << m_soundEffects << "\n";
    clog << "  Music Volume : " << m_music << "\n";

    // __________ NETWORK CONNECTION OPTIONS ___________
    config = options.child("server");
    m_defaultIP = sf::IpAddress(get_value("ip"));
    m_defaultPort = (short unsigned int)stoi(get_value("port"));
    clog << "  Default server IP : " << m_defaultIP << endl;
    clog << "  Default server port : " << m_defaultPort << endl;

    // __________ GAME OPTIONS ___________
    config = options.child("card");
    std::string cardTypeStr = get_value("type");
    if (cardTypeStr=="special"){
        m_cardType = CardType::SPECIAL;
    } else {
        m_cardType = CardType::CLASSICAL;
        cardTypeStr = "classical";
    }
    clog << "  Card Type : " << cardTypeStr << "\n";
}

OptionManager::~OptionManager() {
    using namespace pugi;

    xml_node options = m_options.child("options");
    xml_node config;

    auto set_value_str = [&](const char *attr, std::string what) {
        config.child(attr).first_child().set_value(what.c_str());
    };
    auto set_value_bool = [&](const char *attr, bool what) {
        config.child(attr).first_child().set_value(to_string(what).c_str());
    };
    auto set_value_int = [&](const char *attr, int what) {
        config.child(attr).first_child().set_value(to_string(what).c_str());
    };
    // auto set_value_float = [&](const char * attr, float
    // what){config.child(attr).first_child().set_value(to_string(what).c_str());};

    clog << endl;
    clog << "[OptionManager] Options at the end of the execution :\n";

    // __________ GRAPHICAL OPTIONS __________
    config = options.child("graphics");
    set_value_bool("fullscreen", m_isFullScreen);
    set_value_int("width", m_normalVideoMode.width);
    set_value_int("height", m_normalVideoMode.height);

    clog << "  Fullscreen " << (m_isFullScreen ? "On\n" : "Off\n");
    clog << "  Resolution : " << m_normalVideoMode.width << " "
         << m_normalVideoMode.height << "\n";

    // __________ LANGUAGE OPTIONS ___________
    config = options.child("language");
    set_value_str("lang", language_to_string(m_gameLanguage));
    set_value_bool("isPir", m_nextLaunchInPirate);

    clog << "  Language : " << language_to_string(m_gameLanguage) << "\n";
    if (m_nextLaunchInPirate)
        clog << "  Next round will be pirate!" << endl;

    // __________ SOUND OPTIONS ___________
    config = options.child("sound");
    set_value_bool("noise", m_soundEffects);
    set_value_bool("music", m_music);
    clog << "  Sound effects: " << m_soundEffects << "\n";
    clog << "  Music : " << m_music << "\n";

    // __________ NETWORK CONNECTION OPTIONS ___________
    config = options.child("server");
    // set_value_str("ip", m_defaultIP.toString());
    // set_value_int("port",m_defaultPort);

    clog << "  Default server IP : " << m_defaultIP << endl;
    clog << "  Default server port : " << m_defaultPort << endl;

    // __________ GAME OPTIONS ___________
    config = options.child("card");
    set_value_str("type", cardType_to_string(m_cardType));

    clog << "  Card type : " << cardType_to_string(m_cardType) << "\n";
    m_options.save_file("res/config/options.xml");
}

float OptionManager::get_screen_width() const {
    if (m_isFullScreen)
        return (float)m_fullScreenVideoMode.width;
    return (float)m_normalVideoMode.width;
}

void OptionManager::set_screen_width(float width) {
    m_normalVideoMode.width = (unsigned int)width;
}

float OptionManager::get_screen_height() const {
    if (m_isFullScreen)
        return (float)m_fullScreenVideoMode.height;
    return (float)m_normalVideoMode.height;
}

void OptionManager::set_screen_height(float height) {
    m_normalVideoMode.height = (unsigned int)height;
}


float OptionManager::get_screen_scale() const {
    return get_screen_width() / 1280.f;
}

float OptionManager::get_screen_half_scale() const {
    return (1.f + get_screen_width() / 1280.f)/2.f;
}

void OptionManager::set_full_screen(bool x) { m_isFullScreen = x; }
bool OptionManager::is_full_screen() const { return m_isFullScreen; }


Language OptionManager::get_language() const {
    if (m_isPirate)
        return Language::PIRATE;
    return m_gameLanguage;
}
void OptionManager::set_language(Language l) {
    m_gameLanguage = l;
}


bool OptionManager::is_pirate() const { return m_isPirate; }
void OptionManager::set_pirate(bool p) { m_nextLaunchInPirate = p; }


std::string OptionManager::get_language_string() const {
    if (m_isPirate)
        return language_to_string(Language::PIRATE);
    return language_to_string(m_gameLanguage);
}

int OptionManager::get_language_id() const {
    if (m_isPirate)
        return language_to_id(Language::PIRATE);
    return language_to_id(m_gameLanguage);
}

sf::VideoMode OptionManager::get_window_settings() const {
    if (m_isFullScreen)
        return m_fullScreenVideoMode;
    return m_normalVideoMode;
}

sf::ContextSettings OptionManager::get_context_settings() const {
    sf::ContextSettings settings;
    settings.antialiasingLevel = 8;
    return settings;
}

void OptionManager::set_server_IP(sf::IpAddress const ip) { m_defaultIP = ip; }
sf::IpAddress OptionManager::get_default_server_IP() { return m_defaultIP; }

short unsigned int OptionManager::get_default_server_port() {
    return m_defaultPort;
}

void OptionManager::set_server_port(short unsigned int const port) {
    m_defaultPort = port;
}

bool OptionManager::is_online() const { return m_online; }
void OptionManager::set_online(bool online) { m_online = online; }

sf::String OptionManager::get_player_name() const { return m_playerName; }
void OptionManager::set_player_name(std::string const &s) { m_playerName = s; }

bool OptionManager::is_sound_effect_on() const { return m_soundEffects; }
void OptionManager::toggle_sound_effects(bool b) {
    m_soundEffects = b;
    float volume = b ? 100.f : 0.f;
    soundManager.set_sound_volume(volume);
}

bool OptionManager::is_music_on() const { return m_music; }
void OptionManager::toggle_music(bool b) {
    m_music = b;
    float volume = b ? 100.f : 0.f;
    soundManager.set_music_volume(volume);
}

CardType OptionManager::get_card_type() const { return m_cardType; }
void OptionManager::set_card_type(CardType const ct) { m_cardType = ct;}
