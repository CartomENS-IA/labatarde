/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "StatManager.hpp"
#include "OptionManager.hpp"
#include "SoundManager.hpp"

using namespace std;

#define LOG(msg) clog << "[StatManager] " << msg << endl

StatManager statManager; // Global StatManager

//________________________________________
StatManager::StatManager()
:
    m_hasLoaded(false)
{
    // Default values
    m_info.name = L"Inconnu";
    m_info.nationality = L"Inconnu";
    m_info.elo_title = L"Inconnu";
    m_info.registered_date = L"01/01/2000";
    m_info.first_ranking_date = L"01/01/2000";
    m_info.elo_max_date = L"01/01/2000";
    m_info.elo_max = 1000;
    m_info.elo = 1000;
}


void StatManager::load_profile(){
    m_hasLoaded = true;
}

void StatManager::save_profile() {
    if (optionManager()->is_online()){
        // TODO : send info to server
    } else {
        LOG("Nothing to save : no account authenticated");
    }
}

PlayerInfo StatManager::get_player_info() const {
    return m_info;
}

//_________________________________________

void StatManager::begin_game() {
    m_info.nb_played++;
}

void StatManager::begin_round(const Core::GameState &gs) {
    if (gs.is_choune()) {
        m_info.nb_choune++;
        m_info.nb_mechoune++;
        m_bufMechoune = gs.id_mechouner();
        m_bufChoune = gs.id_chouner();
    } else if (gs.is_mechoune()) {
        m_info.nb_mechoune++;
        m_bufMechoune = gs.id_mechouner();
        m_bufChoune = -1;
    } else {
        m_bufMechoune = -1;
        m_bufChoune = -1;
    }
    m_bufScores = gs.get_scores();
}

void StatManager::end_game(const Core::GameState &gs) {
    auto scores = gs.get_scores();
    int rank = 4;
    auto last = max_element(scores.begin(), scores.end());
    while (last != scores.begin() + gs.my_id()) {
        rank--;
        *last = -1;
        last = max_element(scores.begin(), scores.end());
    }
    m_info.ranks[rank]++;
}

void StatManager::end_round(const Core::GameState &gs) {
    auto scores = gs.get_scores();
    for (int i = 0; i < 4; i++)
        scores[i] -= m_bufScores[i];
}

void StatManager::end_trick(const Core::GameState &gs) {
    auto &stack = gs.last_card_stack();
    if (stack.id_winner() == gs.my_id()) {
        // won a trick
    }
}

//_________________________________________
