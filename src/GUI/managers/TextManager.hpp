/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef DEF_TEXT_RESSOURCE
#define DEF_TEXT_RESSOURCE

#include "Constants.hpp"
#include "enumType.hpp"
#include <string>
#include <unordered_map>

/** \file TextManager.hpp
 *    \author GUIllaume.C
 *    \brief Definition of class TextManager
 **/

/* \class
 *    TextManager is meant to store every messages and strings used in the
 *project. It is implemented using unordered_maps. Strings are accessed using a
 *string key. This has been done for two reasons :
 *        - reduce the initialisation length of classes like Message or
 *TextButton
 *        - allow the user to change the language of the program
 *  NOTE : Every string stored in TextManager are actually wstring.
 *         This is necessary to allow special characters in language such as
 *French or German. \brief A container that stores every string used in the
 *project.
 **/
class TextManager {

  public:
    /** \brief TextManager's constructor **/
    TextManager() = default;
    void load(std::string const &fileName);

    /** \brief Getter for a wstring
     *    \param lang provides the targetted language. It is of enum type
     *Language, which is defined in enumType.hpp
     **/
    std::wstring get_entry(std::string const &key, bool toUpper = false);

  private:
    std::unordered_map<std::string, std::wstring> m_dataContainer;
};

/** \brief the only object of class TextManager that should be instanciated **/
extern TextManager TM;

#endif
