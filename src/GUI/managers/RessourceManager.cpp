/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "RessourceManager.hpp"

RessourceManager RM; // Global ressource manager

RessourceManager::RessourceManager() {
    // set default size of the maps. Research will be more efficient if the map
    // size is near the number of elements stored in it
    textures.reserve(100);
    fonts.reserve(3);
}

sf::Texture &RessourceManager::get_texture(std::string const &name) {
    if (textures.find(name) == textures.end()) {
        // texture not found
        sf::Texture newTexture;
        if (!newTexture.loadFromFile("res/img/" + name)) {
            std::clog << "[RessourceManager] Warning ! Ressource " << name
                      << " has not been found !\n";
        }
        newTexture.setSmooth(true);
        textures.insert({name, newTexture});
    }
    return textures[name];
}

sf::Font &RessourceManager::get_font(std::string const &name) {
    if (fonts.find(name) == fonts.end()) {
        // font not found
        sf::Font newFont;
        if (!newFont.loadFromFile("res/fonts/" + name)) {
            std::clog << "[RessourceManager] Warning ! Ressource " << name
                      << " has not been found !\n";
        }
        fonts.insert({name, newFont});
    }
    return fonts[name];
}

sf::SoundBuffer &RessourceManager::get_sound(std::string const &name) {
    if (sounds.find(name) == sounds.end()) {
        // font not found
        sf::SoundBuffer newSound;
        if (!newSound.loadFromFile("res/music/" + name)) {
            std::clog << "[RessourceManager] Warning ! Ressource " << name
                      << " has not been found !\n";
        }
        sounds.insert({name, newSound});
    }
    return sounds[name];
}
