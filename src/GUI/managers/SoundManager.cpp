/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <exception>
#include <iostream>

#include "OptionManager.hpp"
#include "SoundManager.hpp"

using namespace std;
using namespace sf;

SoundManager soundManager; // Global sound manager. Should be the only instance.

SoundManager::SoundManager(){
    m_soundVolume = optionManager()->is_sound_effect_on() ? 100.f : 0.f;
    m_musicVolume = optionManager()->is_music_on() ? 100.f : 0.f;
}

bool SoundManager::preload_sound(const string &filename) {
    if (m_buffers.count(filename) == 0) {
        m_buffers.emplace(filename, make_unique<SoundBuffer>());
        if (!(m_buffers[filename]->loadFromFile("res/sound/" + filename))) {
            cout << "[SoundManager] Warning ! Sound " << filename
                 << " has not been found !" << endl;
            m_buffers[filename].reset();
            return false;
        }
    }
    return true;
}

bool SoundManager::play_sound(const string &filename) {
    if (m_buffers.count(filename) == 0) {
        preload_sound(filename);
    }
    if (m_buffers[filename]) {
        auto sound =
            find_if(begin(m_sounds), end(m_sounds), [](const Sound &s) {
                return s.getStatus() == Sound::Status::Stopped;
            });
        if (sound == end(m_sounds)) {
            m_sounds.emplace_back();
            sound = m_sounds.begin() + m_sounds.size() - 1;
        }
        sound->setBuffer(*m_buffers[filename]);
        sound->setVolume(m_soundVolume);
        sound->play();
        return true;
    }
    return false;
}

bool SoundManager::play_music(const string &filename) {
    if (m_musics.count(filename) == 0) {
        m_musics.emplace(filename, make_unique<Music>());
        if (!m_musics[filename] ||
            !m_musics[filename]->openFromFile("res/sound/" + filename)) {
            cout << "[SoundManager] Warning ! Music " << filename
                 << " has not been found !" << endl;
            m_musics[filename].reset();
            return false;
        }
    }
    m_musics[filename]->setVolume(m_musicVolume);
    m_musics[filename]->play();
    m_musics[filename]->setLoop(true);
    return true;
}

void SoundManager::stop_music(const string &filename) {
    try {
        m_musics[filename]->stop();
    } catch (const std::exception &e) {
        std::cerr << "[SoundManager] Tried to stop music " << filename
                  << " : No such music is played" << std::endl;
        return;
    }
}

void SoundManager::mute() {
    set_sound_volume(0);
    set_music_volume(0);
}

void SoundManager::set_sound_volume(float volume) { m_soundVolume = volume; }
void SoundManager::set_music_volume(float volume) { m_musicVolume = volume; }

float SoundManager::get_sound_volume() { return m_soundVolume; }

float SoundManager::get_music_volume() { return m_musicVolume; }
