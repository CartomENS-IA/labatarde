/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include <SFML/Audio.hpp>

/** \file SoundManager.hpp
 *   \author Louis.B, GUIllaume.C
 *   \brief Definition of the SoundManager class
 */

/** \class
 *   \brief
 **/
class SoundManager {
  public:
    SoundManager();
    bool preload_sound(const std::string &filename);
    bool play_sound(const std::string &filename);

    bool play_music(const std::string &filename);
    void stop_music(const std::string &filename);

    void mute();

    void set_sound_volume(float);
    float get_sound_volume();

    void set_music_volume(float);
    float get_music_volume();

  private:
    float m_soundVolume;
    float m_musicVolume;
    std::vector<sf::Sound> m_sounds;
    std::unordered_map<std::string, std::unique_ptr<sf::SoundBuffer>> m_buffers;
    std::unordered_map<std::string, std::unique_ptr<sf::Music>> m_musics;
};

extern SoundManager soundManager;
