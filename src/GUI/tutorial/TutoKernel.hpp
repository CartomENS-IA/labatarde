/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Board.hpp"
#include "TutoBubble.hpp"
#include "Core.hpp"
#include <fstream>
#include <queue>
#include <condition_variable>

/**  \file
 *    \author Léo.V, Louis.B, Guillaume.C
 *    \brief Definition of classes TutoEvent, TutoKernel
 *
 **/

/** \class Type of an event in the TutoKernel **/
enum class TutoEventType {
  Bet,
  SetHand,
  Play,
  MultiPlay,
  Message,
  Label, // For performing custom actions in the loop
  Mechoune,
  Choune,
  SetBet,
  BeginBetPhase,
  BeginCardPhase,
  EndCardPhase,
  TrickWon
};

/** \brief Get the name of an element of the TutoEventType enum.
        Mainly for debug and log purposes **/
std::string tutoEventType_to_string(TutoEventType const&);

/** \class An event in the TutoKernel **/
class TutoEvent {
public:
    /** \brief Constructor only sets the type of the event.
        All other attribute have to be set by hand
    **/
  TutoEvent(TutoEventType const);
  TutoEventType type;
  int player;
  Core::Value value;
  int betValue;
  Core::Trump trump;
  Core::Suit suit;
  std::string message;
  sf::Vector2f pos;
  Core::Hand hand;
  Core::AllBets allBets;
};

/** \class A Message in the tutorial to be given to Tutobubble
            in order to be displayed **/
struct TutoMessage {
  std::string messageKey;
  sf::Vector2f pos;
};

/** \class TutoKernel
    \brief A class simulating the behavior of a Kernel in the tutorial loop

    The TutoKernel class allows to define a set of scripted actions that will be
    executed during the game.

    TutoKernel works asynchronously from a configuration file (in res/tuto
    folder). It parses this file and create a FIFO queue of TutoEvent. Those
    TutoEvents can then be executed in order depending on the signals coming
    from the outside.
**/
class TutoKernel {
  public:
    /**  \brief Constructor **/
    TutoKernel(std::string configFileName, Core::GameState *gamestate,
               std::shared_ptr<Core::ParallelPlayer> player, Core::BetQueue *betQueue,
               Board *board);
    /** \brief Launches the execution of the event queue.

        /!\ This method is meant to be called in a separated thread.
        When an event was a message that implies a pause, sets a condition
        variable to stop the execution.
    **/
    void run();
    /** \brief Sends a signal to the run method to free the condition variable
    **/
    void next_event();
    /** \brief Returns true if and only if a new Message
                event has been processed
    **/
    bool has_new_message();
    /** \brief Propagates the last event message seen to a TutoBubble object **/
    void set_message_on_bubble(TutoBubble * bubble);
    /** \brief Returns true if and only if a new Label
                event has been processed
    **/
    bool has_new_label();
    /** \brief Returns the name of the last Label event **/
    std::string get_last_label();

    /** \brief Returns true if the TutoEvent queue is empty **/
    bool no_more_event();
    /** \brief Terminates the execution

        In practice, will set a flag not to wait on the condition variable.
        The thread has then to be detached and will terminate its
        execution freely
    **/
    void end_tutorial();
  private:

    /* Parsing functions for the config files.
       See res/tuto/Grammar.md for a description of the syntax */
    void read_instruction(std::string const filename);
    void read_bet(std::ifstream &file);
    void read_play(std::ifstream &file);
    void read_multiplay(std::ifstream &file);
    void read_message(std::ifstream &file);
    void read_label(std::ifstream &file);
    void read_mechoune(std::ifstream &file);
    void read_choune(std::ifstream &file);
    void read_sethand(std::ifstream &file);
    void read_setbet(std::ifstream &file);
    void read_comment(std::ifstream &file);
    void read_trickwon(std::ifstream &file);

    /** \brief Executes the instructions relative to an event. Is only called
    *     in the public run method
    **/
    void run_next_event();

    bool m_has_ended;

    std::condition_variable m_cv;
    std::mutex m_mtx;

    std::queue<TutoEvent> m_eventQueue;

    Core::GameState *m_gamestate;
    Core::BetQueue *m_betQueue;
    std::shared_ptr<Core::ParallelPlayer> m_player;
    Board *m_board;

    bool m_newMessage;
    TutoMessage m_lastMessage;
    sf::Vector2f m_posMessage;
    std::string m_messageKey;

    bool m_newLabel;
    std::string m_lastLabel;

};

bool run_next_kernel_event(std::shared_ptr<TutoKernel>&);
