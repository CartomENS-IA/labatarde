/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "TutoKernel.hpp"
#include "TextManager.hpp"

#include <exception>
#include <mutex>
#include <iostream>

using namespace std;
using namespace Core;

const vector<string> name_value = {"Cat",      "Dog",         "Juggler",
                                   "Musician", "Entertainer", "Knave",
                                   "Knight",   "Queen",       "King"};

std::string tutoEventType_to_string(TutoEventType const& type){
    switch (type) {
        case TutoEventType::Bet:
            return "Bet";
        case TutoEventType::Play:
            return "Play";
        case TutoEventType::MultiPlay:
            return "MultiPlay";
        case TutoEventType::Message:
            return "Message";
        case TutoEventType::Label:
            return "Label";
        case TutoEventType::Mechoune:
            return "Mechoune";
        case TutoEventType::Choune:
            return "Choune";
        case TutoEventType::SetBet:
            return "SetBet";
        case TutoEventType::SetHand:
            return "SetHand";
        case TutoEventType::BeginBetPhase:
            return "BeginBetPhase";
        case TutoEventType::BeginCardPhase:
            return "BeginCardPhase";
        case TutoEventType::EndCardPhase:
            return "EndCardPhase";
        case TutoEventType::TrickWon:
            return "TrickWon";
        default:
          return "Unknown";
    }
}


TutoEvent::TutoEvent(TutoEventType const t)
    : type(t) { pos.x = -1; pos.y = -1;}

TutoKernel::TutoKernel(string fileName, Core::GameState *gamestate,
                       shared_ptr<Core::ParallelPlayer> player,
                       Core::BetQueue *betQueue,
                       Board *board)
:
    m_has_ended(false),
    m_gamestate(gamestate), m_betQueue(betQueue),
    m_player(player), m_board(board),
    m_newMessage(false),
    m_posMessage(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2),
    m_newLabel(false)
{
    if (m_board) {
        m_board->enable_tuto_mode();
    }
    TM.load(fileName + ".xml");
    read_instruction(fileName);
}

// ______________ Parsing functions ____________________________________________

void TutoKernel::read_instruction(string fileName) {
    std::ifstream file;
    file.open("res/tuto/" + fileName + ".tuto", std::ios::in);
    if (!file) {
        cerr << "[TutoKernel] Failed to load file '" << fileName << "'" << endl;
        throw runtime_error("Runtime error from TutoKernel");
    }
    string buffer;
    while (file >> buffer) {
        // std:: << buffer << std::endl;  // DEBUG
        if (buffer == "Bet") {
            read_bet(file);
        } else if (buffer == "SetHand") {
            read_sethand(file);
        } else if (buffer == "Play") {
            read_play(file);
        } else if (buffer == "MultiPlay") {
            read_multiplay(file);
        } else if (buffer == "Message") {
            read_message(file);
        } else if (buffer == "Label") {
            read_label(file);
        } else if (buffer == "Mechoune") {
            read_mechoune(file);
        } else if (buffer == "Choune") {
            read_choune(file);
        } else if (buffer == "SetBet") {
            read_setbet(file);
        } else if (buffer == "BeginBetPhase") {
            m_eventQueue.emplace(TutoEventType::BeginBetPhase);
        } else if (buffer == "BeginCardPhase") {
            m_eventQueue.emplace(TutoEventType::BeginCardPhase);
        } else if (buffer == "EndCardPhase") {
            m_eventQueue.emplace(TutoEventType::EndCardPhase);
        } else if (buffer == "TrickWon") {
            read_trickwon(file);
        } else if (buffer == "/*") {
            read_comment(file);
        } else {
            cerr << "[TutoKernel] Received an unknown instruction :"
                 << " '" << buffer << "'" << endl;
            throw runtime_error("Runtime error from TutoKernel");
        }
    }
    clog << "[TutoKernel] Read a total of " << m_eventQueue.size()
         << " events from file " << fileName << endl;
    file.close();
}

void TutoKernel::read_bet(ifstream& file) {
    string buffer;
    file >> buffer;
    int player = stoi(buffer);
    if (player >= 4 || player < 0) {
        cerr << "[TutoKernel] Invalid Player ID : " << player << "\n";
        throw runtime_error("Runtime error from TutoKernel");
    }
    int number;
    int trump;
    file >> buffer;
    if (buffer == "_") {
        number = -1;
    } else
        number = stoi(buffer);
    file >> buffer;
    if (buffer == "_") {
        trump = -1;
    } else {
        for (int i = 0; i <= 6; i++) {
            if (i == 6) {
                cerr << "[TutoKernel] Invalid Trump : " << buffer << endl;
                throw runtime_error("Runtime error from TutoKernel");
            }
            if (buffer == NAME_TRUMP[i]) {
                trump = i;
                break;
            }
        }
    }
    TutoEvent event(TutoEventType::Bet);
    event.player = player;
    event.betValue = number;
    event.trump = static_cast<Core::Trump>(trump);
    m_eventQueue.push(event);
}

void TutoKernel::read_play(ifstream& file) {
    string buffer;
    file >> buffer;
    int player = stoi(buffer);
    if (player >= 4 || player < 0) {
        cerr << "[TutoKernel] Invalid Player ID : " << player << "\n";
        throw runtime_error("Runtime error from TutoKernel");
    }

    file >> buffer;
    Core::Value card_value;
    for (int i = 0; i <= 9; i++) {
        if (i == 9) {
            cerr << "[TutoKernel] Invalid value card : " << buffer << "\n";
            throw runtime_error("Runtime error from TutoKernel");
        }
        if (buffer == name_value[i]) {
            card_value = static_cast<Core::Value>(i);
            break;
        }
    }

    file >> buffer;
    Core::Suit card_suit;
    for (int i = 1; i <= 5; i++) {
        if (i == 5) {
            cerr << "[TutoKernel] Invalid suit card : " << buffer << "\n";
            throw runtime_error("Runtime error from TutoKernel");
        }
        if (buffer == NAME_TRUMP[i]) {
            card_suit = static_cast<Core::Suit>(i);
            break;
        }
    }
    TutoEvent event(TutoEventType::Play);
    event.player = player;
    event.value = card_value;
    event.suit = card_suit;
    m_eventQueue.push(event);
}

void TutoKernel::read_multiplay(ifstream& file) {
    TutoEvent event(TutoEventType::MultiPlay);
    string buffer;
    file >> buffer;
    if (buffer != "{") {
        cerr << "[TutoKernel] Invalid Syntax, { expected after SetHand\n";
        throw runtime_error("Runtime error from TutoKernel");
    }

    file >> buffer;
    Core::Hand hand;
    Core::Value card_value;
    Core::Suit card_suit;
    while (buffer != "}") {
        for (int i = 0; i <= 9; i++) {
            if (i == 9) {
                cerr << "[TutoKernel] Invalid value card : " << buffer << "\n";
                throw runtime_error("Runtime error from TutoKernel");
            }
            if (buffer == name_value[i]) {
                card_value = static_cast<Core::Value>(i);
                break;
            }

        }

        file >> buffer;
        for (int i = 1; i <= 5; i++) {
            if (i == 5) {
                cerr << "[TutoKernel] Invalid suit card : " << buffer << "\n";
                throw runtime_error("Runtime error from TutoKernel");
            }
            if (buffer == NAME_TRUMP[i]) {
                card_suit = static_cast<Core::Suit>(i);
                break;
            }
        }

        event.hand.emplace_back(card_suit, card_value);
        file >> buffer;
    }

    file >> buffer;
    if (buffer != "Wrong") {
        cerr << "[TutoKernel] Error : MultiPlay without Wrong stat\n";
        throw runtime_error("Runtime error from TutoKernel");
    }
    file >> buffer;
    if (buffer != "{") {
        cerr << "[TutoKernel] Invalid Syntax, { expected after SetHand\n";
        throw runtime_error("Runtime error from TutoKernel");
    }
    while (buffer != "}") {
        file >> buffer;
    }
    m_eventQueue.push(event);
}

void TutoKernel::read_message(ifstream& file) {
    string buffer;
    TutoEvent event(TutoEventType::Message);
    file >> buffer;
    if (buffer == "Pos") {
        file >> event.pos.x >> event.pos.y;
        event.pos.x *= WINDOW_WIDTH;
        event.pos.y *= WINDOW_HEIGHT;
        file >> buffer;
    }
    event.message = buffer;
    m_eventQueue.push(event);
}

void TutoKernel::read_label(ifstream& file) {
    string buffer;
    TutoEvent event(TutoEventType::Label);
    file >> event.message;
    m_eventQueue.push(event);
}

void TutoKernel::read_sethand(ifstream& file) {
    TutoEvent event(TutoEventType::SetHand);
    string buffer;
    file >> buffer;
    if (buffer != "{") {
        cerr << "[TutoKernel] Invalid Syntax, { expected after SetHand\n";
    }

    file >> buffer;
    while (buffer != "}") {
        if (buffer != "Card") {
            cerr << "[TutoKernel] Invalid Syntax, " + buffer +
                        " element unknown\n";
        }
        file >> buffer;
        Core::Value card_value;
        for (int i = 0; i <= 9; i++) {
            if (i == 9) {
                cerr << "[TutoKernel] Invalid value card : " << buffer << "\n";
                throw runtime_error("Runtime error from TutoKernel");
            }
            if (buffer == name_value[i]) {
                card_value = static_cast<Core::Value>(i);
                break;
            }
        }

        file >> buffer;
        Core::Suit card_suit;
        for (int i = 1; i <= 5; i++) {
            if (i == 9) {
                cerr << "[TutoKernel] Invalid suit card: " << buffer << "\n";
                throw runtime_error("Runtime error from TutoKernel");
            }
            if (buffer == NAME_TRUMP[i]) {
                card_suit = static_cast<Core::Suit>(i);
                break;
            }
        }
        event.hand.emplace_back(card_suit, card_value);
        file >> buffer;
    }
    m_eventQueue.push(event);
}

void TutoKernel::read_mechoune(ifstream& file) {
    TutoEvent event(TutoEventType::Mechoune);
    file >> event.player;
    m_eventQueue.push(event);
}

void TutoKernel::read_choune(ifstream& file) {
    TutoEvent event(TutoEventType::Choune);
    file >> event.player;
    m_eventQueue.push(event);
}

void TutoKernel::read_setbet(ifstream& file) {
    TutoEvent event(TutoEventType::SetBet);
    Core::Trump trumpBet = Core::Trump::NoTrump;
    string buffer;
    file >> buffer;
    if (buffer != "{") {
        for (int i = 0; i <= 6; i++) {
            if (i == 6) {
                cerr
                    << "[TutoKernel] Invalid Syntax, { expected after SetBet\n";
            } else if (buffer == NAME_TRUMP[i]) {
                trumpBet = static_cast<Core::Trump>(i);
                break;
            }
        }
        file >> buffer;
        if (buffer != "{") {
            cerr << "[TutoKernel] Invalid Syntax, { expected after SetBet\n";
        }
    }

    file >> buffer;
    while (buffer != "}") {
        if (buffer != "Bet") {
            cerr << "[TutoKernel] Invalid Syntax, " + buffer +
                        " element unknown\n";
            throw runtime_error("Runtime error from TutoKernel");
        }
        file >> buffer;
        int value = stoi(buffer);

        event.allBets.emplace_back(trumpBet, value);
        file >> buffer;
    }

    if (event.allBets.size() > 4) {
        cerr << "[TutoKernel] Invalid Size of SetBet, they are only 4 players "
                "\n";
        throw runtime_error("Runtime error from TutoKernel");
    }

    while (event.allBets.size() < 4) {
        event.allBets.emplace_back(trumpBet, 0);
    }
    m_eventQueue.push(event);
}

void TutoKernel::read_comment(ifstream& file) {
    string buffer;
    file >> buffer;
    while (buffer != "*/") {
        file >> buffer;
    }
}

void TutoKernel::read_trickwon(ifstream& file) {
    TutoEvent event(TutoEventType::TrickWon);
    file >> event.player;
    m_eventQueue.push(event);
}


// ____________________________ Other methods __________________________________

void TutoKernel::end_tutorial(){
    m_has_ended = true;
    m_cv.notify_one();
}

bool TutoKernel::no_more_event(){
    return m_eventQueue.empty();
}

bool TutoKernel::has_new_message(){
  return m_newMessage;
}

bool TutoKernel::has_new_label(){
  return m_newLabel;
}

void TutoKernel::set_message_on_bubble(TutoBubble * bubble){
    std::unique_lock<std::mutex> lck(m_mtx);
    m_newMessage=false;
    bubble->set_text(m_lastMessage.messageKey);
    if (m_lastMessage.pos.x != -1 and m_lastMessage.pos.y != -1)
        bubble->set_position(m_lastMessage.pos);
}

string TutoKernel::get_last_label(){
    std::unique_lock<std::mutex> lck(m_mtx);
    m_newLabel = false;
    return m_lastLabel;
}

void TutoKernel::run(){
    while (!m_eventQueue.empty() or m_has_ended){
        run_next_event();
    }
}

void TutoKernel::next_event(){
    m_cv.notify_one();
}

void TutoKernel::run_next_event(){
    if (m_eventQueue.empty()) return; // nothing to do

    TutoEvent event = m_eventQueue.front();
    m_eventQueue.pop();

    clog << "[TutoKernel] Executing event of type: "
       << tutoEventType_to_string(event.type) << endl;
    switch (event.type) {

        case TutoEventType::Bet: {
            m_gamestate->notify_next_player(event.player);
            if (event.player == 0) {
                m_board->set_playable_bets((int)event.trump, (int)event.value);
                BetEvent *bev = nullptr;
                while (bev == nullptr) {
                    m_player->ask_bet();
                    Event *ev = m_betQueue->pop();
                    bev = dynamic_cast<BetEvent *>(ev);
                }
                m_gamestate->notify_bet(bev->bet);
            } else {
                m_gamestate->notify_bet(Core::Bet(event.trump, event.betValue));
            }
            break;
        }

        case TutoEventType::Play: {
            m_gamestate->notify_next_player(event.player);
            if (event.player == 0) {
                m_board->set_playable_cards(std::vector<Core::Card>(1,
                                        Core::Card(event.suit, event.value)));
                m_player->play();
            }
            m_gamestate->notify_play(Core::Card(event.suit, event.value));
            break;
        }

        case TutoEventType::MultiPlay: {
            m_gamestate->notify_next_player(0);
            m_board->set_playable_cards(event.hand);
            m_gamestate->notify_play(m_player->play());
            break;
        }

        case TutoEventType::Message: {
            m_mtx.lock();
            m_lastMessage.messageKey = event.message;
            m_lastMessage.pos = event.pos;
            m_newMessage = true;
            m_mtx.unlock();
            if (m_eventQueue.front().type != TutoEventType::Message)
                run_next_event();
            std::unique_lock<std::mutex> lck(m_mtx);
            m_cv.wait(lck);
            break;
        }

        case TutoEventType::Label: {
            std::unique_lock<std::mutex> lck(m_mtx);
            m_lastLabel = event.message;
            m_newLabel = true;
            break;
        }

        case TutoEventType::Mechoune: {
            m_gamestate->notify_mechoune(event.player);
            break;
        }

        case TutoEventType::Choune: {
            m_gamestate->notify_choune();
            break;
        }

        case TutoEventType::SetBet: {
            m_gamestate->notify_begin_bet_state();
            m_board->set_allbet(event.allBets);
            break;
        }

        case TutoEventType::SetHand: {
            m_gamestate->notify_cards(event.hand);
            break;
        }

        case TutoEventType::BeginBetPhase: {
            m_gamestate->notify_begin_bet_state();
            break;
        }

        case TutoEventType::BeginCardPhase: {
            m_gamestate->notify_end_bet_state();
            break;
        }

        case TutoEventType::EndCardPhase: {
            m_gamestate->notify_end_card_state();
            break;
        }

        case TutoEventType::TrickWon: {
            m_gamestate->notify_trick_win(event.player);
            break;
        }

        default:
            cerr << "[TutoKernel] In run_next_event(): "
                 << "Event type not recognized" << endl;
            break;
    }
}
