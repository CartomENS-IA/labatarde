/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef CONSTANTS
#define CONSTANTS

#include <exception>
#include <string>
#include <vector>

#include "OptionManager.hpp"

/** \file Constants.hpp
 *   \brief Definition of all constants of the GUI
 **/

// _________ Exception _____________

/** \brief Exception thrown at the end of the game in order to leave the game
 * loop **/
class EndOfGame : public std::exception {
  public:
    EndOfGame() = default;
};

extern float const ANIMATION_TIME;

// _________ String Trump ___________

extern std::vector<std::string> const NAME_TRUMP;
extern std::vector<std::wstring> const NAME_VALUE;

extern std::vector<std::string> FNAME_VALUE;
extern std::vector<std::string> FNAME_TRUMP;

// ________ Splash Screen ___________

extern float const SPLASH_TIME;
extern float const FADE_SPEED;

// ________ Window Resolutions ________
extern std::vector<std::wstring> const RESOLUTION;

extern std::vector<float> const RESOLUTION_WIDTH;
extern std::vector<float> const RESOLUTION_HEIGHT;

extern int const NB_RESOLUTION;

// ________ Window Size ________
extern float const WINDOW_WIDTH;
extern float const WINDOW_HEIGHT;

extern float const SCALE; // SCALE in relation to 1280*720
extern float const SCALE_TO_MAX; // SCALE in relation to 1920*1080
extern float const HALF_SCALE;
extern float const HALF_SCALE_TO_MAX;

// ______ Colors ______

extern sf::Color const WHITE;
extern sf::Color const YELLOW;
extern sf::Color const BLUE;
extern sf::Color const BLACK;
extern sf::Color const BLACK_LIT;
extern sf::Color const GRAY;
extern sf::Color const CARD_GRAY; // the gray used for darkening non playable cards
extern sf::Color const GRAY_LIT;
extern sf::Color const LIGHT_GRAY;
extern sf::Color const GREEN;
extern sf::Color const GREEN_LIT;
extern sf::Color const RED;
extern sf::Color const RED_LIT;
extern sf::Color const TRANSPARENT;
extern sf::Color const BACKGROUND;

// ______ Font size constants ______

extern float const TITLE_TEXT_SIZE;
extern unsigned int const TITLE_FONT_SIZE;

extern float const MENU_BUTTON_TEXT_SIZE;
extern unsigned int const MENU_BUTTON_FONT_SIZE;

extern float const STAT_TEXT_SIZE;
extern unsigned int const STAT_FONT_SIZE;

// ______ Math constants ______
extern float const PI;

// _______ Main menu Constants _______
extern float const BUTTON_SIZE;
extern float const BETWEEN_BUTTONS;
extern float const MENU_OFFSET;
extern float const MENU_OFFSET_SMALL;

//_________ Center Play Zone ________
extern float const CENTER_PLAY_X;
extern float const CENTER_PLAY_Y;

// ______ Player Mark Size _______
extern float const PLAYER_MARK_SIZE;

// ______ Card Size _______
extern float const WIDTH;
extern float const LENGTH;

extern float const SCALE;

extern float const TIME_WAIT_TRICK;
extern float const TIME_TROPHY_APPEAR;

// _______ Bet Bubble Constant _______

extern float const HEIGHT_BETBUBBLE;
extern float const WIDTH_BETBUBBLE;
extern float const SIZE_ARROW_BETBUBBLE;

extern float const SIZE_TEXT_BETBUBBLE;
extern unsigned const SIZE_FONT_BETBUBBLE;

extern float const DIST_HAND_BETBUBBLE;

extern float const TIME_BETBUBBLE_ANIMATION;

// _____ Card Speed _______

extern float const C_DISTRIB_TIME;
extern float const C_REPLACE_TIME;
extern float const C_PLAY_TIME;
extern float const C_TRICK_TIME;

// ______ Game Constant ________
extern float const DISTRIBUTION_SPEED;

// ______ Hand Constant ________
extern float const ANGLE_FAN;    // degre between each card
extern float const CARD_SPACING; // degree of superposition
extern float const DISTANCE_CIRCLE_CENTER;

// printed size of trumps
extern float const WIDTH_TRUMP; // = HEIGHT_TRUMP for real trumps

#endif
