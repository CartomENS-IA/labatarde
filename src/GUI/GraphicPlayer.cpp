/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "GraphicPlayer.hpp"
#include "Constants.hpp"
#include "SoundManager.hpp"
#include <iostream>

using namespace sf;

// __________________ Constructor _________________________

GraphicPlayer::GraphicPlayer(CardinalPoints cardi, PlayerInfo const &pinfo)
:
    m_mainPhase(false), m_isActive(false),
    m_showPlayerDisplay(true), m_lockPlayerDisplay(false)
{
    float const playerPosOffset = 40.f*SCALE;
    float orientation;
    sf::Vector2f position;
    float scale;
    sf::Vector2f playerDisplayPos;

    m_mainPlayer = (cardi == CardinalPoints::SOUTH);
    bool is_horizontal = false;
    switch (cardi) {
        case CardinalPoints::NORTH :
            is_horizontal = true;
            playerDisplayPos.x = 0.6f*WINDOW_WIDTH;
            playerDisplayPos.y = 10.f*SCALE;
            position = sf::Vector2f(WINDOW_WIDTH / 2, playerPosOffset+10*SCALE);
            orientation = 180.f;
            scale = 0.8f;
            break;
        case CardinalPoints::WEST :
            playerDisplayPos.x = 10.f*SCALE;
            playerDisplayPos.y = WINDOW_HEIGHT/2.f - 100.f*SCALE;
            position = sf::Vector2f(playerPosOffset+130*SCALE, WINDOW_HEIGHT / 2);
            orientation = 270.f;
            scale = 0.8f;
            break;
        case CardinalPoints::EAST :
            playerDisplayPos.x = WINDOW_WIDTH - 110.f*SCALE;
            playerDisplayPos.y = WINDOW_HEIGHT/2.f - 100.f*SCALE;
            position = sf::Vector2f(WINDOW_WIDTH - playerPosOffset -130*SCALE, WINDOW_HEIGHT / 2);
            orientation = 90.f;
            scale = 0.8f;
            break;
        case CardinalPoints::SOUTH :
        default:
            playerDisplayPos = sf::Vector2f(100.f*SCALE,WINDOW_HEIGHT-210.f*SCALE);
            position = sf::Vector2f(WINDOW_WIDTH / 2, WINDOW_HEIGHT - playerPosOffset-70.f*SCALE);
            orientation = 0.f;
            scale = 1.4f;
            break;
    }
    m_playerDisplay = PlayerDisplayBoard(playerDisplayPos, pinfo, is_horizontal);
    m_handPlayer = Hand(position, cardi, scale,
                        m_mainPlayer, (!m_mainPlayer) ? 15 : 3);
    m_tokenPlayer = TokenArray(position + rotateVector(0,
                -DIST_HAND_BETBUBBLE, orientation) + sf::Vector2f(0,-45*SCALE*m_mainPlayer) , orientation);
    m_betBubble = BetBubble(cardi);

}

// _________________ Draw Function ________________________

void GraphicPlayer::draw(sf::RenderWindow &window) {
    if (m_mainPhase) m_tokenPlayer.draw(window);
    draw_minimal(window);
    m_betBubble.update(window);
}

void GraphicPlayer::draw_minimal(sf::RenderWindow &window) {
    m_handPlayer.draw(window);
    if (m_showPlayerDisplay and !m_lockPlayerDisplay)
      m_playerDisplay.update(window);
}

// _________________ Event Function _______________________

std::shared_ptr<Card> GraphicPlayer::handle_mouse_click(float const mouseX,
                                                        float const mouseY) {
    if (m_isActive && m_mainPlayer) {
        return m_handPlayer.play_card(mouseX, mouseY);
    }
    return nullptr;
}

std::shared_ptr<Card> GraphicPlayer::want_play(sf::Vector2f const mousePos) {
    return m_handPlayer.want_play(mousePos);
}

std::shared_ptr<Card> GraphicPlayer::want_play(sf::Vector2i const mousePos) {
    return m_handPlayer.want_play(mousePos);
}

std::shared_ptr<Card> GraphicPlayer::want_play(float const mouseX,
                                               float const mouseY) {
    return m_handPlayer.want_play(mouseX, mouseY);
}

// _________________ Accesor Function ____________________

void GraphicPlayer::your_turn() {
    m_isActive = true;
    m_playerDisplay.your_turn();
}

void GraphicPlayer::end_turn() {
    m_isActive = false;
    m_playerDisplay.end_turn();
}

void GraphicPlayer::set_playable_cards(Core::Hand hand) {
    m_handPlayer.set_playable_cards(hand);
}

void GraphicPlayer::sort_hand(Core::Trump trump) {
    m_handPlayer.sort_hand(trump);
}

void GraphicPlayer::set_first_player(bool first_player) {
    first_player ? m_playerDisplay.your_turn() : m_playerDisplay.end_turn();
}

std::shared_ptr<Card> GraphicPlayer::play_card(Core::Card cardPlayed) {
    soundManager.play_sound("card.wav");
    return m_handPlayer.play_card(cardPlayed);
}

void GraphicPlayer::set_play_zone(sf::Vector2f const position) {
    m_handPlayer.set_play_zone(position);
}

void GraphicPlayer::give_card() {
    soundManager.play_sound("card_scatter.ogg");
    m_handPlayer.give_card();
}

void GraphicPlayer::give_card(Core::Card cardGiven) {
    m_handPlayer.give_card(cardGiven);
}

void GraphicPlayer::give_trick() {
    // soundManager.play_sound("");
    m_tokenPlayer.new_trick();
}

void GraphicPlayer::draw_new_trick() { m_tokenPlayer.draw_new_trick(); }

sf::Vector2f GraphicPlayer::get_pos_free_token() {
    return m_tokenPlayer.get_pos_free_token();
}

void GraphicPlayer::begin_main_phase() {
    m_mainPhase = true;
    m_tokenPlayer.set_nb_token(m_betBubble.get_bet().value);
    m_betBubble.restart();
}

void GraphicPlayer::end_play_moment() {
  m_mainPhase = false;
  hide_player_display();
}

void GraphicPlayer::set_bet(Core::Bet bet, bool draw) {
    m_betBubble.set_bet(bet, draw);
}

void GraphicPlayer::set_message(std::wstring text) {
    m_betBubble.set_message(text);
}

void GraphicPlayer::set_message(std::string textname) {
    m_betBubble.set_message(textname);
}

void GraphicPlayer::set_points(int points){
    m_playerDisplay.set_points(points);
}

Core::Bet GraphicPlayer::get_bet() { return m_betBubble.get_bet(); }

float GraphicPlayer::get_scale_token() { return m_tokenPlayer.get_scale(); }

bool GraphicPlayer::is_animated() {
    return (m_mainPhase && m_tokenPlayer.is_animated()) ||
           m_betBubble.is_animated();
}

void GraphicPlayer::show_player_display(){
  m_showPlayerDisplay = true;
}

void GraphicPlayer::hide_player_display(){
  m_showPlayerDisplay = false;
}

void GraphicPlayer::lock_player_display(){
  m_lockPlayerDisplay = true;
}

void GraphicPlayer::restart_bet() { m_betBubble.restart(); }

int GraphicPlayer::size_hand() { return m_handPlayer.size(); }
