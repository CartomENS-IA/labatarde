/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "PlayerInfo.hpp"

sf::Color const ELO_GREEN(0,124,82);
sf::Color const ELO_GREEN_LIGHT(157,200,180);
sf::Color const ELO_RED(227,28,24);
sf::Color const ELO_RED_LIGHT(246,180,148);
sf::Color const ELO_BLUE(6,53,138);
sf::Color const ELO_BLUE_LIGHT(166,163,206);
sf::Color const ELO_PURPLE(167,0,117);
sf::Color const ELO_PURPLE_LIGHT(215,170,201);

sf::Color get_elo_color_main(unsigned int elo) {
    if (elo < 1400) {
        return ELO_GREEN;
    } else if (elo < 1800) {
        return ELO_RED;
    } else if (elo < 2200) {
        return ELO_PURPLE;
    } else {
        return ELO_BLUE;
    }
}

sf::Color get_elo_color_bg(unsigned int elo) {
    if (elo < 1400) {
        return ELO_GREEN_LIGHT;
    } else if (elo < 1800) {
        return ELO_RED_LIGHT;
    } else if (elo < 2200) {
        return ELO_PURPLE_LIGHT;
    } else {
        return ELO_BLUE_LIGHT;
    }
}
