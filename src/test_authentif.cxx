/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <iostream>
#include <string>

#include "IA.hpp"
#include "Network.hpp"
#include "ServerCommunication.hpp"

using namespace std;
using namespace Network;

// #define IP "140.77.166.19"
#define IP "0.0.0.0"

int main() {

  ServerCommunication sc = ServerCommunication(sf::IpAddress(IP), 4242);
  sc.init();
  string user = string("gui");
  string pass = string("gui");
  sc.authenticate(user.c_str(), pass.c_str());
  Player *p = new IA::IAPlayer(50);

  sc.create_game(p);
  sc.start_game();
  while (true) {
    continue;
  }
  return 0;
}
