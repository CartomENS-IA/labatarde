/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

/**
 * \file GUI.hpp
 * \author GUIllaume.C
 * \brief main hpp file of the workpackage GUI. Definition of GUI class.
 * This file must be included by any other file who uses the PC GUI,
 * and should be the only one included.
 **/

#include <exception>
#include <vector>
#include <memory>

#include "OptionManager.hpp"
#include "SoundManager.hpp"
#include "StatManager.hpp"
#include "TextManager.hpp"

#include "Graphics.hpp"
#include "PlayerDisplay.hpp"
#include "Board.hpp"

#include "IA.hpp"
#include "ClientMessenger.hpp"
#include "Core.hpp"

/**
 *    \brief Main class of the Work Package PC Client.
 *
 *    Several cpp are bound to this class. One cpp correspond to one event loop.
 *    See files in the Loops folder for more details.
 *
 **/
class GUI {
  public:
    /** \brief GUI constructor : instanciate a new GUI. No parameters are needed
     * **/
    GUI();

    /** \brief Runs the GUI loop. Fonctions defined in GUI.cpp **/
    void run();

    /** \brief Temporary run function in order to perform tests
     *           Added by Guillaume. Don't forget to remove at release
     **/
    void run_test();

private:

    enum class Function : int {
        NONE,
        AUTHENTICATE,
        BACK,
        QUIT,
        DISCONNECT,
        OPTIONS_LOOP,
        CREDITS,
        STATS,
        MODE_SELECTION,
        DIFFICULTY_SELECTION,
        INIT_IA_LVL1,
        INIT_IA_LVL2,
        INIT_IA_LVL3,
        INIT_IA_LVL4,
        INIT_ONLINE,
        LOGIN_GAME,
        LOGIN_STAT,
        MULTIPLAYER_LOBBY,
        TUTORIAL_SELECTION,
        TUTORIAL_CARDS_LOOP,
        TUTORIAL_TRICKS_LOOP,
        TUTORIAL_BET_LOOP,
        TUTORIAL_FLOW_LOOP,
        CREATE_ACCOUNT
    };

    struct LoginInfo {
        LoginInterface* login;
        Function successAction;
    };

    std::shared_ptr<Network::ClientMessenger> m_serverLink;

    sf::RenderWindow app;
    bool m_splashActive;
    Background m_splashScreen;
    AnimatedSprite m_title;

    void choose_functionnality(Function const);

    void run_loop(std::vector<MessPtr> &messages,
                  std::vector<ButPtr> &buttons,
                  std::vector<Function> &functionnalities,
                  bool display_title = true,
                  LoginInfo * logInfo = 0);

    // These methods are only called during GUI's run method execution

    /** \brief **/
    FourPlayerInfo retrieve_player_info(bool onlineGame, Difficulty const diff);
    FourPlayerInfo retrieve_player_info_tuto();
    void init_game_loop(bool onlineGame,
                        Difficulty const diff=Difficulty::VERY_GOOD);
    void run_game_loop(shared_ptr<Core::KernelInterface> kernel,
                       shared_ptr<Core::ParallelPlayer> gui_player,
                       bool onlineGame, Difficulty const);

    void run_main_menu();

    void run_mode_selection_loop();
    void run_connect_game();

    void run_IA_difficulty_selection();
    void run_multiplayer_lobby(char const serverID = 0);

    void run_tutorial_loop(std::string path);
    void run_tutorial_card_loop();
    void run_tutorial_menu();

    void run_tuto_cards_loop();
    void run_tuto_tricks_loop();
    void run_tuto_bets_loop();
    void run_tuto_flow_loop();

    void run_credits_loop();

    void run_connect_stats();
    void run_stat_loop();

    void run_options_loop();

    void run_work_in_progress();

};

void open_url(std::string const &url);
