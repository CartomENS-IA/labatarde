/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "NetworkMessage.hpp"
#include "GameMessage.hpp"

#include <iostream>

using namespace std;
using namespace Network;

/** \file
*	 Main file for test mode
**/

int main() {
	// GUI theGui;
	// theGui.run_test();
	GameMessage mess;
	mess.add_node("bet");
	mess.add_field("bet", "value","4");
	mess.add_field("bet","trump", "coeur");

	mess.add_node("bet2");
	mess.add_field("bet2", "value", "2");
	mess.add_field("bet2", "trump", "carreau");

	cout << mess.read_field("value") << endl;
	cout << mess.read_field("value") << endl;

	auto res = mess.read_fields("value");
	for (auto &r : res){
		cout << r << endl;
	}

}
