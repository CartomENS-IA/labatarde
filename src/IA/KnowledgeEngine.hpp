/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Core.hpp"
#include <iostream>
#include <list>
#include <random>
#include <set>
#include <vector>

/**
 * \file KnowledgeEngine.hpp
 * \author Louis.B
 * \brief Define a motor that deduce informations about state of the game
 **/

/** \brief Class that contains information we have on a specific player **/
class Belief {
  public:
    /** \brief construct the belief with the set of cards he may have **/
    Belief(const Core::Hand &, int);

    /** \brief remove a card from set of possible cards **/
    void remove(const Core::Card &);

    /** \brief remove a color fromset of possible colors **/
    void remove(const Core::Suit &);

    /** \brief remove all cards than the card provided in argument **/
    void remove_stronger(const Core::Card &, const Core::CardStack &);

    /** \brief check if a card domines a whole game **/
    bool domines(const Core::Card &, const Core::CardStack &) const;

    /** \brief test if a card provided in argument will lose the trick against
     * current adversary **/
    bool must_lose_trick(const Core::Card &, const Core::CardStack &) const;

    /** \brief test if a card provided in argument will win the trick against
     * current adversary **/
    bool must_win_trick(const Core::Card &, const Core::CardStack &) const;

    /** \brief test if this player may have this card **/
    bool may_have(const Core::Card &) const;

    /** \brief test if a player may have this color **/
    bool may_have(const Core::Suit &) const;

    /** \brief get the whole set of cards **/
    Core::Hand get_cards() const;

    /** \brief the number of cards the player really has **/
    int nb_remaining_cards() const;

    /** \brief reduce the number of cards played **/
    void reduce_nb_remaining_cards();

  private:
    std::vector<std::list<Core::Card>> mayHave;
    int nbCards;
};

/** \brief Class that contains informations on the whole game, and generate new
 * hands according to it **/
class KnowledgeEngine {
  public:
    KnowledgeEngine(int, Core::Trump, const Core::Hand &);

    /** \brief Update knowledges with a GameState Event **/
    void update(const std::shared_ptr<Core::Event>);

    /** \brief Return true ONLY if we are sure that this card will win the
     * current trick **/
    bool must_win_trick(const Core::Card &) const;

    bool must_lose_trick(const Core::Card &) const;

    /** \brief Test if this card is the strongest that remain in the game in its
     * own color **/
    bool is_dominant(const Core::Card &) const;

    /** \brief Return true only if we are sure that nobody has a trump **/
    bool no_trump_anymore() const;

    /** \brief Return true if the player may have a trump **/
    bool may_have_trump(int idPlayer) const;

    /** \brief Return true if the player may have this color **/
    bool may_have(const Core::Suit &, int idPlayer) const;

    /** \brief Return true if the player may have this card **/
    bool may_have(const Core::Card &, int idPlayer) const;

    /** \brief Return all card yet played **/
    std::vector<Core::Card> yet_played_cards() const;

    /** \brief Get a random deal that takes into account all the knowledges **/
    Core::AllHands get_valid_deal() const;

    /** \brief Get a random deal that preserves your hand **/
    Core::AllHands get_stupid_deal() const;

    class RandomBacktrack {
      public:
        RandomBacktrack(const std::vector<Belief> &, int);
        Core::AllHands generate_hands();

      private:
        static std::default_random_engine gen;
        bool backtrack(int);

        std::vector<std::set<Core::Card, Core::CardComparatorForODS>> available;
        std::vector<Core::Card> cards;
        std::vector<std::vector<int>> domain;
        std::vector<int> affectedto;
        std::vector<int> sizeHand;
        int myId;
        const int dog = 4;
    };

  private:
    void remove_from_hands(const Core::Card &);
    void test_color_accuracy(int, const Core::Card &);
    void test_trump_climbing(int, const Core::Card &);

    int nbCards;
    std::vector<Core::Card> m_yetPlayedCards;
    std::vector<Belief> beliefs;
    Core::CardStack stack;
    Core::Trump trump;
    int myId;
};
