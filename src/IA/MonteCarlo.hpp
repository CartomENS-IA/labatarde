/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef MONTE_CARLO
#define MONTE_CARLO

#include <memory>

#include "Core.hpp"
#include "KnowledgeEngine.hpp"
#include "RandomPlayer.hpp"

/** \file
 *   \author Léo.V
 *   \brief Definition of class IA::MonteCarlo
 **/

namespace IA {
namespace MonteCarlo {
/**
\brief explore trees to choose a card to play
**/
Core::Card chooseCard(const Core::GameState &gs, const KnowledgeEngine &engine,
                      unsigned int);

/**
\brief explore trees to generate the best bets
**/
std::array<std::vector<double>, 6> generateBet(const Core::GameState &gs,
                                               const KnowledgeEngine &engine,
                                               unsigned int,
                                               std::array<double, 10> &);

double evaluate(const std::array<std::array<int, 4>, 6> &scores,
                const Core::GameState &gs, std::array<double, 10> &);
} // namespace MonteCarlo
} // namespace IA

#endif
