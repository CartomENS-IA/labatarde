/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "BotDeter.hpp"

#include <algorithm>
#include <array>
#include <iterator>
#include <random>
#include <string>
#include <vector>

using namespace std;
using namespace Core;

namespace IA {

void BotDeter::debug(const string s) {
    if (false)
        cout << s << endl;
}

void BotDeter::load_parameters() {
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;

    const auto playables =
        gs.playable_cards(); // get the array of cards we can play

    CardStack stack = gs.card_stack();
    int my_id = gs.my_id();
    const auto objectives = gs.nb_tricks_objective();
    int m_obj = objectives[my_id];
    const auto current = gs.nb_tricks_current();
    int m_tricks = current[my_id];

    const auto m_hand = gs.my_hand(); /* TODO : we only need the size of our
                                         hand, not all the cards... */
    int nbr_tricks_left = (int)m_hand.size();

    /* Definition of m_strong : set of our hand cards that are strong */
    const auto m_strong /* = strong_filter(); */ =
        m_hand; /* TODO : define wich cards of your hand are strong */
    int nbr_strong = (int)m_strong.size();

    winning_cards.clear();
    copy_if(playables.begin(), playables.end(), back_inserter(winning_cards),
            [&](Core::Card card) { return ke.must_win_trick(card); });

    my_trumps.clear();
    copy_if(playables.begin(), playables.end(), back_inserter(my_trumps),
            [&](Core::Card card) { return gs.is_trump(card); });

    /* The main booleans used to distinguish cases */

    aggressive = overall_aggressive_game();
    deeply_defensive = deeply_defensive_game();
    overpowered_hand = (nbr_strong > nbr_tricks_left);
    can_win = !(winning_cards.empty());

    first_player = stack.empty();
    middle_player = ((stack.size() == 1) || (stack.size() == 2));

    aiming_min = (m_tricks >= m_obj);
    aiming_max = ((m_obj - m_tricks) >= nbr_tricks_left);

    doit_defausser = (!stack.empty() && playables[0].suit() != stack.suit() &&
                      !gs.stronger_than(playables[0], stack.stack()[0]));

    doit_couper = (!(stack.empty()) && playables[0].suit() != stack.suit() &&
                   gs.stronger_than(playables[0], stack.stack()[0]));

    doit_fournir_atout = (!(stack.empty()) && gs.is_trump(playables[0]) &&
                          playables[0].suit() == stack.suit());

    doit_monter_atout =
        (!(stack.empty()) && gs.is_trump(stack.strongest_card()) &&
         gs.stronger_than(playables[0], stack.strongest_card()));

    est_coupe = (stack.size() >= 2 && gs.is_trump(stack.strongest_card()) &&
                 !(gs.is_trump(stack.stack()[0])));

    /* on a de l'atout */
    /* si c'est possible que notre atout le plus faible se fasse surcoupé */

    /* Probas */
    q = 0.5;
}

bool BotDeter::overall_aggressive_game() {
    const auto &gs = *m_gameState;
    int to_take = 0;
    for (int player = 0; player < 4; ++player) {
        to_take += std::max(gs.nb_tricks_objective()[player] -
                                gs.nb_tricks_current()[player],
                            0);
    };
    return (to_take > (int)gs.my_hand().size());
}

bool BotDeter::deeply_defensive_game() {
    const auto &gs = *m_gameState;
    for (int player = 0; player < 4; ++player) {
        if (player != gs.my_id() && (gs.nb_tricks_objective()[player] >
                                     gs.nb_tricks_current()[player])) {
            return false;
        }
    }
    return true;
}

bool BotDeter::may_be_in_game(const Core::Card card) {
    const auto &ke = *m_engine;
    for (int player = 0; player < 4; ++player) {
        if (ke.may_have(card, player)) {
            return true;
        }
    }
    return false;
}

Core::Card BotDeter::pick_strongest(
    const Core::Hand set) { // return the card of highest rank
    // cout << "Strongest " << endl;
    /*for (int i = 0 ; i<set.size() ; i++) {
        cout << set[i] << endl;
    };*/
    // cout << set.size() << " Done " << endl;
    return *max_element(begin(set), end(set),
                        [](const Core::Card &left, const Core::Card &right) {
                            return left.value() < right.value();
                        });
}

Core::Card BotDeter::pick_strongest_weight(const Core::Hand set) {
    if (!first_player &&
        !doit_defausser) { // we are forced to play a specific suit
        /* return the card of highest rank */
        return pick_strongest(set);
    } else {
        /* TODO : define a mesure of preference on the cards */
        return *max_element(
            begin(set), end(set),
            [&](const Core::Card &left, const Core::Card &right) {
                return winning_probability(left) < winning_probability(right);
            });
    }
}

Core::Card
BotDeter::pick_weekest(const Core::Hand set) { // return the card of lowest rank
    debug("Weekest along :");
    /*for (int i = 0 ; i<set.size(); i++) {
        cout << set[i] << endl;
    };
    cout << "Meaning " << set.size() << " cards" << endl;*/
    return *min_element(begin(set), end(set),
                        [](const Core::Card &left, const Core::Card &right) {
                            return left.value() < right.value();
                        });
}

Core::Card BotDeter::pick_weekest_weight(const Core::Hand set) {
    if (!first_player &&
        !doit_defausser) { // we are forced to play a specific suit
        /* return the card of lowest rank */
        return pick_weekest(set);
    } else {
        /* TODO : define a mesure of preference on the cards */
        return *min_element(
            begin(set), end(set),
            [&](const Core::Card &left, const Core::Card &right) {
                return winning_probability(left) < winning_probability(right);
            });
    }
}

double BotDeter::winning_probability(const Core::Card m_card) {
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;
    CardStack stack = gs.card_stack();

    // Two cases don't need complex computations
    if (!stack.empty() && !stack.stronger_than(m_card, stack.strongest_card()))
        return 0.;
    if (ke.must_win_trick(m_card))
        return 1.;

    if (doit_fournir_atout) {
        double proba = 1;
        /* We iterate on the trumps greater than m_card */
        Core::Hand all_cards = generate_full_game();
        for (int i = 0; i < 36; i++) {

            Core::Card card = all_cards[i];
            if (gs.stronger_than(card, m_card) && may_be_in_game(card)) {

                int ic =
                    36 -
                    4 * gs.nb_cards_round(); // we initialize with the number
                                             // of cards not distributed
                int sc = 0;

                bool already_played = stack.id_first_player() > gs.my_id();

                for (int player = 0; player < 4; ++player) {
                    if (player == stack.id_first_player())
                        already_played = true;
                    if (player == gs.my_id()) {
                        already_played = false;
                    } else {
                        if (already_played && ke.may_have(card, player)) {
                            ic += (int)gs.my_hand().size() -
                                  1; // size of player's hand
                        };
                        if (!already_played && ke.may_have(card, player)) {
                            sc += (int)gs.my_hand()
                                      .size(); // size of player's hand
                        };
                    }
                };
                proba *= (double)ic / (double)(ic + sc);
            }
        }
        return proba;
    } else if (gs.trump() == Trump::NoTrump ||
               (!stack.is_trump(m_card) && ke.no_trump_anymore())) {
        double proba = 1;
        /* We iterate on the cards stronger than m_card */
        Core::Hand all_cards = generate_full_game();
        for (int i = 0; i < 36; i++) {

            Core::Card card = all_cards[i];
            if (gs.stronger_than(card, m_card) && may_be_in_game(card)) {

                int cinf =
                    36 -
                    4 * gs.nb_cards_round(); // we initialize with the number of
                                             // cards not distributed
                int csup = 0;

                bool already_played = stack.id_first_player() > gs.my_id();

                for (int player = 0; player < 4; ++player) {
                    if (player == stack.id_first_player())
                        already_played = true;
                    if (player == gs.my_id()) {
                        already_played = false;
                    } else {
                        if (already_played && ke.may_have(card, player)) {
                            cinf += (int)gs.my_hand().size() -
                                    1; // size of player's hand
                        };
                        if (!already_played && ke.may_have(card, player) &&
                            (gs.nb_tricks_objective()[player] >
                             gs.nb_tricks_current()[player])) {
                            csup += (int)gs.my_hand()
                                        .size(); // size of player's hand
                        };
                    }
                };
                proba *= (double)cinf / (double)(cinf + csup);
            }
        }
        return proba;
    } else {
        double proba = 0.5;
        /* TODO : work needed */
        return proba;
    }
}

Core::Hand BotDeter::m_good_winning_proba(const Core::Hand set,
                                          const double m_q) {
    Core::Hand good_cards;
    copy_if(set.begin(), set.end(), back_inserter(good_cards),
            [&](Core::Card card) { return (winning_probability(card) > m_q); });
    // cout << "Found " << good_cards.size() << endl;
    return good_cards;
}

Core::Hand BotDeter::m_good_losing_proba(const Core::Hand set,
                                         const double m_q) {
    Core::Hand good_cards;
    copy_if(set.begin(), set.end(), back_inserter(good_cards),
            [&](Core::Card card) { return (winning_probability(card) < m_q); });
    // cout << "Found " << good_cards.size() << endl;
    return good_cards;
}

BotDeter::BotDeter() : m_engine(nullptr) {}

Core::Card BotDeter::play() {
    processEvent(); // please dot not touch this line

    /*
     * Probabilistic queries may be first coded with an arbitrarial constant.
     * You can add other methods if you need... in this case don't forget to
     * edit inc/IA/BotDeter.hpp
     */
    const auto &gs = *m_gameState;
    load_parameters();

    /*cout << "(*) Trick :" << endl;
    for (int i = 0 ; i<gs.card_stack().size() ; i++) {
        cout << "    " << gs.card_stack().stack()[i] << endl;
    };*/

    if (first_player) {
        cout << "~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~" << endl;
    };

    cout << "(*) Player " << gs.my_id() << " :" << endl;
    /*for (int i = 0 ; i<gs.my_hand().size() ; i++) {
        Core::Card card = gs.my_hand()[i];
        if (ke.must_win_trick(card)) {
            cout << "    " << card <<
    "---------------------------------------------------------------------------->
    winner" << endl;
        };
    };*/

    /* If only one card is playable, we don't have to think of any strategy */
    const auto playables = gs.playable_cards();
    // cout << "Playables : " << playables.size() << " Winning_cards : " <<
    // winning_cards.size() << " Trumps : " << my_trumps.size() << endl;

    if (playables.size() == 1) {
        cout << "==> " << playables[0] << ", done !\n" << endl;
        return playables[0];
    };

    Core::Card c;
    switch (gs.trump()) {
    case Trump::NoTrump:
        debug("-> NoTrump");
        c = play_NoTrump();
        break;
    case Trump::AllTrump:
        debug("-> AllTrump");
        c = play_AllTrump();
        break;
    default /* OneTrump */:
        debug("-> OneTrump");
        c = play_OneTrump();
        break;
    }

    cout << "==> " << c << ", done !\n" << endl;

    return c;
}

/***** Part one : Algorithm when trump is NoTrump *****/

Core::Card BotDeter::play_NoTrump() {
    if (aiming_min) {
        debug("--> aiming_min");
        return play_NoTrump_aiming_min();
    } else if (aiming_max) {
        debug("--> aiming_max");
        return play_NoTrump_aiming_max();
    } else /* AI needs to win some tricks and to lose other ones */ {
        debug("--> in_between");
        return play_NoTrump_in_between();
    }
}

Core::Card BotDeter::play_NoTrump_aiming_min() {
    const auto &gs = *m_gameState;
    const auto playables = gs.playable_cards();
    if (first_player) {
        debug("---> first");
        /* si possible, jouer sa carte de meilleure valeur (ponderee) qui a une
         * bonne proba de ne pas prendre */
        Core::Hand good_cards = m_good_losing_proba(playables, q);
        if (good_cards.empty()) {
            debug("----> pas de faible carte");
            /* jouer la carte la plus faible */
            return pick_weekest_weight(playables);
        } else {
            debug("----> faible carte");
            return pick_strongest(good_cards);
        }
    } else if (middle_player) {
        debug("---> middle");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte de meilleure valeur (ponderee) */
            return pick_strongest_weight(playables);
        } else /* doit fournir */ {
            debug("----> fournir");
            /* si possible, jouer la carte la plus haute qui a une bonne proba
             * de ne pas prendre */
            Core::Hand good_cards = m_good_losing_proba(playables, q);
            if (good_cards.empty()) {
                debug("-----> pas de faible carte");
                /* sinon, jouer la carte la plus haute */
                return pick_strongest(playables);
            } else {
                debug("-----> faible carte");
                return pick_strongest(good_cards);
            }
        }
    } else /* last position */ {
        debug("---> last");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte de meilleure valeur (ponderee) */
            return pick_strongest_weight(playables);
        } else /* doit fournir */ {
            debug("----> fournir");
            /* si possible, jouer la carte la plus haute qui a une bonne proba
             * de ne pas prendre */
            Core::Hand good_cards = m_good_losing_proba(playables, q);
            if (good_cards.empty()) {
                debug("-----> pas de faile carte");
                /* sinon, jouer la carte la plus haute */
                return pick_strongest(playables);
            } else {
                debug("-----> faible carte");
                return pick_strongest(good_cards);
            }
        }
    }
}

Core::Card BotDeter::play_NoTrump_aiming_max() {
    const auto &gs = *m_gameState;
    const auto playables = gs.playable_cards();
    if (first_player) {
        debug("---> first");
        /* jouer la carte qui a la meilleure probabilite de prendre */
        return *max_element(
            begin(playables), end(playables),
            [&](const Core::Card &left, const Core::Card &right) {
                return winning_probability(left) < winning_probability(right);
            });
    } else if (middle_player) {
        debug("---> middle");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte de plus faible valeur ponderee */
            return pick_weekest_weight(playables);
        } else /* doit fournir */ {
            debug("----> fournir");
            /* si possible, jouer la carte la plus haute qui a une bonne proba
             * de prendre */
            Core::Hand good_cards = m_good_winning_proba(playables, q);
            if (good_cards.empty()) {
                debug("-----> pas de forte carte");
                /* sinon, jouer la carte la plus faible */
                return pick_weekest(playables);
            } else {
                debug("-----> forte carte");
                return pick_strongest(good_cards);
            }
        }
    } else /* last position */ {
        debug("---> last");
        if (can_win) {
            debug("----> can_win");
            /* parmi les cartes de winning_cards, jouer la carte la plus faible
             */
            return pick_weekest(winning_cards);
        } else {
            debug("----> not can_win");
            /* jouer la carte la plus faible */
            return pick_weekest_weight(playables);
        }
    }
}

Core::Card BotDeter::play_NoTrump_in_between() {
    const auto &gs = *m_gameState;
    const auto playables = gs.playable_cards();
    if (aggressive) {
        debug("---> aggressive");
        if (first_player) {
            debug("----> first");
            if (overpowered_hand) {
                debug("-----> overp");
                /* jouer la carte la plus forte */
                return pick_strongest_weight(playables);
            } else {
                debug("-----> not overp");
                if (can_win) {
                    debug("------> can_win");
                    return pick_weekest_weight(winning_cards);
                } else {
                    debug("------> not can_win");
                    /* si on a des cartes qui ont une bonne proba d'etre maitre,
                     * jouer la plus haute parmi celles-ci */
                    Core::Hand good_cards = m_good_winning_proba(playables, q);
                    if (good_cards.empty()) {
                        debug("-------> pas de forte carte");
                        /* sinon, jouer la carte de plus faible valeur
                         * (ponderee) */
                        return pick_weekest_weight(playables);
                    } else {
                        debug("-------> forte carte");
                        return pick_strongest_weight(good_cards);
                    }
                }
            }
        } else if (middle_player) {
            debug("----> middle");
            /* si possible, jouer la carte la plus haute qui a une bonne proba
             * de prendre */
            Core::Hand good_cards = m_good_winning_proba(playables, q);
            if (good_cards.empty()) {
                debug("> pas de forte carte");
                /* sinon, jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            } else {
                debug("> forte carte");
                return pick_strongest(good_cards);
            }
        } else /* last position */ {
            debug("----> last");
            if (can_win) {
                debug("-----> can_win");
                if (overpowered_hand) {
                    debug("------> overp");
                    /* gagner le pli avec la carte la plus forte */
                    return pick_strongest(winning_cards);
                } else {
                    debug("------> not overp");
                    /* parmi les cartes de winning_cards, jouer la carte la plus
                     * faible */
                    return pick_weekest(winning_cards);
                }
            } else {
                debug("-----> can_win");
                /* jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            }
        }
    } else /* unwanted tricks incoming */ {
        debug("---> not aggressive");
        if (overpowered_hand) {
            debug("----> overp");
            /* jouer comme si tous les plis annonces etaient faits */
            return play_NoTrump_aiming_min();
        } else {
            debug("----> not overp");
            if (first_player) {
                debug("-----> first");
                /* jouer la carte la plus forte */
                return pick_strongest_weight(playables);
            } else if (middle_player) {
                debug("-----> middle");
                /* si possible, jouer la carte la plus faible ayant une bonne
                 * proba de gagner le pli */
                Core::Hand good_cards = m_good_winning_proba(playables, q);
                if (good_cards.empty()) {
                    debug("------> pas de forte carte");
                    /* sinon, jouer la carte la plus faible */
                    return pick_weekest_weight(playables);
                } else {
                    debug("------> forte carte");
                    return pick_weekest(good_cards);
                }
            } else /* last position */ {
                debug("-----> last");
                if (can_win) {
                    debug("------> can_win");
                    /* parmi les cartes de winning_cards, jouer la carte la plus
                     * faible */
                    return pick_weekest(winning_cards);
                } else {
                    debug("------> not can_win");
                    /* jouer la carte la plus faible */
                    return pick_weekest_weight(playables);
                }
            }
        }
    }
}

/***** Part two : Algorithm when trump is AllTrump *****/

Core::Card BotDeter::play_AllTrump() {
    if (aiming_min) {
        debug("--> aiming_min");
        return play_AllTrump_aiming_min();
    } else if (aiming_max) {
        debug("--> aiming_max");
        return play_AllTrump_aiming_max();
    } else /* AI needs to win some tricks and to lose other ones */ {
        debug("--> in_between");
        return play_AllTrump_in_between();
    }
}

Core::Card BotDeter::play_AllTrump_aiming_min() {
    const auto &gs = *m_gameState;
    const auto playables = gs.playable_cards();
    if (first_player) {
        debug("---> first");
        /* si possible, jouer sa carte de meilleure valeur ponderee qui a une
         * bonne proba de ne pas prendre */
        Core::Hand good_cards = m_good_losing_proba(playables, q);
        if (good_cards.empty()) {
            debug("----> pas de faible carte");
            /* jouer la carte la plus faible */
            return pick_weekest_weight(playables);
        } else {
            debug("----> faible carte");
            return pick_strongest(good_cards);
        }
    } else if (middle_player) {
        debug("---> middle");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte de meilleure valeur (ponderee) */
            return pick_strongest_weight(playables);
        } else /* doit fournir */ {
            debug("----> fournir");
            /* si possible, jouer la carte la plus haute qui a une bonne proba
             * de ne pas prendre */
            Core::Hand good_cards = m_good_losing_proba(playables, q);
            if (good_cards.empty()) {
                debug("-----> pas de faible carte");
                /* jouer la carte la plus faible */
                return pick_weekest(playables);
            } else {
                debug("-----> faible carte");
                return pick_strongest(good_cards);
            }
        }
    } else /* last position */ {
        debug("---> last");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte de meilleure valeur (ponderee) */
            return pick_strongest_weight(playables);
        } else /* doit fournir */ {
            debug("----> fournir");
            /* jouer la carte la plus haute */
            return pick_strongest(playables);
        }
    }
}

Core::Card BotDeter::play_AllTrump_aiming_max() {
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;
    const auto playables = gs.playable_cards();
    if (first_player) {
        debug("---> first");
        if (ke.must_win_trick(pick_strongest_weight(
                playables))) { // si notre atout le plus fort est maitre
            debug("----> plus fort atout maitre");
            /* jouer l'atout le plus fort */
            return pick_strongest_weight(playables);
        } else {
            debug("----> plus fort atout pas maitre");
            if (winning_probability(pick_strongest_weight(playables)) >
                q) { // si notre atout le plus fort a une bonne proba d'etre
                     // maitre
                debug("-----> plus fort atout fort");
                /* jouer l'atout le plus fort */
                return pick_strongest_weight(playables);
            } else {
                debug("-----> plus fort atout pas fort");
                /* sinon, jouer sa plus petite carte */
                return pick_weekest_weight(playables);
            }
        }
    } else if (middle_player) {
        debug("---> middle");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte de plus faible valeur (ponderee) */
            return pick_weekest_weight(playables);
        } else /* doit fournir */ {
            debug("----> fournir");
            /* si possible, jouer la carte qui a une tres bonne proba de prendre
             */
            Core::Hand good_cards = m_good_winning_proba(playables, q);
            if (good_cards.empty()) {
                debug("-----> pas de forte carte");
                /* sinon, jouer la carte la plus faible */
                return pick_weekest(playables);
            } else {
                debug("-----> forte carte");
                return pick_strongest(good_cards);
            }
        }
    } else /* last position */ {
        debug("---> last");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte de plus faible valeur (ponderee) */
            return pick_weekest_weight(playables);
        } else /* doit fournir */ {
            debug("----> fournir");
            if (can_win) {
                debug("-----> can_win");
                /* jouer la plus faible carte qui monte */
                return pick_weekest(winning_cards);
            } else {
                debug("-----> not can_win");
                /* jouer la carte la plus faible */
                return pick_weekest(playables);
            }
        }
    }
}

Core::Card BotDeter::play_AllTrump_in_between() {
    const auto &gs = *m_gameState;
    const auto playables = gs.playable_cards();
    if (aggressive) {
        debug("---> aggressive");
        if (first_player) {
            debug("----> first");
            if (overpowered_hand) {
                debug("-----> overp");
                /* jouer la carte la plus haute */
                return pick_strongest_weight(playables);
            } else {
                debug("-----> not overp");
                if (can_win) {
                    debug("------> can_win");
                    /* jouer la carte maitre de valeur ponderee la plus faible
                     */
                    return pick_weekest_weight(winning_cards);
                } else {
                    debug("------> not can_win");
                    /* si on a des cartes qui ont une bonne proba d'etre maitre,
                     * jouer la plus haute */
                    Core::Hand good_cards = m_good_winning_proba(playables, q);
                    if (good_cards.empty()) {
                        debug("-------> pas de forte carte");
                        /* sinon, jouer la carte de plus faible valeur
                         * (ponderee) */
                        return pick_weekest_weight(playables);
                    } else {
                        debug("-------> forte carte");
                        return pick_strongest_weight(good_cards);
                    }
                }
            }
        } else if (middle_player) {
            debug("----> middle");
            if (doit_monter_atout) {
                debug("-----> monter");
                /* jouer la carte la plus haute */
                return pick_strongest(playables);
            } else {
                debug("-----> pas monter");
                /* jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            }
        } else /* last position */ {
            debug("----> last");
            if (doit_monter_atout) {
                debug("-----> monter");
                if (overpowered_hand) {
                    debug("------> overp");
                    /* gagner avec la carte la plus forte */
                    return pick_strongest(winning_cards);
                } else {
                    debug("------> not overp");
                    /* gagner avec la carte la plus faible possible */
                    return pick_weekest(winning_cards);
                }
            } else {
                debug("-----> pas monter");
                /* jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            }
        }
    } else /* unwanted tricks incoming */ {
        debug("---> not aggressive");
        if (overpowered_hand) {
            debug("----> overp");
            /* jouer comme si tous les plis annonces etaient faits */
            return play_AllTrump_aiming_min();
        } else {
            debug("----> not overp");
            if (first_player) {
                debug("-----> first");
                /* jouer la carte la plus haute */
                return pick_strongest_weight(playables);
            } else if (middle_player) {
                debug("-----> middle");
                if (doit_monter_atout) {
                    debug("------> monter");
                    /* si on a une bonne proba de gagner le pli, jouer la carte
                     * la plus faible le permettant */
                    Core::Hand good_cards = m_good_winning_proba(playables, q);
                    if (good_cards.empty()) {
                        debug("-------> pas de forte carte");
                        /* sinon, jouer la carte la plus faible */
                        return pick_weekest(playables);
                    } else {
                        debug("-------> forte carte");
                        return pick_weekest(good_cards);
                    }
                } else {
                    debug("------> pas monter");
                    /* jouer la carte la plus faible */
                    return pick_weekest_weight(playables);
                }
            } else /* last position */ {
                debug("-----> last");
                /* jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            }
        }
    }
}

/***** Part three : Algorithm when trump is one of the four suits *****/

Core::Card BotDeter::play_OneTrump() {
    if (aiming_min) {
        debug("--> aiming_min");
        return play_OneTrump_aiming_min();
    } else if (aiming_max) {
        debug("--> aiming_max");
        return play_OneTrump_aiming_max();
    } else /* AI needs to win some tricks and to lose other ones */ {
        debug("--> in_between");
        return play_OneTrump_in_between();
    }
}

Core::Card BotDeter::play_OneTrump_aiming_min() {
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;
    const auto playables = gs.playable_cards();
    if (first_player) {
        debug("---> first");
        if (!(my_trumps.empty())) { // on a de l'atout
            debug("----> on a de l'atout");
            if (!(ke.must_win_trick(
                    pick_weekest(my_trumps)))) { // notre atout le plus faible
                                                 // n'est pas maitre
                debug("-----> plus faible atout pas maitre");
                /* jouer l'atout le plus faible */
                return pick_weekest(my_trumps);
            } else {
                debug("-----> plus faible atout maitre");
                /* si possible, jouer la plus haute carte qui a une tres bonne
                 * proba d'etre prise */
                Core::Hand good_cards = m_good_losing_proba(playables, q);
                if (good_cards.empty()) {
                    debug("------> pas de faible carte");
                    /* jouer la carte la plus faible */
                    return pick_weekest_weight(playables);
                } else {
                    debug("------> faible carte");
                    return pick_strongest_weight(good_cards);
                }
            }
        } else /* on n'a pas d'atout */ {
            debug("----> on a pas d'atout");
            /* si possible, jouer la plus haute carte qui a une tres bonne proba
             * d'etre prise */
            Core::Hand good_cards = m_good_losing_proba(playables, q);
            if (good_cards.empty()) {
                debug("-----> pas de faible carte");
                /* jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            } else {
                debug("-----> faible carte");
                return pick_strongest_weight(good_cards);
            }
        }
    } else /* any other position */ {
        debug("---> not first");
        if (doit_defausser) {
            debug("----> doit_defausser");
            /* jouer sa carte de valeur (ponderee) la plus forte */
            return pick_strongest_weight(playables);
        } else if (doit_fournir_atout || doit_couper) {
            debug("----> doit_jouer_atout");
            if (doit_monter_atout) {
                debug("-----> doit_monter");
                if (!(ke.must_win_trick(pick_weekest(
                        my_trumps)))) { // si c'est possible que notre atout le
                                        // plus faible se fasse surmonter
                    debug("------> plus faible atout pas maitre");
                    /* jouer l'atout le plus faible */
                    return pick_weekest(playables);
                } else {
                    debug("------> plus faible atout maitre");
                    /* jouer l'atout le plus fort */
                    return pick_strongest(playables);
                }
            } else /* on ne peut pas monter a l'atout */ {
                debug("-----> ne monte pas");
                /* jouer l'atout le plus fort */
                return pick_strongest(playables);
            }
        } else /* doit fournir */ {
            debug("----> doit_fournir");
            if (est_coupe) {
                debug("-----> est_coupé");
                /* jouer la carte la plus haute */
                return pick_strongest(playables);
            } else {
                debug("-----> pas coupé");
                /* si possible, jouer la carte la plus haute telle qu'il y a une
                 * bonne proba qu'on perde le pli */
                Core::Hand good_cards = m_good_losing_proba(playables, q);
                if (good_cards.empty()) {
                    debug("------> pas de faible carte");
                    /* sinon, jouer la carte la plus haute */
                    return pick_strongest(playables);
                } else {
                    debug("------> faible carte");
                    return pick_strongest(good_cards);
                }
            }
        }
    }
}

Core::Card BotDeter::play_OneTrump_aiming_max() {
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;
    const auto playables = gs.playable_cards();
    if (first_player) {
        debug("---> first");
        if (!ke.no_trump_anymore()) { // il peut rester des atouts chez les
                                      // autres
            debug("----> atouts chez les autres");
            if (!my_trumps.empty() &&
                ke.must_win_trick(
                    pick_strongest(my_trumps))) { // si on a un atout maitre
                debug("-----> atout maitre");
                /* jouer un atout maitre */
                return pick_strongest(my_trumps);
            } else {
                debug("-----> pas d'atout maitre");
                /* jouer la carte la plus faible (ponderee) */
                return pick_weekest_weight(playables);
            }
        } else {
            debug("----> pas d'atout chez les autres");
            if (deeply_defensive) {
                debug("-----> defensive");
                if (!my_trumps.empty()) { // si il nous reste des atouts
                    debug("------> on a de l'atout");
                    // en jouer un
                    return pick_strongest(my_trumps);
                } else {
                    debug("------> on a pas d'atout");
                    if (can_win) {
                        debug("-------> can_win");
                        // jouer la carte de plus faible valeur (ponderee) dont
                        // on sait qu'elle peut remporter le pli
                        return pick_weekest_weight(winning_cards);
                    } else {
                        debug("-------> not can_win");
                        // jouer sa carte la plus faible
                        return pick_weekest_weight(playables);
                    }
                }
            } else {
                debug("-----> not defensive");
                if (can_win) {
                    debug("------> can_win");
                    /* jouer la carte de valeur (ponderee) la plus faible dont
                     * on sait qu'elle est maitre */
                    return pick_weekest_weight(winning_cards);
                } else {
                    debug("------> not can_win");
                    /* jouer sa carte la plus faible */
                    return pick_weekest_weight(playables);
                }
            }
        }
    } else if (middle_player) {
        debug("---> middle");
        if (doit_defausser) {
            debug("----> defausse");
            /* jouer la carte la plus faible */
            return pick_weekest_weight(playables);
        } else if (doit_fournir_atout) {
            debug("----> fournir atout");
            if (doit_monter_atout) {
                debug("-----> monter atout");
                /* si possible, jouer l'atout le plus faible qui a une bonne
                 * proba de ne pas se faire depasser */
                Core::Hand good_cards = m_good_winning_proba(playables, q);
                if (good_cards.empty()) {
                    debug("------> pas de forte carte");
                    /* jouer la carte la plus faible */
                    return pick_weekest(playables);
                } else {
                    debug("------> forte carte");
                    return pick_weekest(good_cards);
                }
            } else /* on ne peut pas monter a l'atout */ {
                debug("-----> pas monter atout");
                /* jouer son atout le plus faible */
                return pick_weekest(playables);
            }
        } else if (doit_couper) {
            debug("----> doit_couper");
            if (doit_monter_atout) {
                debug("-----> monter atout");
                /* si possible, jouer l'atout le plus faible qui a une bonne
                 * proba de ne pas se faire surcouper */
                Core::Hand good_cards = m_good_winning_proba(playables, q);
                if (good_cards.empty()) {
                    debug("------> pas de forte carte");
                    /* jouer la carte la plus faible */
                    return pick_weekest(playables);
                } else {
                    debug("------> forte carte");
                    return pick_weekest(good_cards);
                }
            } else /* on ne peut pas monter a l'atout */ {
                debug("-----> pas monter atout");
                /* jouer l'atout le plus faible */
                return pick_weekest(playables);
            }
        } else /* doit fournir */ {
            debug("----> doit_fournir");
            /* si possible, jouer la carte la plus faible qui a une forte proba
             * de gagner le pli */
            Core::Hand good_cards = m_good_winning_proba(playables, q);
            if (good_cards.empty()) {
                debug("-----> pas de forte carte");
                /* sinon, jouer la carte la plus faible */
                return pick_weekest(playables);
            } else {
                debug("-----> forte carte");
                return pick_weekest(good_cards);
            }
        }
    } else /* last position */ {
        debug("---> last");
        if (can_win) {
            debug("----> can_win");
            /* parmi les cartes de winning_cards, jouer la carte la plus faible
             * (ponderee) */
            return pick_weekest(winning_cards);
        } else {
            debug("----> not can_win");
            /* jouer la carte la plus faible (ponderee) */
            return pick_weekest_weight(playables);
        }
    }
}

Core::Card BotDeter::play_OneTrump_in_between() {
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;
    const auto playables = gs.playable_cards();
    if (aggressive) {
        debug("---> aggressive");
        if (first_player) {
            debug("----> first");
            if (overpowered_hand) {
                debug("-----> overp");
                /* jouer la carte la plus forte */
                return pick_strongest_weight(playables);
            } else {
                debug("-----> not overp");
                if (ke.no_trump_anymore()) {
                    debug("------> pas d'atout chez les autres");
                    if (can_win) {
                        debug("-------> can_win");
                        /* jouer la carte de valeur (ponderee) la plus faible
                         * dont on sait qu'elle est maitre */
                        return pick_weekest_weight(winning_cards);
                    } else {
                        debug("-------> not can_win");
                        /* jouer la carte la plus faible */
                        return pick_weekest_weight(playables);
                    }
                } else {
                    debug("------> atout chez les autres");
                    if (!my_trumps.empty() &&
                        ke.must_win_trick(pick_strongest(
                            my_trumps))) { // si on a l'atout maitre
                        debug("-------> atout maitre");
                        /* jouer l'atout maitre */
                        return pick_strongest_weight(my_trumps);
                    } else {
                        debug("-------> pas atout maitre");
                        /* jouer la carte la plus faible (en valeur ponderee) */
                        return pick_weekest_weight(playables);
                    }
                }
            }
        } else if (middle_player) {
            debug("----> middle");
            if (doit_defausser) {
                debug("-----> defausse");
                /* jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            } else if (doit_couper) {
                debug("-----> doit_couper");
                if (overpowered_hand) {
                    debug("------> overp");
                    if (winning_probability(pick_weekest(playables)) <
                        q) { // si notre atout le plus faible a une faible
                             // probabilite de prendre
                        debug("-------> plus faible atout pas fort");
                        /* jouer notre atout le plus faible */
                        return pick_weekest(playables);
                    } else {
                        debug("-------> plus faible atout fort");
                        /* sinon, jouer l'atout le plus fort */
                        return pick_strongest(playables);
                    }
                } else {
                    debug("------> not overp");
                    /* si on a une bonne proba de prendre, jouer l'atout le plus
                     * faible qui le permet */
                    Core::Hand good_cards = m_good_winning_proba(playables, q);
                    if (good_cards.empty()) {
                        debug("-------> pas de forte carte");
                        /* sinon, jouer l'atout le plus faible possible */
                        return pick_weekest(playables);
                    } else {
                        debug("-------> forte carte");
                        return pick_weekest(good_cards);
                    }
                }
            } else if (doit_fournir_atout) {
                debug("-----> doit_fournir_atout");
                if (doit_monter_atout) {
                    debug("------> doit_monter");
                    /* jouer son atout le plus fort */
                    return pick_strongest(playables);
                } else /* on ne peut pas monter a l'atout */ {
                    debug("------> pas monter");
                    /* jeter la carte la plus faible */
                    return pick_weekest(playables);
                }
            } else /* doit fournir */ {
                debug("-----> doit_fournir");
                if (overpowered_hand) {
                    debug("------> overp");
                    Core::Hand
                        losing_cards; /* losing_cards : playable cards that are
                                         sure to lose the trick */
                    copy_if(playables.begin(), playables.end(),
                            back_inserter(losing_cards), [&](Core::Card card) {
                                return ke.must_lose_trick(card);
                            }); // TODO : must_lose_trick not possible
                    if (losing_cards.empty()) {
                        debug("-------> pas de carte nulle");
                        /* jouer la carte la plus haute */
                        return pick_strongest(playables);
                    } else {
                        debug("-------> carte nulle");
                        /* jouer la carte la plus haute qui est sure de perdre
                         */
                        return pick_strongest(losing_cards);
                    }
                } else {
                    debug("------> not overp");
                    /* si possible, jouer la carte la plus faible qui a une
                     * bonne proba de gagner le pli */
                    Core::Hand good_cards = m_good_winning_proba(playables, q);
                    if (good_cards.empty()) {
                        debug("-------> pas de forte carte");
                        /* sinon, jouer la carte la plus faible */
                        return pick_weekest(playables);
                    } else {
                        debug("-------> forte carte");
                        return pick_weekest(good_cards);
                    }
                }
            }
        } else /* last position */ {
            debug("----> last");
            if (can_win) {
                debug("-----> can_win");
                if (overpowered_hand) {
                    debug("------> overp");
                    /* jouer la carte la plus forte */
                    return pick_strongest(playables);
                } else {
                    debug("------> not overp");
                    /* jouer la plus faible carte qui gagne le pli */
                    return pick_weekest(winning_cards);
                }
            } else {
                debug("-----> not can_win");
                /* jouer la carte la plus faible */
                return pick_weekest_weight(playables);
            }
        }
    } else /* unwanted tricks incoming */ {
        debug("---> not aggressive");
        if (overpowered_hand) {
            debug("----> overp");
            /* jouer comme si tous les plis annonces etaient faits */
            return play_OneTrump_aiming_min();
        } else {
            debug("----> not overp");
            if (first_player) {
                debug("-----> first");
                /* jouer la carte la plus forte */
                return pick_strongest_weight(playables);
            } else if (middle_player) {
                debug("-----> middle");
                /* si possible, jouer la carte la plus faible ayant une bonne
                 * proba de gagner le pli */
                Core::Hand good_cards = m_good_winning_proba(playables, q);
                if (good_cards.empty()) {
                    debug("------> pas de forte carte");
                    /* sinon, jouer la carte la plus faible */
                    return pick_weekest_weight(playables);
                } else {
                    debug("------> forte carte");
                    return pick_weekest(good_cards);
                }
            } else /* last position */ {
                debug("-----> last");
                if (can_win) {
                    debug("------> can_win");
                    /* parmi les cartes de winning_cards, jouer la carte la plus
                     * faible */
                    return pick_weekest(winning_cards);
                } else {
                    debug("------> not can_win");
                    /* jouer la carte la plus faible */
                    return pick_weekest_weight(playables);
                }
            }
        }
    }
}

static default_random_engine gen(5555);
Core::Bet BotDeter::bet() {

    /* Pari de MonteCarlo*/

    processEvent();
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;
    std::array<double, 10> errorCoeff = {1., 1., 1., 1., 1.,
                                         1., 1., 1., 1., 1.};
    auto m_resultArray = MonteCarlo::generateBet(gs, ke, 500, errorCoeff);

    int max;
    int i_max;
    int j_max;

    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 9; j++) {
            if (m_resultArray[i][j] < 0) {
                m_resultArray[i][j] *= -1;
            }
        }
    }

    do {
        max = 0;
        i_max = 0;
        j_max = 0;
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 9; j++) {
                if (m_resultArray[i][j] > max) {
                    max = (int)m_resultArray[i][j];
                    i_max = i;
                    j_max = j;
                }
            }
        }
        m_resultArray[i_max][j_max] *= -1;
    } while (gs.bet_info(Bet(static_cast<Trump>(i_max), j_max)) !=
             BetInfo::ValidBet);
    return Bet(static_cast<Trump>(i_max), j_max);

    return Bet(
        gs.trump(),
        1); // if the bets sum to the number of cards, we take one more trick

    /*
        Trump trump = static_cast<Trump>(gen() %
       (static_cast<int>(Trump::AllTrump)+1)); int nb =
       gen()%(m_gameState->my_hand().size()+1); return Bet(trump, nb);
    */
}

bool BotDeter::mechoune() { return false; }

bool BotDeter::choune() { return false; }

void BotDeter::ready() {}

void BotDeter::set_game_state(const GameState &gameState) {
    auto &gs = gameState;
    Player::set_game_state(gs);
    m_engine =
        make_unique<KnowledgeEngine>(gs.my_id(), gs.trump(), gs.my_hand());
}

void BotDeter::processEvent() {
    auto &gs = *m_gameState;
    while (gs.state_changed()) {
        auto nextEvent = gs.get_next_event();
        if (auto temp = Core::handle<Core::DealCardsEvent>(nextEvent)) {
            m_engine = make_unique<KnowledgeEngine>(gs.my_id(), gs.trump(),
                                                    gs.my_hand());
        }
        m_engine->update(nextEvent);
    }
}
} // namespace IA
