/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <algorithm>
#include <chrono>
#include <cmath>
#include <iostream>
#include <iterator>
#include <random>
#include <thread>

#include "BotPlayer.hpp"
#include "MonteCarlo.hpp"

using namespace std;
using namespace Core;

namespace IA {
namespace MonteCarlo {
Core::Card chooseCard(const Core::GameState &gs, const KnowledgeEngine &engine,
                      unsigned int NUMBER_EXPLORE) {
    const Hand &hand = gs.playable_cards();

    if (hand.size() == 1) {
        return hand[0];
    }

    // do explores

    vector<unsigned int> victoryArray(hand.size(), 0);
    for (unsigned int cardId = 0; cardId < hand.size(); ++cardId) {
        for (unsigned int explore = 0; explore < NUMBER_EXPLORE; explore++) {

            array<shared_ptr<Core::Player>, 4> players;
            for (int player = 0; player < (int)players.size(); ++player) {
                players[player] = make_shared<IA::RandomPlayer>();
            }

            Card card = hand[cardId];
            Core::AllHands possibleHand = engine.get_valid_deal();
            Core::AllBets bet = gs.bets();
            Core::CardStack cardStack = gs.card_stack();
            cardStack.add_card(hand[cardId], gs.my_id());

            auto myCardPos = find(begin(possibleHand[gs.my_id()]),
                                  end(possibleHand[gs.my_id()]), card);
            if (myCardPos == end(possibleHand[gs.my_id()]))
                throw runtime_error("[MonteCarlo.cpp] Impossible to erase a "
                                    "card the player don't have");
            possibleHand[gs.my_id()].erase(myCardPos);
            auto ker = make_shared<Kernel>(
                players, possibleHand, bet, gs.id_best_bet(),
                gs.nb_tricks_current(), cardStack, (gs.my_id() + 1) % 4);

            victoryArray[cardId] += (ker->run())[gs.my_id()];
            // victoryArray[cardId] +=
            // Core::FastKernel::run_card_phase(possibleHand, bet,
            // gs.first_player_round(), gs.nb_tricks_current(), cardStack,
            // (gs.my_id()+1)%4) [gs.my_id()];
        }
    }

    auto posMinError = min_element(begin(victoryArray), end(victoryArray));

    return hand[posMinError - begin(victoryArray)];
}

array<vector<double>, 6> generateBet(const Core::GameState &gs,
                                     const KnowledgeEngine &engine,
                                     unsigned int NUMBER_EXPLORE,
                                     std::array<double, 10> &errorCoeff) {

    array<vector<double>, 6> victoryArray;
    victoryArray.fill(vector<double>(10, 0.));
    for (unsigned int j = 0; j < NUMBER_EXPLORE; j++) {

        // coefficient qui dit à quel point la main aléatoire dealée est
        // réaliste en fonction des annoncs des autres joueurs
        double coefCredibility = 1.;

        Core::AllHands possibleHand = engine.get_valid_deal();
        array<array<int, 4>, 6> nbTricks;

        for (int trumpID = 0; trumpID < 6; ++trumpID) {
            Core::Trump trump = static_cast<Core::Trump>(trumpID);

            array<shared_ptr<Core::Player>, Core::nbPlayers> players;
            for (auto &player : players) {
                player = make_shared<IA::RandomPlayer>();
            }

            Core::AllBets bet(4, Bet(trump, 0));
            auto ker = make_shared<Kernel>(
                players, possibleHand, bet, gs.id_best_bet(),
                std::array<int, 4>{{0, 0, 0, 0}}, CardStack(trump),
                gs.turn_number() % 4);

            auto scores = ker->run();
            // auto scores = Core::FastKernel::run_card_phase(possibleHand, bet,
            // gs.id_best_bet(),std::array<int, 4> {{0,0,0,0}},
            // CardStack(trump), gs.turn_number()%4);
            nbTricks[trumpID] = scores;
        }

        coefCredibility *= evaluate(nbTricks, gs, errorCoeff);

        for (int trump = 0; trump < 6; ++trump) {
            victoryArray[trump][nbTricks[trump][gs.my_id()]] += coefCredibility;
        }
    }
    return victoryArray;
}

double evaluate(const array<array<int, 4>, 6> &scores, const GameState &gs,
                std::array<double, 10> &errorCoeff) {
    // bets are the bets that have been made since the beginning of time bu the
    // pplyaers it's of the kind [[1,3,heart],[2,4,spades]] if player number1has
    // bet 3 of hearts and player number 2 has bet 4of spades after him

    double coefTotal = 1.;
    const auto &lastBetColor = gs.last_bet_color();
    for (int player = 0; player < Core::nbPlayers; ++player) {
        if (player == gs.my_id())
            continue;
        for (int color = 0; color < 6; color++) {
            int lastBet = lastBetColor[color][player];
            if (lastBet != -1) {
                int delta = abs(scores[color][player] - lastBet);
                coefTotal *= errorCoeff[delta];
            }
        }
    }
    return coefTotal;
}
} // namespace MonteCarlo
} // namespace IA
