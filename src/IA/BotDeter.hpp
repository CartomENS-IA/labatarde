/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <memory>

#include "Core.hpp"
#include "KnowledgeEngine.hpp"
#include "MonteCarlo.hpp"
#include <string>

/** \file
 *   \author Nicolas, Robin, Tristan
 *   \brief Definition of class IA::BotDeter
 *   Implement the pseudo code on https://pad.aliens-lyon.fr/p/heuristiques
 **/

namespace IA {

class BotDeter final : public Core::Player {
  public:
    BotDeter();

    Core::Bet bet();
    bool mechoune();
    bool choune();
    Core::Card play();

    virtual void ready();
    virtual void set_game_state(const Core::GameState &gameState);

  private:
    void debug(const string); // Temporary debug function

    void load_parameters();

    // Parameters of the BotDeter
    Core::Hand winning_cards; // playable cards that are sure to win the trick
    Core::Hand my_trumps;     // our playable trumps

    bool aggressive; // true if players are fighting for tricks, i.e. more
                     // tricks are annonced than the number of cards in a hand
    bool deeply_defensive; // true if no other players wants tricks
    bool overpowered_hand; // true if we have too much strong card
    bool can_win; // true if we have cards that are sure to win the trick

    bool first_player;  // true if we play first
    bool middle_player; // true if we play second or third

    bool aiming_min; // true if we do not want to win a trick anymore
    bool aiming_max; // true if we do not want to lose a trick anymore

    bool doit_defausser; // true if we can't follow the suit or play a stronger
                         // suit
    bool doit_couper; // true if we can't follow the suit and we have to play a
                      // trump (and not to play a trumpet, it is not the same, I
                      // mean, obviously)
    bool doit_fournir_atout; // true if the suit asked is trump and we have some
    bool doit_monter_atout;  // true if we can play a higher trump than the
                             // strongest one in the trick
    bool
        est_coupe; // true if someone played trump while a weaker suit was asked

    double q;

    // Tool functions
    bool
    overall_aggressive_game(); // True if the overall number of announced tricks
                               // is greater than the number of cards in a hand
    bool deeply_defensive_game();
    bool may_be_in_game(const Core::Card);

    Core::Card pick_strongest(const Core::Hand);
    Core::Card pick_strongest_weight(const Core::Hand);
    Core::Card pick_weekest(const Core::Hand);
    Core::Card pick_weekest_weight(const Core::Hand);

    double winning_probability(const Core::Card);
    Core::Hand m_good_winning_proba(const Core::Hand, double);
    Core::Hand m_good_losing_proba(const Core::Hand, double);

    // Main functions
    Core::Card play_NoTrump();
    Core::Card play_NoTrump_aiming_min();
    Core::Card play_NoTrump_aiming_max();
    Core::Card play_NoTrump_in_between();

    Core::Card play_AllTrump();
    Core::Card play_AllTrump_aiming_min();
    Core::Card play_AllTrump_aiming_max();
    Core::Card play_AllTrump_in_between();

    Core::Card play_OneTrump();
    Core::Card play_OneTrump_aiming_min();
    Core::Card play_OneTrump_aiming_max();
    Core::Card play_OneTrump_in_between();

    void processEvent();
    std::unique_ptr<KnowledgeEngine> m_engine;
};
} // namespace IA
