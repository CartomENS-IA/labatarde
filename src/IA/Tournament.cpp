/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Tournament.hpp"

using namespace std;
namespace IA {
int nbCardPerTurn[10] = {5, 6, 7, 8, 9, 9, 8, 7, 6, 5};

int cinplayer() {
    int number;
    char a;
    bool retry = true;
    while (retry) {

        retry = false;
        cin >> a;

        if (cin.fail()) {
            retry = true;
            cout << "Error : Saisie incorrect, Recommencez: ";

            cin.clear();
            cin.seekg(0, ios::end);
            if (!cin.fail()) {
                cin.ignore(numeric_limits<streamsize>::max());
            } else {
                cin.clear();
            }
        }

        switch (a) {
        case '0':
        case 'a':
        case 'A':
            number = 0;
            break;
        case '1':
        case 'z':
        case 'Z':
            number = 1;
            break;
        case '2':
        case 'e':
        case 'E':
            number = 2;
            break;
        case '3':
        case 'r':
        case 'R':
            number = 3;
            break;
        case '4':
        case 't':
        case 'T':
            number = 4;
            break;
        default:
            cout << "joueur invalide, recommencez : ";
            retry = true;
            break;
        }
    }
    return number;
}

bool cinbool() {
    char a;
    bool retry = true;
    while (retry) {
        cin >> a;

        if (cin.fail()) {
            retry = true;
            cout << "Error : Saisie incorrect, Recommencez: ";

            cin.clear();
            cin.seekg(0, ios::end);
            if (!cin.fail()) {
                cin.ignore(numeric_limits<streamsize>::max());
            } else {
                cin.clear();
            }
        }

        switch (a) {
        case '0':
        case 'a':
        case 'A':
            return false;
            break;
        case '1':
        case 'z':
        case 'Z':
            return true;
            break;
        default:
            cout << "saisie invalide, recommencez : ";
            retry = true;
            break;
        }
    }

    return false;
}

Core::Card cincard() {
    char a;
    char b;
    bool retry = true;
    Core::Value value;
    Core::Suit suit;
    while (retry) {
        retry = false;

        cin >> a >> b;

        if (cin.fail()) {
            retry = true;
            cout << "Error : Saisie incorrect, Recommencez: ";

            cin.clear();
            cin.seekg(0, ios::end);
            if (!cin.fail()) {
                cin.ignore(numeric_limits<streamsize>::max());
            } else {
                cin.clear();
            }
        }

        switch (a) {
        case 'q':
        case 'Q':
            value = Core::Value::Cat;
            break;
        case 's':
        case 'S':
            value = Core::Value::Dog;
            break;
        case 'h':
        case 'H':
            value = Core::Value::Knave;
            break;
        case 'j':
        case 'J':
            value = Core::Value::Knight;
            break;
        case 'k':
        case 'K':
            value = Core::Value::Queen;
            break;
        case 'l':
        case 'L':
            value = Core::Value::King;
            break;
        case 'd':
        case 'D':
            value = Core::Value::Juggler;
            break;
        case 'f':
        case 'F':
            value = Core::Value::Musician;
            break;
        case 'g':
        case 'G':
            value = Core::Value::Entertainer;
            break;
        default:
            cout << "carte invalide, recommencez :";
            ;
            retry = true;
            break;
        }

        switch (b) {
        case 'x':
        case 'X':
            suit = Core::Suit::Club;
            break;
        case 'c':
        case 'C':
            suit = Core::Suit::Diamond;
            break;
        case 'v':
        case 'V':
            suit = Core::Suit::Heart;
            break;
        case 'b':
        case 'B':
            suit = Core::Suit::Spade;
            break;
        default:
            cout << "carte invalide, recommencez :";
            retry = true;
            break;
        }
    }
    return Core::Card(suit, value);
}

Core::Bet cinbet() {
    char a;
    int number;
    char b;
    bool retry = true;
    Core::Trump trump;
    while (retry) {
        retry = false;

        do {
            cin >> a >> b;
            if (cin.fail()) {
                retry = true;
                cout << "Error : Saisie incorrect, Recommencez: ";

                cin.clear();
                cin.seekg(0, ios::end);
                if (!cin.fail()) {
                    cin.ignore(numeric_limits<streamsize>::max());
                } else {
                    cin.clear();
                }
            } else
                retry = false;
        } while (retry);

        switch (a) {
        case '0':
        case 'a':
        case 'A':
            number = 0;
            break;
        case '1':
        case 'z':
        case 'Z':
            number = 1;
            break;
        case '2':
        case 'e':
        case 'E':
            number = 2;
            break;
        case '3':
        case 'r':
        case 'R':
            number = 3;
            break;
        case '4':
        case 't':
        case 'T':
            number = 4;
            break;
        case '5':
        case 'y':
        case 'Y':
            number = 5;
            break;
        case '6':
        case 'u':
        case 'U':
            number = 6;
            break;
        case '7':
        case 'i':
        case 'I':
            number = 7;
            break;
        case '8':
        case 'o':
        case 'O':
            number = 8;
            break;
        case '9':
        case 'p':
        case 'P':
            number = 9;
            break;
        default:
            cout << "parie invalide , recommencez :";
            retry = true;
            break;
        }

        switch (b) {
        case 'w':
        case 'W':
            trump = Core::Trump::NoTrump;
            break;
        case 'x':
        case 'X':
            trump = Core::Trump::Club;
            break;
        case 'c':
        case 'C':
            trump = Core::Trump::Diamond;
            break;
        case 'v':
        case 'V':
            trump = Core::Trump::Heart;
            break;
        case 'b':
        case 'B':
            trump = Core::Trump::Spade;
            break;
        case 'n':
        case 'N':
            trump = Core::Trump::AllTrump;
            break;
        default:
            cout << "parie invalide , recommencez :";
            retry = true;
            break;
        }
    }
    return Core::Bet(trump, number);
}

void tournament_run() {
    Core::GameState gamestate(0);
    IAPlayer ia(2000);
    ia.set_game_state(gamestate);

    cout << "__________________________________________________________________"
            "_________________________________________________\n\n";
    cout << "Ceci est l'interface IA avec une partie sur table. \n\n";

    cout << "utilisez ces lettres :\n";
    cout << "Q (chat), S (chien), D (jongleur), F (musicien), G (fou), H "
            "(valet), J (cavalier), K (Dame), L (Roi)\n";
    cout << "pour désigner les hauteurs des cartes\n\n";

    cout << "pour les couleurs, utilisez W (Sans atout), X (Trèfle), C "
            "(Carreau 'Diamond en anglais'), V (Coeur), B (Pique), N (Tout "
            "atout 'all')\n\n";

    cout << "pour les nombres de plis à l'atout, vous pouvez utiliser les "
            "chiffres mais aussi : A (0), Z(1), E(2), R(3), T(4), Y(5), U(6), "
            "I(7), O(8), P(9) \n\n";

    cout << "quand une carte est demandée, donnez la hauteur puis la couleur, "
            "exemple : 'C C' pour cavalier coeur ou '2 D' pour chien carreau\n";
    cout << "quand une couleur est demandée, donnez le nombre de plis puis "
            "couleur, exemple : '1 C' pour 1 coeur ou '2 A' pour 2 tout "
            "atout\n\n";

    cout << "*\nFaites très attention aux fautes de frappes !!!! pour cela "
            "n'allez pas trop vite.\n*\n\n";
    cout << "__________________________________________________________________"
            "_________________________________________________\n\n";

    // On demande la manche en cours
    int first_player;
    int manche;
    cout << "A quelle manche sommes-nous ? :";
    cin >> manche;

    for (manche = manche; manche <= 10; manche++) {

        // Information de la manche
        Core::Hand hand;
        bool mechoune = false;
        cout << "Manche " << manche << "/10\n\n";

        cout << "Premier Joueur ? (0 ia, 1 à sa gauche, 2 en face, 3 à sa "
                "droite) : ";
        cin >> first_player;
        gamestate.notify_next_player(first_player);
        cout << "\n\n";

        cout << "Donnez la Main de l'IA (attendu " << nbCardPerTurn[manche - 1]
             << " cartes )\n";
        for (int j = 0; j < nbCardPerTurn[manche - 1]; j++) {
            cout << "carte " << j + 1 << ": ";
            hand.push_back(cincard());
        }
        gamestate.notify_cards(hand);

        cout << "\nMain de l'IA :\n";
        for (unsigned int i = 0; i < hand.size(); i++)
            cout << hand[i] << "\n";

        // Phase de Pari
        cout << "\nPhase de Pari:\n\n";
        ia.ready();
        gamestate.notify_begin_bet_state();

        // ia.preprocess();

        int player = first_player;
        Core::Bet bet;

        while (player != gamestate.id_best_bet()) {
            gamestate.notify_next_player(player);
            if (player) {
                cout << "Que parie le joueur " << player << " ? :";
                gamestate.notify_bet(cinbet());
                if (!mechoune) {
                    cout
                        << "\n\nQuelqu'un a t'il mechouné (0 = NON, 1=OUI) ? :";
                    mechoune = cinbool();
                    if (mechoune)
                        gamestate.notify_mechoune(0);
                }
            } else {
                bet = ia.bet();
                cout << "*\n*\nL'IA parie : " << bet << "\n*\n*\n\n";
                if (!mechoune) {
                    cout << "Une mechoune a t-il eu lieu avant (0 = NON, 1 = "
                            "OUI) ?";
                    mechoune = cinbool();
                    if (mechoune) {
                        gamestate.notify_mechoune(0);
                        bet = ia.bet();
                        cout << "*\n*\nL'IA parie : " << bet << "\n*\n*\n\n";
                    } else {
                        cout << "Quelqu'un a t'il mechouné l'IA (0 = NON, 1 = "
                                "OUI) ? :";
                        mechoune = cinbool();
                        if (mechoune)
                            gamestate.notify_mechoune(0);
                    }
                }
                gamestate.notify_bet(bet);
            }
            player = (player + 1) % 4;
        }

        // Phase de Jeu
        gamestate.notify_end_bet_state();

        auto bets = gamestate.bets();
        for (int i = 0; i < 4; i++) {
            cout << bets[i] << endl;
        }

        cout << "Phase de Jeu:\n\n";

        for (int j = 0; j < nbCardPerTurn[manche - 1]; j++) {
            int _player;
            for (int k = 0; k < 4; k++) {
                _player = (first_player + k) % 4;
                gamestate.notify_next_player(_player);
                if (_player) {
                    cout << "Que joue le joueur " << _player << "?:\n";
                    gamestate.notify_play(cincard());
                    cout << "\n";
                } else {
                    cout << "*\n*\nL'IA joue : ";
                    Core::Card cardPlayed = ia.play();
                    cout << cardPlayed << "\n";
                    gamestate.notify_play(cardPlayed);
                    cout << "\n*\n*\n\n";
                }
            }

            int winner = gamestate.card_stack().id_winner();
            cout << "Le Gagnant du pli est " << winner << "!!!\n";
            gamestate.notify_trick_win(winner);
            first_player = winner;
            cout << "L'IA a " << gamestate.nb_tricks_current()[0] << " plis\n";
        }
        gamestate.notify_end_card_state();
    }
}
} // namespace IA
