/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <chrono>
#include <cmath>
#include <iostream>
#include <random>
#include <thread>

#include "BotPlayer.hpp"

using namespace std;
using namespace Core;

namespace IA {
// Old values : 3119
static default_random_engine gen(5555);

BotPlayer::BotPlayer(bool _isWaiting) : m_isWaiting(_isWaiting) {}

void BotPlayer::ready() {}

void BotPlayer::wait_a_little() {
    if (m_isWaiting) {
        uniform_int_distribution<int> distrib(400, 1000);
        this_thread::sleep_for(chrono::milliseconds(distrib(gen)));
    }
}

double BotPlayer::eval_trump(const Trump &trump) const {
    const auto &gs = *m_gameState;
    double nbTricks = 0;
    bool juggler = false;
    bool musician = false;
    Suit last_suit = Suit::Club;
    for (const auto &card : gs.my_hand()) {
        Suit suit = card.suit();
        if (suit != last_suit) {
            juggler = false;
            musician = false;
        }
        if (suit != trump && trump != Trump::AllTrump) {
            switch (card.value()) {
            case Value::King:
            case Value::Queen:
                nbTricks += 1.01;
                break;

            case Value::Knight:
                nbTricks += 0.42;
                break;

            default:
                break;
            }
        } else {
            switch (card.value()) {
            case Value::Juggler:
                juggler = true;
                break;
            case Value::Musician:
                nbTricks += (juggler) ? 1.01 : 0.42;
                musician = true;
                break;
            case Value::Entertainer:
                nbTricks += 1.01;
                break;

            case Value::King:
                if (juggler)
                    nbTricks += 0.42;
                if (musician)
                    nbTricks += 0.6;
                break;

            default:
                break;
            }
        }
        last_suit = suit;
    }
    return nbTricks;
}

Bet BotPlayer::bet() {
    wait_a_little();
    const auto &gs = *m_gameState;
    double bestDelta = 1.0;
    double bestValue = 0.0;
    Trump bestTrump = Trump::NoTrump;
    for (const auto &trump : {Trump::NoTrump, Trump::Club, Trump::Diamond,
                              Trump::Heart, Trump::Spade, Trump::AllTrump}) {
        double trumpValue = eval_trump(trump);
        // cout << "Player " << gs.my_id() << " evals " << trump << " at " <<
        // trumpValue << "\n";
        if (gs.bet_info(Bet(trump, int(trumpValue))) == BetInfo::BadNumber)
            trumpValue += 1.01;
        if (gs.bet_info(Bet(trump, int(trumpValue))) == BetInfo::ValidBet) {
            double delta = min(fabs(trumpValue - int(trumpValue)),
                               fabs(int(trumpValue) + 1 - trumpValue));
            if (delta < bestDelta) {
                bestDelta = delta;
                bestValue = trumpValue;
                bestTrump = trump;
            }
        }
    }
    return Bet(bestTrump, int(bestValue));
}

bool BotPlayer::mechoune() { return false; }

bool BotPlayer::choune() { return false; }

Card BotPlayer::play() {
    wait_a_little();
    const auto &gs = *m_gameState;
    const Hand &hand = gs.playable_cards();
    // cout << gs.my_id() << "\n";

    /*
            while(gs.state_changed()) {
                auto currentEvent = gs.get_next_event();
                if (auto cardEvent =
       Core::handle<Core::PlayEvent>(currentEvent)) {
                    m_engine->update(*cardEvent);
                } else if (auto endBetPart =
       Core::handle<Core::EndBetPhaseEvent>(currentEvent)) { m_engine =
       make_unique<KnowledgeEngine>(gs);
                }
            }

            const auto& known = *m_engine;
            cout << "I'm " << gs.my_id() << " and I think :\n";
            for (int player = 0; player < Core::nbPlayers; ++player) {
                cout << player << (known.may_have_trump(player)? " has trump\n"
       : " doesn't have trump\n");
            }
            cout << "\n";
    */
    const auto &betTrick = gs.nb_tricks_current();
    const auto &betObjec = gs.nb_tricks_objective();
    int myLack = betObjec[gs.my_id()] - betTrick[gs.my_id()];
    int globalLack = 0;
    for (int player = 0; player < Core::nbPlayers; ++player) {
        globalLack += betObjec[player];
        globalLack -= betTrick[player];
    }

    // si on commence
    if (gs.card_stack().empty()) { // on commence
        if (myLack <= 0) {         // on veut pas prendre
            const Card *maxCard = nullptr;
            for (unsigned int i = 0; i < hand.size();
                 i++) { // on joue la carte la plus forte qu'on a
                if (!maxCard || m_gameState->stronger_than(hand[i], *maxCard)) {
                    maxCard = &(hand[i]);
                }
            }
            return *maxCard;

        } else { // on veut prendre
            for (unsigned int i = 0; i < hand.size(); i++) {
                if (m_gameState->is_trump(hand[i]) &&
                    hand[i].value() ==
                        Value::King) { // si j'ai un roi non atout je le joue
                    return hand[i];
                }
                return hand[gen() % hand.size()]; // sinon random
            }
        }
    }

    const Card &bestCard = m_gameState->card_stack().strongest_card();

    // si on ne commence pas
    if (myLack) { // on veut perdre
        const Card *maxCard = nullptr;
        const Card *ifnotCard = nullptr;
        for (unsigned int i = 0; i < hand.size();
             i++) { // On joue notre carte la plus forte qui ne prend pas
            if ((!maxCard || m_gameState->stronger_than(hand[i], *maxCard))) {
                ifnotCard = &(hand[i]);
                if (m_gameState->stronger_than(bestCard, hand[i])) {
                    maxCard = &(hand[i]);
                }
            }

            if (maxCard) {
                return *maxCard;
            } else { // Si elles prennent toutes, on joue la plus forte
                return *ifnotCard;
            }
        }
    } else { // On veut gagner, on fait l'inverse
        const Card *minCard = nullptr;
        const Card *ifnotCard = nullptr;
        for (unsigned int i = 0; i < hand.size();
             i++) { // On joue notre carte la plus faible qui prend
            if ((!minCard || m_gameState->stronger_than(*minCard, hand[i]))) {
                ifnotCard = &(hand[i]);
                if (m_gameState->stronger_than(hand[i], bestCard)) {
                    minCard = &(hand[i]);
                }
            }

            if (minCard) {
                return *minCard;
            } else { // Si elles perdent toutes, on joue la plus faible
                return *ifnotCard;
            }
        }
    }

    return hand[gen() % hand.size()]; // si j'ai rien fait je joue au hasard
}
} // namespace IA
