/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "Core.hpp"

/** \file
 *   \brief Definition of class IA::RandomPlayer
 **/

namespace IA {
/** Class RandomPlayer inherits from class Player
 * \brief A bot picking cards randomly
 * \todo Use time and seeding so that's really random
 **/
class RandomPlayer final : public Core::Player {
  public:
    /** \brief Bet 2 tricks on a All Trump  **/
    Core::Bet bet();
    /** \brief This bot doesn't mechoune  **/
    bool mechoune();
    /** \brief This bot doesn't choune  **/
    bool choune();
    /** \brief Pick a random card from his hand  **/
    Core::Card play();

    void ready();
};
} // namespace IA
