/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "IAPlayer.hpp"
#include <algorithm>
#include <math.h>
#include <random>
#include <stdexcept>

#include <SFML/System.hpp>

using namespace std;
using namespace Core;

namespace IA {

IAPlayer::IAPlayer(int nbExplorations, array<double, 10> errorCoeff)
    : m_engine(nullptr), m_nbExplorations(nbExplorations),
      m_errorCoeff(errorCoeff) {}

void IAPlayer::processEvent() {
    auto &gs = *m_gameState;
    while (gs.state_changed()) {
        auto nextEvent = gs.get_next_event();
        if (auto temp = Core::handle<Core::DealCardsEvent>(nextEvent)) {
            m_engine = make_unique<KnowledgeEngine>(gs.my_id(), gs.trump(),
                                                    gs.my_hand());
        } else if (auto _temp =
                       Core::handle<Core::EndBetPhaseEvent>(nextEvent)) {
            m_loss.clear();
        }
        m_engine->update(nextEvent);
    }
}

void IAPlayer::ready() {
    processEvent();
    precompute();
    print_mechoune();
}

void IAPlayer::print_mechoune() {
    cout << "L'IA ne mechoune PAS si elle parie en DERNIER" << endl;
    cout << "[IAPlayer] Couleurs mechounees : ";
    bool atLeastOne = false;
    for (int color = 0; color < 6; color++) {
        if (m_colorMechoune[color]) {
            atLeastOne = true;
            std::cout << static_cast<Core::Trump>(color) << " ";
        }
    }
    if (!atLeastOne) {
        cout << "rien du tout";
    }
    cout << "\n" << endl;
}

Bet IAPlayer::bet() {
    m_loss.clear();
    processEvent();
    precompute();
    sf::sleep(sf::milliseconds(200));
    return choose_bet(m_loss);
}

Loss::Loss(Core::Trump trump, int amount, double _totalError)
    : bet(trump, amount), totalError(_totalError) {}
bool operator<(const Loss &a, const Loss &b) {
    return a.totalError < b.totalError;
}

void IAPlayer::compute_loss(const array<vector<double>, 6> &scoresBet) {
    for (int trump = 0; trump < 6; ++trump) {
        for (int amount = 0; amount <= 9; ++amount) {
            int totalError = 0;
            for (int real = 0; real <= 9; ++real) {
                int error =
                    (int)scoresBet[trump][real] * abs((int)(amount - real));
                totalError += error;
            }
            m_loss.emplace_back(static_cast<Trump>(trump), amount, totalError);
        }
    }
    sort(begin(m_loss), end(m_loss));
}

Bet IAPlayer::choose_bet(const vector<Loss> &loss) const {
    const auto &gs = *m_gameState;
    for (const auto &bet : loss) {
        if (gs.bet_info(bet.bet) == BetInfo::ValidBet) {
            return bet.bet;
        }
    }
    throw runtime_error("[IAPlayer.cpp] No valid bet available");
}

void IAPlayer::precompute() {
    processEvent();
    if (m_loss.empty()) {
        const auto &gs = *m_gameState;
        const auto &ke = *m_engine;
        array<vector<double>, 6> scoresBet =
            MonteCarlo::generateBet(gs, ke, m_nbExplorations, m_errorCoeff);
        compute_loss(scoresBet);
        for (auto bet : m_loss) {
            bool test = (bet.totalError <= 0.20 * m_nbExplorations);
            if (test) {
                m_colorMechoune[static_cast<int>(bet.bet.trump)] = true;
            }
        }
    }
}

bool IAPlayer::mechoune() {
    auto trump = m_gameState->best_bet().trump;
    sf::sleep(sf::milliseconds(200));
    return m_colorMechoune[static_cast<int>(trump)];
}

bool IAPlayer::choune() {
    auto trump = m_gameState->best_bet().trump;
    return m_colorMechoune[static_cast<int>(trump)];
}

Card IAPlayer::play() {
    processEvent();
    const auto &gs = *m_gameState;
    const auto &ke = *m_engine;
    return MonteCarlo::chooseCard(gs, ke, m_nbExplorations);
}

void IAPlayer::set_game_state(const GameState &gameState) {
    auto &gs = gameState;
    Player::set_game_state(gs);
    m_engine =
        make_unique<KnowledgeEngine>(gs.my_id(), gs.trump(), gs.my_hand());
}
} // namespace IA
