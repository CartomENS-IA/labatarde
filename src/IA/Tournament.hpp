/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef TOURNAMENT
#define TOURNAMENT

#include "Core.hpp"
#include "IA.hpp"
#include "iostream"

/** \file
 *   \author Léo.V
 *   \brief Interface for game on table which a IA played
 **/

namespace IA {
void tournament_run();
}

#endif
