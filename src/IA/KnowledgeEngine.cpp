/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "KnowledgeEngine.hpp"

#include <algorithm>
#include <cassert>
#include <iterator>
#include <random>
#include <stdexcept>

using namespace std;
using namespace Core;

Belief::Belief(const Hand &hand, int _nbCards) : mayHave(4), nbCards(_nbCards) {
    for (const auto &card : hand) {
        int i_suit = static_cast<int>(card.suit()) - 1;
        mayHave[i_suit].emplace_back(card);
    }
}

void Belief::remove(const Card &card) {
    int i_suit = static_cast<int>(card.suit()) - 1;
    mayHave[i_suit].remove(card);
}

void Belief::remove(const Suit &suit) {
    int i_suit = static_cast<int>(suit) - 1;
    mayHave[i_suit].clear();
}

void Belief::remove_stronger(const Card &card, const CardStack &stack) {
    int i_suit = static_cast<int>(card.suit()) - 1;
    auto is_stronger = [&](const Card &other) {
        return stack.stronger_than(other, card) || other == card;
    };
    mayHave[i_suit].remove_if(is_stronger);
}

bool Belief::domines(const Core::Card &card, const Core::CardStack &stack)
    const { // Not yet used, as is_dominant
    int i_suit = static_cast<int>(card.suit()) - 1;
    for (const auto &other : mayHave[i_suit]) {
        if (stack.stronger_than(other, card)) {
            return false;
        }
    }
    return true;
}

bool Belief::must_win_trick(const Core::Card &card,
                            const CardStack &stack) const {
    int i_suit = static_cast<int>(card.suit()) - 1;
    if (!stack.is_trump(card) && stack.trump() != Trump::NoTrump &&
        stack.trump() != Trump::AllTrump &&
        may_have(static_cast<Suit>(stack.trump())))
        return false;
    for (const auto &other : mayHave[i_suit]) {
        if (stack.stronger_than(other, card)) {
            return false;
        }
    }
    return true;
}

bool Belief::must_lose_trick(const Core::Card &,
                             const Core::CardStack &) const {
    return false; // We don't have any knowledge, only beliefs
}

bool Belief::may_have(const Card &card) const {
    int i_suit = static_cast<int>(card.suit()) - 1;
    auto &subHand = mayHave[i_suit];
    return find(begin(subHand), end(subHand), card) != end(subHand);
}

bool Belief::may_have(const Suit &suit) const {
    int i_suit = static_cast<int>(suit) - 1;
    return !mayHave[i_suit].empty();
}

Hand Belief::get_cards() const {
    Hand hand;
    for (const auto &color : mayHave) {
        for (const auto &card : color) {
            hand.push_back(card);
        }
    }
    return hand;
}

int Belief::nb_remaining_cards() const { return nbCards; }

void Belief::reduce_nb_remaining_cards() { nbCards -= 1; }

KnowledgeEngine::KnowledgeEngine(int _myId, Trump _trump, const Hand &myHand)
    : stack(_trump), trump(_trump), myId(_myId) {
    Hand reducedGame;
    for (const auto &card : generate_full_game()) {
        if (find(begin(myHand), end(myHand), card) == end(myHand)) {
            reducedGame.push_back(card);
        }
    }
    for (int player = 0; player < Core::nbPlayers; ++player) {
        if (player != myId) {
            beliefs.emplace_back(reducedGame, myHand.size());
        } else {
            beliefs.emplace_back(myHand, myHand.size());
        }
    }
}

void KnowledgeEngine::update(const shared_ptr<Event> event) {
    if (auto temp = handle<PlayEvent>(event)) {
        int player = temp->player;
        Card card = temp->card;
        beliefs[player].reduce_nb_remaining_cards();
        remove_from_hands(card);
        m_yetPlayedCards.push_back(card);
        if (player == myId) {
            stack.add_card(card, player);
            return; // nothing to learn about me
        }
        test_color_accuracy(player, card);
        test_trump_climbing(player, card);
        stack.add_card(card, player);
    } else if (auto _temp = handle<TrickWonByEvent>(event)) {
        stack = CardStack(trump);
    }
}

bool KnowledgeEngine::must_win_trick(const Card &card) const {
    if (!stack.empty() && !stack.stronger_than(card, stack.strongest_card()))
        return false;
    for (int player = 0; player + int(stack.size()) + 1 < Core::nbPlayers;
         ++player) {
        if (!beliefs[(player + myId + 1) % Core::nbPlayers].must_win_trick(
                card, stack))
            return false;
    }
    return true;
}

bool KnowledgeEngine::must_lose_trick(const Card &card) const {
    if (!stack.empty() && !stack.stronger_than(card, stack.strongest_card()))
        return true;
    for (int player = 0; player + int(stack.size()) < Core::nbPlayers;
         ++player) {
        if (!beliefs[(player + myId + 1) % Core::nbPlayers].must_lose_trick(
                card, stack))
            return false;
    }
    return true;
}

bool KnowledgeEngine::is_dominant(const Card &card) const { // Not yet used
    for (int player = 0; player < Core::nbPlayers; ++player) {
        if (player == myId)
            continue;
        if (!beliefs[player].domines(card, stack))
            return false;
    }
    return true;
}

bool KnowledgeEngine::no_trump_anymore() const {
    for (int player = 0; player < Core::nbPlayers; ++player) {
        if (player == myId)
            continue;
        if (may_have_trump(player)) {
            return false;
        }
    }
    return true;
}

bool KnowledgeEngine::may_have_trump(int player) const {
    switch (stack.trump()) {
    case Trump::NoTrump:
        return false;
    case Trump::Club:
        return may_have(Suit::Club, player);
    case Trump::Diamond:
        return may_have(Suit::Diamond, player);
    case Trump::Heart:
        return may_have(Suit::Heart, player);
    case Trump::Spade:
        return may_have(Suit::Spade, player);
    case Trump::AllTrump:
        return true;
    }
    return false;
}

bool KnowledgeEngine::may_have(const Card &card, int player) const {
    return beliefs[player].may_have(card);
}

bool KnowledgeEngine::may_have(const Suit &suit, int player) const {
    return beliefs[player].may_have(suit);
}

std::vector<Core::Card> KnowledgeEngine::yet_played_cards() const {
    return m_yetPlayedCards;
}

default_random_engine KnowledgeEngine::RandomBacktrack::gen(6432);

KnowledgeEngine::RandomBacktrack::RandomBacktrack(const vector<Belief> &beliefs,
                                                  int _myId)
    : available(4), sizeHand(5), myId(_myId) {
    set<Card, CardComparatorForODS> set_cards;
    sizeHand[dog] = 4 * 9;
    for (int player = 0; player < Core::nbPlayers; ++player) {
        sizeHand[player] = beliefs[player].nb_remaining_cards();
        sizeHand[dog] -= sizeHand[player];
        auto remaining = beliefs[player].get_cards();
        available[player].insert(begin(remaining), end(remaining));
        if (player == myId)
            continue;
        set_cards.insert(begin(available[player]), end(available[player]));
    }
    cards.reserve(set_cards.size());
    copy(begin(set_cards), end(set_cards), back_inserter(cards));
    domain.resize(cards.size());
    affectedto.resize(cards.size());
}

AllHands KnowledgeEngine::RandomBacktrack::generate_hands() {
    shuffle(begin(cards), end(cards), gen);
    for (int i = 0; i < (int)cards.size(); ++i) {
        Card card = cards[i];
        for (int player = 0; player < Core::nbPlayers; ++player) {
            if (player != myId &&
                available[player].find(card) != end(available[player])) {
                domain[i].push_back(player);
            }
        }
        shuffle(begin(domain[i]), end(domain[i]), gen);
        if (sizeHand[dog] != 0)
            domain[i].push_back(dog);
    }
    bool isOk = backtrack(0);
    if (!isOk)
        throw logic_error(
            "Unable to generate a correct hand in Knowledge Engine");
    AllHands hands(4);
    for (int i = 0; i < (int)cards.size(); ++i) {
        if (affectedto[i] == dog)
            continue;
        hands[affectedto[i]].push_back(cards[i]);
    }
    copy(begin(available[myId]), end(available[myId]),
         back_inserter(hands[myId]));
    return hands;
}

bool KnowledgeEngine::RandomBacktrack::backtrack(int i) {
    if (i >= (int)cards.size())
        return true;
    Card card = cards[i];
    for (auto player : domain[i]) {
        if (sizeHand[player] == 0)
            continue;

        bool isOk = true;

        sizeHand[player] -= 1;
        array<bool, 4> isHere = {false, false, false, false};
        for (int otherPlayer = 0; otherPlayer < Core::nbPlayers;
             ++otherPlayer) {
            auto pos = available[otherPlayer].find(card);
            if (pos != end(available[otherPlayer])) {
                available[otherPlayer].erase(pos);
                isHere[otherPlayer] = true;
                if ((int)available[otherPlayer].size() < sizeHand[otherPlayer])
                    isOk = false;
            }
        }

        affectedto[i] = player;
        isOk = isOk && backtrack(i + 1);

        for (int otherPlayer = 0; otherPlayer < Core::nbPlayers;
             ++otherPlayer) {
            if (isHere[otherPlayer])
                available[otherPlayer].insert(card);
        }
        sizeHand[player] += 1;

        if (isOk)
            return true;
    }
    return false;
}

AllHands KnowledgeEngine::get_valid_deal() const {
    RandomBacktrack bk(beliefs, myId);
    return bk.generate_hands();
    // return get_stupid_deal();
}

AllHands KnowledgeEngine::get_stupid_deal() const {
    Hand hand = generate_full_game();
    int cur = 0;
    AllHands hands(4);
    hands[myId] = beliefs[myId].get_cards();
    for (int player = 0; player < Core::nbPlayers; ++player) {
        if (player == myId)
            continue;
        for (int i = 0; i < beliefs[player].nb_remaining_cards(); ++cur) {
            if (find(begin(hands[myId]), end(hands[myId]), hand[cur]) ==
                end(hands[myId])) {
                hands[player].push_back(hand[cur]);
                ++i;
            }
        }
    }
    return hands;
}

void KnowledgeEngine::remove_from_hands(const Card &card) {
    for (auto &belief : beliefs) {
        belief.remove(card);
    }
}

void KnowledgeEngine::test_color_accuracy(int player, const Card &card) {
    if (stack.empty() || card.suit() == stack.suit())
        return;
    beliefs[player].remove(stack.suit());
    /*cout << "Pas de " << stack.suit() << " pour " << player << " according to
     * " << card << " ------------------------------" << endl;*/
    if (card.suit() == stack.trump())
        return;
    switch (stack.trump()) {
    case Trump::Club:
        beliefs[player].remove(Suit::Club);
        /*cout << "Pas de Club pour " << player << " according to " << card << "
         * ------------------------------" << endl;*/
        break;
    case Trump::Diamond:
        beliefs[player].remove(Suit::Diamond);
        /*cout << "Pas de Diamond pour " << player << " according to " << card
         * << " ------------------------------" << endl;*/
        break;
    case Trump::Heart:
        beliefs[player].remove(Suit::Heart);
        /*cout << "Pas de Heart pour " << player << " according to " << card <<
         * " ------------------------------" << endl;*/
        break;
    case Trump::Spade:
        beliefs[player].remove(Suit::Spade);
        /*cout << "Pas de Spade pour " << player << " according to " << card <<
         * " ------------------------------" << endl;*/
        break;
    default:
        break;
    }
}

void KnowledgeEngine::test_trump_climbing(int player, const Card &card) {
    if (!stack.is_trump(card))
        return;
    for (const auto &otherCard : stack.stack()) {
        if (card == otherCard)
            throw runtime_error(
                "[Knowledge Engine.cpp] card already played in KE");
        if (stack.stronger_than(otherCard, card)) {
            beliefs[player].remove_stronger(otherCard, stack);
        }
    }
}
