/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <iostream>
#include <random>
#include <stdexcept>

#include "RandomPlayer.hpp"

using namespace std;
using namespace Core;

namespace IA {
// Old values : 3119
static default_random_engine gen(5555);

Bet RandomPlayer::bet() {
    Trump trump =
        static_cast<Trump>(gen() % (static_cast<int>(Trump::AllTrump) + 1));
    int nb = (int)gen() % ((int)(m_gameState->my_hand().size()) + 1);
    return Bet(trump, nb);
}

void RandomPlayer::ready() {}

bool RandomPlayer::mechoune() { return false; }

bool RandomPlayer::choune() { return false; }

Card RandomPlayer::play() {
    const auto &gs = *m_gameState;
    const Hand &hand = gs.playable_cards();
    if (hand.size() == 0) {
        cout << "player" << gs.my_id() << " hand empty, cannot play\n";
        throw logic_error("Empty hand causes this call to fail");
    }
    return hand[gen() % hand.size()];
}
} // namespace IA
