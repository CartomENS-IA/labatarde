/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef IA_PLAYER
#define IA_PLAYER

#include "Core.hpp"
#include "MonteCarlo.hpp"
#include <memory>

/** \file
 *   \author Léo.V and Louis.B
 *   \brief Definition of class IA::IAPlayer
 **/

namespace IA {
/** Class IAPlayer inherits from class Player
 * \brief A IA which used intelligent technic like Monte-Carlo
 **/
struct Loss {
    Loss(Core::Trump, int, double);
    Core::Bet bet;
    double totalError;
};
bool operator<(const Loss &, const Loss &);

class IAPlayer final : public Core::Player {
  public:
    IAPlayer(int, std::array<double, 10> coefError = {1., 0.9, 0.8, 0.7, 0.6,
                                                      0.5, 0.5, 0.5, 0.5, 0.5});

    Core::Bet bet();
    bool mechoune();
    bool choune();
    Core::Card play();

    void ready();

    virtual void set_game_state(const Core::GameState &gameState);

  private:
    void processEvent();
    void compute_loss(const std::array<std::vector<double>, 6> &);
    Core::Bet choose_bet(const std::vector<Loss> &) const;
    void precompute();
    void print_mechoune();

    std::unique_ptr<KnowledgeEngine> m_engine;
    std::vector<Loss> m_loss;
    std::array<bool, 6> m_colorMechoune;

    int m_nbExplorations;
    std::array<double, 10> m_errorCoeff;
};
} // namespace IA

#endif
