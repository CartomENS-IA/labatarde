/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef BOT_PLAYER
#define BOT_PLAYER

#include <memory>

#include "Core.hpp"
#include "KnowledgeEngine.hpp"

/** \file
 *   \author Léo.V, Louis.B
 *   \brief Definition of class IA::BotPlayer
 **/

namespace IA {
/** Class RandomPlayer inherits from class Player
 * \brief A bot picking cards randomly
 *   \todo Use time and seeding so that's really random
 **/
class BotPlayer final : public Core::Player {
  public:
    BotPlayer(bool wait = true);
    Core::Bet bet();

    /** \brief This bot doesn't mechoune  **/
    bool mechoune();
    /** \brief This bot doesn't choune  **/
    bool choune();
    /** \brief Picks a random card from his hand  **/
    Core::Card play();
    /** \brief This bot is immediately ready **/
    void ready();

  private:
    /** \brief DOCUMENTATION TODO **/
    double eval_trump(const Core::Trump &) const;

    /** \brief DOCUMENTATION TODO **/
    void wait_a_little();

    bool m_isWaiting;
    std::unique_ptr<KnowledgeEngine> m_engine;
};
} // namespace IA

#endif
