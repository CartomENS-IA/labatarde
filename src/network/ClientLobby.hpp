/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef ClientLobby_hpp_INCLUDED
#define ClientLobby_hpp_INCLUDED

#include <vector>

#include "ClientUser.hpp"
#include "RemoteKernel.hpp"
#include "ServerConstants.hpp"

/** \file ClientLobby.hpp
 * \brief Definition of class Network::ClientLobby
 */

namespace Network {
class ClientUser;

/**
 * \brief The client's view of the Lobby hosted on the server
 *
 * Shows the players connected. Receives the messages from Network::Lobby to
 * keep the informations updated. It will spawn the RemoteKernel when the
 * game starts
 */

class ClientLobby {
  public:
    /**
     * \brief Constructs an empty lobby
     */
    ClientLobby();

    ~ClientLobby();

    /**
     * \brief Returns true if the game is running, false if players are still
     * waiting
     */
    bool is_running() const;

    /**
     * \brief Returns the number of human players in the lobby.
     */
    char nb_user() const;

    /**
     * \brief Returns a vector containing the pointers to the users in the
     * lobby. Fixed size. If seat not taken, the vector contains nullptr
     */
    std::vector<ClientUser *> users() const;

    /**
     * \brief Returns the id of the Lobby
     */
    char id() const;

    /**
     * \brief Starts the ClientLobby loop. Used to listen for User's messages.
     * The main server no longer listen for them, the Lobby is taking these
     * messages. Must be spawned by the server
     */
    RemoteKernel *run();

  private:
    char m_id;
    char m_nb_users;
    bool m_running;
    std::vector<ClientUser *> m_users;

    void add_user(ClientUser *);
    void remove_user(ClientUser *);
};

} // namespace Network
#endif // ClientLobby_hpp_INCLUDED
