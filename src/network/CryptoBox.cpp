/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "CryptoBox.hpp"

#define LOG(msg) clog << "[CryptoBox] " << msg << endl

namespace Network {
CryptoBox::CryptoBox() {
    m_initiated = false;
    m_isClient = false;
    m_target_key_loaded = false;
    m_key_loaded = false;

    LOG("Initialisation of the crypto protocols");
    // Initiate libsodium
    if (sodium_init() < 0) {
        LOG("The sodium library could not be initialized properly.\nAborting "
            "mission.");
        throw(std::runtime_error("Sodium lib could not initialize"));
    }

    for (size_t i = 0; i < NONCE_SIZE; i++) {
        m_nonce_send[i] = 0;
        m_nonce_receive[i] = 0;
    }
}

void CryptoBox::generate_key_pair() {
    crypto_kx_keypair(m_public_key, m_secret_key);
    m_key_loaded = true;
}

void CryptoBox::load_server_key(unsigned char *key) {
    memcpy(m_target_public_key, key, crypto_kx_PUBLICKEYBYTES);
    m_target_key_loaded = true;
}

void CryptoBox::init_client(sf::TcpSocket *socket) {
    if (not m_target_key_loaded) {
        throw(std::runtime_error("The server's public key must be loaded "
                                 "before calling the init function"));
    }

    if (not m_key_loaded) {
        generate_key_pair();
    }

    // Prepare the data to send
    size_t message_size = PUBKEY_SIZE + NONCE_SIZE + NONCE_SIZE;
    unsigned char *message =
        (unsigned char *)malloc(message_size * sizeof(unsigned char));
    size_t offset = 0;

    // Copy my public key
    memcpy(message + offset, m_public_key, PUBKEY_SIZE);
    offset += PUBKEY_SIZE;

    // Generate two random nonces and adding them to the message
    randombytes_buf(m_nonce_send, NONCE_SIZE);
    randombytes_buf(m_nonce_receive, NONCE_SIZE);

    memcpy(message + offset, m_nonce_send, NONCE_SIZE);
    offset += NONCE_SIZE;
    memcpy(message + offset, m_nonce_receive, NONCE_SIZE);

    // Cypher our public key and a nonces for the server
    size_t cyphered_size = SEAL_SIZE + message_size;
    unsigned char *cyphered_message =
        (unsigned char *)malloc(cyphered_size * sizeof(unsigned char));

    crypto_box_seal(cyphered_message, message, message_size,
                    m_target_public_key);

    // Creating the packet to send
    sf::Packet packet;
    packet.append(cyphered_message, cyphered_size);

    // Send it to the server
    socket->send(packet);

    // Cleaning the pointers
    free(cyphered_message);
    free(message);

    // Receiving confirmation
    sf::Packet answer;
    socket->receive(answer);

    // The answer is the client's public key cyphered and signed by the server
    size_t answer_size = answer.getDataSize();
    if (answer_size != MAC_SIZE + PUBKEY_SIZE) {
        throw std::runtime_error("Server sent message off wrong size");
    }

    unsigned char answer_message[PUBKEY_SIZE];
    decypher_message(answer_message, (unsigned char *)answer.getData(),
                     PUBKEY_SIZE + MAC_SIZE);

    if (strncmp((const char *)answer_message, (const char *)m_public_key,
                PUBKEY_SIZE) != 0) {
        throw std::runtime_error(
            "The server replied with the wrong public key");
    }

    // Derivate the send key and receive key
    if (crypto_kx_client_session_keys(m_receive_key, m_send_key, m_public_key,
                                      m_secret_key, m_target_public_key) != 0) {
        throw std::runtime_error("Suspicious keys");
    }

    m_initiated = true;
    m_isClient = true;
}

void CryptoBox::init_server(sf::TcpSocket *socket) {
    if (not m_key_loaded) {
        throw(std::runtime_error("The CryptoBox must have a loaded key pair in "
                                 "order to start initialization"));
    }

    // Receive the client's public key, cyphered with server's public key
    sf::Packet packet;
    socket->receive(packet);

    size_t message_size = PUBKEY_SIZE + NONCE_SIZE + NONCE_SIZE;
    size_t cyphered_size = message_size + SEAL_SIZE;

    if (packet.getDataSize() != cyphered_size) {
        throw std::runtime_error("Client sent data with wrong size");
    }

    unsigned char *message =
        (unsigned char *)malloc(message_size * sizeof(unsigned char));

    if (crypto_box_seal_open(message, (unsigned char *)packet.getData(),
                             cyphered_size, m_public_key, m_secret_key) != 0) {
        throw std::runtime_error("Could not decypher client's message");
    }

    // Reading the fields
    size_t offset = 0;
    // The client's key
    memcpy(m_target_public_key, message + offset, PUBKEY_SIZE);
    offset += PUBKEY_SIZE;
    m_target_key_loaded = true;

    // The two nonces
    memcpy(m_nonce_receive, message + offset, NONCE_SIZE);
    offset += NONCE_SIZE;
    memcpy(m_nonce_send, message + offset, NONCE_SIZE);

    free(message);

    // The answer is the client's public key, cyphered like normal
    unsigned char answer[PUBKEY_SIZE];
    unsigned char cyphered_answer[PUBKEY_SIZE + MAC_SIZE];

    memcpy(answer, m_target_public_key, PUBKEY_SIZE);
    cypher_message(cyphered_answer, answer, PUBKEY_SIZE);

    sf::Packet answer_packet;
    answer_packet.append(cyphered_answer, PUBKEY_SIZE + MAC_SIZE);

    // Send it
    socket->send(answer_packet);

    // Derivate the send key and receive key
    if (crypto_kx_server_session_keys(m_receive_key, m_send_key, m_public_key,
                                      m_secret_key, m_target_public_key) != 0) {
        throw std::runtime_error("Suspicious keys");
    }

    m_initiated = true;

    LOG("Crypto initialization ok.");
}

void CryptoBox::cypher_message(unsigned char *cyphered,
                               const unsigned char *source, size_t size) {
    crypto_secretbox_easy(cyphered, source, size, m_nonce_send, m_send_key);
    increment_nonce(m_nonce_send);
}

void CryptoBox::decypher_message(unsigned char *decyphered,
                                 unsigned char *source, size_t size) {
    if (crypto_secretbox_open_easy(decyphered, source, size, m_nonce_receive,
                                   m_receive_key) != 0) {
        throw(std::runtime_error("Could not decypher. Message may be forged"));
    }
    increment_nonce(m_nonce_receive);
}

unsigned char *CryptoBox::public_key() { return m_public_key; }

void CryptoBox::increment_nonce(unsigned char *nonce) {
    nonce[0]++;
    size_t i = 0;
    while (i < NONCE_SIZE and nonce[i] == 0) {
        nonce[i + 1]++;
        i++;
    }
}
} // namespace Network
