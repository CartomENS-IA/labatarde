/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "ClientMessenger.hpp"
#include <iomanip> // std::setfill, std::setw
#include <iostream>
#include <sstream> // std::sstream

#define LOG(msg) clog << "[ClientMessenger] " << msg << endl

using namespace RemoteCore;

namespace Network {

ClientMessenger::ClientMessenger(sf::TcpSocket *socket, CryptoBox *crypto)
    : Messenger(socket, crypto), m_init(true) {}

ClientMessenger::ClientMessenger(sf::IpAddress addr, unsigned short port)
    : Messenger(nullptr, nullptr), m_init(false), m_addr(addr), m_port(port),
      m_timeout(timeout) {}

sf::Socket::Status ClientMessenger::init() {
    if (not m_init) {
        // Reset the crypto
        delete (m_crypto);
        m_crypto = new CryptoBox();

        // Reset the socket
        m_socket = new sf::TcpSocket();

        LOG("Connecting to remote server");
        sf::Socket::Status st = m_socket->connect(m_addr, m_port, m_timeout);

        LOG("Connected to server (" << m_socket->getRemoteAddress() << " : "
                                    << m_socket->getRemotePort() << ")");

        LOG("Starting the key exchange protocol");
        unsigned char server_public_key[crypto_kx_PUBLICKEYBYTES];
        size_t received;
        // Getting the server public key
        if (m_socket->receive(server_public_key, crypto_kx_PUBLICKEYBYTES,
                              received) != sf::Socket::Status::Done) {
            throw(std::runtime_error("Key exchange error"));
        }

        // Setup the crypto
        m_crypto->load_server_key(server_public_key);
        m_crypto->init_client(m_socket);

        LOG("Crypto initialization done!");
        m_init = true;
        return st;
    } else {
        // already initialized
        return sf::Socket::Status::Done;
    }
}

/** DEPRECATED : We only do matchmaking for now

RemoteKernel* ClientMessenger::create_game(Core::Player *player) {
    ServerMessage message;
    message.write_header(ServerHeader::CreateGame);

    // Send game creation
    send(message);
    // receive answer
    GameMessage answer = *receive();

    if (answer.read_status() != Status::Done)
        throw(std::runtime_error("An error occured during game creation"));

    RemoteKernel *kernel = new RemoteKernel(player, m_socket, m_crypto);
    kernel->lobbyId = message.read_joined_lobby();
    kernel->nbPlayers = message.read_nb_human();

    cerr << "[client] Kernel built!" << endl;
    return kernel;
}
**/

/** DEPRECATED : We only do matchmaking for now
RemoteKernel *ClientMessenger::join_game(Core::Player *player,
                                             char game_id) {
    ServerMessage message;
    message.write_header(ServerHeader::JoinGame);
    message.write_lobby(game_id);

    // Send game join
    message.send(m_socket, m_crypto);
    // receive answer
    message.receive(m_socket, m_crypto);

    if (message.read_status() != Status::Done)
        throw(std::runtime_error("An error occured during game creation"));

    RemoteKernel *kernel = new RemoteKernel(player, m_socket, m_crypto);
    kernel->lobbyId = message.read_joined_lobby();
    kernel->nbPlayers = message.read_nb_human();

    cerr << "[client] Kernel built!" << endl;
    return kernel;
}
**/

RemoteKernel *ClientMessenger::join_matchmaking(Core::Player *player) {
    ServerMessage *message = new ServerMessage();
    message->set_header(ServerMessageHeader::JoinLobby);

    // Send game fill message
    send(message);
    // receive answer
    message = static_cast<ServerMessage *>(receive());

    if (message->read_server_status() != ServerMessageStatus::Done)
        throw(std::runtime_error("An error occured during game creation"));

    RemoteKernel *kernel = new RemoteKernel(player, shared_from_this());
    kernel->lobbyId = message->read_lobby_id();
    kernel->nbPlayers = message->read_nb_human();

    delete message;
    LOG("Kernel built!");
    return kernel;
}

void ClientMessenger::start_game() {
    LobbyMessage message;
    message.set_header(LobbyMessageHeader::Start);
    send(&message);
}

ServerMessageStatus ClientMessenger::authenticate(const char *user,
                                                  const char *password) {
    LOG("Asking for authentication");
    ServerMessage message;

    // Send the user name
    message.set_header(ServerMessageHeader::Authenticate);
    LOG("Header sent : " << (int)(message.header()));
    message.set_user(user);
    send(&message);
    LOG("Sent the username");

    ServerMessage *found_user = static_cast<ServerMessage *>(receive());
    if (found_user->read_server_status() != ServerMessageStatus::Done)
        throw(std::runtime_error("Invalid username"));

    LOG("Received an answer");
    // Receive the salt
    unsigned char cypheredSalt[cypheredSaltSize];
    size_t received;
    if (m_socket->receive(cypheredSalt, cypheredSaltSize, received) !=
        sf::Socket::Status::Done) {
        throw(std::runtime_error(
            "An error occured during the receiving of the salt"));
    }

    char salt[saltSize];
    m_crypto->decypher_message((unsigned char *)salt, cypheredSalt,
                               cypheredSaltSize);

    LOG("Salt : " << bin_to_hex((unsigned char *)salt, saltSize));

    // Hash the password with the salt
    unsigned char saltedPass[saltedPassSize];
    if (crypto_pwhash(saltedPass, saltedPassSize, password, strlen(password),
                      (unsigned char *)salt, crypto_pwhash_OPSLIMIT_INTERACTIVE,
                      crypto_pwhash_MEMLIMIT_INTERACTIVE,
                      crypto_pwhash_ALG_DEFAULT) != 0) {
        throw(
            std::runtime_error("Went out of memory when salting the password"));
    }

    LOG("Salted pass : " << bin_to_hex(saltedPass, saltedPassSize));

    // Receive the random string
    unsigned char cypheredRandom[cypheredSaltSize];
    if (m_socket->receive(cypheredRandom, cypheredSaltSize, received) !=
        sf::Socket::Status::Done) {
        throw(std::runtime_error(
            "An error occured during the receiving of the random string"));
    }

    unsigned char random[saltSize];
    m_crypto->decypher_message(random, cypheredRandom, cypheredSaltSize);

    // Hash the salted password with the random string
    unsigned char key[keySize];
    if (crypto_pwhash(key, keySize, (char *)saltedPass, saltedPassSize, random,
                      crypto_pwhash_OPSLIMIT_INTERACTIVE,
                      crypto_pwhash_MEMLIMIT_INTERACTIVE,
                      crypto_pwhash_ALG_DEFAULT) != 0) {
        throw(std::runtime_error(
            "Went out of memory when hashing salted password and random"));
    }

    LOG("Key : " << bin_to_hex(key, keySize));

    // Send back the hash of random | salted password
    unsigned char cypheredKey[cypheredKeySize];
    m_crypto->cypher_message(cypheredKey, key, keySize);

    if (m_socket->send(cypheredKey, cypheredKeySize, received) !=
        sf::Socket::Status::Done) {
        throw(std::runtime_error("Failed to send the key"));
    }

    // Receive the Authentication result
    ServerMessage *result = static_cast<ServerMessage *>(receive());
    return result->read_server_status();
}

string bin_to_hex(unsigned char *data, size_t len) {
    std::stringstream ss;
    ss << std::hex << std::setfill('0');
    for (size_t i = 0; i < len; ++i) {
        ss << std::setw(2) << static_cast<unsigned>(data[i]);
    }
    return ss.str();
}

char *hex_to_bin(string data) {
    size_t out_len = data.length() / 2;
    char *res = (char *)malloc(sizeof(char) * out_len);

    for (size_t i = 0; i < out_len; ++i) {
        std::stringstream ss;
        unsigned c;
        ss << hex << data.substr(2 * i, 2);
        ss >> c;
        res[i] = (char)c;
    }
    return res;
}

} // namespace Network
