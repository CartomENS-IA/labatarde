/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef ClientMessenger_hpp_INCLUDED
#define ClientMessenger_hpp_INCLUDED

/**
 * \file
 * \brief Definition of class ClientMessenger
 **/

#include "Messenger.hpp"
#include <SFML/Network.hpp>
#include "RemoteCore.hpp"

namespace Network {

/** \class
 * \brief Extension of the Messenger class to the client side
 *
 *  The ClientMessenger class is meant to be initialized client side as a singleton,
 *  and will handle every message a client has to send to the server.
 **/
class ClientMessenger : public Messenger,
                        public std::enable_shared_from_this<ClientMessenger>
{
public:
    /**
    *
    **/
    ClientMessenger(sf::TcpSocket *socket, CryptoBox *crypto);

    ClientMessenger(sf::IpAddress addr, unsigned short port);

    /**
    *  \brief Initializes the Messenger. Has to be called before any attempt
    *         to use other methods
    *
    * Only necessary when m_init is set to false. In this case, the method will
    * create a TcpSocket bound to the IP adress and the port provided
    * in the constructor.
    **/
    sf::Socket::Status init();

    /**
     * \brief Asks the server to join a lobby
     **/
    RemoteCore::RemoteKernel* join_matchmaking(Core::Player *);

    /** DEPRECATED : We only do matchmaking for now
    RemoteCore::RemoteKernel* join_game(Core::Player *player, char game_id);
    **/

    /** DEPRECATED : We only do matchmaking for now
    RemoteCore::RemoteKernel* create_game(Core::Player *player);
    **/

    /**
     * \brief Asks the server to authenticate an account
     **/
    ServerMessageStatus authenticate(const char * username,
                                     const char * password);

    /**
     * \brief Sends a request to start the game
     **/
    void start_game();

private:
    bool m_init;
    sf::IpAddress m_addr;
    unsigned short m_port;
    sf::Time m_timeout;
};

string bin_to_hex(unsigned char *, size_t);
char *hex_to_bin(string);

} // namespace Network

#endif // ClientMessenger_hpp_INCLUDED
