/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef ClientUser_hpp_INCLUDED
#define ClientUser_hpp_INCLUDED

#include <string>

#include "ClientLobby.hpp"

/** \file ClientUser.hpp
 * \brief Definition of class Network::ClientUser
 */

namespace Network {
class ClientLobby;

/**
 * \brief A user in ClientLobby
 *
 * Holds all the informations of a user in the lobby: name, id, avatar,...
 */

class ClientUser {
  public:
    ClientUser();
    ~ClientUser();

    std::string name() const;
    bool is_admin() const;
    bool is_ready() const;

  private:
    std::string m_name;
    bool m_admin;
    bool m_ready;
};
} // namespace Network

#endif // ClientUser_hpp_INCLUDED
