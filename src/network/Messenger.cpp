/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "Messenger.hpp"

#define LOG(msg) clog << "[server] " << msg << endl

namespace Network {

const char *Timeout::what() const throw() {
    return "A human timeouts : end of the game";
}

Messenger::Messenger(sf::TcpSocket *socket, CryptoBox *crypto)
    : m_socket(socket), m_crypto(crypto) {}

Messenger::~Messenger() {
    free(m_crypto);
    free(m_socket);
}

sf::TcpSocket *Messenger::socket() const { return m_socket; }

CryptoBox *Messenger::crypto() const { return m_crypto; }

void Messenger::send(Message *mess) {
    std::string msg = mess->to_string();

    // Computing the message size
    size_t cyphSize = msg.size() + crypto_secretbox_MACBYTES;

    // Preparing the buffer for the cyphered message
    unsigned char *content =
        (unsigned char *)malloc(sizeof(unsigned char) * cyphSize);

    // Cyphering the message
    m_crypto->cypher_message(content, (const unsigned char *)msg.c_str(),
                             msg.size());

    sf::Packet packet;
    packet.append(content, cyphSize);

    // Sending the message
    LOG("Sending message of size " << msg.size()
                                   << ". When cyphered : " << cyphSize);
    if (m_socket->send(packet) != sf::Socket::Status::Done) {
        free(content);
        throw(std::runtime_error("Failed to send the message"));
    }
    free(content);
}

Message *Messenger::receive() {
    sf::Socket::Status st;
    sf::Packet packet;

    // Receive the message
    st = m_socket->receive(packet);
    size_t cyphSize = packet.getDataSize();
    size_t clearSize = cyphSize - crypto_secretbox_MACBYTES;
    check_status(st);
    LOG("Receiving message of size " << clearSize
                                     << ". When cyphered :" << cyphSize);

    // Prepare the buffer for the decyphered message
    unsigned char *msg =
        (unsigned char *)malloc(sizeof(unsigned char) * clearSize);

    // Decypher the content
    m_crypto->decypher_message(msg, (unsigned char *)packet.getData(),
                               cyphSize);
    // Parse the content in the xml object
    pugi::xml_parse_result result_status;
    pugi::xml_document result_xml;
    result_status = result_xml.load_buffer_inplace(msg, clearSize);
    if (result_status.status != pugi::xml_parse_status::status_ok) {
        throw(std::runtime_error("Error when parsing the message"));
    }
    return new Message(result_xml);
}

void Messenger::check_status(sf::Socket::Status status) {
    switch (status) {
    case (sf::Socket::Status::NotReady):
        throw(std::runtime_error("Socket not ready"));
    case (sf::Socket::Status::Partial):
        throw(std::runtime_error("Received part of the data"));
    case (sf::Socket::Status::Disconnected):
        throw(std::runtime_error("Socket not connected"));
    case (sf::Socket::Status::Error):
        throw(std::runtime_error("Unexpected error"));
    case (sf::Socket::Status::Done):
        break;
    default:
        throw(std::runtime_error("Unknown status"));
    }
}

void Messenger::wait_for_receive() {
    sf::SocketSelector selector;
    selector.add(*m_socket);
    if (!selector.wait(timeout))
        throw(Timeout());
}

void Messenger::set_blocking(bool blocking) { m_socket->setBlocking(blocking); }

void Messenger::disconnect() { m_socket->disconnect(); }

} // namespace Network
