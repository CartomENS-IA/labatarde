/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef Messenger_hpp_INCLUDED
#define Messenger_hpp_INCLUDED

#include "pugixml.hpp"
#include <SFML/Network.hpp>
#include <cstdlib>
#include <exception>

#include "CryptoBox.hpp"
#include "GameMessage.hpp"
#include "LobbyMessage.hpp"
#include "MessageHeaders.hpp"
#include "NetworkMessage.hpp"
#include "ServerMessage.hpp"

namespace Network {

/** \brief Duration for Timeouts for remote commmunications **/
static sf::Time timeout = sf::seconds(60.);

class Timeout : public std::exception {
  public:
    Timeout() = default;
    virtual const char *what() const throw();
};

class Messenger {
  public:
    /**
     * \brief Constructor. Takes pointers to a TCP socket and a CryptoBox
     **/
    Messenger(sf::TcpSocket *socket, CryptoBox *crypto);

    /** \brief Destructor : destroys all data, closes the socket **/
    ~Messenger();

    /** \brief Accessor to the TCP socket attribute **/
    sf::TcpSocket *socket() const;

    /** \brief Accessor to the CryptoBox attribute **/
    CryptoBox *crypto() const;

    /**
     * \brief Cyphers and sends the message through the socket
     */
    void send(Message *);

    /**
     * \brief Receives and decyphers a message through the socket
     **/
    Message *receive();

    /**
     * \brief Waits for a receive on the socket and throw a timeout error
     * if needed
     **/
    void wait_for_receive();

    /**
     * \brief Enables or disables blocking mode on the socket
     **/
    void set_blocking(bool);

    /** \brief Disconnects the socket **/
    void disconnect();

  protected:
    /** \brief Check a return status, and throw an exception if it is not ok
        \param status The status to check
    **/
    void check_status(sf::Socket::Status status);

    sf::TcpSocket *m_socket;
    CryptoBox *m_crypto;
};

} // namespace Network

#endif // Messenger_hpp_INCLUDED
