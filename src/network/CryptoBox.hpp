/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef CryptoBox_hpp_INCLUDED
#define CryptoBox_hpp_INCLUDED

#include <SFML/Network.hpp>
#include <cstring>
#include <iostream>
#include <sodium.h>
#include <stdexcept>
#include <string.h>

#define NONCE_SIZE crypto_secretbox_NONCEBYTES
#define SEAL_SIZE crypto_box_SEALBYTES
#define PUBKEY_SIZE crypto_kx_PUBLICKEYBYTES
#define SECKEY_SIZE crypto_kx_SECRETKEYBYTES
#define MAC_SIZE crypto_box_MACBYTES

/**
 * \file CryptoBox.hpp
 * \brief Defines the class Network::CryptoBox
 */

using namespace std;

namespace Network {

/**
 * \brief Object containing all the crypto parameters, keys and protocols needed
 * to cypher and decypher any type of message
 */

class CryptoBox {
  public:
    /**
     * \brief Builds a new crypto box, adapted to the usage
     */
    explicit CryptoBox();

    /**
     * \brief Generates a new key pair.
     */
    void generate_key_pair();

    /**
     * \brief Loads a public key. For example, the server public key
     */
    void load_target_public_key(char *path);

    /**
     * \brief Loads a key pair. For example, the server loading its long term
     * keys
     */
    void load_key_pair(char *path);

    /**
     * \brief Load server's public key
     * \param key the key to load
     */
    void load_server_key(unsigned char *key);

    /**
     * \brief Key exchange protocol with the server. Must be called by the
     * client
     * \param socket The socket to connect to the server
     */
    void init_client(sf::TcpSocket *socket);

    /**
     * \brief Key exchange protocol with the client. Must be called by the
     * server
     * \param socket The socket to connect to the client
     */
    void init_server(sf::TcpSocket *socket);

    /**
     * \brief Cyphers the source text and stores it in the cyphered buffer
     * \param cyphered Where the result cypherd text is stored
     * \param source The clear text to cypher
     * \param size Size of the clear text to cypher
     */
    void cypher_message(unsigned char *cyphered, const unsigned char *source,
                        size_t size);

    /**
     * \brief Decyphers the source text and stores it in the decyphered
     * buffer \param decyphered Where the decyphered will be stored \param
     * sourve Cyphered text to decypher \param size Size of the cyphered
     * text to decypher \throws runtime_error If decryption failed (wrong
     * key, forged message, etc...)
     */
    void decypher_message(unsigned char *decyphered, unsigned char *source,
                          size_t size);

    /**
     * \brief Returns the public key attached to this box
     */
    unsigned char *public_key();

    /**
     * \brief Increments the nonce given
     * \param nonce The nonce to increment
     */
    void increment_nonce(unsigned char *nonce);

  private:
    unsigned char m_public_key[crypto_kx_PUBLICKEYBYTES];
    unsigned char m_secret_key[crypto_kx_SECRETKEYBYTES];

    unsigned char m_target_public_key[crypto_kx_PUBLICKEYBYTES];

    unsigned char m_receive_key[crypto_kx_SESSIONKEYBYTES];
    unsigned char m_send_key[crypto_kx_SESSIONKEYBYTES];

    unsigned char m_nonce_send[crypto_secretbox_NONCEBYTES];
    unsigned char m_nonce_receive[crypto_secretbox_NONCEBYTES];

    bool m_isClient;
    bool m_initiated;
    bool m_target_key_loaded;
    bool m_key_loaded;
};
} // namespace Network

#endif // Server_hpp_INCLUDED
