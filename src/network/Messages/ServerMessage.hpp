/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include <SFML/Network.hpp>
#include <memory>
#include <sodium.h>
#include <string>

#include "CryptoBox.hpp"
#include "NetworkMessage.hpp"

using namespace std;

namespace Network {

/** \brief A message that will be exchanged between the server and the
 * user before the game. Used for setup, authenticate, join a lobby etc.
 * Once the user joined the lobby we use LobbyMessage
 **/

class ServerMessage : public Message {
  public:
    explicit ServerMessage();

    ServerMessageHeader header() const;
    void set_header(ServerMessageHeader const&);

    const char* read_user() const;
    void set_user(const char* user);

    char read_lobby_id() const;
    void set_lobby_id(char);

    int read_nb_human() const;
    void set_nb_human(int);

    ServerMessageStatus read_server_status() const;
    void set_server_status(ServerMessageStatus const&);
};

} // namespace Network
