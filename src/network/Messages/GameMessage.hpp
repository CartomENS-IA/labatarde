/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef GameMessage_hpp_INCLUDED
#define GameMessage_hpp_INCLUDED

/** \file
 *  \brief Definition of class GameMessage
 **/

#include "core/Card.hpp"
#include "core/Bet.hpp"
#include "CryptoBox.hpp"
#include "ServerConstants.hpp"
#include "NetworkMessage.hpp"
#include <SFML/Network.hpp>
#include <string>

/*
   Structure of communications

   RemoteGameState -> RemoteKernel:
   NotifySomething + encoded argument given to notify_something

   RemotePlayer -> RemoteKernel:
   AskSomething

   RemoteKernel -> RemotePlayer:
   Something + encoded argument
*/

using namespace std;

namespace Network {

/** \brief An enum to represent Suit with known values for interfacing **/
enum class Suit : char {
    Club = 'c',
    Diamond = 'd',
    Heart = 'h',
    Spade = 's',
};

/** \brief An enum to represent Value with known values for interfacing **/
enum class Value : char {
    Cat = '0',
    Dog = '1',
    Juggler = '2',
    Musician = '3',
    Entertainer = '4',
    Knave = '5',
    Knight = '6',
    Queen = '7',
    King = '8'
};

/** \brief An enum to represent Trump with known values for interfacing **/
enum class Trump : char {
    NoTrump = 'n',
    Club = 'c',
    Diamond = 'd',
    Heart = 'h',
    Spade = 's',
    AllTrump = 'a'
};

/** \class
 *  \brief An object that contains and handle a buffer for in-game
 *  communications. Inherits from class Message
 *
 * A GameMessage handles a char buffer of size tcpBufferSize,
 * and has methods to fill it with the various messages needed during the game
 * and to read the received messages.
 **/
class GameMessage : public Message {
  private:
    /** \brief Convert from Core::Trump to Network::Trump **/
    Network::Trump encode_trump(Core::Trump);
    /** \brief Convert from Core::Suit to Network::Suit **/
    Network::Suit encode_suit(Core::Suit);
    /** \brief Convert from Core::Value to Network::Value **/
    Network::Value encode_value(Core::Value);

    /** \brief Convert from Network::Trump to Core::Trump **/
    Core::Trump decode_trump(Network::Trump);
    /** \brief Convert from Network::Suit to Core::Suit **/
    Core::Suit decode_suit(Network::Suit);
    /** \brief Convert from Network::Value to Core::Value **/
    Core::Value decode_value(Network::Value);

    /** \brief Writes a bet in the buffer **/
    void write_bet(const Core::Bet &);
    /** \brief Writes a card in the buffer **/
    void write_card(const Core::Card &);
    /** \brief Writes a hand in the buffer **/
    void write_hand(const Core::Hand &);
    /** \brief Writes a player in the buffer **/
    void write_player(int);
    /** \brief Writes a bool in the buffer **/
    void write_bool(bool);
    // void write_allbets(const Core::AllBets&);

    /** \brief Writes the header to the buffer **/
    void write_header(GameMessageHeader);

  public:
    /** \brief Initialises an empty GameMessage **/
    GameMessage();

    /** \brief Get the header of the message **/
    GameMessageHeader header();

    void set_header(GameMessageHeader const);

    /** \brief Fill the buffer with a ready request **/
    void fill_with_askready();
    /** \brief Fill the buffer with a bet request **/
    void fill_with_askbet();
    /** \brief Fill the buffer with a play request **/
    void fill_with_askplay();
    /** \brief Fill the buffer with a mechoune request **/
    void fill_with_askmechoune();
    /** \brief Fill the buffer with a choune request **/
    void fill_with_askchoune();
    /** \brief Fill the buffer with a ready message **/
    void fill_with_ready();
    /** \brief Fill the buffer with a bet message
     *  \param bet The bet announce **/
    void fill_with_bet(const Core::Bet &);
    /** \brief Fill the buffer with a play message
     * \param card The card to play **/
    void fill_with_play(const Core::Card &);
    /** \brief Fill the buffer with a mechoune message
     * \param mechoune True iff I want to mechoune **/
    void fill_with_mechoune(bool);
    /** \brief Fill the buffer with a choune message
     * \param mechoune True iff I want to choune **/
    void fill_with_choune(bool);
    /** \brief Fill the buffer with a notify_bet message
     * \param bet The bet to notify **/
    void fill_with_notifybet(const Core::Bet &);
    /** \brief Fill the buffer with a notify_play message
     * \param card The card to notify **/
    void fill_with_notifyplay(const Core::Card &);
    /** \brief Fill the buffer with a notiyf_cards message
     * \param The hand to notify **/
    void fill_with_notifycards(const Core::Hand &);
    /** \brief Fill the buffer with a notify_mechoune message
     * \param player The number of the player that has mechouned **/
    void fill_with_notifymechoune(int);
    /** \brief Fill the buffer with a notify_choune message
     * \param player The number of the player that has chouned **/
    void fill_with_notifychoune();
    /** \brief Fill the buffer with a notify_next_player message
     * \param player The number of the player to play **/
    void fill_with_notifynextplayer(int);
    /** \brief Fill the buffer with a notify_trick_win message
     * \param player The number of the player who has won the trick **/
    void fill_with_notifytrickwin(int);
    /** \brief Fill the buffer with a notify_end_bet_state message **/
    void fill_with_notifyendbetstate();
    /** \brief Fill the buffer with a notify_end_card_state message **/
    void fill_with_notifyendcardstate();
    /** \brief Fill the buffer with a notify_game_win message
     * \param player The number of the player who has won the game **/
    void fill_with_notifygamewin(int);
    /** \brief Fill the buffer with a notify_ready_player message **/
    void fill_with_notifyreadyplayer(int);
    /** \brief Fill the buffer with a notify_begin_bet_state message
     * \param player The number of the player who is ready **/
    void fill_with_notifybeginbetstate();
    /** \brief Fill the buffer with a init_game_state message
     * \param player The number of the player  **/
    void fill_with_initgamestate(int i);

    void fill_with_lobby_id(char id);

    void fill_with_nb_player(int nb);

    /** \brief Read a bet in the buffer **/
    Core::Bet read_bet();
    /** \brief Read a card in the buffer **/
    Core::Card read_card();
    /** \brief Read a hand in the buffer **/
    Core::Hand read_hand();

    /** \brief Read a player number in the buffer **/
    int read_player();

    int read_nbplayer();
    char read_lobby_id();

    /** \brief Read a bool in the buffer **/
    bool read_bool();
};
} // namespace Network

#endif // GameMessage_hpp_INCLUDED
