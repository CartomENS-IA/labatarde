/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

/** \file
 *  \brief Definition of class LobbyMessage
 **/

#ifndef LobbyMessage_hpp_INCLUDED
#define LobbyMessage_hpp_INCLUDED

#include <SFML/Network.hpp>
#include <cstring>
#include <memory>

#include "CryptoBox.hpp"
#include "NetworkMessage.hpp"
#include "ServerConstants.hpp"

using namespace std;

namespace Network {
/** \class
 *  \brief A message that will be exchanged between the lobby and the
 *   user before the game. Once the game is started, we use GameMessage
 **/
class LobbyMessage : public Message {
  public:
    LobbyMessage();

    LobbyMessageHeader header() const;
    void set_header(LobbyMessageHeader const&);
};

} // namespace Network
#endif // LobbyMessage_hpp_INCLUDED
