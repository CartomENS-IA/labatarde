/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once

#include "pugixml.hpp"
#include <SFML/Network.hpp>
#include <cstdlib>
#include <memory>
#include <string>
#include <vector>

#include "CryptoBox.hpp"
#include "MessageHeaders.hpp"

/** \file
 * \brief Definition of class Network::Message
 *
 * NOTE : This file is not called Message.hpp because such a file already
 * exists in the GUI, and was causing include conflicts
 */

namespace Network {

/**
 * \brief A superclass that holds a message. There is one sub-class for each
 * different type of message (Lobby, Server, Game, ...). It is the container
 * that will hold the received message and the messages to be sent
 */
class Message {
  public:
    /**
     * \brief Default constructor. Creates an empty message of size zero
     */
    Message();

    /**
     * \biref Constructor from an already built xml tree
     **/
    Message(pugi::xml_document&);

    /**
     * \brief Reads the header of the message
     * \throws runtime_error if failed to parse text
     */
    MessageType type() const;

    /**
     * \brief Exporting the xml in a string format, in order to
     *  be sent on the network
     **/
    std::string to_string() const;


    /** \brief Adds a child node of node 'father'
     * \param father the father node name
     * \param node_name the child node name
     **/
    void add_node(std::string father, std::string node_name);

    /** \brief Adds a child node to the root node
     *  shortcut for add_node("root",node_name)
     **/
    void add_node(std::string node_name);

    /**
     * \brief Adds a field to the xml document
     * \param node_name name of the father node
     * \param field_name name of the field
     * \param value Content of the field. Can be a string or a char
     */
    void add_field(std::string node_name, std::string field_name, std::string value);
    void add_field(std::string node_name, std::string field_name, char value);

    /**
     * \brief Adds a field to the xml document
     * shortcut for add_field("root", field_name, value);
     */
    void add_field(std::string field_name, std::string value);
    void add_field(std::string field_name, char value);

    /**
     * \brief Reads a field from the xml document.
     *   If the field name appears multiple times, only returns the first occurence
     * \param key Name of the field to read
     */
    const char* read_field(std::string key) const;

    /**
     * \brief Reads a field from the xml document
     * \returns The vector of all the occurences of the field
     * \param key Name of the field to read
     */
    std::vector<std::string> read_fields(std::string key) const;

    /**
     * \brief Writes the status OK in the message
     *
     * Status is stored in a string as a node of the xml tree
     */
    void set_status_ok();

    /**
     * \brief Writes the status ERROR in the message and writes the error
     * description
     *
     * Status is stored in a string as a node of the xml tree
     */
    void set_status_error(std::runtime_error &e);

    /** \brief Accessor to the status of the message
     *
     * Status is stored in a string as a node of the xml tree
     **/
    sf::Socket::Status read_status() const;

protected:
    std::unique_ptr<pugi::xml_document> m_xml;
};
} // namespace Network
