/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#ifndef MessageHeader_hpp_INCLUDED
#define MessageHeader_hpp_INCLUDED

namespace Network {

/** \class
 *  \brief The type of message to be received or sent.
 *   Depending of this type, a Message objcet can then be casted into one of
 *   its daughter class, GameMessage, ServerMessage or LobbyMessage
 **/
enum class MessageType : char {
    Empty = 0,
    Game = 1,
    Server = 2,
    Lobby = 3
};

/** \brief An enum to represent types of message with known values for
 * interfacing
 **/
enum class GameMessageHeader : char {
    AskReady = 0,
    AskBet = 1,
    AskPlay = 2,
    AskMechoune = 3,
    AskChoune = 4,

    Ready = 5,
    Bet = 6,
    Play = 7,
    Mechoune = 8,
    Choune = 9,

    NotifyBet = 10,
    NotifyPlay = 11,
    NotifyCards = 12,
    NotifyMechoune = 13,
    NotifyChoune = 14,
    NotifyNextPlayer = 15,
    NotifyTrickWin = 16,
    NotifyEndBetState = 17,
    NotifyEndCardState = 18,
    NotifyGameWin = 19,
    NotifyReadyPlayer = 20,
    NotifyBeginBetState = 21,

    InitGameState = 22,

    LobbyId = 23,
    NbPlayers = 24
};

/** \brief An enum to represent types of message in LobbyMessage class **/
enum class LobbyMessageHeader : char {
    ReturnStatus = 0,
    Join = 1, // A new player joins the Lobby
    Leave = 2, // A player leavers the lobby
    Start = 3, // The game starts
};

/** \brief An enum to represent types of message in ServerMessage class **/
enum class ServerMessageHeader : char {
    ReturnStatus = 0, // Result of an operation to the user
    Authenticate = 1,   // The user wants to authenticate
    UpdateDataBase = 2,
    JoinLobby = 3,     // The user wants to join a lobby
};

enum class ServerMessageStatus : char {
    Done = 0,
    Full = 1,
    NotFound = 2,

    CryptoFailed = 3,

    InvalidDB = 4,
    InvalidUser = 5,

    WrongPass = 6,
    NotAuthenticated = 7,

    Error = 8
};


} // namespace Network

#endif // MessageHeader_hpp_INCLUDED
