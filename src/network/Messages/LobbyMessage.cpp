/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "LobbyMessage.hpp"
#include <string>

using namespace std;

namespace Network {

LobbyMessage::LobbyMessage() {
    m_xml = make_unique<pugi::xml_document>();
    m_xml->append_child("root");
    add_field("type", std::to_string((char)MessageType::Lobby));
}

LobbyMessageHeader LobbyMessage::header() const {
    return static_cast<LobbyMessageHeader>((char)read_field("header")[0]);
}


void LobbyMessage::set_header(LobbyMessageHeader const &header){
    add_field("header", std::to_string((char)header));
}

} // namespace Network
