/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "ServerMessage.hpp"

using namespace std;

namespace Network {

ServerMessage::ServerMessage() {
    m_xml = make_unique<pugi::xml_document>();
    m_xml->append_child("root");
    add_field("type", std::to_string((char)MessageType::Server));
}

ServerMessageHeader ServerMessage::header() const {
    return static_cast<ServerMessageHeader>((char)read_field("header")[0]);
}

void ServerMessage::set_header(ServerMessageHeader const &header) {
    add_field("header", std::to_string((char)header));
}

const char* ServerMessage::read_user() const {
    return read_field("user");
}

void ServerMessage::set_user(const char* user){
    add_field("user", std::string(user));
}

char ServerMessage::read_lobby_id() const {
    std::string id = read_field("id");
    return id[0];
}

void ServerMessage::set_lobby_id(char id) {
    add_field("id", id);
}

int ServerMessage::read_nb_human() const {
    return static_cast<int>(read_field("nb")[0]);;
}

void ServerMessage::set_nb_human(int n) {
    add_field("nb", std::to_string(n));
}

void ServerMessage::set_server_status(ServerMessageStatus const &status) {
    add_field("server_status", std::to_string((char)status));
}

ServerMessageStatus ServerMessage::read_server_status() const {
    return static_cast<ServerMessageStatus>((char)read_field("server_status")[0]);
}

} // namespace Network
