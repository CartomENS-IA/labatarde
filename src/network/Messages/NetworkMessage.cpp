/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include "NetworkMessage.hpp"

#define LOG(msg) clog << "[Message] " << msg << endl

namespace Network {

// _________________ PugiXML Utilities _________________________________________

struct xml_string_writer : pugi::xml_writer {
    // Writer to export xml to std::string
    std::string result;
    virtual void write(const void *data, size_t size) {
        result.append(static_cast<const char *>(data), size);
    }
};


// A walker to find the first node with a given name
struct node_finder: pugi::xml_tree_walker
{
    node_finder(std::string _name) : name(_name){}

    std::string name; // the name to find
    pugi::xml_node* result = 0;

    virtual bool for_each(pugi::xml_node& node)
    {
        if (node.name() == name){
            result = &node;
            return false;
        }
        return true; // continue traversal
    }
};

// A walker to find all the nodes with a given name
struct list_finder: pugi::xml_tree_walker
{
    list_finder(std::string _name) : name(_name){}

    std::string name; // the name to find
    std::vector<pugi::xml_node> result;

    virtual bool for_each(pugi::xml_node& node)
    {
        if (node.name() == name){
            result.push_back(node);
        }
        return true; // continue traversal
    }
};

// _________________ Message class implementation  _____________________________


std::string Message::to_string() const {
    xml_string_writer writer;
    m_xml->save(writer);
    return writer.result;
}


Message::Message() {
    m_xml = make_unique<pugi::xml_document>();
    m_xml->append_child("root");
    add_field("type", std::to_string((char)MessageType::Empty));
}

Message::Message(pugi::xml_document &_doc) {
    m_xml = make_unique<pugi::xml_document>();
    m_xml->reset(_doc);
}

MessageType Message::type() const {
    pugi::xml_node node = *m_xml;
    std::string type = node.child_value("type");
    try {
        return (MessageType)std::stoi(type);
    } catch (std::logic_error &e) {
        throw std::runtime_error("Could not parse header");
    }
}

void Message::add_node(std::string father, std::string node_name){
    node_finder finder(father);
    m_xml->traverse(finder);
    if (finder.result==NULL){
        throw std::runtime_error("In Network::Message : Tried to access the node '"+father+"' that does not exist");
    }
    pugi::xml_node node = *finder.result;
    node.append_child(node_name.c_str());
}

void Message::add_node(std::string node_name){
    add_node("root",node_name);
}

void Message::add_field(std::string father, std::string field_name, std::string value) {
    node_finder finder(father);
    m_xml->traverse(finder);
    if (finder.result==NULL){
        throw std::runtime_error("In Network::Message : Tried to access the node '"+father+"' that does not exist");
    }
    pugi::xml_node node = *finder.result;
    pugi::xml_node descr = node.append_child(field_name.c_str());
    descr.append_child(pugi::node_pcdata).set_value(value.c_str());
}

void Message::add_field(std::string father, std::string field_name, char value) {
    add_field(father, field_name, std::to_string(value));
}

void Message::add_field(std::string field_name, std::string value) {
    add_field("root", field_name, value);
}

void Message::add_field(std::string field_name, char value) {
    add_field("root", field_name, std::to_string(value));
}

const char* Message::read_field(std::string key) const {
    node_finder finder(key);
    m_xml->traverse(finder);
    if (finder.result==NULL){
        throw std::runtime_error("In Network::Message : Tried to access the node '"+key+"' that does not exist");
    }
    return finder.result->child_value();
}

std::vector<std::string> Message::read_fields(std::string key) const {
    list_finder finder(key);
    m_xml->traverse(finder);
    if (finder.result.size()==0){
        throw std::runtime_error("In Network::Message : Tried to access the node '"+key+"' that does not exist");
    }
    std::vector<std::string> result;
    for (auto& node : finder.result) result.push_back(node.child_value());
    return result;
}

void Message::set_status_ok() { add_field("status", "ok"); }

void Message::set_status_error(std::runtime_error &e) {
    add_field("status", "error");
    add_field("error", e.what());
}

sf::Socket::Status Message::read_status() const {
    std::string status = read_field("status");
    if (status=="ok"){
        return sf::Socket::Status::Done;
    } else if (status=="error"){
        return sf::Socket::Status::Error;
    } else {
        throw(std::runtime_error(
            "Message::read_status : status not recognized "));
    }
}

} // namespace Network
