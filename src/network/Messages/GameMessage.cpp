/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#include <SFML/Network.hpp>
#include <memory>
#include <stdexcept>

#include "GameMessage.hpp"

namespace Network {

GameMessage::GameMessage() {
    m_xml = make_unique<pugi::xml_document>();
    m_xml->append_child("root");
    add_field("type", std::to_string((char)MessageType::Game));
}

GameMessageHeader GameMessage::header() {
    return static_cast<GameMessageHeader>((char)read_field("header")[0]);
}

void GameMessage::set_header(GameMessageHeader const hdr) {
    add_field("header", static_cast<char>(hdr));
}

void GameMessage::fill_with_askready() {
    set_header(GameMessageHeader::AskReady);
}

void GameMessage::fill_with_askbet() {
    set_header(GameMessageHeader::AskBet);
}

void GameMessage::fill_with_askplay() {
    set_header(GameMessageHeader::AskPlay);
}

void GameMessage::fill_with_askmechoune() {
    set_header(GameMessageHeader::AskMechoune);
}

void GameMessage::fill_with_askchoune() {
    set_header(GameMessageHeader::AskChoune);
}

void GameMessage::fill_with_ready() {
    set_header(GameMessageHeader::Ready);
}

void GameMessage::fill_with_bet(const Core::Bet &bet) {
    set_header(GameMessageHeader::Bet);
    write_bet(bet);
}

void GameMessage::fill_with_play(const Core::Card &card) {
    set_header(GameMessageHeader::Play);
    write_card(card);
}

void GameMessage::fill_with_mechoune(bool mechoune) {
    set_header(GameMessageHeader::Mechoune);
    write_bool(mechoune);
}

void GameMessage::fill_with_choune(bool choune) {
    set_header(GameMessageHeader::Choune);
    write_bool(choune);
}

void GameMessage::fill_with_notifybet(const Core::Bet &bet) {
    set_header(GameMessageHeader::NotifyBet);
    write_bet(bet);
}

void GameMessage::fill_with_notifyplay(const Core::Card &card) {
    set_header(GameMessageHeader::NotifyPlay);
    write_card(card);
}

void GameMessage::fill_with_notifycards(const Core::Hand &hand) {
    set_header(GameMessageHeader::NotifyCards);
    write_hand(hand);
}

void GameMessage::fill_with_notifymechoune(int player) {
    set_header(GameMessageHeader::NotifyMechoune);
    write_player(player);
}

void GameMessage::fill_with_notifychoune() {
    set_header(GameMessageHeader::NotifyChoune);
}

void GameMessage::fill_with_notifynextplayer(int player) {
    set_header(GameMessageHeader::NotifyNextPlayer);
    write_player(player);
}

void GameMessage::fill_with_notifytrickwin(int player) {
    set_header(GameMessageHeader::NotifyTrickWin);
    write_player(player);
}

void GameMessage::fill_with_notifyendbetstate() {
    set_header(GameMessageHeader::NotifyEndBetState);
}

void GameMessage::fill_with_notifyendcardstate() {
    set_header(GameMessageHeader::NotifyEndCardState);
}

void GameMessage::fill_with_notifygamewin(int player) {
    set_header(GameMessageHeader::NotifyGameWin);
    write_player(player);
}
void GameMessage::fill_with_notifyreadyplayer(int player) {
    set_header(GameMessageHeader::NotifyReadyPlayer);
    write_player(player);
}
void GameMessage::fill_with_notifybeginbetstate() {
    set_header(GameMessageHeader::NotifyBeginBetState);
}

void GameMessage::fill_with_initgamestate(int playerId) {
    set_header(GameMessageHeader::InitGameState);
    write_player(playerId);
}

void GameMessage::fill_with_lobby_id(char id) {
    set_header(GameMessageHeader::LobbyId);
    add_field("id", id);
}

void GameMessage::fill_with_nb_player(int nb) {
    set_header(GameMessageHeader::NbPlayers);
    add_field("nb", (char)nb);
}

Core::Bet GameMessage::read_bet() {
    int nbTricks = static_cast<int>(read_field("nb")[0]);
    Core::Trump trump = decode_trump(
        static_cast<Network::Trump>(read_field("trump")[0]));
    return Core::Bet(trump, nbTricks);
}

Core::Card GameMessage::read_card() {
    Core::Value value = decode_value(
        static_cast<Network::Value>(read_field("value")[0]));
    Core::Suit suit = decode_suit(
        static_cast<Network::Suit>(read_field("suit")[0]));
    return Core::Card(suit, value);
}

Core::Hand GameMessage::read_hand() {
    int size = static_cast<int>(read_field("size")[0]);
    vector<Core::Card> hand(size);
    for (int i = 0; i < size; i++) {
        pugi::xml_node node = m_xml->child(("card"+std::to_string(i)).c_str());
        Core::Value value =
            decode_value(static_cast<Network::Value>(node.child_value("value")[0]));
        Core::Suit suit =
            decode_suit(static_cast<Network::Suit>(node.child_value("suit")[0]));
        hand[i] = Core::Card(suit, value);
    }
    return hand;
}

int GameMessage::read_player() {
    return static_cast<int>(read_field("player")[0]);
}

int GameMessage::read_nbplayer() {
    return static_cast<int>(read_field("nb")[0]);
}

char GameMessage::read_lobby_id() {
    return static_cast<char>(read_field("id")[0]);
}

bool GameMessage::read_bool() {
    return static_cast<bool>(read_field("bool")[0]);
}

Network::Trump GameMessage::encode_trump(Core::Trump trump) {
    switch (trump) {
    case Core::Trump::NoTrump:
        return Network::Trump::NoTrump;
    case Core::Trump::Club:
        return Network::Trump::Club;
    case Core::Trump::Diamond:
        return Network::Trump::Diamond;
    case Core::Trump::Heart:
        return Network::Trump::Heart;
    case Core::Trump::Spade:
        return Network::Trump::Spade;
    case Core::Trump::AllTrump:
        return Network::Trump::AllTrump;
    default:
        throw(std::range_error("bad trump"));
    }
}

Network::Suit GameMessage::encode_suit(Core::Suit suit) {
    switch (suit) {
    case Core::Suit::Club:
        return Network::Suit::Club;
    case Core::Suit::Diamond:
        return Network::Suit::Diamond;
    case Core::Suit::Heart:
        return Network::Suit::Heart;
    case Core::Suit::Spade:
        return Network::Suit::Spade;
    default:
        throw(std::range_error("bad suit"));
    }
}

Network::Value GameMessage::encode_value(Core::Value value) {
    switch (value) {
    case Core::Value::Cat:
        return Network::Value::Cat;
    case Core::Value::Dog:
        return Network::Value::Dog;
    case Core::Value::Juggler:
        return Network::Value::Juggler;
    case Core::Value::Musician:
        return Network::Value::Musician;
    case Core::Value::Entertainer:
        return Network::Value::Entertainer;
    case Core::Value::Knave:
        return Network::Value::Knave;
    case Core::Value::Knight:
        return Network::Value::Knight;
    case Core::Value::Queen:
        return Network::Value::Queen;
    case Core::Value::King:
        return Network::Value::King;
    default:
        throw(std::range_error("bad value"));
    }
}

Core::Trump GameMessage::decode_trump(Network::Trump trump) {
    switch (trump) {
    case Network::Trump::NoTrump:
        return Core::Trump::NoTrump;
    case Network::Trump::Club:
        return Core::Trump::Club;
    case Network::Trump::Diamond:
        return Core::Trump::Diamond;
    case Network::Trump::Heart:
        return Core::Trump::Heart;
    case Network::Trump::Spade:
        return Core::Trump::Spade;
    case Network::Trump::AllTrump:
        return Core::Trump::AllTrump;
    default:
        throw(std::range_error("bad trump"));
    }
}

Core::Suit GameMessage::decode_suit(Network::Suit suit) {
    switch (suit) {
    case Network::Suit::Club:
        return Core::Suit::Club;
    case Network::Suit::Diamond:
        return Core::Suit::Diamond;
    case Network::Suit::Heart:
        return Core::Suit::Heart;
    case Network::Suit::Spade:
        return Core::Suit::Spade;
    default:
        throw(std::range_error("bad suit"));
    }
}

Core::Value GameMessage::decode_value(Network::Value value) {
    switch (value) {
    case Network::Value::Cat:
        return Core::Value::Cat;
    case Network::Value::Dog:
        return Core::Value::Dog;
    case Network::Value::Juggler:
        return Core::Value::Juggler;
    case Network::Value::Musician:
        return Core::Value::Musician;
    case Network::Value::Entertainer:
        return Core::Value::Entertainer;
    case Network::Value::Knave:
        return Core::Value::Knave;
    case Network::Value::Knight:
        return Core::Value::Knight;
    case Network::Value::Queen:
        return Core::Value::Queen;
    case Network::Value::King:
        return Core::Value::King;
    default:
        throw(std::range_error("bad value"));
    }
}

void GameMessage::write_bet(const Core::Bet &bet) {
    add_node("bet");
    add_field("bet", "value", static_cast<char>(bet.value));
    add_field("bet", "trump", static_cast<char>(encode_trump(bet.trump)));
}

void GameMessage::write_card(const Core::Card &card) {
    add_node("card");
    add_field("card","value", static_cast<char>(encode_value(card.value())));
    add_field("card","suit", static_cast<char>(encode_suit(card.suit())));
}

void GameMessage::write_hand(const Core::Hand &hand) {
    add_node("hand");
    add_field("hand", "size", static_cast<char>(hand.size()));
    for (unsigned int i = 0; i < hand.size(); i++) {
        std::string cardi = "card"+std::to_string(i+1);
        add_node("hand",cardi);
        add_field(cardi, "value", static_cast<char>(encode_value(hand[i].value())));
        add_field(cardi, "suit", static_cast<char>(encode_suit(hand[i].suit())));
    }
}

void GameMessage::write_player(int player) {
    add_field("player",static_cast<char>(player));
}

void GameMessage::write_bool(bool b) {
    add_field("bool", static_cast<char>(b));
}

} // namespace Network
