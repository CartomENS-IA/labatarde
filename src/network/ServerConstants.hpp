/*
 * Copyright CartomENS-IA / ENS de Lyon / LaBatarde.com, (2017-2018)
 * cartomensia@ens-lyon.fr
 */

#pragma once
#include <sodium.h>

namespace Network {
    
static const size_t maxMailSize = 64;
static const size_t cypheredMailSize = maxMailSize + crypto_secretbox_MACBYTES;

static const size_t dataSize = 20;
static const size_t cypheredSize = dataSize + crypto_secretbox_MACBYTES;

static const size_t saltSize = crypto_pwhash_SALTBYTES;
static const size_t cypheredSaltSize = saltSize + crypto_secretbox_MACBYTES;

static const size_t saltedPassSize = crypto_box_SEEDBYTES;
static const size_t keySize = saltedPassSize;
static const size_t cypheredKeySize = keySize + crypto_secretbox_MACBYTES;

static const int LOBBY_MAX_ID = 10000;

} // namespace Network
