ifdef DEBUG
    CXX_FLAGS = -g
endif

# Used for OS specifity
OS = $(shell uname)

# Compiler flags. Uncomment end of line for hard and complete warnings
CXX_FLAGS += -std=c++14 -Wall -pedantic-errors -Wextra -Wconversion -Wshadow -Wpedantic

# Libs needed.
ifeq ($(OS),Darwin)
	LD_LIBS += -L/opt/local/lib -lsodium -lpugixml -lsfml-system -lsfml-network -lsfml-window -lsfml-graphics -lsfml-audio
else
	LD_LIBS += -pthread -lsodium -lpugixml -lsfml-system -lsfml-network -lsfml-window -lsfml-graphics -lsfml-audio
endif

# Tell where to look for the .hpp files
INC+= -I ./src
INC+= -I ./src/core
INC+= -I ./src/remoteCore
INC+= -I ./src/network
INC+= -I ./src/network/Messages
INC+= -I ./src/IA
INC+= -I ./src/GUI
INC+= -I ./src/GUI/managers
INC+= -I ./src/GUI/objects
INC+= -I ./src/GUI/objects/text
INC+= -I ./src/GUI/objects/gameObjects
INC+= -I ./src/GUI/objects/statObjects
INC+= -I ./src/GUI/objects/buttons
INC+= -I ./src/GUI/objects/basic
INC+= -I ./src/GUI/objects/bubbles
INC+= -I ./src/GUI/objects/login
INC+= -I ./src/GUI/overlay
INC+= -I ./src/GUI/tutorial
INC+= -I ./src/GUI/utilities

ifeq ($(OS),Darwin)
	INC+= -I /opt/local/include
endif

# Final binary name
BIN ?= LaBatarde

# Put all auto generated stuff to this build dir.
BUILD_DIR = ./build

# File containing the main function
MAINFILE?=src/main.cxx
MAINOBJ=$(MAINFILE:%.cxx=$(BUILD_DIR)/%.o)

CPP = $(shell find . -iname "*.cpp")

# All .o files go to build dir.
OBJ = $(CPP:%.cpp=$(BUILD_DIR)/%.o)

# Gcc/Clang will create these .d files containing dependencies.
DEP = $(OBJ:%.o=%.d)

.SILENT:

.PHONY: all
all: build

# Default target named after the binary.
.PHONY: build
build: $(BIN)

$(BIN) : $(BUILD_DIR)/$(BIN)
	echo "Build done!"

# Actual target of the binary - depends on all .o files.
$(BUILD_DIR)/$(BIN) : $(MAINOBJ) $(OBJ)
	mkdir -p $(@D)
	echo "Linking and building: " "$(BIN)"
	$(CXX) $(CXX_FLAGS) $(INC) $(OBJ) $(MAINOBJ) -o $@ $(LD_LIBS)

-include $(DEP)

$(BUILD_DIR)/%.o : %.cpp
	mkdir -p $(@D)
	echo "Compilation: " "$<"
	$(CXX) $(CXX_FLAGS) $(INC) -MMD -c $< -o $@

$(BUILD_DIR)/%.o : %.cxx
	mkdir -p $(@D)
	echo "Compilation: " "$<"
	$(CXX) $(CXX_FLAGS) $(INC) -MMD -c $< -o $@

.PHONY : clean
clean :
	echo Cleaning the build directory...
	-rm -rf $(BUILD_DIR)
	echo Cleaning the documentation...
	-rm -rf docs/doc_doxygen/

.PHONY : run
run:
	./$(BUILD_DIR)/$(BIN)

# Builds the doc with doxyge
doc:
	cd docs && \
	  	doxygen Doxyfile.in && \
	  	firefox doc_doxygen/html/index.html

test:
	make MAINFILE=src/test.cxx -j8

dl_libsodium:
	wget https://download.libsodium.org/libsodium/releases/libsodium-1.0.16.tar.gz
	tar -xvzf libsodium-1.0.16.tar.gz
	cd libsodium-1.0.16 && ./configure && make && make check

install_sodium:
	cd libsodium-1.0.16 && make install && ldconfig
